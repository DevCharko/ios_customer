//
//  AppDelegate.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 2. 28..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

import Kingfisher
import SwiftyBeaver
import SwiftyJSON
import Rollbar
import ChannelIO
//import SendBirdSDK
import UserNotifications
import Firebase
import FacebookCore
import FirebaseMessaging

//전역 선언
let log = SwiftyBeaver.self

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // logging
        #if DEBUG
        setSwiftBeaver()
        #endif
        
        // status bar
        UIApplication.shared.statusBarStyle = .lightContent
        
        
        let Settings=ChannelPluginSettings()
        Settings.pluginKey = "e566696b-97b7-463c-8331-45550cbce995"
        Settings.debugMode = true
        
        ChannelIO.boot(with:Settings)
        
        // 채널 io plugin
        //ChannelPlugin.initialize(pluginId: "e566696b-97b7-463c-8331-45550cbce995") //홈마채널
        //        ChannelPlugin.initialize(pluginKey: "9ea18f0d-72ae-4d83-a43c-a6f2c34da629") //테스트채널
        //ChannelPlugin.debugMode = false
        //        checkInChannelIO()
        
//        getCommonInfo()
        log.info("롤바")
        // 롤바
        setRollbar()
        
        // 홈버튼 누를때 뱃지 갱신을 위해 다시 checkIn
        //        let notificationCenter = NotificationCenter.default
        //        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
        // push
        registerForRemoteNotification()
        
        // 커스텀버튼 사용
        //ChannelPlugin.hideLauncherButton = true
        
        // 샌드버드
        //        SBDMain.initWithApplicationId("EC5137BD-C6BC-4C47-A44E-A37792C6C06D")
        //        SBDMain.setLogLevel(SBDLogLevel.debug)
        //        SBDOptions.setUseMemberAsMessageSender(true)
        
        // FCM
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        // GA - pod 'Google/Analytics'
        //        guard let gai = GAI.sharedInstance() else {
        //            assert(false, "Google Analytics not configured correctly")
        //        }
        //        gai.tracker(withTrackingId: "UA-101192929-1")
        //         Optional: automatically report uncaught exceptions.
        //        gai.trackUncaughtExceptions = true
        
        //         Optional: set Logger to VERBOSE for debug information.
        //         Remove before app release.
        //        gai.logger.logLevel = .verbose
        
        
        return true
    }
    
    //롤바 설정
    func setRollbar() {
        let config: RollbarConfiguration = RollbarConfiguration()
        #if DEBUG
            config.environment = "development"
        #else
            config.environment = "production"
        #endif
        Rollbar.initWithAccessToken("dee3d43c9540424793a9ed246c1dc2c1", configuration: config)
        
        config.crashLevel = "warning"
        
        if DataStore.userModel != nil {
            config.setPersonId(DataStore.userModel?.phone_number, username: DataStore.userModel?.name ?? "", email: "")
        }
//        Rollbar.info(withMessage: "test")
    }
    
    func setSwiftBeaver() {
        // Override point for customization after application launch.
        //        let file = FileDestination()  // log to default swiftybeaver.log file
        let cloud = SBPlatformDestination(appID: "m5OAzE",
                                          appSecret: "tchk5vqhgcSxpeWzsqladr60oeJrll9l",
                                          encryptionKey: "Km0B1uaso2gmrubvteYzj9vkuitj06aq") // to cloud
        
        let console = ConsoleDestination()  // log to Xcode Console
        console.format = "$DHH:mm:ss$d $C$L$c $N:$F[line:$l]$M"
        log.addDestination(console)
        
        log.addDestination(cloud)
        log.info("debug start")
    }
    
    // 액티브상태일때 알람창을 띄움
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(
                UIUserNotificationSettings(types: [.sound, .alert], categories: nil)
            )
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func getCommonInfo() {
        log.info("getCommonInfo()")
        guard let rootVC = UIApplication.shared.windows.first?.rootViewController else { return }
        guard rootVC.childViewControllers.count != 0 else { return }
        // 이미 팝업이 있으면 리턴
        if rootVC.presentedViewController is UpdatePopUpViewController {
            return
        }
        
        NetworkManager.getCommonInfo { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert((UIApplication.shared.windows.first?.rootViewController)!, title: nil, message: networkError.description)
            } else {
                let json = JSON(jsonData!)["result"]
                
                if let lastVer = json["ios"]["version_name"].string {
                    
                    let currentVer : String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
                    
                    switch lastVer.compare(currentVer) {
                    case .orderedAscending, .orderedSame:
                        log.debug("현재 버전이 최신버전입니다")
                        
                    case .orderedDescending:
                        log.debug("현재 버전이 구버전입니다")
                        guard let is_required_update = json["ios"]["is_required_update"].int else { return }
                        guard let popUpMessage = json["ios"]["message"].string else { return }
                        
                        let updatePopUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpdatePopUpViewController") as! UpdatePopUpViewController
                        updatePopUp.isForceUpdate = is_required_update == 1 ? true : false
                        updatePopUp.message = popUpMessage
                        updatePopUp.modalPresentationStyle = .overCurrentContext
                        rootVC.present(updatePopUp, animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    // 채널 플러그인 checkOut을 하면 푸시토큰이 제거되므로 백그라운드로 갈때 체크아웃하면 안됨
    // 포그라운드로
    //    func appMovedToForeground() {
    //        checkInChannelIO()
    //    }
    
    
    //    func checkInChannelIO() {
    //        // 실행시 deviceID checkIn
    //        let checkinObj = Checkin()
    //        checkinObj.with(userId: DataStore.getDeviceId!)
    //
    //        ChannelPlugin.checkIn(checkinObj){ (ChannelCheckinCompletionStatus) in
    //            log.debug(ChannelCheckinCompletionStatus)
    //            switch ChannelCheckinCompletionStatus {
    //            case .success:
    //                break
    //            case .duplicated:
    //                break
    //            case .networkTimeout:
    //                break
    //            default:
    //                break
    //            }
    //        }
    //    }
    
    // 실행중일때
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        log.debug("--------------userInfo--------------")
        log.debug(userInfo)
        
        // FCM 토픽
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            log.debug("Message ID: \(messageID)")
        //        }
        // 샌드버드
        if userInfo["sendbird"] != nil {
            
            let alertMsg = (userInfo["aps"] as? NSDictionary)?["alert"] as? NSDictionary
            let payload = userInfo["sendbird"] as? NSDictionary
            
            log.debug(alertMsg ?? "alertMsg is nil")
            log.debug(payload ?? "payload is nil")
            // Your custom way to parse data
            completionHandler(.sound)
        }
            
            // 채널 IO
        else if ChannelIO.isChannelPushNotification(userInfo) {
            ChannelIO.handlePushNotification(userInfo)
            completionHandler(.sound)
        }
            
            // FCM
        else // 서버 userInfo 처리
        {
            if let landing = userInfo["gcm.notification.landing"] as? String {
                log.debug("landing: \(landing)")
                LandingManager.shared.checkLanding(url: URL.init(string: landing)!)
                completionHandler([.alert, .sound]) // 푸시팝업, 사운드(진동)
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        log.debug("--------------userInfo--------------")
        log.debug(userInfo)
        
        // 샌드버드
        if userInfo["sendbird"] != nil {
            
            let alertMsg = (userInfo["aps"] as? NSDictionary)?["alert"] as? NSDictionary
            let payload = userInfo["sendbird"] as? NSDictionary
            
            log.debug(alertMsg ?? "alertMsg is nil")
            log.debug(payload ?? "payload is nil")
            // Your custom way to parse data
        }
            
            // 채널 IO
        else if ChannelIO.isChannelPushNotification(userInfo) {
            ChannelIO.handlePushNotification(userInfo)
        }
            
            // FCM
        else {
            if let landing = userInfo["gcm.notification.landing"] as? String {
                log.debug("landing: \(landing)")
                LandingManager.shared.checkLanding(url: URL.init(string: landing)!)
            }
        }
        completionHandler()
    }
    
    //iOS 9 이하
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        log.debug("--------------userInfo--------------")
        log.debug(userInfo)
        
        if application.applicationState == .inactive {
            log.debug("inactive")
            
        } else if application.applicationState == .active {
            log.debug("active")
            
        } else {
            log.debug("background")
            
        }
        
        // 샌드버드
        if userInfo["sendbird"] != nil {
            
            let alertMsg = (userInfo["aps"] as? NSDictionary)?["alert"] as? NSDictionary
            let payload = userInfo["sendbird"] as? NSDictionary
            
            log.debug(alertMsg ?? "alertMsg is nil")
            log.debug(payload ?? "payload is nil")
            // Your custom way to parse data
            completionHandler(.newData)
        }
            
            // 채널 IO
        else if ChannelIO.isChannelPushNotification(userInfo) {
            ChannelIO.handlePushNotification(userInfo)
            completionHandler(.noData)
        }
            
            // FCM
        else {
            if let landing = userInfo["gcm.notification.landing"] as? String {
                log.debug("landing: \(landing)")
                LandingManager.shared.checkLanding(url: URL.init(string: landing)!)
                completionHandler(.newData)
            }
        }
    }
    
    // 푸시 등록
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        log.debug("deviceToken = \(deviceTokenString)")
        
        // 채널에는 apns토큰 등록, 서버에는 fcm토큰 등록
        Messaging.messaging().apnsToken = deviceToken // FCM token등록 -> 변환
        
        ChannelIO.initPushToken(deviceToken: deviceToken)
        // sendBird
        /*
         SBDMain.registerDevicePushToken(deviceToken, unique: true) { (status, error) in
         if error == nil {
         if status == SBDPushTokenRegistrationStatus.pending {
         
         }
         else {
         
         }
         }
         else {
         log.debug(error!)
         }
         }
         */
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        getCommonInfo()
        AppEventsLogger.activate(application)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//        ChannelPlugin.checkOut()
        NetworkManager.shareInstance.cancelAllRequest()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        LandingManager.shared.checkLanding(url: url)
        return true
    }
    
}

// MARK: - FCM Delegate
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        log.debug("Firebase registration token: \(fcmToken)")
        
        DataStore.setPushToken(token: fcmToken)
        
        NetworkManager.updatePushToken(pushToken: fcmToken) { (jsonData, error) in
            if (error != nil) {
                log.error(error!)
            }
        }
        
        // FCM Topic Subscribe
        Messaging.messaging().subscribe(toTopic: "ios")
        Messaging.messaging().subscribe(toTopic: "customer")
        Messaging.messaging().subscribe(toTopic: "night_push")
        Messaging.messaging().subscribe(toTopic: "event_push")
        
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        log.debug("Received data message: \(remoteMessage.appData)")
        
    }
    // [END ios_10_data_message]
    
}
