//
//  DefaultCell.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 6..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 라디오 버튼 셀
class RadioCell: UITableViewCell {
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var bottomLineView: UIView!
    
    static var height:CGFloat {
        get {
            return 53 * UIScreen.main.bounds.width / 320
        }
        set (newValue){
            self.height = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(leftButton:UIButton, name:String, rightText:String) {
        self.leftButton = leftButton
        nameLabel.text = name;
        rightLabel.text = rightText
    }
    
    func setData(leftButton:UIButton, name:String) {
        setData(leftButton:leftButton, name:name, rightText:"")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            leftButton.isSelected = true
        }
        else {
            leftButton.isSelected = false
        }
    }

}
