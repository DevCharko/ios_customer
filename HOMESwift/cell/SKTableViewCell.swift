//
//  SKTableViewCell.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 7..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class SKTableViewCell: UITableViewCell {
    
    static var identifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    static func nib() -> UINib {
        return UINib (nibName: identifier, bundle: nil)
    }
    
    static func regist(_ tableView:UITableView) {
        tableView.register(self.nib(), forCellReuseIdentifier:identifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.selectionStyle = .none
    }

}
