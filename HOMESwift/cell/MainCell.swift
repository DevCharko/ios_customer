//
//  MainCell.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class MainCell: UITableViewCell {

    static var height: CGFloat {
        return 368.5
    }
}
