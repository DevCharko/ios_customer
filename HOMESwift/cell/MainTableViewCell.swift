//
//  MainTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 20..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 메인 셀
class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    
    static var height : CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return (89+13) * screenHeight / 568
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        colorView.layer.cornerRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
