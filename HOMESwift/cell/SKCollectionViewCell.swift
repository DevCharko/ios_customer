//
//  SKCollectionViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 29..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class SKCollectionViewCell: UICollectionViewCell {
    
    static var identifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    static func nib() -> UINib {
        return UINib (nibName: identifier, bundle: nil)
    }
    
    static func regist(_ collectionView:UICollectionView) {
        collectionView.register(self.nib(), forCellWithReuseIdentifier:identifier)
    }
    
    func setCellColor(color: UIColor) {
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 2
        self.layer.borderColor = color.cgColor
    }

}
