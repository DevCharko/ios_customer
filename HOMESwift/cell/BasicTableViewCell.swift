//
//  BasicTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 25..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class BasicTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
