//
//  BannerCell.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import Kingfisher


/// 메인 회전 배너 셀
class BannerCell: UITableViewCell {
    
    static var height : CGFloat {
        let screenWidth = UIScreen.main.bounds.width
        return 216.0 * screenWidth / 320.0
    }
    
    static var width : CGFloat {
        let screenWidth = UIScreen.main.bounds.width
        return 320.0 * screenWidth / 320.0
    }
    
    var bannerList: BannerListModel?
    
    var bannerTimer: Timer?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func awakeFromNib() {
//        dataSource = ["4", "1", "2", "3", "4", "1"]
        self.contentView.backgroundColor = UIColor.rgb(fromHex: 0xe4e6eb)
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.reuseIdentifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(stopTimer), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        center.addObserver(self, selector: #selector(startTimer), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    func setModel(model: BannerListModel) {

        pageControl.numberOfPages = model.count

        self.bannerList = model
        
        guard self.bannerList != nil else {
            return
        }
        
        guard self.bannerList!.count != 0 else {
            return
        }

        self.bannerList?.list.insert((self.bannerList?.list[self.bannerList!.count-1])!, at: 0)
        self.bannerList?.list.insert((self.bannerList?.list[1])!, at: self.bannerList!.count)
        
        initPageScroll()
        
    }
    
    func initPageScroll() {
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath.init(row: 1, section: 0), at: .left, animated: false)
        startTimer()
    }
    
    @objc func startTimer() {
        if self.bannerTimer == nil && self.bannerList != nil && self.pageControl.numberOfPages > 0 {
            self.bannerTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: (#selector(self.moveBanner)), userInfo: nil, repeats: true)
        }
    }
    
    @objc func stopTimer() {
        if self.bannerTimer != nil {
            self.bannerTimer?.invalidate()
            self.bannerTimer = nil
        }
    }
    
    @objc func moveBanner() {
        let maxIndex = pageControl.numberOfPages - 1
        var tmpIndex = self.pageControl.currentPage
        tmpIndex += 1 // next page
        
        self.collectionView.scrollToItem(at: IndexPath.init(row: tmpIndex+1, section: 0), at: .right, animated: true)
        
        if self.pageControl.currentPage == maxIndex {
            self.pageControl.currentPage = 0
        } else {
            self.pageControl.currentPage = tmpIndex
        }
    }
}

extension BannerCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as ImageCell
        cell.imageView.kf.setImage(with: URL(string: (self.bannerList?.list[indexPath.row].image_url)!), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        return cell;
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bannerList == nil ? 0 : self.bannerList!.count
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.startTimer()
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        self.stopTimer()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        
        let tmpIndex = (scrollView.contentOffset.x + pageWidth / 2) / pageWidth
        
        if tmpIndex >= CGFloat((self.bannerList?.count)!) - 0.5 {
            
            //처음으로 돌아가기
            collectionView.scrollToItem(at: IndexPath.init(row: 1, section: 0), at: .left, animated: false)
            pageControl.currentPage = 0
            
        } else if tmpIndex <= 0.5 {
            
            //마지막으로 가기
            collectionView.scrollToItem(at: IndexPath.init(row: (self.bannerList?.count)!-2, section: 0), at: .left, animated: false)
            pageControl.currentPage = (self.bannerList?.count)!-3
            
        } else {
            pageControl.currentPage = Int(tmpIndex)-1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: BannerCell.width, height: BannerCell.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard self.bannerList != nil else {
            return
        }
        
        // 배너링크
        if self.bannerList!.count > 0 {
            log.debug("url:"+self.bannerList!.list[indexPath.row].landing.replace(target:"homemaster://?TG=web&DATA=",withString:""));
            //UIApplication.shared.openURL(URL.init(string: self.bannerList!.list[indexPath.row].landing)!)
            UIApplication.shared.openURL(URL.init(string: self.bannerList!.list[indexPath.row].landing.replace(target:"homemaster://?TG=web&DATA=",withString:""))!)
        }
    }
}

extension String
{
    func replace(target: String, withString:String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options:NSString.CompareOptions.literal, range:nil)
    }
}
