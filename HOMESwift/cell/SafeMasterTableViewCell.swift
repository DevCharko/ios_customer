//
//  safeMasterTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 2..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class SafeMasterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbNumber: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
}
