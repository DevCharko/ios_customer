//
//  DefaultCell.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 6..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class DefaultCell: UITableViewCell {
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static var height:CGFloat {
        return 50
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(name:String, thumbUrl:String) {
        nameLabel.text = name;
        let url = URL(string: thumbUrl)
        thumbImageView.kf.setImage(with: url)
        
        thumbImageView.kf.setImage(with: url, placeholder: UIImage.init(named: "default"), options:nil, progressBlock: { (receivedSize, totalSize) in
            
        }) { (image, error, type, url) in
            
        }
    }
    
    
}
