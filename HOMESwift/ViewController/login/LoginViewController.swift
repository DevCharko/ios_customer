//
//  LoginViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 16..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class LoginViewController: SKPViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickClose(_ sender: UIButton) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
        else {
            self.dismiss(animated: true, completion: { 
                
            })
        }
    }

}
