//
//  MenuViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 17..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import FirebaseAnalytics

enum MenuType:Int {
    case Main           = 7
    case Reservation    = 4
    case Card           = 0
    case Recommend      = 1
    case Coupon         = 2
    case Notice         = 3
    case Setting        = 5
    case PersonalInfo   = 6
    case Terms          = 8
    case PayHistroy     = 9
}

var kAnimationDefault:TimeInterval {
    return 0.2
}

protocol MenuDelegate: NSObjectProtocol {
    func selectMenuIndex(index: MenuType)
}

/// 사이드 메뉴, drawer 뷰컨트롤러
class MenuViewController: UIViewController {
    @IBOutlet weak var tableViewTopMargin: NSLayoutConstraint!
    
    let viewwidth = 213 * UIScreen.main.bounds.width / 320
    @IBOutlet weak var backView: UIVisualEffectView!
    @IBOutlet weak var tableView: UITableView!
    open weak var delegate: MenuDelegate?
    @IBOutlet weak var tableViewLeading: NSLayoutConstraint!
    
    var menuArray : Array<Array<String>> {
        return [["설정"], ["예약내역"], ["결제 내역"], ["자동결제 카드정보", "추천하고 할인받기", "쿠폰함", "공지사항"], ["개인정보 취급방침", "서비스 이용약관"]]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(MenuUserCell.self)
        tableView.register(MenuTableViewCell.self)
        tableView.register(BasicTableViewCell.self)
      
        if DeviceInfo.modelName != "iPhone X" {
            tableViewTopMargin.constant = 20.0
        }
    }
    
    @IBAction func clickView(_ sender: UITapGestureRecognizer) {
        hide(true)
    }
    
    func hide(_ animated:Bool = false) {
        let timeInterval = animated ? kAnimationDefault : 0
        UIView.animate(withDuration: timeInterval, animations: {
            self.tableViewLeading.constant = -self.viewwidth
            self.backView.alpha = 0.0
            self.view.layoutIfNeeded()
        })
        { (finished:Bool) in
            log.debug("")
            self.view.frame = CGRect(x: -self.view.bounds.size.width,
                                     y: 0,
                                     width: self.view.bounds.size.width,
                                     height: self.view.bounds.size.height)
        }
    }
    
    func show(_ animated:Bool = false) {
        FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.menu.rawValue)

        let timeInterval = animated ? kAnimationDefault : 0
   
        self.view.frame = CGRect(x: 0,
                                 y: 0,
                                 width: self.view.bounds.size.width,
                                 height: self.view.bounds.size.height)
        UIView.animate(withDuration: timeInterval, animations: {
            self.tableViewLeading.constant = 0
            self.backView.alpha = 1.0
            self.view.layoutIfNeeded()
        }) { (finished:Bool) in
            log.debug("")
        }
    }
}
//return [["설정"], ["예약내역"], ["자동결제 카드정보", "쿠폰함", "추천하고 할인받기"], ["공지사항"]]

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MenuUserCell
            cell.selectionStyle = .none
            cell.nameLabel.text = "\(DataStore.userModel?.name ?? "OO") 고객님"
            cell.lbCredit.text = "\(DataStore.userModel?.point ?? 0)".stringFormatDecimal()
            cell.settingButton.addTarget(self, action: #selector(clickSetting), for: .touchUpInside)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MenuTableViewCell
            cell.selectionStyle = .none
//            cell.newImageView.isHidden = Constants.isNotice == "0" ? true : false
            cell.newImageView.isHidden = true
            cell.imageView?.image = UIImage(named: "icon_menu_01")
            cell.menuTitleLabel?.text = menuArray[indexPath.section][indexPath.row]
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MenuTableViewCell
            cell.selectionStyle = .none
            cell.imageView?.image = UIImage(named: "icon_menu_06")
            cell.menuTitleLabel?.text = menuArray[indexPath.section][indexPath.row]
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MenuTableViewCell
            cell.selectionStyle = .none
            cell.imageView?.image = UIImage(named: "icon_menu_0\(indexPath.row + 2)")
            cell.menuTitleLabel?.text = menuArray[indexPath.section][indexPath.row]
            return cell
        default:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as BasicTableViewCell
            cell.selectionStyle = .none
            cell.textLabel?.text = menuArray[indexPath.section][indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(10))
            cell.textLabel?.textColor = UIColor.main
            return cell
        }
    }
    
    @objc func clickSetting() {
        hide()

        self.delegate?.selectMenuIndex(index: .Setting)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return MenuUserCell.height
        }
        else if indexPath.section == 4 {
            return 35 * UIScreen.main.bounds.height / 568
        }
        else {
            return MenuTableViewCell.height
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 4 { // 개인정보 취급
            return 15
        } else {
            return 0.0001
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 3:
            return 1
        default:
            return 0.0001
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.line
        return lineView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.section == 0) {
            self.delegate?.selectMenuIndex(index: .Setting)
        }
        else if (indexPath.section == 1) {
            self.delegate?.selectMenuIndex(index: .Reservation)
        }
        else if indexPath.section == 2 {
            self.delegate?.selectMenuIndex(index: .PayHistroy)
        }
        else if indexPath.section == 3 {
            self.delegate?.selectMenuIndex(index: MenuType(rawValue: indexPath.row)!)
        }
        else if (indexPath.section == 4) {
            //개인정보 취급
            if indexPath.row == 0 {
                self.delegate?.selectMenuIndex(index: .PersonalInfo)
            }
            //서비스 이용약관
            else {
                self.delegate?.selectMenuIndex(index: .Terms)
            }
        }
        hide()
    }
    
    // color change when cell is pressed
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if (indexPath.section == 0) {
            return true
        }
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
        return true
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if (indexPath.section == 0) {
            return 
        }
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.rgb(fromHex: 0xffffff)
    }

    
}
