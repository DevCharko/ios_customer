//
//  MenuTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 25..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet var menuImageView: UIImageView!
    @IBOutlet var menuTitleLabel: UILabel!
    @IBOutlet var newImageView: UIImageView!
    
    static var height : CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return 53 * screenHeight / 568
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
