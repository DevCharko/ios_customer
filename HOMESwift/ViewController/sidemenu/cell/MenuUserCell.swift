//
//  MenuUserCell.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 17..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class MenuUserCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var lbCredit: UILabel!
    
//    static var height : CGFloat {
//        let screenHeight = UIScreen.main.bounds.height
//        return 44 * screenHeight / 568
//    }
    
    static var height : CGFloat {
        return 104
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data:Any) {
        
    }
}
