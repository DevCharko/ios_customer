//
//  PostCodeWebViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 5. 23..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import WebKit
//import String_Extensions
import SwiftyJSON

protocol PostCodeDelegate: NSObjectProtocol {
    func didSelectAddr(_ addr: String, postcode: String)
    
    func didDisappear()
}


/// 주소 검색 뷰컨트롤러. 다음 주소검색웹페이지 연결.
class PostCodeWebViewController: SKPViewController {
   
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {

    }

    weak var delegate: PostCodeDelegate?
    
    var webView:UIWebView?
    
    override func loadView() {
        super.loadView()
        self.webView = UIWebView(frame: self.view.bounds)
        self.view = self.webView!
        self.webView?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLeftBarButton(type: .Exit, action: #selector(clickExitNavi))
        setNavigationTitle(title: "주소 검색")
        self.webView?.loadRequest(URLRequest(url:URL(string: kWEB_DAUM_POST_CODE)!))
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        SKHUD.hide()
        
        delegate?.didDisappear()
    }
    
}

extension PostCodeWebViewController: UIWebViewDelegate
{
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        log.debug("\(request.url)")
        log.debug("테스트11")
        
        if let requestURL = request.url?.absoluteString {
            if requestURL.hasPrefix("homemaster://") {
                log.debug("\(requestURL)")
                log.debug("테스트22")
                if requestURL.removingPercentEncoding == nil {
                    log.debug("requestURL은 nil")
                    return false;
                }
                var decode = requestURL.removingPercentEncoding!
                decode = decode.substring(from: decode.index(decode.startIndex, offsetBy: "homemaster://".count))
//                let decode = "{\"postcode\":\"135-840\",\"zonecode\":\"06194\",\"addr\":\"서울특별시 강남구 테헤란로 78길 14-12 (동영빌딩)\"}"
                
                if let decodeData = decode.data(using: String.Encoding.utf8) {
                    let jsonObject = try? JSONSerialization.jsonObject(with: decodeData, options: .allowFragments ) as! Dictionary<String, Any>
//                    let json = JSON(init:decodeString)
//                    let zonecode = json["zonecode"].string
//                    let address = json["addr"].string
//                    log.debug("\(String(describing: jsonObject))")
                    SKHUD.show()

                    NetworkManager.checkAvailableAddress(address: jsonObject?["addr"] as! String,jibun: jsonObject?["jibun"] as? String, block: { (jsonData, error) in
                        if let error = error as? NetworkError {
                            log.error(error.description)
                            AlertView().alert(self, title: nil, message: error.description)
                            self.webView?.loadRequest(URLRequest(url:URL(string: kWEB_DAUM_POST_CODE)!))
                        } else {
                            log.debug("\(jsonData)");
                            self.delegate?.didSelectAddr(jsonObject?["addr"] as! String, postcode: jsonObject?["zonecode"] as! String)
                            self.clickExitNavi()
                        }
                        SKHUD.hide()
                    })
                    
//                    }
                }
                else {
                    log.debug("error")
                }
                
//                let decode = requestURL.decodeHTML()
//                log.debug("\(decode)")
//                let decode1 = requestURL.replacingPercentEscapes(using: String.Encoding.utf8)
//                log.debug("\(decode1)")
                
                return false
            }
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SKHUD.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SKHUD.hide()
    }
}

