//
//  ViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 2. 28..
//  Copyright © 2017년 skoopmedia . All rights reserved.
//

import UIKit

enum BarButtonType: String {
    case None       = "None"
    case My         = "My"
    case Talk       = "Talk"
    case Back       = "Back"
    case Setting    = "Setting"
    case Exit       = "Exit"
    case Add        = "Add"
}


/// 디폴트 뷰컨트롤러
/// 모든 뷰 컨트롤러에서 공통적으로 사용하는 함수들을 정의해놓음.
class SKPViewController: UIViewController{
    class var className: String {
        return String(describing: self)
    }
    
    /// 네비 좌측 버튼 설정
    ///
    /// - Parameters:
    ///   - type: 버튼 타입
    ///   - action: 버튼 클릭 시 액션
    ///   - isBadge: 뱃지 사용 여부
    func setLeftBarButton(type:BarButtonType, action:Selector?, isBadge:Bool = false) {
        let leftButton = UIButton.init(type: .custom)
        leftButton.setImage(barButtonImage(type: type).norImage, for: .normal)
        leftButton.setImage(barButtonImage(type: type).prsImage, for: .highlighted)
        leftButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        if let action = action{
            leftButton.addTarget(self, action: action, for: .touchUpInside)
        }
        let leftBarItem = UIBarButtonItem.init(customView: leftButton)
        leftBarItem.width = 44
        
        // new badge button
        if isBadge {
            leftBarItem.setBadge(text: "N", withOffsetFromTopRight: CGPoint(x: -7, y: 7))
        }
        
        let leftSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil);
        leftSpacer.width = -16
        self.navigationItem.leftBarButtonItems = [leftSpacer, leftBarItem];
    }
    
    /// 네비 오른쪽 버튼 설정
    ///
    /// - Parameters:
    ///   - type: 버튼 타입
    ///   - action: 버튼 클릭 시 액션
    ///   - isBadge: 뱃지 사용 여부
    func setRightBarButton(type:BarButtonType, action:Selector?, isBadge:Bool = false) {
        let rightButton = UIButton.init(type: .custom)
        rightButton.setImage(barButtonImage(type: type).norImage, for: .normal)
        rightButton.setImage(barButtonImage(type: type).prsImage, for: .highlighted)
        rightButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        if let action = action{
            rightButton.addTarget(self, action: action, for: .touchUpInside)
        }
        let rightBarItem = UIBarButtonItem.init(customView: rightButton)
        rightBarItem.width = 44
        
        // new badge button
        if isBadge {
            rightBarItem.setBadge(text: "N", withOffsetFromTopRight: CGPoint(x: -7, y: 7))
        }
        
        let rightSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil);
        rightSpacer.width = -16
        self.navigationItem.rightBarButtonItems = [rightSpacer, rightBarItem];
    }
    
    /// 네비 우측 버튼 설정
    ///
    /// - Parameter array: 버튼 어레이
    func setRightBarButtons(array: Array<(BarButtonType, Selector)>) {
        let barButtonArray = NSMutableArray()
        let rightSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil);
        rightSpacer.width = -16
        barButtonArray.add(rightSpacer)
        for item in array {
            let rightButton = UIButton.init(type: .custom)
            rightButton.setImage(barButtonImage(type: item.0).norImage, for: .normal)
            rightButton.setImage(barButtonImage(type: item.0).prsImage, for: .highlighted)
            rightButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            rightButton.addTarget(self, action: item.1, for: .touchUpInside)
            let rightBarItem = UIBarButtonItem.init(customView: rightButton)
            rightBarItem.width = 44
        }
        
        let array = barButtonArray as Array as? [UIBarButtonItem]
        self.navigationItem.rightBarButtonItems = array
    }
    
    func barButtonImage(type:BarButtonType) -> (norImage:UIImage?, prsImage:UIImage?) {
        switch type {
        case .None:
            return (nil, nil)
        case .My:
            return (UIImage(named:"btn_my_nor"), UIImage(named:"btn_my_nor"))
        case .Back:
            return (UIImage(named:"btn_back_nor"), UIImage(named:"btn_back_prs"))
        case .Exit:
            return (UIImage(named:"btn_close_nor"), UIImage(named:"btn_close_prs"))
        case .Add:
            return (UIImage(named:"btn_add_nor"), UIImage(named:"btn_add_nor"))
        case .Talk:
            return (UIImage(named:"btn_talk_nor"), UIImage(named:"btn_talk_prs"))
        default:
            return (UIImage(named:"btn_my_nor"), UIImage(named:"btn_my_prs"))
        }
    }
    
    func setNavigationTitle(title: String = "") {
        self.navigationController?.navigationBar.barTintColor = UIColor.main
        
        if title == "" {
            self.navigationItem.titleView = UIImageView(image: UIImage(named: "img_m_logo"))
        }
        else {
            self.navigationItem.title = title
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                            NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15)]
        }
    }
    
    // 뒤로가기 버튼
    @objc func clickBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // dismiss를 navigation back처럼 구현
    func clickNaviDismiss() {
        let transition = CATransition()
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromRight
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        self.dismiss(animated: false)
    }
    
    // X버튼
    @objc func clickExit(){
        dismiss(animated: true, completion: nil)
    }
    
    // 네비게이션 pop을 dismiss처럼 구현
    @objc func clickExitNavi() {
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    func showViewController(_ vc: SKPViewController, animated:Bool) {
        self.navigationController?.pushViewController(vc, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        log.info(self.classForCoder.description())
//        log.debug(NSStringFromClass(self.classForCoder))
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        switch self {
        case is LaunchViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.splash.rawValue)
            
        case is HomeViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.main.rawValue)

        case is OnceDateViewController:
            if let orderType = ReservationModel.shared.order_type?.getPrefix() {
                FirebaseAnalyticsManager.shared.setAnalytics(orderType + ScreenName.reservation_date_choose.rawValue)
            }

        case is CancelGuideViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_refund_policy.rawValue)

        case is CancelViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_cancel.rawValue)
            
        case is EvaluationViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_grade.rawValue)
            
        case is FinishReserveViewController:
            if let masterType = ReservationModel.shared.masterType?.getPrefix(), let orderType = ReservationModel.shared.order_type?.getPrefix() {
                FirebaseAnalyticsManager.shared.setAnalytics(masterType + "_" + orderType + ScreenName.reservation_complete.rawValue)
            }
            
        case is RegularDateViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_cycle_choose.rawValue)
            
        case is ReservationDetailViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_detail.rawValue)
            
        case is ReservationPageViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_list.rawValue)
            
        case is SelectReserveViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_service_choose.rawValue)
            
        case is SelectFavoriteViewController:
            if let masterType = ReservationModel.shared.masterType?.getPrefix(), let orderType = ReservationModel.shared.order_type?.getPrefix()  {
                FirebaseAnalyticsManager.shared.setAnalytics(masterType + "_" + orderType + ScreenName.reservation_favorite_master_choose.rawValue)
            }
            
        case is RequestsViewController:
            if let masterType = ReservationModel.shared.masterType?.getPrefix(), let orderType = ReservationModel.shared.order_type?.getPrefix() {
                FirebaseAnalyticsManager.shared.setAnalytics(masterType + "_" + orderType + ScreenName.reservation_request_choose.rawValue)
            }

        case is AddressListViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.address_list.rawValue)
            
        case is CardAddViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.card_add.rawValue)
        
        case is CardListViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.card_list.rawValue)
        
        case is CardNickChangeViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.card_nickname.rawValue)
        
        case is ChangePhoneViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.phone_change.rawValue)
        
        case is CouponAddViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.coupon_add.rawValue)
        
        case is CouponListViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.coupon_list.rawValue)
            
        case is LeaveViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.cancel.rawValue)
        
        case is NoticeWebViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.notice.rawValue)
        
        case is PostCodeWebViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.address_search.rawValue)
        
        case is RecommendViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.recommender.rawValue)
        
        case is SafeMasterPopupViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.master_1_description.rawValue)
            
        case is SetAddressViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.address_nickname.rawValue)
            
        case is SettingsViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.profile.rawValue)
            
        case is PayHistoryViewController:
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.payment_list.rawValue)
            
        default:
            break
        }
    }
    
    func setStatusBarColor() {
        let bar = UIView(frame: UIApplication.shared.statusBarFrame)
        
        bar.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2862745098, blue: 0.3254901961, alpha: 1)
        self.view.addSubview(bar)
    }
    
    deinit {
        log.info(self.classForCoder.description() + "종료")
    }
}

