//
//  RecommendViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import MessageUI

/// 추천할인 뷰컨트롤러
class RecommendViewController: SKPViewController {

    @IBOutlet weak var recommendTextfield: UITextField!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var kakaoButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recommendTextfield.text = DataStore.userModel?.recommend_code
        setView()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationTitle(title: "추천하고 할인받기")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
    }
    
    @IBAction func copyButton(_ sender: Any) {
        UIPasteboard.general.string = recommendTextfield.text
        AlertView().alert(self, title: nil, message: "복사되었습니다.")
    }
    
    @IBAction func clickKakaoLink(_ sender: Any) {
       
        // 카카오링크 v1
        
//        let text = KakaoTalkLinkObject.createLabel("믿고쓰는 가사도우미\n홈마스터\n\n지금 홈마스터 앱 다운 받고\n가사도우미 예약시\n추천인 코드를 입력하면 혜택이!\n\n추천인 코드 : \(self.getUnWrappingValue(self.recommendTextfield.text))\n\n# 홈마스터 가입시 추천인 코드를 입력하면\n친구분과 고객님께 할인 쿠폰이 지급됩니다!")
//        
//        let webLink = KakaoTalkLinkObject.createWebLink("홈페이지 가보기", url: "http://homemaster.co.kr")
//        
//        let iosAppAction = KakaoTalkLinkAction.createAppAction(.IOS, devicetype: .phone, marketparamString: "itms-apps://itunes.apple.com/kr/app/homemaster/id1083523259?mt=8", execparamString: "")!
//        let androidAppAction = KakaoTalkLinkAction.createAppAction(.android, devicetype: .phone, marketparamString: "market://details?id=kr.co.homemaster", execparamString: "")!
//        let appLink = KakaoTalkLinkObject.createAppButton("앱 다운받으러 가기 : ", actions: [iosAppAction, androidAppAction])
//        
//        if KOAppCall.canOpenKakaoTalkAppLink() {
//            KOAppCall.openKakaoTalkAppLink([text!, webLink!, appLink!])
//        } else {
//            self.view.stopLoading()
//            log.debug("error \(error)")
//            AlertView().alert(self, title: nil, message: "카카오톡이 설치되어 있지 않습니다.")
//        }
        
        
        
        // 카카오링크 v2

        // Feed 타입 템플릿 오브젝트 생성
        let template = KLKFeedTemplate.init { (feedTemplateBuilder) in
            
            // 컨텐츠
            feedTemplateBuilder.content = KLKContentObject.init(builderBlock: { (contentBuilder) in
                contentBuilder.title = "믿고쓰는 가사도우미, 홈마스터\n추천코드: [\(self.getUnWrappingValue(self.recommendTextfield.text))]"
                contentBuilder.desc = "가입시 추천코드를 입력하면 친구분과 고객님께 할인쿠폰이 지급됩니다!"
                if let imgUrl = URL.init(string: "http://stage.homemaster.co.kr/asset/img/kakao.jpg") {
                    contentBuilder.imageURL = imgUrl
                }
                contentBuilder.link = KLKLinkObject.init(builderBlock: { (linkBuilder) in
                    linkBuilder.mobileWebURL = URL.init(string: "http://homemaster.co.kr")
                })
            })

            // 버튼
            feedTemplateBuilder.addButton(KLKButtonObject.init(builderBlock: { (buttonBuilder) in
                buttonBuilder.title = "홈페이지"
                buttonBuilder.link = KLKLinkObject.init(builderBlock: { (linkBuilder) in
                    linkBuilder.mobileWebURL = URL.init(string: "http://homemaster.co.kr")
                })
            }))
            feedTemplateBuilder.addButton(KLKButtonObject.init(builderBlock: { (buttonBuilder) in
                buttonBuilder.title = "앱에서 보기"
                buttonBuilder.link = KLKLinkObject.init(builderBlock: { (linkBuilder) in
                    linkBuilder.iosExecutionParams = "param1=value1&param2=value2"
                    linkBuilder.androidExecutionParams = "param1=value1&param2=value2"
                })
            }))
        }
        
        // 카카오링크 실행
        SKHUD.show()
        KLKTalkLinkCenter.shared().sendDefault(with: template, success: { (warningMsg, argumentMsg) in
            
            // 성공
            SKHUD.hide()
            log.debug("warning message: \(String(describing: warningMsg))")
            log.debug("argument message: \(String(describing: argumentMsg))")
            
        }, failure: { (error) in
            // 실패
            SKHUD.hide()
            log.debug("error \(error)")
            
            AlertView().alert(self, title: nil, message: "카카오톡이 설치되어 있지 않습니다.")
        })

    }
    
    @IBAction func clickSMSButton(_ sender: Any) {
        let messageVC = MFMessageComposeViewController()
        
        messageVC.body = "믿고쓰는 가사도우미\n홈마스터\n\n지금 홈마스터 앱 다운 받고\n가사도우미 예약시\n추천인 코드를 입력하면 혜택이!\n\n추천인 코드 : \(self.getUnWrappingValue(self.recommendTextfield.text))\n앱 다운받으러 가기 : \n아이폰: https://itunes.apple.com/app/id1083523259\n안드로이드: https://goo.gl/qByuek\n\n# 홈마스터 가입시 추천인 코드를 입력하면\n친구분과 고객님께 할인 쿠폰이 지급됩니다!"
        messageVC.messageComposeDelegate = self
        self.present(messageVC, animated: true, completion: nil)
    }
    
    func setView() {
        copyButton.drawBorder(lineWidth: 1, cornerRadius: 2, color: UIColor.main)
        kakaoButton.layer.cornerRadius = 2
        messageButton.layer.cornerRadius = 2
    }
}

extension RecommendViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        self.dismiss(animated: true) { () -> Void in
            switch (result) {
                
            case .cancelled:
                break
                
            case .failed:
                break
                
            case .sent:
                AlertView().alert(self, title: nil, message: "문자로 공유하기를 완료하였습니다.")
                break
            }
        }
    }
}
