//
//  CouponAddViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON

/// 쿠폰 등록뷰컨트롤러
class CouponAddViewController: SKPViewController, UITextFieldDelegate {
    @IBOutlet weak var topMargin: NSLayoutConstraint!
    
    @IBOutlet weak var couponTextfield: SKTextField!
    @IBOutlet weak var nextButton: NextButton!
    
    @IBOutlet weak var alertLabel: UILabel!
    
    @IBAction func clickNextButton(_ sender: Any) {
        dismissKeyboard()
        SKHUD.show()
        
        NetworkManager.registCoupon(code: couponTextfield.text!, block: { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
                self.alertLabel.isHidden = false
                self.alertLabel.text = networkError.description
            }
            else {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self,
                                      title: nil,
                                      message: return_message,
                                      cancelButtonTitle: "확인",
                                      otherButtonTitles: []) {
                                        (_ alert: AlertView, _ buttonIndex:Int) in
                                        self.clickExit()
                                        
                    }
                    self.alertLabel.isHidden = true
                }
                
                 if json["return_code"] == 0 {
                    self.alertLabel.isHidden = false
                 } else {
                    self.alertLabel.isHidden = true
                }
                FirebaseAnalyticsManager.shared.sendLog(ScreenName.coupon_add_completed.rawValue)

            }
            SKHUD.hide()
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "쿠폰 등록")
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickExit))
        
        couponTextfield.delegate = self
        couponTextfield.inputAccessoryView = getKeyboardToolbar()
        couponTextfield.addTarget(self, action: #selector(checkTextField(_ :)), for: .editingChanged)
        
        couponTextfield.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if #available(iOS 11.0, *) {
            topMargin.constant = 27.0 + self.view.safeAreaInsets.top
        }
        else {
            topMargin.constant = 91.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        dismissKeyboard()
    }
    
    @objc func checkTextField(_ sender: UITextField){
        if (sender.text?.isEmpty)!{
            nextButton.setNextButton(bool: false)
        } else {
            nextButton.setNextButton(bool: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nextButton.sendActions(for: .touchUpInside)
        return true
    }
}
