//
//  CouponListViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

protocol CouponListDelegate: NSObjectProtocol {
    func didSelectCoupon(coupon_seq:String)
    func selectCoupon(coupon:CouponInfo)
}

/// 쿠폰 리스트 뷰컨트롤러
class CouponListViewController: SKPViewController {
    weak var delegate: CouponListDelegate?

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nextButton: NextButton!
    @IBOutlet weak var nextButtonHeight: NSLayoutConstraint!
    let buttonHeight = 53
    
    var couponList: CouponList?
    
    var isUpdatedMode:Bool = false
    var isUseMode = false // 사용 버튼 활성화

    //Mark: collectionView 사이즈 조정
    let columns = 1
    
    let spaceBetweenRows = 13
    let spaceHorisonMargin = 13
    
    var callSelectTimeView:Bool = false
    
    var cellWidth: CGFloat {
        get {
            return collectionView.frame.width/CGFloat(columns)-CGFloat(spaceHorisonMargin*2)
        }
    }

    @IBAction func clickNextButton(_ sender: Any) {
        let vc = ReservationPageViewController(nibName:"ReservationPageViewController", bundle:nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: "쿠폰함")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        setRightBarButton(type: .Add, action: #selector(clickAdd))

        collectionView.contentInset.top = 13
        collectionView.contentInset.bottom = 13
        
        collectionView.register(CouponCollectionViewCell.self)
        collectionView.register(EmptyCollectionViewCell.self)
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

        getCouponList()
    }

    func getCouponList() {
        SKHUD.show()
        
        NetworkManager.getCouponList(){ (jsonData, error) in
            
            if (jsonData != nil) {
                log.debug(jsonData)
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let json = JSON(jsonData!)
                let mapper = Mapper<CouponList>()
                
                    // 예약내역에서 진입
                    if self.navigationController?.previousViewController() is ReservationDetailViewController
                    || self.navigationController?.previousViewController() is SelectTimeViewController {
                        self.isUseMode = true
                        let model = mapper.map(JSONString: json.rawString()!)
                        
                        model?.list = model?.list?.filter({ (CouponInfo) -> Bool in
                            if (CouponInfo.expiration_date?.toDate()?.timeIntervalSinceNow)! < 0 || CouponInfo.is_used {
                                return false
                            } else {
                                return true
                            }
                        })
                        
                        self.couponList = model
                        self.nextButton.isHidden = true
                        self.nextButtonHeight.constant = 0
                        
                    // 쿠폰함에서 진입
                    } else {
                        self.isUseMode = false
                        self.nextButton.isHidden = false
                        self.couponList = mapper.map(JSONString: json.rawString()!)
                        
                        // 사용가능한 쿠폰 있는지 검사해서 적용하러가기 버튼 설정
                        let useCoupons = self.couponList?.list?.filter({ (CouponInfo) -> Bool in
                            // 날짜가 만료되거나 사용되었을 경우
                            if (CouponInfo.expiration_date?.toDate()?.timeIntervalSinceNow)! < 0 || CouponInfo.is_used {
                                return false
                            } else {
                                return true
                            }
                        })

                        if (useCoupons?.isEmpty)! {
                            self.nextButton.setNextButton(bool: false)
                        } else {
                            self.nextButton.setNextButton(bool: true)
                        }
                        
                        // 사용한 쿠폰 밑으로
                        self.couponList?.list?.sort {
                            !($0.is_used) && $1.is_used
                        }
                }
                self.collectionView.reloadData()
            }
            
            SKHUD.hide()
        }
    }
    
    @objc func clickAdd(){
        let naviVC = UINavigationController(rootViewController: CouponAddViewController(nibName: "CouponAddViewController", bundle: nil))
        
        present(naviVC, animated: true, completion: nil)
    }
}

extension CouponListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return couponList == nil || (couponList?.list?.count == 0) ? 1 : couponList!.list!.count // 쿠폰이 없으면 기본 셀 호출 위해
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.couponList?.list == nil || (couponList?.list?.count == 0) {
            let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as EmptyCollectionViewCell
            cell.drawBorder(lineWidth: 1, cornerRadius: 3, color: UIColor.lightGray)
            cell.titleLabel.text = "쿠폰함이 비어있습니다."
            cell.descLabel.numberOfLines = 0
            cell.descLabel.text = "우측 상단의 '+'버튼을 터치하시면\n결제 쿠폰을 등록하실 수 있습니다."
            return cell
        }
        else { // 커스텀셀
            let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as CouponCollectionViewCell
            
            cell.setCouponInfoModel(couponList?.list?[indexPath.row])
            
            if self.isUseMode {
                cell.useButton.isHidden = false
                cell.useTouchViewButton.isHidden = false
                cell.useTouchViewButton.addTarget(self, action: #selector(clickUseCoupon(_ : event:)), for: .touchUpInside)
            } else {
                cell.useButton.isHidden = true
                cell.useTouchViewButton.isHidden = true
            }
            return cell
        }
    }
    
    //cell 간격 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    //cell 사이즈를 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: CouponCollectionViewCell.height)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if delegate != nil && couponList != nil && (couponList?.list?.count)! > 0 {
//            let coupon = couponList?.list?[indexPath.row]
//            delegate?.didSelectCoupon(coupon_seq: (coupon?.coupon_seq)!)
//            
//            self.clickBack()
//        }
//    }
    
    @objc func clickUseCoupon(_ sender: UIButton, event: UIEvent) {
        if delegate != nil && couponList != nil && (couponList?.list?.count)! > 0 {
            let touch = event.allTouches?.first
            let point = (touch?.location(in: collectionView))! as CGPoint
            let indexPath = collectionView.indexPathForItem(at: point)

            let coupon = couponList?.list?[(indexPath?.row)!]
            
            if callSelectTimeView {
                self.delegate?.selectCoupon(coupon: coupon!)
                self.clickBack()
                
            }
            else {
                AlertView().alert(self, title: "", message: "쿠폰을 사용하시면, 현재 금액에서 바로 차감됩니다\n정말 사용하시겠습니까?", cancelButtonTitle: "취소", otherButtonTitles: ["사용"], buttonColor: [UIColor.alert, UIColor.rgb(fromHex: 0x999999)], block: { (AlertView, buttonIndex) in
                    if buttonIndex == 0 {
                        self.delegate?.didSelectCoupon(coupon_seq: (coupon?.seq)!)
                        self.clickBack()
                        
                    } else {
                        self.dismiss(animated: true)
                    }
                })
            }
        }
    }
}

