//
//  CouponListCollectionViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 4..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class CouponCollectionViewCell: UICollectionViewCell {
    
    static var height : CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return 133 * screenHeight / 568
    }
    
    var dashLineHeight : CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return 50 * screenHeight / 568
    }
    
    var lineColor = UIColor.rgb(fromHex: 0xb8bbbf) {
        didSet{
            self.drawBorder(lineWidth: 1, cornerRadius: 1.5, color: lineColor)
            self.topView?.drawDashedLine(fromPoint: CGPoint(x: 0, y: dashLineHeight),
                                         toPoint: CGPoint(x: self.bounds.width, y: dashLineHeight),
                                         lineWidth: 1,
                                         color: lineColor)
        }
    }
    
    var textColor = UIColor.main {
        didSet{
            self.priceLabel.textColor = textColor
            self.dateLabel.textColor = textColor
            self.couponTitleLabel.textColor = textColor
        }
    }
    
    var isExpire = false {
        didSet{
            if isExpire == true { // 사용기간이 만료되면
                lineColor = UIColor.rgb(fromHex: 0xd0d3db)
                textColor = UIColor.placeholder
                
                self.useButton.isHidden = true
                self.useTouchViewButton.isHidden = true
                self.expireLabel.isHidden = false
            }
            else {
                lineColor = UIColor.rgb(fromHex: 0xb8bbbf)
                textColor = UIColor.main
                
                self.useButton.isHidden = false
                self.useTouchViewButton.isHidden = false
                self.expireLabel.isHidden = true
            }
            
        }
    }
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var couponTitleLabel: UILabel!
    @IBOutlet weak var useButton: UIButton!
    @IBOutlet weak var useTouchViewButton: UIButton!
    @IBOutlet weak var expireLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        //        self.useButton.expandTouchArea(offset: 20)
        super.awakeFromNib()
    }
    
    func setCouponInfoModel(_ info: CouponInfo?) {
        guard let coupon = info?.coupon else {
            return
        }
        
        couponTitleLabel.text = coupon.title
        
        if let discountPrice = coupon.discount_price_string {
            priceLabel.text = discountPrice
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.M.d"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy.M.d HH:mm:ss"
        
        if let received_date = info?.received_date, let expiration_date = info?.expiration_date2, let is_used = info?.is_used {
            dateLabel.text = formatter.string(from: received_date.toDate()! ) + " ~ " + formatter.string(from: expiration_date.toDate()!)
            
            if (expiration_date.toDate()?.timeIntervalSinceNow)! < 0 {
                self.isExpire = true
                expireLabel.text = "사용기한 완료"
            }
            else {
                self.isExpire = false
            }
            
            if is_used {
                self.isExpire = true
                expireLabel.text = "사용완료"
            }
        }
    }
}
