//
//  CardAddViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON

/// 신용카드 등록 뷰컨트롤러
class CardAddViewController: SKPViewController {

    @IBOutlet weak var formScrollView: UIScrollView!

    @IBOutlet weak var personalButtonView: UIView!
    @IBOutlet weak var businessButtonView: UIView!
    @IBOutlet weak var personalButton: UIButton!
    @IBOutlet weak var businessButton: UIButton!
    @IBOutlet var tfCardNumbers: [SKTextField]!
    @IBOutlet weak var tfExpireMM: SKTextField!
    @IBOutlet weak var tfExpireYY: SKTextField!
    @IBOutlet weak var tfBirthday: SKTextField!
    @IBOutlet weak var tfPassword: SKTextField!
    @IBOutlet weak var tfCVC: SKTextField!
    @IBOutlet weak var tfBusinessNumber: SKTextField!
    @IBOutlet weak var tfCardNicname: SKTextField!
    @IBOutlet weak var defaultButton: UIButton!
    @IBOutlet weak var defaultButtonView: UIView!
    
    // 생년월일 숨김
    @IBOutlet weak var birthLabel: UILabel!
    @IBOutlet weak var birthLine: UIView!
    
    @IBOutlet var tfArray: [SKTextField]!
    @IBOutlet var hiddenViews: [UIView]!
    @IBOutlet var individualConstraint: NSLayoutConstraint!
    @IBOutlet var businessConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nextButton: NextButton!
    
    var cardCount: Int? // 개수에 따라 기본카드 설정 버튼 보이기
    
    @IBAction func clickNextButton(_ sender: NextButton) {
        if checkValidate() == true {
            SKHUD.show()

            NetworkManager.registCard(card_type: personalButton.isSelected ? 1 : 2,
                                      card_number: "\(tfCardNumbers[0].text!)\(tfCardNumbers[1].text!)\(tfCardNumbers[2].text!)\(tfCardNumbers[3].text!)",
                month: tfExpireMM.text!,
                year: tfExpireYY.text!,
                birth: tfBirthday.text!,
                password: tfPassword.text!,
                cvc: tfCVC.text!,
                business_number: tfBusinessNumber.text!,
                nickname: tfCardNicname.text!,
                is_default: defaultButton.isSelected ? "1"  : "0",
                block: { (jsonData, error) in
                    
                    if let networkError = error as? NetworkError {
                        AlertView().alert(self, title: nil,
                                          message: networkError.description)
                    }
                    else {
                        let json = JSON(jsonData!)
                        let return_message = json["return_message"].string!
                        if return_message != "" {
                            
                            AlertView().alert(self,
                                              title: nil,
                                              message: return_message,
                                              cancelButtonTitle: "확인",
                                              otherButtonTitles: []) {
                                              (_ alert: AlertView, _ buttonIndex:Int) in
                                                self.clickExit()
                            }
                        }
                        FirebaseAnalyticsManager.shared.sendLog(ScreenName.card_add_completed.rawValue)
                    }
                    
                    SKHUD.hide()
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "카드등록")
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickExit))
        
        hideKeyboardWhenTappedAround()
        setupViewResizerOnKeyboardShown()
        
        initTextfield()
        setDefaultButtonView()
        initRadioButton()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        dismissKeyboard()
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        formScrollView.contentInset.bottom = keyboardFrame.size.height

    }
    
    override func keyboardWillHide(notification: NSNotification) {
        formScrollView.contentInset = UIEdgeInsets.zero

    }
    
    func initRadioButton() {
        personalButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapRadioButton(_ :))))
        businessButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapRadioButton(_ :))))
        
        tapRadioButton(personalButtonView.gestureRecognizers?.first as! UITapGestureRecognizer)
    }
    
    @objc func tapRadioButton(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        if let button = view?.subviews[0] as? UIButton {
            if button.isEqual(personalButton) {
                personalButton.isSelected = true
                businessButton.isSelected = false
                
                _ = hiddenViews.map{$0.isHidden = true}
                UIView.animate(withDuration: 0.25) {
                    self.individualConstraint.isActive = true
                    self.businessConstraint.isActive = false
                }
                birthLabel.isHidden = false
                birthLine.isHidden = false
                tfArray[6].isHidden = false
                
                // 생년월일로 이동
                tfArray[5].setToolbar(prev: tfArray[4], next: tfArray[6])
                tfArray[7].setToolbar(prev: tfArray[6], next: tfArray[8])
                
                // 사업자 건너뛰기
                tfArray[8].setToolbar(prev: tfArray[7], next: tfArray[10])
                tfArray[10].setToolbar(prev: tfArray[8], next: nil)
                
            } else {
                personalButton.isSelected = false
                businessButton.isSelected = true

                _ = hiddenViews.map{$0.isHidden = false}
                UIView.animate(withDuration: 0.25) {
                    self.individualConstraint.isActive = false
                    self.businessConstraint.isActive = true
                }
                
                birthLabel.isHidden = true
                birthLine.isHidden = true
                tfArray[6].isHidden = true
                
                // 생년월일 건너뛰기
                tfArray[5].setToolbar(prev: tfArray[4], next: tfArray[7])
                tfArray[7].setToolbar(prev: tfArray[5], next: tfArray[8])
                
                // 사업자로 이동
                tfArray[8].setToolbar(prev: tfArray[7], next: tfArray[9])
                tfArray[9].setToolbar(prev: tfArray[8], next: tfArray[10])
                tfArray[10].setToolbar(prev: tfArray[9], next: nil)

            }
            checkFormIsFill()

        }
    }
    
    func setDefaultButtonView() {
        guard cardCount != nil && cardCount != 0 else {
            self.defaultButtonView.isHidden = true
            return
        }
        
        self.defaultButtonView.isHidden = false
        let geusture = UITapGestureRecognizer(target: self, action: #selector(tapDefaultButton))
        self.defaultButtonView.addGestureRecognizer(geusture)
    }
    
    @objc func tapDefaultButton() {
        self.defaultButton.isSelected = !self.defaultButton.isSelected
    }
    
    func initTextfield() {
        
        // 스피너 이미지 추가
//        tfExpireMM.rightViewMode = .always
//        tfExpireYY.rightViewMode = .always
//        tfExpireMM.rightView = UIImageView(image: UIImage(named: "icon_select"))
//        tfExpireYY.rightView = UIImageView(image: UIImage(named: "icon_select"))        
        
        let cnt = tfArray.count
        
        // 처음과 마지막은 따로 등록
        tfArray[0].setToolbar(prev: nil, next: tfArray[1])
        tfArray[cnt-1].setToolbar(prev: tfArray[cnt-2], next: nil)
        
        tfArray[0].delegate = self
        tfArray[cnt-1].delegate = self
        // 이전 이후 텍스트필등 등록
        for (i, tf) in tfArray.enumerated() {
            tf.delegate = self
            tf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
            if i > 0 && i < tfArray.count-1 {
                tf.setToolbar(prev: tfArray[i-1], next: tfArray[i+1])
            }
        }
        
        // 사업자 건너뛰기
        tfArray[8].setToolbar(prev: tfArray[7], next: tfArray[10])
        tfArray[10].setToolbar(prev: tfArray[8], next: nil)
        
    }
}

extension CardAddViewController: UITextFieldDelegate {

    // move next textfield
    @objc func textFieldDidChange(_ textField: SKTextField) {
        let text = textField.text
        
        if text?.utf16.count == textField.maxLength {
            textField.nextAction()
            
            if let nextField = textField.nextTextField {
                nextField.becomeFirstResponder()
                formScrollView.scrollRectToVisible(nextField.frame, animated: true)
            }
        }
        checkFormIsFill()
    }
    
    // 비어있는 텍스트필드를 검사해서 다음버튼 활성화
    func checkFormIsFill() {
        
        // 비어있는 텍스트필드를 필터
        let checkedTfArray = tfArray.filter { (tf: SKTextField) -> Bool in
            if tf == tfCardNicname { // 카드별명은 제외
                return false
            }
            
            if tf == tfBusinessNumber && personalButton.isSelected { // 개인일때 사업자등록번호 제외
                return false
            }
            
            if tf == tfBirthday && businessButton.isSelected { // 사업자일때 생년월일 제외
                return false
            }
            
            return (tf.text?.isEmpty)!
        }
        
        // 하나라도 빈 텍스트필드가 있으면 비활성화
        if !checkedTfArray.isEmpty {
            nextButton.setNextButton(bool: false)
        } else {
            nextButton.setNextButton(bool: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfCardNicname {
            nextButton.sendActions(for: .touchUpInside)
        }
        return true
    }
    
    func checkValidate() -> Bool {
        
        for number in tfCardNumbers {
            guard Validator.isInt().apply(number.text) else {
                number.becomeFirstResponder()
                AlertView().alert(self, title: nil, message: "카드번호를 정확히 입력해 주세요.")
                return false
            }
        }
        
        guard Validator.isDate("MM").apply(tfExpireMM.text) && Validator.exactLength(tfExpireMM.maxLength).apply(tfExpireMM.text) else {
            tfExpireMM.becomeFirstResponder()
            AlertView().alert(self, title: nil, message: "날짜를 정확히 입력해 주세요.")
            return false
        }
        
        guard Validator.isDate("yy").apply(tfExpireYY.text) && Validator.exactLength(tfExpireYY.maxLength).apply(tfExpireYY.text) else {
            tfExpireYY.becomeFirstResponder()
            AlertView().alert(self, title: nil, message: "날짜를 정확히 입력해 주세요.")
            return false
        }
        
        guard Validator.exactLength(tfPassword.maxLength).apply(tfPassword.text) else {
            tfPassword.becomeFirstResponder()
            AlertView().alert(self, title: nil, message: "비밀번호를 정확히 입력해 주세요.")
            return false
        }
        
        guard Validator.isInt().apply(tfCVC.text) && Validator.exactLength(tfCVC.maxLength).apply(tfCVC.text) else {
            tfCVC.becomeFirstResponder()
            AlertView().alert(self, title: nil, message: "CVC번호를 정확히 입력해 주세요.")
            return false
        }
        
        if personalButton.isSelected {
            guard !Validator.isEmpty().apply(tfBirthday.text) && Validator.isDate("yyMMdd").apply(tfBirthday.text) else {
                tfBirthday.becomeFirstResponder()
                AlertView().alert(self, title: nil, message: "생년월일을 정확히 입력해 주세요.")
                return false
            }
        } else {
            guard !Validator.isEmpty().apply(tfBusinessNumber.text) else {
                tfBusinessNumber.becomeFirstResponder()
                AlertView().alert(self, title: nil, message: "사업자 등록번호를 입력해 주세요.")
                return false
            }
        }
        
        return true
    }
}
