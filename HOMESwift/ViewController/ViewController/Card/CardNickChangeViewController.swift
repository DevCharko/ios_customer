//
//  CardNickChangeViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON

/// 카드 별명 변경 뷰컨트롤러
class CardNickChangeViewController: SKPViewController, UITextFieldDelegate {
    
    @IBOutlet weak var couponTextfield: SKTextField!
    @IBOutlet weak var nextButton: NextButton!
    
    var cardSeq: String!
    
    @IBAction func clickNextButton(_ sender: Any) {
        dismissKeyboard()
        SKHUD.show()

        let trimmed = couponTextfield.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        
        NetworkManager.updateCard(nickname: trimmed!, card_seq: cardSeq) { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self,
                                      title: "",
                                      message: return_message,
                                      cancelButtonTitle: "확인",
                                      otherButtonTitles: []) {
                                        (_ alert: AlertView, _ buttonIndex:Int) in
                                        self.clickBack()
                    }
                }
                FirebaseAnalyticsManager.shared.sendLog(ScreenName.card_nickname_update.rawValue)
            }
            SKHUD.hide()
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "카드 별명")
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickBack))
        
        couponTextfield.delegate = self
        couponTextfield.inputAccessoryView = getKeyboardToolbar()
        couponTextfield.addTarget(self, action: #selector(checkTextField(_ :)), for: .editingChanged)
        
        hideKeyboardWhenTappedAround()
        
        couponTextfield.becomeFirstResponder()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        dismissKeyboard()
    }
    
    @objc func checkTextField(_ sender: UITextField){
        if (sender.text?.isEmpty)!{
            nextButton.setNextButton(bool: false)
        } else {
            nextButton.setNextButton(bool: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nextButton.sendActions(for: .touchUpInside)
        return true
    }
}
