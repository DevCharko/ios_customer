//
//  CardCollectionViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 30..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

enum CardMode {
    case list
    case change
}

class CardCollectionViewCell: UICollectionViewCell {
    
    static var height : CGFloat {
        let screenWidth = UIScreen.main.bounds.width - 26
        return 172 * screenWidth / 294.0
    }
    
    @IBOutlet weak var cardBgImageView: UIImageView!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var starButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var defaultCardLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var cardNumStackView: UIStackView!
    @IBOutlet weak var cardNickLabel: UILabel!
    @IBOutlet weak var cardExpireLabel: UILabel!
    
    @IBOutlet weak var nickButton: UIButton!
    
    @IBOutlet var heightConstraint: [NSLayoutConstraint]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        borderWidth = 1
        cornerRadius = 2
        
        deselectCard()
        let screenHeight = UIScreen.main.bounds.height
        for height in heightConstraint {
            height.constant = screenHeight * height.constant / 568
        }
    }
    
    func setModel(card: Card, mode: CardMode, cardSeq: String? = nil) {
        if mode == .list { // 리스트에서 진입시 기본카드 등록 버튼생성 및 기본카드 테두리설정
            self.starButton.isHidden = false
            if card.is_default == "1" {
                selectCard()
                self.defaultCardLabel.text = "기본 결제카드 지정"
            } else {
                deselectCard()
                self.defaultCardLabel.text = ""
            }
        } else { // 예약상세에서 카드변경으로 진입시 등록된 카드 테두리 설정
            // 기본카드 등록 버튼 제거
            self.starButton.isHidden = true
            self.starButtonWidth.constant = 15
            if card.is_default == "1" {
                self.defaultCardLabel.text = "기본 결제카드"
            } else {
                self.defaultCardLabel.text = ""
            }

            if card.seq == cardSeq {
                selectCard()
            } else {
                deselectCard()
            }
        }
        

        if card.nickname != nil {
            self.cardNickLabel.isHidden = false
            self.cardNickLabel.text = card.nickname
            self.nickButton.isHidden = true
        }
        else {
            self.nickButton.isHidden = false
            self.cardNickLabel.isHidden = true
        }
        
        if card.month == nil {
            self.cardExpireLabel.text = "** / **"
        } else {
            self.cardExpireLabel.text = "\(card.month!) / \(card.year!)"
        }
        
        if let cardNum = card.number {
            let index1 = cardNum.index(cardNum.startIndex, offsetBy: 4)
            //                let index2 = cardNum.index(cardNum.startIndex, offsetBy: 8)
            let index3 = cardNum.index(cardNum.startIndex, offsetBy: 12)
            
            (self.cardNumStackView.subviews[0] as! UILabel).text = cardNum.substring(to: index1)
            //                (cell.cardNumStackView.subviews[1].subviews[0] as! UILabel).text = cardNum.substring(with: index1..<index2)
            //                (cell.cardNumStackView.subviews[2] as! UILabel).text = cardNum.substring(with: index2..<index3)
            (self.cardNumStackView.subviews[3] as! UILabel).text = cardNum.substring(from: index3)
        } else {
            for numberView in self.cardNumStackView.subviews {
                if let numberLabel = numberView as? UILabel {
                    numberLabel.text = "****"
                }
            }
        }
    }
    
    func selectCard() {
        starButton.isSelected = true
        cardBgImageView.image = UIImage(named: "bg_card_on")
        borderColor = UIColor.rgb(fromHex: 0xff5859)
    }
    
    func deselectCard() {
        starButton.isSelected = false
        cardBgImageView.image = UIImage(named: "bg_card_off")
        borderColor = UIColor.rgb(fromHex: 0xb8bbbf)
    }
    
}
