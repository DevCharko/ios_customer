//
//  EmptyCollectionViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 5..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class EmptyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
