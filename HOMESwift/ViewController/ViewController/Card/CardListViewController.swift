//
//  CardListViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

protocol CardListDelegate: NSObjectProtocol {
    func didSelectCard(card_seq:String)
}

/// 자동결제 카드정보 리스트 뷰컨트롤러
class CardListViewController: SKPViewController {
    weak var delegate: CardListDelegate?
    
    var selectedCardIndex: String?

    @IBOutlet weak var cardCollectionView: UICollectionView!
    
    var cardList: CardList? {
        didSet{
            sortAndReload()
        }
    }
    
    // MARK: - collectionView 사이즈 조정
    let columns = 1
    
    let spaceBetweenRows = 13
    let spaceHorisonMargin = 13
    
    var cellWidth: CGFloat {
        get {
            return cardCollectionView.frame.width/CGFloat(columns)-CGFloat(spaceHorisonMargin*2)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController?.previousViewController() is ReservationDetailViewController {
            setNavigationTitle(title: "결제정보 변경")
        } else {
            setNavigationTitle(title: "자동결제 카드정보")
        }
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        cardCollectionView.delegate = self
        cardCollectionView.dataSource = self
        
        cardCollectionView.contentInset.top = 13
        cardCollectionView.contentInset.bottom = 13
        cardCollectionView.register(CardCollectionViewCell.self)
        cardCollectionView.register(EmptyCollectionViewCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setRightBarButton(type: .Add, action: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getCardList()
    }
    
    func getCardList() {
        SKHUD.show()
        
        NetworkManager.getCardList(){ (jsonData, error) in
            if let networkError = error as? NetworkError {
                self.setRightBarButton(type: .Add, action: nil)
                AlertView().alert(self, title: nil, message: networkError.description, cancelButtonTitle: "취소", otherButtonTitles: ["다시 시도"], block: { (AlertView, idx) in
                    if idx == 0 {
                        self.getCardList()
                    } else {
                        self.clickBack()
                    }
                })
            }
            else {
                self.setRightBarButton(type: .Add, action: #selector(self.clickAdd))
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let json = JSON(jsonData!)
                let mapper = Mapper<CardList>()
                self.cardList = mapper.map(JSONString: json.rawString()!)
            }
            
            SKHUD.hide()
        }
    }
    
    @objc func clickAdd() {
        let cardAddVC = CardAddViewController(nibName: "CardAddViewController", bundle: nil)
        cardAddVC.cardCount = cardList?.count
        let naviVC = UINavigationController(rootViewController: cardAddVC)
    
        present(naviVC, animated: true, completion: nil)
    }
    
    func sortAndReload() {
        if self.navigationController?.previousViewController() is ReservationDetailViewController {
            cardList?.list?.sort(by: { (c1, c2) -> Bool in
                return c1.seq == selectedCardIndex ? true : false // 선택된 카드가 제일 위로
            })
        } else {
            cardList?.list?.sort(by: { (c1, c2) -> Bool in
                return c1.is_default == "1" ? true : false // 기본카드가 제일 위로
            })
        }
        cardCollectionView.reloadData()
    }
}

extension CardListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return cardList?.list == nil || (cardList?.list?.count == 0) ? 1 : (cardList?.list?.count)! // 등록된 카드가 없으면 1을 리턴해서 기본셀 호출
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.cardList?.list == nil || (cardList?.list?.count == 0) {
            let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as EmptyCollectionViewCell
            cell.titleLabel.text = "편리하게 결제하세요!"
            cell.descLabel.numberOfLines = 0
            cell.descLabel.text = "우측 상단의 ' + '버튼을 터치하시면\n결제 카드를 등록하실 수 있습니다."
            cell.drawBorder(lineWidth: 1, cornerRadius: 3, color: UIColor.silver)
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as CardCollectionViewCell
        
            // 카드번호
            if let card = self.cardList?.list?[indexPath.row] {
                // 예약내역에서 진입시
                let preVC = self.navigationController?.previousViewController()
                if preVC is ReservationDetailViewController {
                    cell.setModel(card: card, mode: CardMode.change, cardSeq: selectedCardIndex)
                } else {
                    cell.setModel(card: card, mode: CardMode.list)
                    cell.starButton.addTarget(self, action: #selector(markAction(_ : event:)), for: .touchUpInside)
                }
                cell.nickButton.addTarget(self, action: #selector(nickAction(_ : event:)), for: .touchUpInside)
                cell.deleteButton.addTarget(self, action: #selector(deleteAction(_: event:)), for: .touchUpInside)
            }
            return cell
        }
    }
    
    @objc func deleteAction(_ sender: UIButton, event: UIEvent) {
        guard cardList != nil else {
            return
        }
        
        let touch = event.allTouches?.first
        let point = (touch?.location(in: cardCollectionView))! as CGPoint
        let indexPath = cardCollectionView.indexPathForItem(at: point)
        
        let model = cardList?.list?[(indexPath?.row)!]
        
        SKHUD.show()
        
        NetworkManager.removeCard(card_seq: (model?.seq)!, block: { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                self.cardList?.remove(at: (indexPath?.row)!)
                
                // 삭제 후 인덱스패스 동기화
                if self.cardList?.list?.count != 0 {
                    self.cardCollectionView.deleteItems(at: [indexPath!]) // 딜리트 전과 후의 갯수가 달라야함
                } else {
                    self.cardCollectionView.reloadItems(at: [indexPath!]) // 리로드 전과 후의 갯수가 같아야함 -> list가 0일때 임의로 1을 리턴해주기 때문에 동작함
                }
                
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                FirebaseAnalyticsManager.shared.sendLog(ScreenName.card_deleted.rawValue)
            }
            SKHUD.hide()
        })
    }
    
    // 선택, 해제
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let beforecell = collectionView.cellForItem(at: IndexPath.init(row: selectedCardIndex, section: 0)) as? CardCollectionViewCell
//        beforecell?.deselectAddress()
//        
//        let cell = collectionView.cellForItem(at: indexPath) as? CardCollectionViewCell
//        cell?.selectAddress()
//        selectedCardIndex = indexPath.row
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as? CardCollectionViewCell
//        cell?.deselectAddress()
//    }
    
    @objc func markAction(_ sender: UIButton, event: UIEvent) {
        guard cardList != nil else {
            return
        }
        
        let touch = event.allTouches?.first
        let point = (touch?.location(in: cardCollectionView))! as CGPoint
        let indexPath = cardCollectionView.indexPathForItem(at: point)
        
        let model = cardList?.list?[(indexPath?.row)!]
        
        if (model?.is_default!)! == 0 {
            SKHUD.show()
            
            NetworkManager.markCard(card_seq: (model?.seq)!, block: { (jsonData, error) in
                if let networkError = error as? NetworkError {
                    AlertView().alert(self, title: nil,
                                      message: networkError.description)
                }
                else {
                    var indexPaths = [IndexPath]()
                    for (index , element) in (self.cardList?.list?.enumerated())! {
                        if (element.is_default != nil) {
                            element.is_default = "0"
                            indexPaths.append(IndexPath(row: index, section: 0))
                        }
                        if model?.seq == element.seq {
                            element.is_default = "1"
                            indexPaths.append(IndexPath(row: index, section: 0))
                        }
                    }
                    //                    self.getcardList()
                    self.cardCollectionView .reloadItems(at: indexPaths)
                    
                    let return_message = JSON(jsonData!)["return_message"].string!
                    if return_message != "" {
                        AlertView().alert(self, title: nil, message: return_message)
                    }
                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.card_mark.rawValue)
                }
                
                SKHUD.hide()
            })
        }
    }

    @objc func nickAction(_ sender: UIButton, event: UIEvent) {
        guard cardList != nil else {
            return
        }
        
        let touch = event.allTouches?.first
        let point = (touch?.location(in: cardCollectionView))! as CGPoint
        let indexPath = cardCollectionView.indexPathForItem(at: point)
        
        let model = cardList?.list?[(indexPath?.row)!]
        
        //TODO: go to change card nick
        let nickChangeView = CardNickChangeViewController(nibName: "CardNickChangeViewController", bundle: nil)
        nickChangeView.cardSeq = model?.seq
        self.navigationController?.pushViewController(nickChangeView, animated: true)
    }
    
    //cell 간격 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    //cell 사이즈를 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 26, height: CardCollectionViewCell.height)
    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil && cardList != nil && (cardList?.list?.count)! > 0 {
            let card = cardList?.list?[indexPath.row]
            AlertView().alert(self, title: "", message: "결제카드로 지정하시겠습니까?", cancelButtonTitle: "취소", otherButtonTitles: ["지정"], buttonColor: [UIColor.alert, UIColor.rgb(fromHex: 0x999999)], block: { (AlertView, buttonIndex) in
                if buttonIndex == 0 {
                    self.delegate?.didSelectCard(card_seq: (card?.seq)!)
                    self.clickBack()
                } else {
                    self.dismiss(animated: true)
                }
            })
        }
    }
}

