//
//  ChattingListViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 8..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SendBirdSDK
import CHPlugin

class ChattingListViewController: SKPViewController {
    fileprivate var channels: [SBDGroupChannel] = []
    fileprivate var groupChannelListQuery: SBDGroupChannelListQuery?

    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var floatingButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "대화")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        tableView.delegate = self
        tableView.dataSource = self
        addDelegates()
        ChattingListTableViewCell.regist(tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        floatingButton.alpha = 0
        ChannelPlugin.badgeDelegate = self

        // badge 갱신
        self.floatingButton.setBadge(text: DataStore.channelIOBadgeCount)
        self.getChannelList()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        showFloatingButton() // 애니메이션
    }
    
    func showFloatingButton() {
        UIView.animate(withDuration: 0.5) {
            self.floatingButton.alpha = 1.0
        }
    }
    
    func addDelegates() {
        SBDMain.add(self as SBDChannelDelegate, identifier: self.description)
        SBDMain.add(self as SBDConnectionDelegate, identifier: self.description)
    }
    
    // 채널io 대화생성
    @IBAction func clickFloatingButton(_ sender: Any) {
        ChannelPlugin.show(animated: true)
    }
    
    func getChannelList() {
        self.channels.removeAll()
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        self.groupChannelListQuery = SBDGroupChannel.createMyGroupChannelListQuery()
        self.groupChannelListQuery?.limit = 20
        
        self.groupChannelListQuery?.loadNextPage(completionHandler: { (channels, error) in
            if error != nil {
                log.debug("groupChannelListQuery error")
                return
            }
            
            for channel in channels! {
                self.channels.append(channel)
            }
            
            DispatchQueue.main.async {
                if self.channels.count == 0 {
                    self.emptyView.isHidden = false
                }
                else {
                    self.emptyView.isHidden = true
                }
                self.tableView.reloadData()
            }
        })
    }
}

// MARK: - Channel IO Delegate
extension ChattingListViewController : ChannelBadgeDelegate {
    func badgeDidChanged(count: Int) {
        //런처버튼의 뱃지카운트를 업데이트
        if count > 99 {
            self.floatingButton.setBadge(text: "99+")
            DataStore.channelIOBadgeCount = "99+"
        } else if count == 0 {
            self.floatingButton.setBadge(text: "")
            DataStore.channelIOBadgeCount = ""
        } else {
            self.floatingButton.setBadge(text: "\(count)")
            DataStore.channelIOBadgeCount = "\(count)"
        }
    }
}

// MARK: - SBD Delegate
extension ChattingListViewController : SBDConnectionDelegate, SBDChannelDelegate {

    func channel(_ sender: SBDBaseChannel, didReceive message: SBDBaseMessage) {
        // Received a chat message
        self.tableView.reloadData()
    }
    
    func channel(_ sender: SBDGroupChannel, userDidJoin user: SBDUser) {
        // When a new member joined the group channel
    }
    
    func channel(_ sender: SBDGroupChannel, userDidLeave user: SBDUser) {
        // When a member left the group channel
    }
    
    func channelWasDeleted(_ channelUrl: String, channelType: SBDChannelType) {
        // When a channel has been deleted
    }
    
    func channel(_ sender: SBDBaseChannel, messageWasDeleted messageId: Int64) {
        // When a message has been deleted
    }
}

// MARK: - TableView Delegate
extension ChattingListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ChattingListTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard self.channels.count > indexPath.row else {
            log.error("채널 인덱스 에러")
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ChattingListTableViewCell.identifier, for: indexPath) as! ChattingListTableViewCell
        cell.selectionStyle = .none
        cell.setModel(aChannel: self.channels[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ChattingViewController(nibName: "ChattingViewController", bundle: nil)
        
        var roomName = "대화상대 없음"

        // 채널의 유저 목록중에 상대방 유저정보를 얻고 닉네임으로 roomName설정
        for member in self.channels[indexPath.row].members! as NSArray as! [SBDUser] {
            if member.userId == SBDMain.getCurrentUser()?.userId {
                continue
            }
            roomName = member.nickname!
        }
        
        vc.roomName = roomName
        vc.channel = self.channels[indexPath.row]
        
        let naviVC = UINavigationController(rootViewController: vc)
        present(naviVC, animated: true, completion: nil)

    }
    
    // color change when cell is pressed
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let cell = tableView.cellForRow(at: indexPath) as? ChattingListTableViewCell
        cell?.contentView.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
        return true
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ChattingListTableViewCell
        cell.contentView.backgroundColor = UIColor.rgb(fromHex: 0xffffff)
    }
    
}
