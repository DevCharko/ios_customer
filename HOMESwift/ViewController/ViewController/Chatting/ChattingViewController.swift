//
//  ChattingViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 9..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SendBirdSDK

class ChattingViewController: SKPViewController {
    
    var channel: SBDGroupChannel!
    fileprivate var users: [SBDUser] = []
    fileprivate var userListQuery: SBDUserListQuery?
    fileprivate var selectedUsers: [SBDUser] = []
    fileprivate var preSendMessages: [String:SBDBaseMessage] = [:]
    fileprivate var resendableMessages: [String:SBDBaseMessage] = [:]
    fileprivate var messages: [SBDBaseMessage] = []
    fileprivate var messageQuery: SBDPreviousMessageListQuery!
    
    var testDataSource: NSMutableArray? //test
    
    var roomName = ""
    var stopMeasuringVelocity: Bool = true
    var initialLoading: Bool = true
    let limitCount = 3
    var lastMessageHeight: CGFloat = 0
    var scrollLock: Bool = false
    var lastOffset: CGPoint = CGPoint(x: 0, y: 0)
    var lastOffsetCapture: TimeInterval = 0
    var isScrollingFast: Bool = false
    
    private var hasNext: Bool = true
    private var isLoading: Bool = false
    
    var incomingUserMessageSizingTableViewCell: IncomingMessageTableViewCell?
    var outgoingUserMessageSizingTableViewCell: OutgoingMessageTableViewCell?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var bottomInputView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: roomName)
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickExit))
        
        hideKeyboardWhenTappedAround()
        setupViewResizerOnKeyboardShown()
        
        initChattingView()
        addDelegates()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // constraint 적용 후 셀 높이 구하기 위해
        initSizingCell()
        
        // 기존 채팅이 있으면 로딩
        self.loadPreviousMessage(initial: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        //        removeDelegates()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.dismissKeyboard()
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        super.keyboardWillShow(notification: notification)
        DispatchQueue.main.async {
            self.scrollToBottom(animated: true, force: true)
        }
    }
    
    fileprivate func loadPreviousMessage(initial: Bool) {
        if initial == true {
            self.resendableMessages.removeAll()
            self.preSendMessages.removeAll()
            
            self.tableView.isHidden = true
            self.messageQuery = self.channel.createPreviousMessageListQuery()
            self.hasNext = true
            self.messages.removeAll()
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        if self.hasNext == false {
            self.tableView.isHidden = false;
            return
        }
        
        if self.isLoading == true {
            self.tableView.isHidden = false;
            return
        }
        
        self.isLoading = true
        
        self.messageQuery.loadPreviousMessages(withLimit: limitCount, reverse: !initial) { (messages, error) in
            if error != nil {
//                let vc = UIAlertController(title: Bundle.sbLocalizedStringForKey(key: "ErrorTitle"), message: error?.domain, preferredStyle: UIAlertControllerStyle.alert)
//                let closeAction = UIAlertAction(title: Bundle.sbLocalizedStringForKey(key: "CloseButton"), style: UIAlertActionStyle.cancel, handler: nil)
//                vc.addAction(closeAction)
//                DispatchQueue.main.async {
//                    self.present(vc, animated: true, completion: nil)
//                }
//                
                self.tableView.isHidden = false
                self.isLoading = false
                
                return
            }
            
            if messages?.count == 0 {
                self.hasNext = false
            }
            
            if initial == true {
                for message in messages! {
                    self.messages.append(message)
                }
                
                self.channel.markAsRead()
            }
            else {
                for message in messages! {
                    self.messages.insert(message, at: 0)
                }
            }
            
            if initial == true {
                self.initialLoading = true
                
                if (messages?.count)! > 0 {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        DispatchQueue.main.async {
                            self.scrollToBottom(animated: false, force: true)
                            self.tableView.isHidden = false
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.tableView.isHidden = false
                    }
                }
                
                self.initialLoading = false
                self.isLoading = false
            }
            else {
                if (messages?.count)! > 0 {
                    DispatchQueue.main.async {
                        let contentSizeBefore = self.tableView.contentSize;
                        
                        self.tableView.reloadData()
                        self.tableView.layoutIfNeeded()
                        
                        let contentSizeAfter = self.tableView.contentSize;
                        
                        let newContentOffset = CGPoint(x: 0, y: contentSizeAfter.height - contentSizeBefore.height)
                        self.tableView.setContentOffset(newContentOffset, animated: false)
                        
                        DispatchQueue.main.async {
                            self.tableView.isHidden = false
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.tableView.isHidden = false
                    }
                }
                self.isLoading = false
            }
        }

    }
    
    @IBAction func clickSendMessage(_ sender: Any) {
        guard channel != nil else {
            return
        }
        
        self.textViewHeight.constant = 36
        
        if self.tvMessage.text.characters.count > 0 {
            let message = self.tvMessage.text
            self.tvMessage.text = ""
            
            let preSendMessage = self.channel.sendUserMessage(message, data: "", customType: "", targetLanguages: [], completionHandler: { (userMessage, error) in
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(150), execute: {
                    let preSendMessage = self.preSendMessages[(userMessage?.requestId)!] as! SBDUserMessage
                    self.preSendMessages.removeValue(forKey: (userMessage?.requestId)!)
                    
                    if error != nil {
                        self.resendableMessages[(userMessage?.requestId)!] = userMessage
                        self.tableView.reloadData()
                        DispatchQueue.main.async {
                            self.scrollToBottom(animated: true, force: true)
                        }
                        
                        return
                    }
                    
                    self.messages[(self.messages.index(of: preSendMessage))!] = userMessage!
                    
                    self.tableView.reloadData()
                    DispatchQueue.main.async {
                        self.scrollToBottom(animated: true, force: true)
                    }
                })
            })
            self.preSendMessages[(preSendMessage.requestId)!] = preSendMessage
            self.messages.append(preSendMessage)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                DispatchQueue.main.async {
                    self.scrollToBottom(animated: true, force: true)
                }
            }
        }
    }
    
    func initChattingView() {
        initTextView()
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0)
        self.tvMessage.textContainerInset = UIEdgeInsetsMake(10, 0, 9, 0)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        IncomingMessageTableViewCell.regist(tableView)
        OutgoingMessageTableViewCell.regist(tableView)
        //        setTestData()
        
        
    }
    
    // 셀 사이즈를 구하기 위한 셀 초기화
    func initSizingCell() {
        self.incomingUserMessageSizingTableViewCell = IncomingMessageTableViewCell.nib().instantiate(withOwner: self, options: nil)[0] as? IncomingMessageTableViewCell
        self.incomingUserMessageSizingTableViewCell?.frame = self.view.frame
        self.incomingUserMessageSizingTableViewCell?.isHidden = true
        self.view.addSubview(self.incomingUserMessageSizingTableViewCell!)
        
        self.outgoingUserMessageSizingTableViewCell = OutgoingMessageTableViewCell.nib().instantiate(withOwner: self, options: nil)[0] as? OutgoingMessageTableViewCell
        self.outgoingUserMessageSizingTableViewCell?.frame = self.view.frame
        self.outgoingUserMessageSizingTableViewCell?.isHidden = true
        self.view.addSubview(self.outgoingUserMessageSizingTableViewCell!)
    }
    
    func scrollToBottom(animated: Bool, force: Bool) {
        if self.messages.count == 0 {
            return
        }
        
        if self.scrollLock == true && force == false {
            return
        }
        
        self.tableView.scrollToRow(at: IndexPath.init(row: self.messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: false)
    }
    
    func scrollToPosition(position: Int) {
        if self.messages.count == 0 {
            return
        }
        
        self.tableView.scrollToRow(at: IndexPath.init(row: position, section: 0), at: UITableViewScrollPosition.top, animated: false)
    }
    
    func setTestData() {
        testDataSource = [["incomming", "profile", "오홈마님, 안녕하세요. 정기예약 토요일\n사무실 청소를 끝내놓았습니다. 겨우내내\n바닥청소를 안하셔서 그런지 구석구석 먼지속에서 물건들을 발견하였어요. ", "오후 4:20"],
                          ["incomming", "profile", "정리해서 올려놨습니다. ", "오후 4:20"],
                          ["incomming", "profile", "오홈마님 안녕하세요!\n오늘도 좋은하루 되세요~", "오후 4:20"]]
    }
    
    func hideKeyboardWhenFastScrolling(view: UIView) {
        DispatchQueue.main.async {
            //            self.bottomMargin.constant = 0
            self.view.layoutIfNeeded()
            self.scrollToBottom(animated: true, force: false)
        }
        self.view.endEditing(true)
    }
    
}

extension ChattingViewController: SBDChannelDelegate, SBDConnectionDelegate {
    
    func addDelegates() {
        SBDMain.add(self as SBDChannelDelegate, identifier: self.description)
        SBDMain.add(self as SBDConnectionDelegate, identifier: self.description)
    }
    
    //    func removeDelegates() {
    //        SBDMain.removeChannelDelegate(forIdentifier: self.description)
    //        SBDMain.removeConnectionDelegate(forIdentifier: self.description)
    //    }
    
    // MARK: SBDChannelDelegate
    func channel(_ sender: SBDBaseChannel, didReceive message: SBDBaseMessage) {
        log.debug("didReceive")
        
        if sender == self.channel {
            self.channel?.markAsRead()
            
            self.messages.append(message)
            self.tableView.reloadData()
            DispatchQueue.main.async {
                self.scrollToBottom(animated: true, force: false)
            }
        }
    }
    
    func channelDidUpdateReadReceipt(_ sender: SBDGroupChannel) {
        log.debug("channelDidUpdateReadReceipt")
        
        if sender == self.channel {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func channelDidUpdateTypingStatus(_ sender: SBDGroupChannel) {
        log.debug("channelDidUpdateTypingStatus")
        
        if sender == self.channel {
            if sender.getTypingMembers()?.count == 0 {
            }
            else {
                if sender.getTypingMembers()?.count == 1 {
                    //                    self.startTypingIndicator(text: String(format: Bundle.sbLocalizedStringForKey(key: "TypingMessageSingular"), (sender.getTypingMembers()?[0].nickname)!))
                }
                else {
                    //                    self.startTypingIndicator(text: Bundle.sbLocalizedStringForKey(key: "TypingMessagePlural"))
                }
            }
        }
    }
    
    func channelWasChanged(_ sender: SBDBaseChannel) {
        if sender == self.channel {
            DispatchQueue.main.async {
                log.debug("channelWasChanged")
                //                self.navItem.title = String(format: Bundle.sbLocalizedStringForKey(key: "GroupChannelTitle"), self.groupChannel.memberCount)
            }
        }
    }
    
    func channelWasDeleted(_ channelUrl: String, channelType: SBDChannelType) {
        log.debug("channelWasDeleted")
    }
    
    func channel(_ sender: SBDBaseChannel, messageWasDeleted messageId: Int64) {
        if sender == self.channel {
            for message in self.messages {
                if message.messageId == messageId {
                    self.messages.remove(at: (self.messages.index(of: message))!)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    break
                }
            }
        }
    }
}


extension ChattingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat = 0
        let msg = self.messages[indexPath.row]
        let userMessage = msg as? SBDUserMessage
        let sender = userMessage?.sender
        
        if sender?.userId == SBDMain.getCurrentUser()?.userId {
            
            // Outgoing
            if indexPath.row > 0 {
                self.outgoingUserMessageSizingTableViewCell?.setPreviousMessage(aPrevMessage: self.messages[indexPath.row - 1])
            }
            else {
                self.outgoingUserMessageSizingTableViewCell?.setPreviousMessage(aPrevMessage: nil)
            }
            self.outgoingUserMessageSizingTableViewCell?.setModel(aMessage: userMessage)
            height = (self.outgoingUserMessageSizingTableViewCell?.getHeightOfViewCell())!
        }
        else {
            // Incoming
            if indexPath.row > 0 {
                self.incomingUserMessageSizingTableViewCell?.setPreviousMessage(aPrevMessage: self.messages[indexPath.row - 1])
            }
            else {
                self.incomingUserMessageSizingTableViewCell?.setPreviousMessage(aPrevMessage: nil)
            }
            self.incomingUserMessageSizingTableViewCell?.setModel(aMessage: userMessage)
            height = (self.incomingUserMessageSizingTableViewCell?.getHeightOfViewCell())!
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingMessageTableViewCell", for: indexPath) as! IncomingMessageTableViewCell
        var cell: UITableViewCell?
        
        let msg = self.messages[indexPath.row]
        
        let userMessage = msg as? SBDUserMessage
        let sender = userMessage?.sender
        
        if sender?.userId == SBDMain.getCurrentUser()?.userId {
            // Outgoing
            cell = tableView.dequeueReusableCell(withIdentifier: OutgoingMessageTableViewCell.identifier) as! OutgoingMessageTableViewCell
            //            cell?.frame = CGRect(x: (cell?.frame.origin.x)!, y: (cell?.frame.origin.y)!, width: (cell?.frame.size.width)!, height: (cell?.frame.size.height)!)
            if indexPath.row > 0 {
                (cell as! OutgoingMessageTableViewCell).setPreviousMessage(aPrevMessage: self.messages[indexPath.row - 1])
            }
            else {
                (cell as! OutgoingMessageTableViewCell).setPreviousMessage(aPrevMessage: nil)
            }
            
            if indexPath.row+1 < messages.count {
                (cell as! OutgoingMessageTableViewCell).setNextMessage(nextMessage: self.messages[indexPath.row + 1])
            } else {
                (cell as! OutgoingMessageTableViewCell).setNextMessage(nextMessage: nil)
            }
            
            (cell as! OutgoingMessageTableViewCell).setModel(aMessage: userMessage)
            
            
            //            (cell as! OutgoingMessageTableViewCell).delegate = self.delegate
            //
            //            if self.preSendMessages[userMessage.requestId!] != nil {
            //                (cell as! OutgoingMessageTableViewCell).showSendingStatus()
            //            }
            //            else {
            //                if self.resendableMessages[userMessage.requestId!] != nil {
            //                    //                        (cell as! OutgoingUserMessageTableViewCell).showMessageControlButton()
            //                    (cell as! OutgoingMessageTableViewCell).showFailedStatus()
            //                }
            //                else {
            //                    (cell as! OutgoingMessageTableViewCell).showMessageDate()
            //                    (cell as! OutgoingMessageTableViewCell).showUnreadCount()
            //                }
            //            }
        }
        else {
            // Incoming
            cell = tableView.dequeueReusableCell(withIdentifier: IncomingMessageTableViewCell.identifier)
            cell?.frame = CGRect(x: (cell?.frame.origin.x)!, y: (cell?.frame.origin.y)!, width: (cell?.frame.size.width)!, height: (cell?.frame.size.height)!)
            if indexPath.row > 0 {
                (cell as! IncomingMessageTableViewCell).setPreviousMessage(aPrevMessage: self.messages[indexPath.row - 1])
            }
            else {
                (cell as! IncomingMessageTableViewCell).setPreviousMessage(aPrevMessage: nil)
            }
            
            if indexPath.row+1 < messages.count {
                (cell as! IncomingMessageTableViewCell).setNextMessage(nextMessage: self.messages[indexPath.row + 1])
            } else {
                (cell as! IncomingMessageTableViewCell).setNextMessage(nextMessage: nil)
            }
            
            (cell as! IncomingMessageTableViewCell).setModel(aMessage: userMessage)
            //            (cell as! IncomingMessageTableViewCell).delegate = self.delegate
        }
        
        return cell!
    }
    
    // MARK: UITableViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.stopMeasuringVelocity = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.stopMeasuringVelocity = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            if self.stopMeasuringVelocity == false {
                let currentOffset = scrollView.contentOffset
                let currentTime = NSDate.timeIntervalSinceReferenceDate
                
                let timeDiff = currentTime - self.lastOffsetCapture
                if timeDiff > 0.1 {
                    let distance = currentOffset.y - self.lastOffset.y
                    let scrollSpeedNotAbs = distance * 10 / 1000
                    let scrollSpeed = fabs(scrollSpeedNotAbs)
                    if scrollSpeed > 2 {
                        self.isScrollingFast = true
                    }
                    else {
                        self.isScrollingFast = false
                    }
                    
                    self.lastOffset = currentOffset
                    self.lastOffsetCapture = currentTime
                }
                
                if self.isScrollingFast {
                    self.hideKeyboardWhenFastScrolling(view: self.view)
                }
            }
            
            if scrollView.contentOffset.y + scrollView.frame.size.height + self.lastMessageHeight < scrollView.contentSize.height {
                self.scrollLock = true
            }
            else {
                self.scrollLock = false
            }
            
            if scrollView.contentOffset.y == -64 {
                if self.messages.count > 0 {
                    self.loadPreviousMessage(initial: false)
                }
            }
        }
    }
}

extension ChattingViewController: UITextViewDelegate {
    
    func initTextView() {
        tvMessage.delegate = self
        tvMessage.drawBorder(lineWidth: 1, cornerRadius: 2, color: .line)
        tvMessage.text = "메시지를 입력해주세요"
        tvMessage.textColor = UIColor.placeholder
        //        tvMessage.textContainer.maximumNumberOfLines = 5
        //        tvMessage.textContainer.lineBreakMode = NSLineBreakMode.byTruncatingTail
        //        tvMessage.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.placeholder {
            textView.text = nil
            textView.textColor = UIColor.main
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            tvMessage.text = "메시지를 입력해주세요"
            textView.textColor = UIColor.placeholder
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        
        if newSize.height > 136.0 {
            textViewHeight.constant = 136.0
            textView.isScrollEnabled = false
        }
        else if newSize.height < 36 {
            textViewHeight.constant = 36
            textView.isScrollEnabled = false
        }
        else {
            textViewHeight.constant = newSize.height
            textView.isScrollEnabled = false
        }
        
        if textView.text.length == 0 {
            log.debug("END typing")
            //            [_channel, endTyping]
        }
        else {
            log.debug("START typing")
            //            [_channel, startTyping]
        }
        
    }
}
