//
//  ChattingListTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 8..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SendBirdSDK

class ChattingListTableViewCell: SKTableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeCountLabel: UILabel!
    
    private var channel: SBDGroupChannel!
    
    static var height: CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return 71 * screenHeight / 568
    }
    
    func setModel(aChannel: SBDGroupChannel) {
        self.channel = aChannel
        
        // 도우미가 채팅방을 나갈경우?
        if self.channel.memberCount == 1 {
            self.profileImageView.image = UIImage(named: "profile") // 기본 프로필 사진
            self.nameLabel.text = "대화상대 없음"
        }
        else {
            for member in self.channel.members! as NSArray as! [SBDUser] {
                if member.userId == SBDMain.getCurrentUser()?.userId {
                    continue
                }
                self.profileImageView.imageFromUrl(member.profileUrl!, defaultImgPath: "profile")
                self.nameLabel.text = member.nickname
            }
        }
        
        self.profileImageView.setRound()
        
        var lastMessageTimestamp: Int64 = 0
        
        if self.channel.lastMessage != nil {
            let lastMessage = (self.channel.lastMessage as! SBDUserMessage)
            self.contentLabel.text = lastMessage.message
            lastMessageTimestamp = Int64(lastMessage.createdAt)
            
            // Last message date time
            let lastMessageDateFormatter = DateFormatter()
            lastMessageDateFormatter.dateFormat = "yyyy.MM.dd.E"
            
            var lastMessageDate: Date?
            if String(format: "%lld", lastMessageTimestamp).characters.count == 10 {
                lastMessageDate = Date.init(timeIntervalSince1970: Double(lastMessageTimestamp))
            }
            else {
                lastMessageDate = Date.init(timeIntervalSince1970: Double(lastMessageTimestamp) / 1000.0)
            }
            let currDate = Date()
            
            let lastMessageDateComponents = NSCalendar.current.dateComponents([.day, .month, .year], from: lastMessageDate! as Date)
            let currDateComponents = NSCalendar.current.dateComponents([.day, .month, .year], from: currDate as Date)
            
            if lastMessageDateComponents.year != currDateComponents.year || lastMessageDateComponents.month != currDateComponents.month || lastMessageDateComponents.day != currDateComponents.day {
                self.dateLabel.text = lastMessageDateFormatter.string(from: lastMessageDate!)
            }
            else {
                self.dateLabel.text = lastMessageDateFormatter.string(from: lastMessageDate!)
            }
        }
        
        self.badgeView.isHidden = false
        if self.channel.unreadMessageCount == 0 {
            self.badgeView.isHidden = true
        }
        else if self.channel.unreadMessageCount <= 99 {
            self.badgeCountLabel.text = String(format: "%ld", self.channel.unreadMessageCount)
        }
        else {
            self.badgeCountLabel.text = "99+"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        badgeView.roundCorners(corners: .allCorners, radius: 6.5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
