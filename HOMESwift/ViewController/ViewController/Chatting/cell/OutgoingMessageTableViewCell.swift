//
//  OutgoingMessageTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 16..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SendBirdSDK

class OutgoingMessageTableViewCell: SKTableViewCell {

    let dateContanerViewMaxHeight: CGFloat = 63.0
    let nameMaxHeight: CGFloat = 11.0
    let nameLabelMinBottom: CGFloat = 2.0
    let nameLabelMaxBottom: CGFloat = 7.0
    let sameBubbleSpace:CGFloat = 2.0
    let differentBubbleSpace:CGFloat = 18.0
    
    private var message: SBDUserMessage!
    private var prevMessage: SBDBaseMessage!
    private var nextMessage: SBDBaseMessage!

    @IBOutlet weak var dateSeperatorContainerView: UIView!
    @IBOutlet weak var dateSeperatorLabel: UILabel!
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageDateLabel: UILabel!
    @IBOutlet weak var isReadLabel: UILabel!
    
    @IBOutlet weak var dateContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dateContainerBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var messageContainerTopPadding: NSLayoutConstraint!
    @IBOutlet weak var messageContainerBottomPadding: NSLayoutConstraint!
    @IBOutlet weak var messageContainerRightMargin: NSLayoutConstraint!
    @IBOutlet weak var messageContainerRightPadding: NSLayoutConstraint!
    @IBOutlet weak var messageContainerLeftPadding: NSLayoutConstraint!
    @IBOutlet weak var messageContainerLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var messageDateLabelLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var messageDateLabelWidth: NSLayoutConstraint!

    func setPreviousMessage(aPrevMessage: SBDBaseMessage?) {
        self.prevMessage = aPrevMessage
    }
    
    func setNextMessage(nextMessage: SBDBaseMessage?) {
        self.nextMessage = nextMessage
    }

    func buildMessage() -> NSAttributedString {
        let messageAttribute = [
            NSFontAttributeName: DataStore.messageFont()
        ]
        
        let message = self.message?.message
        
        let fullMessage = NSMutableAttributedString.init(string: message!)
        fullMessage.addAttributes(messageAttribute, range: NSMakeRange(0, (message?.characters.count)!))
        
        return fullMessage
    }
    
    func getHeightOfViewCell() -> CGFloat {
        let fullMessage = self.buildMessage()
        var fullMessageRect: CGRect
        
        let messageLabelMaxWidth = self.frame.size.width -
            (self.messageContainerRightMargin.constant +
                self.messageContainerRightPadding.constant +
                self.messageContainerLeftPadding.constant +
                self.messageContainerLeftMargin.constant +
                self.messageDateLabelLeftMargin.constant +
                self.messageDateLabelWidth.constant)
        
        fullMessageRect = fullMessage.boundingRect(with: CGSize.init(width: messageLabelMaxWidth, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        let cellHeight = self.dateContainerBottomMargin.constant +
            self.dateContainerViewHeight.constant +
            self.messageContainerTopPadding.constant +
            fullMessageRect.size.height +
            self.messageContainerBottomPadding.constant
        
//        log.debug(fullMessageRect.size.height)

        return cellHeight
    }
    
    func setModel(aMessage: SBDUserMessage?) {
        self.message = aMessage
        
        let fullMessage = self.buildMessage()
        
        self.messageLabel.attributedText = fullMessage
        
        // Unread message count
        let channelOfMessage = SBDGroupChannel.getChannelFromCache(withChannelUrl: self.message.channelUrl!)
        if channelOfMessage != nil {
            let unreadMessageCount = channelOfMessage?.getReadReceipt(of: self.message)
            if unreadMessageCount == 0 {
                self.isReadLabel.text = "읽음"
            }
            else {
                self.isReadLabel.text = "안읽음"
            }
        }
        
        // Message Date
        let messageDateAttribute = [
            NSFontAttributeName: DataStore.messageDateFont(),
            NSForegroundColorAttributeName: DataStore.messageDateColor()
        ]
        
        let messageTimestamp = Double(self.message.createdAt) / 1000.0
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        let messageCreatedDate = NSDate(timeIntervalSince1970: messageTimestamp)
        let messageDateString = dateFormatter.string(from: messageCreatedDate as Date)
        let messageDateAttributedString = NSMutableAttributedString(string: messageDateString, attributes: messageDateAttribute)
        self.messageDateLabel.attributedText = messageDateAttributedString

        if self.nextMessage != nil {
            if self.message.sender?.userId == (self.nextMessage as! SBDUserMessage).sender?.userId {
                self.isReadLabel.isHidden = true
                
                let nextTimeStamp = Double((self.nextMessage as! SBDUserMessage).createdAt) / 1000.0
                let nextTime = NSDate(timeIntervalSince1970: nextTimeStamp)
                let prevTimeString = messageDateString
                let nextTimeString = dateFormatter.string(from: nextTime as Date)
                
                if prevTimeString == nextTimeString {
                    self.messageDateLabel.isHidden = true
                } else {
                    self.messageDateLabel.isHidden = false
                }
            } else {
                self.isReadLabel.isHidden = false
                self.messageDateLabel.isHidden = false
            }
        }
        else {
            self.isReadLabel.isHidden = false
            self.messageDateLabel.isHidden = false
        }
        
        // Seperator Date
        let seperatorDateFormatter = DateFormatter()
        seperatorDateFormatter.dateFormat = "yyyy.MM.dd(E)"
        self.dateSeperatorLabel.text = seperatorDateFormatter.string(from: messageCreatedDate as Date)
        
        // Relationship between the current message and the previous message
        self.dateSeperatorContainerView.isHidden = false
        self.dateContainerViewHeight.constant = dateContanerViewMaxHeight
        self.dateContainerBottomMargin.constant = 0
        
        if self.prevMessage != nil {
            // Day Changed
            let prevMessageDate = NSDate(timeIntervalSince1970: Double(self.prevMessage.createdAt) / 1000.0)
            let currMessageDate = NSDate(timeIntervalSince1970: Double(self.message.createdAt) / 1000.0)
            let prevMessageDateComponents = NSCalendar.current.dateComponents([.day, .month, .year], from: prevMessageDate as Date)
            let currMessagedateComponents = NSCalendar.current.dateComponents([.day, .month, .year], from: currMessageDate as Date)
            
            if prevMessageDateComponents.year != currMessagedateComponents.year || prevMessageDateComponents.month != currMessagedateComponents.month || prevMessageDateComponents.day != currMessagedateComponents.day {
                // Show date seperator.
                self.dateSeperatorContainerView.isHidden = false
                self.dateContainerViewHeight.constant = dateContanerViewMaxHeight
                self.dateContainerBottomMargin.constant = 0
            }
            else {
                // Hide date seperator.
                self.dateSeperatorContainerView.isHidden = true
                self.dateContainerViewHeight.constant = 0
                
                // Continuous Message
                if self.prevMessage is SBDAdminMessage {
                    self.dateContainerBottomMargin.constant = 2.0
                }
                else {
                    var prevMessageSender: SBDUser?
                    var currMessageSender: SBDUser?
                    
                    if self.prevMessage is SBDUserMessage {
                        prevMessageSender = (self.prevMessage as! SBDUserMessage).sender
                    }
                    else if self.prevMessage is SBDFileMessage {
                        prevMessageSender = (self.prevMessage as! SBDFileMessage).sender
                    }
                    
                    currMessageSender = self.message.sender
                    
                    if prevMessageSender != nil && currMessageSender != nil {
                        if prevMessageSender?.userId == currMessageSender?.userId {
                            // Reduce margin
                            self.dateContainerBottomMargin.constant = sameBubbleSpace
                        }
                        else {
                            // Set default margin.
                            self.dateContainerBottomMargin.constant = differentBubbleSpace
                        }
                    }
                }
            }
        }

        self.layoutIfNeeded()
    }
    
    func hideUnreadCount() {
        self.isReadLabel.isHidden = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
