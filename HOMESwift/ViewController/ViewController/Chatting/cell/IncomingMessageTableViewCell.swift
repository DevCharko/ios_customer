//
//  OutgoingMessageTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 9..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SendBirdSDK

class IncomingMessageTableViewCell: SKTableViewCell {
    
    let dateContanerViewMaxHeight: CGFloat = 63.0
    let nameMaxHeight: CGFloat = 11.0
    let nameLabelMinBottom: CGFloat = 0
    let nameLabelMaxBottom: CGFloat = 7.0
    let sameBubbleSpace:CGFloat = 2.0
    let differentBubbleSpace:CGFloat = 18.0
    
    var message: SBDUserMessage!
    private var prevMessage: SBDBaseMessage!
    private var nextMessage: SBDBaseMessage!

    @IBOutlet weak var dateSeperatorContainerView: UIView!
    @IBOutlet weak var dateSeperatorLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageDateLabel: UILabel!
    
    @IBOutlet weak var dateContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dateContainerBottomMargin: NSLayoutConstraint!
    
    @IBOutlet weak var profileImageLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var profileImageWidth: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var nameBottomMargin: NSLayoutConstraint!
    
    @IBOutlet weak var messageContainerLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var messageContainerTopPadding: NSLayoutConstraint!
    @IBOutlet weak var messageContainerRightPadding: NSLayoutConstraint!
    @IBOutlet weak var messageContainerLeftPadding: NSLayoutConstraint!
    @IBOutlet weak var messageContainerBottomPadding: NSLayoutConstraint!
    
    @IBOutlet weak var messageDateLabelLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var messageDateLabelRightMargin: NSLayoutConstraint!
    @IBOutlet weak var messageDateLabelWidth: NSLayoutConstraint!
    
    func setPreviousMessage(aPrevMessage: SBDBaseMessage?) {
        self.prevMessage = aPrevMessage
    }
    
    func setNextMessage(nextMessage: SBDBaseMessage?) {
        self.nextMessage = nextMessage
    }

    func buildMessage() -> NSAttributedString {
        let messageAttribute = [
            NSFontAttributeName: DataStore.messageFont()
        ]
        
        let message = self.message?.message
        
        let fullMessage = NSMutableAttributedString.init(string: message!)
        fullMessage.addAttributes(messageAttribute, range: NSMakeRange(0, (message?.characters.count)!))
        
        
        return fullMessage
    }
    
    func getHeightOfViewCell() -> CGFloat {
        let fullMessage = self.buildMessage()
        var fullMessageRect: CGRect
        
        let messageLabelMaxWidth = self.frame.size.width -
            (self.profileImageLeftMargin.constant +
                self.profileImageWidth.constant +
                self.messageContainerLeftMargin.constant +
                self.messageContainerRightPadding.constant +
                self.messageContainerLeftPadding.constant +
                self.messageDateLabelLeftMargin.constant +
                self.messageDateLabelWidth.constant +
                self.messageDateLabelRightMargin.constant)
        
        fullMessageRect = fullMessage.boundingRect(with: CGSize.init(width: messageLabelMaxWidth, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        let cellHeight = self.dateContainerBottomMargin.constant +
            self.nameLabelHeight.constant +
            self.nameBottomMargin.constant +
            self.dateContainerViewHeight.constant +
            self.messageContainerTopPadding.constant +
            fullMessageRect.size.height +
            self.messageContainerBottomPadding.constant
        
        return cellHeight
    }
    
    
    func setModel(aMessage: SBDUserMessage?) {
        self.message = aMessage
        
        self.profileImageView.imageFromUrl(self.message.sender?.profileUrl!, defaultImgPath: "profile")
        self.profileImageView.setRound()
        self.nickNameLabel.text = self.message.sender?.nickname!
        
        //        let profileImageTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(clickProfileImage))
        //        self.profileImageView.isUserInteractionEnabled = true
        //        self.profileImageView.addGestureRecognizer(profileImageTapRecognizer)
        
        //        let messageContainerTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(clickUserMessage))
        //        self.messageContainerView.isUserInteractionEnabled = true
        //        self.messageContainerView.addGestureRecognizer(messageContainerTapRecognizer)
        
        // Message Date
        let messageDateAttribute = [
            NSFontAttributeName: DataStore.messageDateFont(),
            NSForegroundColorAttributeName: DataStore.messageDateColor()
        ]
        
        let messageTimestamp = Double(self.message.createdAt) / 1000.0
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        let messageCreatedDate = NSDate(timeIntervalSince1970: messageTimestamp)
        let messageDateString = dateFormatter.string(from: messageCreatedDate as Date)
        let messageDateAttributedString = NSMutableAttributedString(string: messageDateString, attributes: messageDateAttribute)
        self.messageDateLabel.attributedText = messageDateAttributedString
        
        if self.nextMessage != nil {
            if self.message.sender?.userId == (self.nextMessage as! SBDUserMessage).sender?.userId {
                
                let nextTimeStamp = Double((self.nextMessage as! SBDUserMessage).createdAt) / 1000.0
                let nextTime = NSDate(timeIntervalSince1970: nextTimeStamp)
                let prevTimeString = messageDateString
                let nextTimeString = dateFormatter.string(from: nextTime as Date)
                
                if prevTimeString == nextTimeString {
                    self.messageDateLabel.isHidden = true
                } else {
                    self.messageDateLabel.isHidden = false
                }
            } else {
                self.messageDateLabel.isHidden = false
            }
        }
        else {
            self.messageDateLabel.isHidden = false
        }
        
        // Seperator Date
        let seperatorDateFormatter = DateFormatter()
        seperatorDateFormatter.dateFormat = "yyyy.MM.dd(E)"
        self.dateSeperatorLabel.text = seperatorDateFormatter.string(from: messageCreatedDate as Date)
        
        // Relationship between the current message and the previous message
        
        self.dateContainerBottomMargin.constant = 0
        self.dateContainerViewHeight.constant = dateContanerViewMaxHeight
        self.dateSeperatorContainerView.isHidden = false
        self.profileImageView.isHidden = false
        self.nickNameLabel.isHidden = false
        
        self.nameLabelHeight.constant = nameMaxHeight
        self.nameBottomMargin.constant = nameLabelMaxBottom
        
        if self.prevMessage != nil {
            // Day Changed
            let prevMessageDate = NSDate(timeIntervalSince1970: Double(self.prevMessage.createdAt) / 1000.0)
            let currMessageDate = NSDate(timeIntervalSince1970: Double(self.message.createdAt) / 1000.0)
            let prevMessageDateComponents = NSCalendar.current.dateComponents([.day, .month, .year], from: prevMessageDate as Date)
            let currMessagedateComponents = NSCalendar.current.dateComponents([.day, .month, .year], from: currMessageDate as Date)
            
            // 날짜가 다르면
            if prevMessageDateComponents.year != currMessagedateComponents.year || prevMessageDateComponents.month != currMessagedateComponents.month || prevMessageDateComponents.day != currMessagedateComponents.day {
                // Show date seperator.
                self.dateSeperatorContainerView.isHidden = false
                self.dateContainerViewHeight.constant = dateContanerViewMaxHeight
                self.dateContainerBottomMargin.constant = 0
            }
            else { // 같으면 날짜 뷰 없앰
                // Hide date seperator.
                self.dateSeperatorContainerView.isHidden = true
                self.dateContainerViewHeight.constant = 0
                self.nameLabelHeight.constant = 0
                self.nameBottomMargin.constant = nameLabelMinBottom
            }
            
            // Continuous Message
            var prevMessageSender: SBDUser?
            var currMessageSender: SBDUser?
            
            prevMessageSender = (self.prevMessage as! SBDUserMessage).sender
            
            currMessageSender = self.message.sender
            
            if prevMessageSender != nil && currMessageSender != nil {
                if prevMessageSender?.userId == currMessageSender?.userId {
                    // 같은 유저일경우
                    self.profileImageView.isHidden = true
                    self.nickNameLabel.isHidden = true
                    self.nameLabelHeight.constant = 0
                    self.nameBottomMargin.constant = nameLabelMinBottom
                    self.dateContainerBottomMargin.constant = sameBubbleSpace
                }
                else {
                    self.profileImageView.isHidden = false
                    self.nickNameLabel.isHidden = false
                    self.nameLabelHeight.constant = nameMaxHeight
                    self.nameBottomMargin.constant = nameLabelMaxBottom
                    self.dateContainerBottomMargin.constant = differentBubbleSpace
                }
                
            }

        }
        
        let fullMessage = self.buildMessage()
        self.messageLabel.attributedText = fullMessage
        
        self.layoutIfNeeded()
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
