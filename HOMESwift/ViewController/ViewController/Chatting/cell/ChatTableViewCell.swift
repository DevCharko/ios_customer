//
//  ChatTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 18..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SendBirdSDK

// 부모 셀로 리펙토링할것
class ChatTableViewCell: SKTableViewCell {

    let dateContanerViewMaxHeight: CGFloat = 63.0
    let nameMaxHeight: CGFloat = 11.0
    let nameLabelMinBottom: CGFloat = 0
    let nameLabelMaxBottom: CGFloat = 7.0
    let sameBubbleSpace:CGFloat = 2.0
    let differentBubbleSpace:CGFloat = 18.0

    var message: SBDUserMessage!
    private var prevMessage: SBDBaseMessage!
    private var nextMessage: SBDBaseMessage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
