//
//  PayHistoryViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 16..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

/// 결졔 내역 뷰컨트롤러
class PayHistoryViewController: SKPViewController {
    
    var payHistoryListModel: PayHistoryListModel? {
        didSet{
            tableView.reloadData()
            if self.payHistoryListModel == nil || self.payHistoryListModel?.count == 0 {
                emptyView.isHidden = false
            } else {
                emptyView.isHidden = true
            }
        }
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationTitle(title: "결제 내역")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        tableView.register(PayHistoryTableViewCell.self)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

        self.getHistoryList()
    }

    func getHistoryList() {
        SKHUD.show()

        NetworkManager.getPayHistoryList { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            } else {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let mapper = Mapper<PayHistoryListModel>()
                
                self.payHistoryListModel = mapper.map(JSONString: json.rawString()!)
            }
            self.tableView.reloadData()
            SKHUD.hide()
        }
    }
}

extension PayHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = payHistoryListModel {
            return model.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PayHistoryTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as PayHistoryTableViewCell
        cell.setModel(model: payHistoryListModel?.list?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let seq = payHistoryListModel?.list?[indexPath.row].reservation_cycle_seq {
            let vc = ReservationDetailViewController(nibName: "ReservationDetailViewController", bundle: nil)
            vc.seq = seq // reservation_cycle_seq
            showViewController(vc, animated: true)
        }
    }
}
