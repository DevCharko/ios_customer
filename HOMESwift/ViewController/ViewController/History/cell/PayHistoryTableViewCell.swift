//
//  PayHistoryTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 16..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class PayHistoryTableViewCell: UITableViewCell {
    
    static var height: CGFloat {
        return 71
    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setModel(model: PayHistoryModel?) {
        if let title = model?.cycle {
            self.titleLabel.text = title
        }
        if let payDate = model?.completed_at, let isComplete = model?.is_complete {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // 서버에서 오는 날짜 포맷
            let date = dateFormatter.date(from: payDate)
            dateFormatter.dateFormat = "yyyy. MM. dd. E. a HH:mm " // 뷰에 띄울 포맷

            var text = dateFormatter.string(from: date!)
            if isComplete == "1" {
                text = text + "결제 완료"
            }
            
            self.discriptionLabel.text = text
        }
        if let price = model?.price {
            self.priceLabel.text = price.stringFormatDecimal()! + "원"
        }
    }
}
