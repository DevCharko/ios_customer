//
//  NoticeWebViewController.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 7. 5..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 공지사항 연결 웹뷰컨트롤러
class NoticeWebViewController: SKPViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        setNavigationTitle(title: "공지사항")
        
        webView.delegate = self
        
        SKHUD.show()
        webView.loadRequest(URLRequest(url: URL(string: "http://api.homemaster.co.kr/web/notice/general")!))
    }

}

extension NoticeWebViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SKHUD.hide()
    }
}
