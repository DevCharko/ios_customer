//
//  NoticeTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 4..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class NoticeTableViewCell: SKTableViewCell {
    
    static var height : CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return 53 * screenHeight / 568
    }

    
    @IBOutlet weak var noticeContentLabel: UILabel!
    @IBOutlet weak var noticeDateLabel: UILabel!
    @IBOutlet weak var newImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
