//
//  InformationViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 5..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import FirebaseAnalytics

/// 개인정보취급방침,이용약관 뷰컨트롤러
class InformationViewController: SKPViewController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationTitle(title: DataStore.naviTitleName)
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        SKHUD.show()
        webView.delegate = self
        
        if DataStore.naviTitleName == "개인정보 취급방침" {
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.agree_1.rawValue)
            webView.loadRequest(URLRequest(url: URL(string: "http://api.homemaster.co.kr/asset/privacy/private_policy.html")!))
        } else {
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.agree_2.rawValue)
            webView.loadRequest(URLRequest(url: URL(string: "http://api.homemaster.co.kr/asset/privacy/terms_of_service.html")!))
        }
    }
}

extension InformationViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SKHUD.hide()
    }
}

