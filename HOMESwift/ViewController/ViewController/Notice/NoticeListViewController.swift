//
//  NoticeListViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import FirebaseAnalytics

class NoticeListViewController: SKPViewController {
    
    var selectedIndexList = [Bool]()
    var noticeList: NoticeListModel?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true

        NoticeTableViewCell.regist(tableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Analytics.setScreenName(ScreenName.Notice.rawValue, screenClass: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationTitle(title: "공지사항")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        SKHUD.show()
        NetworkManager.getNotice(is_master: 0) { (jsonData, error) in
            if error != nil {
                log.error(error!)
            } else {
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let json = JSON(init: jsonData!)
                let mapper = Mapper<NoticeListModel>()
                self.noticeList = mapper.map(JSONString: json.rawString()!)
                
                for _ in 0..<self.noticeList!.count {
                    self.selectedIndexList.append(false)
                }
                
                self.tableView.reloadData()

                SKHUD.hide()
            }
        }
    }
}

extension NoticeListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectedIndexList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // 공지 타이틀 높이
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return NoticeTableViewCell.height
    }
    
    // 공지 내용 높이
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if selectedIndexList[section] == true {
            let message = noticeList?.list?[section].contents
            
            //calc height for message string
            return message!.height(constraintedWidth: tableView.frame.width, leftMargin: 18, rightMargin: 31, topMargin: 18, bottomMargin: 18, font: .systemFont(ofSize: 12))

        } else {
            return 0.00001
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    // 공지 타이틀 뷰
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NoticeTableViewCell.identifier) as! NoticeTableViewCell
        cell.noticeContentLabel.text = noticeList?.list?[indexPath.section].title
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.date(from: (noticeList?.list?[indexPath.section].created_at)!)
        
        let today = Date()
//        log.debug("날짜 : \(noticeList?.list?[indexPath.section].updated_at)")
//        log.debug("오늘 : \(today)")
        
        if date != nil {
            formatter.dateFormat = "yyyy.MM.dd.E"
            cell.noticeDateLabel.text = formatter.string(from: date!)

            let interval = today.timeIntervalSince(date!)
//            log.debug(interval)
            
            if interval < (86400 * 7) { // 1주일 이내면 new표시
                // new 이미지
                cell.newImage.isHidden = false
            } else {
                cell.newImage.isHidden = true
            }
        }
        
        return cell
    }
    
    // 공지 내용 뷰
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.background
        view.clipsToBounds = true
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 0))
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.rgb(fromHex: 0x777777)
        label.numberOfLines = 0
        label.text = noticeList?.list?[section].contents
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        
        view.addSubview(label)

        label.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: label, attribute: .topMargin, relatedBy: .equal, toItem: view, attribute: .topMargin, multiplier: 1.0, constant: 18)
        let bottomConstraint = NSLayoutConstraint(item: label, attribute: .bottomMargin, relatedBy: .equal, toItem: view, attribute: .bottomMargin, multiplier: 1.0, constant: -18)
        let leftConstraint = NSLayoutConstraint(item: label, attribute: .leadingMargin, relatedBy: .equal, toItem: view, attribute: .leadingMargin, multiplier: 1.0, constant: 18)
        let rightConstraint = NSLayoutConstraint(item: label, attribute: .trailingMargin, relatedBy: .equal, toItem: view, attribute: .trailingMargin, multiplier: 1.0, constant: -31)
    
        topConstraint.priority = 999
        bottomConstraint.priority = 999
        
        view.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])

        // 라인
        let bottomLine = UIView()
        view.addSubview(bottomLine)
        bottomLine.backgroundColor = UIColor.line
        
        bottomLine.snp.makeConstraints({ (make) in
            make.left.equalTo(view).offset(0)
            make.right.equalTo(view).offset(0)
            make.height.equalTo(1)
            make.bottom.equalTo(view.snp.bottom).offset(0)
        })
        
        return view
    }

    // 공지사항 열기
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedIndexList[indexPath.section] = true
        tableView.reloadRows(at: [indexPath], with: .automatic)
        return indexPath
    }
    
    // 공지사항 닫기
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedIndexList[indexPath.section] = false
        tableView.reloadRows(at: [indexPath], with: .automatic)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! NoticeTableViewCell
        cell.contentView.backgroundColor = UIColor.white
    }
}
