//
//  ChangePhoneViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

/// 전화번호 변경 뷰컨트롤러
class ChangePhoneViewController: SKPViewController {
    
    var userModel = UserModel()
    var timer: Timer?
    var remainTime = 180
    var isAuthenticating = false
    var completeAuth = false {
        didSet {
            isAuthenticating = false
            if completeAuth == true {
                authLabel.text = "인증된 연락처입니다."
                authLabel.textColor = UIColor.valid
                authButton.isHidden = true
                callTextField.isEnabled = false
                nextButton.setNextButton(bool: true)
            } else {
                authLabel.text = "인증이 필요합니다."
                authButton.isHidden = false
                callTextField.isEnabled = true
                authLabel.textColor = UIColor.alert
                nextButton.setNextButton(bool: false)
            }
        }
    }
    
    @IBOutlet weak var callTextField: UITextField! // 전화번호 입력 필드
    @IBOutlet weak var nextButton: NextButton!
    @IBOutlet weak var authLabel: UILabel!
    @IBOutlet var authButton: BorderButton! // 인증 버튼
    @IBOutlet var authViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var authTextField: LimitedLengthField! // 인증번호 입력 필드
    @IBOutlet var authCheckButton: BorderButton! // 확인 버튼
    @IBOutlet var timerLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dismissKeyboard()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "연락처 변경")
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickExit))
        
        hideKeyboardWhenTappedAround()
        
        callTextField.addTarget(self, action: #selector(checkCallTextField), for: .editingChanged)
        authTextField.addTarget(self, action: #selector(checkAuthTextField), for: .editingChanged)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        timer?.invalidate()

        dismissKeyboard()
    }
    
    // 인증 버튼 클릭
    @IBAction func authButton(_ sender: Any) {
        guard !Validator.isEmpty().apply(callTextField.text) else {
            callTextField.becomeFirstResponder()
            return
        }
        
        if self.isAuthenticating == true {
            return
        }
        
        guard let phoneNumber = callTextField.text else {
            return
        }
        
        SKHUD.show()
        
        NetworkManager.requestSMS(phone_number: phoneNumber) { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                let json = JSON(jsonData!)

                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                if json["return_code"] != 0 {
                    self.isAuthenticating = true
                    self.authLabel.isHidden = false
                    
                    // 180초 = 3분
                    self.remainTime = 180
                    self.timerLabel.text = "\(self.remainTime / 60)분 \(self.remainTime % 60)초 남았습니다"
                    
                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
                    
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                        self.authViewHeightConstraint.constant = 68
                    }
                }
            }
            
            SKHUD.hide()
        }
    }
    
    // 확인 버튼 클릭
    @IBAction func clickAuthCheckButton(_ sender: Any) {
        guard let phoneNumber = callTextField.text else {
            return
        }
        guard let code = authTextField.text else {
            return
        }
        timer?.invalidate()
        dismissKeyboard()
        
        NetworkManager.confirmSMS(phone_number: phoneNumber,
                                  auth_code: code) { (jsonData, error) in
                                    if let networkError = error as? NetworkError {
                                        AlertView().alert(self, title: nil,
                                                          message: networkError.description)
                                    }
                                    else {
                                        let json = JSON(jsonData!)
                                        
                                        let return_message = JSON(jsonData!)["return_message"].string!
                                        if return_message != "" {
                                            AlertView().alert(self, title: nil, message: return_message)
                                        }
                                        
                                        self.completeAuth = json["return_code"].boolValue
                                        
                                        UIView.animate(withDuration: 0.1){
                                            self.view.layoutIfNeeded()
                                            self.authViewHeightConstraint.constant = 0
                                        }
                                    }
                                    
        }
    }

    @objc func updateTimer() {
        
        remainTime -= 1
//        log.debug(remainTime)
        
        timerLabel.text = "\(self.remainTime / 60)분 \(self.remainTime % 60)초 남았습니다"
        
        if remainTime == 0 {
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
                self.authViewHeightConstraint.constant = 0
            }
            
            completeAuth = false
            authButton.isUserInteractionEnabled = true
            authTextField.isEnabled = true
            
            timer?.invalidate()
            isAuthenticating = false
        }
    }
    
    func updateUserInfo() {
        SKHUD.show()
        
        NetworkManager.updateUser(phone_number: callTextField.text, use_event_push: nil, use_chat_push: nil, use_notice_push: nil) { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let mapper = Mapper<UserModel>()
                if let user = json["result"].rawString() {
                    DataStore.userModel = mapper.map(JSONString: user)
                }
            }
            FirebaseAnalyticsManager.shared.sendLog(ScreenName.phone_change_complete.rawValue)
            SKHUD.hide()
        }
    }

    @IBAction func clickNextButton(_ sender: Any) {
        updateUserInfo()
    }
    
    @objc func checkCallTextField() {
        if callTextField.text?.count == 0 {
            authButton.isEnabled = false
        } else {
            authButton.isEnabled = true
        }
    }
    
    @objc func checkAuthTextField() {
        if authTextField.text?.count == 0 {
            authCheckButton.isEnabled = false
        } else {
            authCheckButton.isEnabled = true
        }
    }
}

