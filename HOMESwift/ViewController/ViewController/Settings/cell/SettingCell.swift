//
//  SettingCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 27..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

enum cellType: Int {
    case name = 0
    case call
    case address
    case event
    case notice
}

protocol settingCellDelegate: class {
    func clickButton(_ cell: SettingCell)
}

class SettingCell: UITableViewCell {

    static var height: CGFloat {
        return UIView.getFlexibleFontSize(44)
    }
    
    weak var delegate: settingCellDelegate?
    
    @IBOutlet weak var textNameLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var switchButton: UIButton!
    
    @IBAction func btnClick(_ sender: Any) {
        delegate?.clickButton(self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setEntity(_ row: cellType, model: UserModel?) {
        guard model != nil else {
            return
        }
        self.selectionStyle = .none

        switch row {
        case .name:
            button.isHidden = true
            textNameLabel.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(12))
            if let name = model?.name {
                textNameLabel.text = name
                DataStore.userModel?.name = name
            } else {
                textNameLabel.text = DataStore.userModel?.name
            }
            
        case .call:
            button.isHidden = true // 임시로 제거
            button.setTitle("변경", for: .normal)
            if let phone_number = model?.phone_number {
                textNameLabel.text = phone_number.toPhoneNumber()
                DataStore.userModel?.phone_number = phone_number
            } else {
                textNameLabel.text = DataStore.userModel?.phone_number.toPhoneNumber()
            }
        case .address:
            button.isHidden = false
            button.setTitle("관리", for: .normal)
            button.expandTouchArea(offset: 30)
            
            if model?.address1 != nil && model?.address2 != nil {
                textNameLabel.text = "\(model!.address1!) \(model?.address2 ?? "")"
                DataStore.userModel?.address1 = model!.address1!
                DataStore.userModel?.address2 = model!.address2!
            } else {
                var addrText = ""
                if let addr1 = DataStore.userModel?.address1 {
                    addrText += addr1
                }
                if let addr2 = DataStore.userModel?.address2 {
                    addrText += " \(addr2)"
                }
                textNameLabel.text = addrText
            }
            
        case .event:
            button.isHidden = true
            switchButton.isHidden = false
            if let isSwitch = model?.use_event_push {
                switchButton.isSelected = isSwitch == "0" ? false : true
                DataStore.userModel?.use_event_push = isSwitch
            } else {
                switchButton.isSelected = DataStore.userModel?.use_event_push == "0" ? false : true
            }
            textNameLabel.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(12))
            textNameLabel.text = "이벤트 알림"
            
        case .notice:
            button.isHidden = true
            switchButton.isHidden = false
            if let isSwitch = model?.use_notice_push {
                switchButton.isSelected = isSwitch == "0" ? false : true
                DataStore.userModel?.use_notice_push = isSwitch
            } else {
                switchButton.isSelected = DataStore.userModel?.use_notice_push == "0" ? false : true
            }
            textNameLabel.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(12))
            textNameLabel.text = "공지사항 알림"
        }
        
    }
}
