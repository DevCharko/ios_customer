//
//  SettingFooterView.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 26..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

protocol SettingFooterViewDelegate: class {
    func clickLeave()
}

class SettingFooterView: UITableViewHeaderFooterView {
    
    weak var delegate: SettingFooterViewDelegate?
    @IBOutlet var leaveButton: UIButton!
    
    @IBAction func clickLeaveButton(_ sender: Any) {
        self.delegate?.clickLeave()
    }
    
    override func awakeFromNib() {
        self.leaveButton.expandTouchArea(offset: 30)
    }

    class func instanceFromNib() -> SettingFooterView {
        return UINib(nibName: "SettingFooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SettingFooterView
    }
}
