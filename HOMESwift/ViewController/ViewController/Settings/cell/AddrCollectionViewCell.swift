//
//  AddrCollectionViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 29..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit


/// 주소 리스트 셀
class AddrCollectionViewCell: UICollectionViewCell {
    
    static var height : CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return 126 * screenHeight / 568
    }
    
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var starTouchViewButton: UIButton!
    @IBOutlet weak var serviceAddrLabel: UILabel!
    @IBOutlet weak var deletButton: UIButton!
    @IBOutlet weak var addrLabel: UILabel!
    @IBOutlet weak var addrNameLabel: UILabel!
    @IBOutlet weak var setAddrButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerRadius = 2
        borderWidth = 1
    }
    
    func setAddress(address: Address)
    {
        if address.is_default {
            self.serviceAddrLabel.isHidden = false
            self.starButton.isSelected = true
            borderColor = UIColor.rgb(fromHex: 0xff5859)
        }
        else {
            self.serviceAddrLabel.isHidden = true
            self.starButton.isSelected = false
            borderColor = UIColor.rgb(fromHex: 0xb8bbbf)
        }
        
        // 주소명 변경 버튼 히든
        if address.title == "" {
            setAddrButton.isHidden = false
        } else {
            setAddrButton.isHidden = true
        }
        addrNameLabel.text = address.title
        
        if address.address1 != nil {
            self.addrLabel.text = "\(address.address1.string!) \(address.address2?.string! ?? "")"
        }
        
    }
}
