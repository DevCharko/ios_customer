//
//  SettingHeaderView.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 26..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class SettingHeaderView: UITableViewHeaderFooterView {

    @IBOutlet var headerTitle: UILabel!
    
    class func instanceFromNib() -> SettingHeaderView {
        return UINib(nibName: "SettingHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SettingHeaderView
    }

}
