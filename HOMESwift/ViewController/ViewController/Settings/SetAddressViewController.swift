//
//  SetAddressViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 31..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON

/// 주소 명 수정 뷰컨트롤러. PostCodeViewController와 연결.
class SetAddressViewController: SKPViewController, UITextFieldDelegate {
    
    var model: Address?

    @IBOutlet weak var addrTitleTextField: SKTextField!
    @IBOutlet weak var nextButton: NextButton!
    
    @IBAction func clickNextButton(_ sender: Any) {
        if let address = model {
            SKHUD.show()

            NetworkManager.updateAddress(address_seq: address.seq, title: address.title, address1: address.address1, address2: address.address2, block: { (jsonData, error) in
                if let networkError = error as? NetworkError {
                    AlertView().alert(self, title: nil,
                                      message: networkError.description)
                }
                else {
                    let return_message = JSON(jsonData!)["return_message"].string!
                    if return_message != "" {
                        AlertView().alert(self, title: nil, message: return_message, cancelButtonTitle: "확인", otherButtonTitles: []) { (AlertView, _ buttonIndex:Int) in
                            self.clickExit()
                        }
                    }
                    self.clickExit()
                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.address_nickname_update.rawValue)
                }
                
                SKHUD.hide()
            })
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        dismissKeyboard()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: "주소명")
        setRightBarButton(type: .Exit, action: #selector(clickExit))

        addrTitleTextField.maxLength = 8
        addrTitleTextField.delegate = self
        addrTitleTextField.inputAccessoryView = getKeyboardToolbar()
        addrTitleTextField.addTarget(self, action: #selector(checkTextField(_ :)), for: .editingChanged)
        
        addrTitleTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func checkTextField(_ sender: UITextField){
        model?.title = sender.text
        if (sender.text?.isEmpty)!{
            nextButton.setNextButton(bool: false)
        } else {
            nextButton.setNextButton(bool: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nextButton.sendActions(for: .touchUpInside)
        return true
    }
}
