//
//  AddressAddViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON


/// 주소 추가 뷰컨트롤러
class AddressAddViewController: SKPViewController, PostCodeDelegate {

//    var addressData = ["", ""]
    
    var address: Address = Address()
    
    @IBOutlet weak var addrTextField: SKTextField!
    @IBOutlet weak var addrNameTextField: SKTextField!
    @IBOutlet weak var nextButton: NextButton!

    @IBOutlet weak var address2View: UIView!
    @IBOutlet weak var addrTextField2: SKTextField!
    @IBOutlet var address2TopMargin: NSLayoutConstraint!
    @IBOutlet var address2Height: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    
    @IBAction func clickNextButton(_ sender: NextButton) {
        
        dismissKeyboard()
        
        SKHUD.show()
        // postcode webview를 통해서 리턴받는 데이터를 confirm 후 추가.
        NetworkManager.addAddress(title: address.title,
                                  address1: address.address1,
                                  address2: address.address2!,
                                  block: { (jsonData, error) in
                                    if let networkError = error as? NetworkError {
                                        AlertView().alert(self, title: nil,
                                                          message: networkError.description)
                                    }
                                    else {
                                        let return_message = JSON(jsonData!)["return_message"].string!
                                        if return_message != "" {
                                            AlertView().alert(self, title: nil, message: return_message)
                                        }
                                        self.clickExit()
                                    }
                                    
                                    SKHUD.hide()
        })
    }
    
    //MARK: - PostCodeViewController
    
    @IBAction func clickAddress(_ sender: UIButton) {
        let postcodeView = PostCodeWebViewController.init(nibName: PostCodeWebViewController.className, bundle: nil)
        postcodeView.delegate = self
        
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        self.showViewController(postcodeView, animated: false)
    }

    func didSelectAddr(_ addr: String, postcode: String) {
        address.address1 = addr
        address.zipcode = postcode
        addrTextField.text = addr //텍스트 필드에 입력
        
        // 상세주소뷰 생성
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()

            self.address2View.isHidden = false
            self.address2TopMargin.constant = 27
            self.address2Height.constant = 50
            
            self.addrTextField2.becomeFirstResponder()
        }
        
    }
    
    func didDisappear() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationTitle(title: "주소추가")
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickExit))
        
        addrTextField.delegate = self
        addrTextField.setToolbar(prev: nil, next: addrTextField2)
        addrTextField.maxLength = 100;
        addrTextField.addTarget(self, action: #selector(checkAddrTextField(_ :)), for: .editingChanged)
        
        addrTextField2.setToolbar(prev: nil, next: addrNameTextField)
        addrTextField2.maxLength = 100
        addrTextField2.addTarget(self, action: #selector(checkAddrTextField(_ :)), for: .editingChanged)
        
        addrNameTextField.delegate = self
        addrNameTextField.setToolbar(prev: addrTextField, next: nil)
        addrNameTextField.maxLength = 50;
        addrNameTextField.addTarget(self, action: #selector(checkAddrNameTextField(_ :)), for: .editingChanged)

        hideKeyboardWhenTappedAround()
        
        // 상세주소 숨김
        self.address2View.isHidden = true
        self.address2TopMargin.constant = 0
        self.address2Height.constant = 0

        setupViewResizerOnKeyboardShown()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        dismissKeyboard()
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        var height = keyboardFrame.size.height - self.nextButton.bounds.height
        if #available(iOS 11, *) {
            height = height - self.view.safeAreaInsets.bottom
        }
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.scrollViewBottom.constant = height
            self.view.layoutIfNeeded()
        })
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.scrollViewBottom.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    //TODO: 두번째 주소 입력으로 UI 변경 필요
    @objc func checkAddrTextField(_ sender: UITextField){
        address.address2 = sender.text!
        if address.address2!.contains("") {
            nextButton.setNextButton(bool: false)
        } else {
            nextButton.setNextButton(bool: true)
        }
    }
    
    @objc func checkAddrNameTextField(_ sender: UITextField){
        address.title = sender.text!
    }
}

extension AddressAddViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            addrNameTextField.becomeFirstResponder()
        } else {
            nextButton.sendActions(for: .touchUpInside)
        }
        return true
    }
}
