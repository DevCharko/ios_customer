//
//  LeaveViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ChannelIO

/// 탈퇴 피드백 뷰컨트롤러
class LeaveViewController: SKPViewController {
    
    @IBOutlet weak var nextButton: NextButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!

    let dataSource = ["서비스가 마음에 들지 않아요", "서비스 이용 지역이 아니에요", "서비스 이용 계획이 없어요", "앱이 불편해요", "기타"]
    
    @IBAction func clickNextButton(_ sender: NextButton) {
        
        //        var reason = [String]()
        //        for item in tableView.indexPathsForSelectedRows! {
        //            reason.append(dataSource[item.row])
        //        }
        let item = tableView.indexPathForSelectedRow
       
        SKHUD.show()
        NetworkManager.retract(reason: dataSource[(item?.row)!]) { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self,
                                      title: nil,
                                      message: return_message,
                                      cancelButtonTitle: "확인",
                                      otherButtonTitles: []) {
                                        (_ alert: AlertView, _ buttonIndex:Int) in
                                        
                                        FirebaseAnalyticsManager.shared.sendLog(ScreenName.cancel_complete.rawValue)

                                        // 유저 정보 삭제
                                        self.deleteUserData()
                                        
                                        if let loginVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController {
                                            self.present(loginVC, animated: true)
                                        }
                                        
//                                        let naviVC = self.presentingViewController as? UINavigationController
//                                        naviVC?.popToRootViewController(animated: false)
//                                        self.dismiss(animated: true) {
//                                            (UIApplication.shared.keyWindow?.rootViewController?.childViewControllers[0] as! HomeViewController).getUserInfo()
//                                        }
                    }
                }
            }
            
            SKHUD.hide()
        }
    }
    
    func deleteUserData() {
        DataStore.userModel = nil
        ChannelIO.shutdown()
        DataStore.isLogin = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: "탈퇴")
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickExit))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RadioCell.self)
        topViewHeight.setFlexibleSize()
    }

}

extension LeaveViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RadioCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RadioCell
        
        cell.selectionStyle = .none
        cell.leftButton.setImage(#imageLiteral(resourceName: "btn_radio_off"), for: UIControlState.normal)
        cell.leftButton.setImage(#imageLiteral(resourceName: "btn_radio_on"), for: UIControlState.selected)

        cell.leftButton.isUserInteractionEnabled = false
        
        cell.setData(leftButton:cell.leftButton, name: dataSource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        // 선택된 셀이 없으면 비활성화
        if tableView.indexPathsForSelectedRows == nil {
            nextButton.setNextButton(bool: false)
        }
        // 직접입력 텍스트 입력창
//        if indexPath.row == dataSource.count-1 {
//            let cell = tableView.cellForRow(at: indexPath) as! RadioCell
//            cell.nameLabel.text = "직접입력"
//        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        nextButton.setNextButton(bool: true)
        // 직접입력 텍스트 입력창
//        if indexPath.row == dataSource.count-1 {
//            let alert = UIAlertController(title: "탈퇴사유를 입력해 주세요", message: nil, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "취소", style: .cancel) { (UIAlertAction) in
//                tableView.deselectRow(at: indexPath, animated: true)
//                if tableView.indexPathsForSelectedRows == nil {
//                    self.nextButton.setNextButton(bool: false)
//                }
//            })
//            alert.addTextField(configurationHandler: nil)
//            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (UIAlertAction) in
//                let cell = tableView.cellForRow(at: indexPath) as! RadioCell
//                if alert.textFields?.first?.text != "" {
//                    cell.nameLabel.text = alert.textFields?.first?.text
//                } else {
//                    tableView.deselectRow(at: indexPath, animated: true)
//                    if tableView.indexPathsForSelectedRows == nil {
//                        self.nextButton.setNextButton(bool: false)
//                    }
//                }
//            }))
//            self.present(alert, animated: true)
//        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        tableViewHeightConstrant.constant = tableView.contentSize.height
    }
}
