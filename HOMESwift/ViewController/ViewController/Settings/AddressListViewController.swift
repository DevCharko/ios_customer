//
//  AddressListViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

/// 주소 리스트 뷰컨트롤러
class AddressListViewController: SKPViewController {
    
    @IBOutlet weak var addrCollectionView: UICollectionView!
    
    var selectedAddressIndex = 0
    
    var addressList: AddressList? {
        didSet{
            self.sortAndReload()
        }
    }
    
    // Mark: collectionView 사이즈 조정
    let columns = 1 // 갯수
    
    let spaceBetweenRows = 13
    let spaceHorisonMargin = 13
    
    var cellWidth: CGFloat {
        get {
            return addrCollectionView.frame.width/CGFloat(columns)-CGFloat(spaceHorisonMargin*2)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "주소 관리")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        setRightBarButton(type: .Add, action: #selector(clickAdd))
        
        addrCollectionView.delegate = self
        addrCollectionView.dataSource = self
        
        addrCollectionView.contentInset.top = 13
        addrCollectionView.contentInset.bottom = 13
        
        addrCollectionView.register(AddrCollectionViewCell.self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getAddressList()
    }

    func getAddressList() {
        SKHUD.show()
        
        NetworkManager.getAddressList() { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let json = JSON(jsonData!)
                let mapper = Mapper<AddressList>()
                self.addressList = mapper.map(JSONString: json.rawString()!)
            }
            SKHUD.hide()
        }
    }
    
    // +버튼
    @objc func clickAdd(){
        let naviVC = UINavigationController(rootViewController: AddressAddViewController(nibName: "AddressAddViewController", bundle: nil))
        
        present(naviVC, animated: true, completion: nil)
    }
    
    func sortAndReload() {
        addressList?.list?.sort(by: { (addr1, addr2) -> Bool in
            return addr1.is_default // 기본주소가 제일 위로
        })
        addrCollectionView.reloadData()
    }
}

extension AddressListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return addressList?.list != nil ? (addressList?.list?.count)! : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as AddrCollectionViewCell
        
        if let address = self.addressList?.list?[indexPath.row] {
            cell.setAddress(address: address)
        }
        
        cell.deletButton.addTarget(self, action: #selector(deleteAction(_ : event:)), for: .touchUpInside)
        cell.starButton.addTarget(self, action: #selector(markAction(_ : event:)), for: .touchUpInside)
        cell.starTouchViewButton.addTarget(self, action: #selector(markAction(_ : event:)), for: .touchUpInside)
        cell.setAddrButton.addTarget(self, action: #selector(setAddrName(_ : event:)), for: .touchUpInside)
        return cell
    }
    
    @objc func deleteAction(_ sender: UIButton, event: UIEvent) {
        let touch = event.allTouches?.first
        let point = (touch?.location(in: addrCollectionView))! as CGPoint
        let indexPath = addrCollectionView.indexPathForItem(at: point)
        
        if addressList?.list?.count == 1 {
            AlertView().alert(self, title: nil, message: "최소 1개의 주소는 남겨주셔야 합니다.")
        }
        else {
            let model = addressList?.list?[(indexPath?.row)!]
            
            SKHUD.show()
            
            NetworkManager.removeAddress(address_seq: (model?.seq)!, block: { (jsonData, error) in
                if let error = error as? NetworkError {
                    log.error(error.description)
                    AlertView().alert(self, title: nil, message: error.description)
                }
                else {
                    
                    self.addressList?.remove(at: (indexPath?.row)!)
                    
                    // 삭제 후 인덱스패스 동기화
                    if self.addressList?.list?.count != 0 {
                        self.addrCollectionView.deleteItems(at: [indexPath!]) // 딜리트 전과 후의 갯수가 달라야함
                    } else {
                        self.addrCollectionView.reloadItems(at: [indexPath!]) // 리로드 전과 후의 갯수가 같아야함 -> list가 0일때 임의로 1을 리턴해주기 때문에 동작함
                    }
                    
                    if (indexPath?.row)! < self.selectedAddressIndex {
                        self.selectedAddressIndex -= 1
                    }
                    
                    let return_message = JSON(jsonData!)["return_message"].string!
                    if return_message != "" {
                        AlertView().alert(self, title: nil, message: return_message)
                    }
                    
                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.address_item_deleted.rawValue)
                }
                
                SKHUD.hide()
            })
            
        }
    }
    
   @objc func markAction(_ sender: UIButton, event: UIEvent) {
        let touch = event.allTouches?.first
        let point = (touch?.location(in: addrCollectionView))! as CGPoint
        let indexPath = addrCollectionView.indexPathForItem(at: point)
        setDefaultAddress(indexPath: indexPath)
    }
    
    func setDefaultAddress(indexPath: IndexPath?) {
        guard indexPath != nil else {
            return
        }
        
        if let model = addressList?.list?[indexPath!.row] {
            if model.is_default == false {
                SKHUD.show()
                
                NetworkManager.markAddress(address_seq: model.seq, block: { (jsonData, error) in
                    if let networkError = error as? NetworkError {
                        AlertView().alert(self, title: nil,
                                          message: networkError.description)
                    }
                    else {
                        var indexPaths = [IndexPath]()
                        for (index , element) in (self.addressList?.list?.enumerated())! {
                            if element.is_default {
                                element.is_default = false
                                indexPaths.append(IndexPath(row: index, section: 0))
                            }
                            if model.seq == element.seq {
                                element.is_default = true
                                indexPaths.append(IndexPath(row: index, section: 0))
                            }
                        }
                        //                    self.getAddressList()
                        self.addrCollectionView .reloadItems(at: indexPaths)
                        
                        let return_message = JSON(jsonData!)["return_message"].string!
                        if return_message != "" {
                            AlertView().alert(self, title: nil, message: return_message)
                        }
                        
                        DataStore.userModel?.address1 = model.address1
                        DataStore.userModel?.address2 = model.address2
                        DataStore.userModel?.seq = "\(model.seq)"
                        
                        FirebaseAnalyticsManager.shared.sendLog(ScreenName.address_item_mark.rawValue)
                    }
                    
                    SKHUD.hide()
                })
            }
        }
    }
    
    @objc func setAddrName(_ sender: UIButton, event: UIEvent) {
        let touch = event.allTouches?.first
        let point = (touch?.location(in: addrCollectionView))! as CGPoint
        
        if let indexPath = addrCollectionView.indexPathForItem(at: point) {
            let model = addressList?.list?[indexPath.row]
            
            let vc = SetAddressViewController(nibName: "SetAddressViewController", bundle: nil)
            vc.model = model
            let naviVC = UINavigationController(rootViewController: vc)
            
            present(naviVC, animated: true, completion: nil)
        }
    }

    //cell 간격 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    //cell 사이즈를 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: AddrCollectionViewCell.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.navigationController?.previousViewController() is UserInputViewController {
            // 주소 선택
            if let address = addressList?.list?[indexPath.row] {
                DataStore.userModel?.address1 = address.address1
                DataStore.userModel?.address2 = address.address2
                DataStore.userModel?.address_seq = address.seq
                navigationController?.popViewController(animated: true)
            }
        } else {
            // 기본주소 설정
            setDefaultAddress(indexPath: indexPath)
        }
    }
}
