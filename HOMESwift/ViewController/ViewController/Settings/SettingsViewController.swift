//
//  SettingsViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

/// 설정 뷰컨트롤러
class SettingsViewController: SKPViewController {
    let cellType: [IndexPath : cellType] = [ IndexPath(row: 0, section: 0) : .name,
                                             IndexPath(row: 0, section: 1) : .call,
                                             IndexPath(row: 0, section: 2) : .address,
                                             IndexPath(row: 0, section: 3) : .event,
                                             IndexPath(row: 1, section: 3) : .notice]
    
    let headerText = ["성함", "연락처", "주소", "알림"]
//    let dataSource = ["오홈마", "010-1234-1234", "서울시 강남구 테헤란로 78길 142-12 동산빌딩 2층 1002호", "이벤트 알림"]
//    let noticeText = ["이벤트 알림", "1:1 대화 알림"]
    var userModel = UserModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SettingCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationTitle(title: "설정")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getUserInfo()
    }

    func getUserInfo() {
        SKHUD.show(touchDisable: false)
        
        NetworkManager.userInfo { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let result = json["result"]
                
                let mapper = Mapper<UserModel>()
                if result.rawString() != nil {
                    self.userModel = mapper.map(JSONString: result.rawString()!)!
                }
                self.tableView.reloadData()
            }
            SKHUD.hide(touchDisable: false)
        }
    }
    
    func updateUserInfo(isEventPush: Bool?, isNoticePush: Bool?) {
        NetworkManager.shareInstance.cancelAllRequest()
        NetworkManager.updateUser(phone_number: nil, use_event_push: isEventPush, use_chat_push: nil, use_notice_push: isNoticePush) { (jsonData, error) in
            
            if jsonData != nil {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let result = json["result"]
                
                let mapper = Mapper<UserModel>()
                self.userModel = mapper.map(JSONString: result.rawString()!)!
                
                if let eventPush = self.userModel.use_event_push {
                    DataStore.userModel?.use_event_push = eventPush
                }
                
                if let noticePush = self.userModel.use_notice_push {
                    DataStore.userModel?.use_notice_push = noticePush
                }
                self.tableView.reloadData()
            }
        }
    }
}

extension SettingsViewController: settingCellDelegate {
    
    func clickButton(_ cell: SettingCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        
        switch (indexPath.section, indexPath.row) {
            
        case (1, _): // 연락처 변경
            let naviVC = UINavigationController(rootViewController: ChangePhoneViewController(nibName: "ChangePhoneViewController", bundle: nil))
            present(naviVC, animated: true, completion: nil)
            
        case (2, _): // 주소 변경
            let vc = AddressListViewController(nibName: "AddressListViewController", bundle: nil)
            navigationController?.pushViewController(vc, animated: true)
            
        case (3, 0): // 이벤트 알림
            cell.switchButton.isSelected = !cell.switchButton.isSelected
//            log.debug("이벤트 알림")
            updateUserInfo(isEventPush: cell.switchButton.isSelected, isNoticePush: nil)
            
        case (3, 1): // 공지사항 알림
            cell.switchButton.isSelected = !cell.switchButton.isSelected
            updateUserInfo(isEventPush: nil, isNoticePush: cell.switchButton.isSelected)
            log.debug("공지사항 알림")
            
        default:
            break
        }
    }
    
}

extension SettingsViewController: SettingFooterViewDelegate {
    func clickLeave() {
        let naviVC = UINavigationController(rootViewController: LeaveViewController(nibName: "LeaveViewController", bundle: nil))
        present(naviVC, animated: true, completion: nil)
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3 { // 알림 섹션
            return 2
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 && userModel.address1 != nil { // 주소일 때
            // 두줄일 경우 계산
            // 상(17) + 하(17) + 라벨 높이 계산 ( 좌(13), 우(64) )
            let flexibleHeight = 17 + 17
                + ("\(getUnWrappingValue(userModel.address1)) \(getUnWrappingValue(userModel.address2))".textHeight(constraintedWidth: tableView.frame.width, margin: 13+64, font: UIFont.systemFont(ofSize: 12)))
            
            return flexibleHeight > SettingCell.height ? flexibleHeight : SettingCell.height
        } else {
            return SettingCell.height
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SettingCell
        
        cell.delegate = self
        if let type = self.cellType[indexPath] {
            cell.setEntity(type, model: userModel)
        }
        
        return cell
    }
    
    // MARK: 헤더푸터뷰
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UIView.getFlexibleFontSize(31)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 3 {
            return UIView.getFlexibleFontSize(50)
        } else {
            return 0.0001
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = SettingHeaderView.instanceFromNib()
        headerView.headerTitle.text = headerText[section]
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 3 {
            let footerView = SettingFooterView.instanceFromNib()
            footerView.delegate = self
            return footerView
        }
        else {
            return nil
        }
    }

}

extension UIButton {
    func underlineButton(text: String) {
        let titleString = NSMutableAttributedString(string: text)
        titleString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, text.count))
        self.setAttributedTitle(titleString, for: .normal)
    }
}
