//
//  ReservationDetailViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 17..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import SnapKit

/// 예약 상세정보 뷰컨트롤러
class ReservationDetailViewController: SKPViewController, CardListDelegate, CouponListDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    var reservationModel: ReservationModel?
    var seq: String!

    var infoDataSource = [["서비스 일시",""], ["서비스 주기","정기 예약 / 매주 여러 번 / 화, 목 / 1회차"], ["마스터",""], ["주소",""]]

    var cardDataSource = [["결제 정보",""], ["쿠폰 정보",""]]

    var status = CycleStatus.Reservation
    
    // 예약 취소하기 슬라이드
    var slideView = UIView()
    var backView = UIVisualEffectView()
    let allCancelView = UIView()
    let allCancelLabel = UILabel()
    let lineView = UIView()
    let someCancelView = UIView()
    let someCancelLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "예약 정보")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.background

        // add Cell
        tableView.register(DetailTopTableViewCell.self)
        tableView.register(ReservationDetailInfoTableViewCell.self)
        tableView.register(ReservationCardTableViewCell.self)
        tableView.register(ReservationCalcTableViewCell.self)
        tableView.register(BottomCancelTableViewCell.self)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let model = reservationModel {
            setModel(model)
        }
        else {
            getReservation(seq)
        }
    }
    
    func getReservation(_ seq: String) {
        SKHUD.show()
        log.debug(seq)
        NetworkManager.reservationInfo(reservation_seq: seq) { (jsonData, error) in
            
            log.debug(jsonData)
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
            }
            else {
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                let json = JSON(jsonData!)
                let mapper = Mapper<ReservationModel>()
                
                self.reservationModel = mapper.map(JSONString: json["result"].rawString()!)
                
                if let model = self.reservationModel {
                    self.setModel(model)
                }
                
            }
            
            SKHUD.hide()
        }
    }
    
    // viewModel로 만들어서 빼면 좋을듯?
    func setModel(_ model: ReservationModel) {
        guard model.cycle_status != nil else {
            return
        }
        
        // section 0
        self.status = model.cycle_status! // 서비스 단계
        
        //평가하기 테스트용
//        self.status = 4
        
        // section 1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // 서버에서 오는 날짜 포맷
        let reservationDate = dateFormatter.date(from: model.date!)
        
        dateFormatter.dateFormat = "yyyy.MM.dd.E." // 뷰에 띄울 포맷
        let date = dateFormatter.string(from: reservationDate!)
        
        // 시작시간 끝시간
        dateFormatter.dateFormat = "HH:mm:ss" // 서버에서 오는 날짜 포맷
        
        if model.begin_time != nil && model.end_time != nil {
            let reservationBeginTimestamp = dateFormatter.date(from: model.begin_time!)
            let reservationEndTimestamp = dateFormatter.date(from: model.end_time!)
            
            dateFormatter.dateFormat = "a h시"
            if model.begin_time?.components(separatedBy: ":")[1] != "00" { // 분 단위 인지 검사
                dateFormatter.dateFormat = "a h시 mm분"
            }
            let beginTime = dateFormatter.string(from: reservationBeginTimestamp!)
            
            dateFormatter.dateFormat = "a h시"
            if model.end_time?.components(separatedBy: ":")[1] != "00" {
                dateFormatter.dateFormat = "a h시 mm분"
            }
            let endTime = dateFormatter.string(from: reservationEndTimestamp!)
            
            infoDataSource[0][1] = "\(date) \(beginTime) ~ \(endTime)"
//            log.debug("\(infoDataSource[0][0]) \(infoDataSource[0][1])")
        } else { // 이사입주일때
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: model.date!)
            dateFormatter.dateFormat = "yyyy.MM.dd.E" // 뷰에 띄울 포맷
            infoDataSource[0][1] = dateFormatter.string(from: date!)
        }
        
        let orderType = model.cycle ?? "알 수 없음"//model.order_type == "1" ? "한번만 예약" : "정기 예약"
        
        if model.cleaning_type == "1" {
            infoDataSource[1][1] = orderType
            
            infoDataSource[2][1] = model.price?.master_type == "1" ? "일반마스터" : "안심마스터"
            infoDataSource[3][1] = "\(model.address?.address1 ?? "") \(model.address?.address2 ?? "")"
        } else { // 이사입주일때
            infoDataSource[0][0] = "서비스 일자"
            infoDataSource[1][0] = "주소"
            infoDataSource[1][1] = "\(model.address?.address1 ?? "") \(model.address?.address2 ?? "")"
            infoDataSource[2][0] = "평수"
            infoDataSource[2][1] = "\(model.cleaning_space ?? "0")평"
        }
        
        // section 2
        if let card = model.card {
            if card.number == nil {
                cardDataSource[0][1] = card.nickname
            } else {
                var cardNumbers = card.number.components(separatedBy: "********")
                cardNumbers.insert("****", at: 1)
                cardNumbers.insert("****", at: 1)
                if card.nickname != nil {
                    cardDataSource[0][1] = card.nickname + " (" + cardNumbers.joined(separator: " - ") + ")"
                } else if card.company_name != nil {
                    cardDataSource[0][1] = card.company_name + " (" + cardNumbers.joined(separator: " - ") + ")"
                }
            }
        } else {
            cardDataSource[0][1] = "카드 정보가 없습니다."
        }
        
        if let coupon = model.coupon {
            cardDataSource[1][1] = coupon.title
        } else {
            cardDataSource[1][1] = "사용된 쿠폰이 없습니다."
        }
        
        tableView.reloadData()
    }
    
    // MARK: - Bottom Slide View
    func setBottomSlideView() {
        // 블러 뷰
        let blur = UIBlurEffect(style: .dark)
        backView.effect = blur
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideSlideView))
        backView.addGestureRecognizer(tapGesture)
        self.view.addSubview(backView)
        
        // 슬라이드 뷰
        slideView.backgroundColor = UIColor.white
        self.view.addSubview(slideView)
        
        // 정기 예약 전체 취소하기
        slideView.addSubview(allCancelView)
        
        // 라디오 버튼
//        allCancelView.addSubview(radioButton)
        
        allCancelLabel.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(12))
        allCancelLabel.text = "정기 예약 전체 취소하기"
        allCancelLabel.textColor = UIColor.main
        allCancelView.addSubview(allCancelLabel)
        
        allCancelView.tag = 1
        let allCancelTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(clickCancel))
        allCancelTapGesture.minimumPressDuration = 0.001
        allCancelView.addGestureRecognizer(allCancelTapGesture)
        
        slideView.addSubview(lineView)
        lineView.backgroundColor = UIColor.line
        
        // 이 회차 예약만 취소하기
        slideView.addSubview(someCancelView)
        
//        someCancelView.addSubview(radioButton2)
        
        someCancelLabel.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(12))
        someCancelLabel.text = "이 회차 예약만 취소하기"
        someCancelLabel.textColor = UIColor.main
        someCancelView.addSubview(someCancelLabel)
        
        someCancelView.tag = 2
        let someCancelTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(clickCancel))
        someCancelTapGesture.minimumPressDuration = 0.001
        someCancelView.addGestureRecognizer(someCancelTapGesture)
        
        setBottomSlideViewOption()
    }
    
    func setBottomSlideViewOption() {
        // 블러뷰
        backView.frame = self.view.frame
        backView.alpha = 0.0
        
        slideView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 107)
        
        allCancelView.snp.makeConstraints { (make) in
            make.height.equalTo(53)
            make.top.equalTo(slideView.snp.top).offset(0)
            make.leading.equalTo(slideView.snp.leading).offset(0)
            make.trailing.equalTo(slideView.snp.trailing).offset(0)
        }
        
//        radioButton.snp.makeConstraints { (make) in
//            make.centerY.equalTo(allCancelView.snp.centerY)
//            make.leading.equalTo(allCancelView.snp.leading).offset(13)
//        }
        
        allCancelLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(allCancelView.snp.centerY)
            make.leading.equalTo(allCancelView.snp.leading).offset(13)
        }
        
        lineView.snp.makeConstraints({ (make) in
            make.height.equalTo(1)
            make.leading.equalTo(slideView.snp.leading).offset(0)
            make.trailing.equalTo(slideView.snp.trailing).offset(0)
            make.top.equalTo(allCancelView.snp.bottom).offset(0)
        })
        
        someCancelView.snp.makeConstraints { (make) in
            make.height.equalTo(53)
            make.top.equalTo(lineView.snp.bottom).offset(0)
            make.leading.equalTo(slideView.snp.leading).offset(0)
            make.trailing.equalTo(slideView.snp.trailing).offset(0)
        }
        
//        radioButton2.snp.makeConstraints { (make) in
//            make.centerY.equalTo(someCancelView.snp.centerY)
//            make.leading.equalTo(someCancelView.snp.leading).offset(13)
//        }
        
        someCancelLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(someCancelView.snp.centerY)
            make.leading.equalTo(someCancelView.snp.leading).offset(13)
        }
    }
    
    @objc func hideSlideView(animation: Bool = true) {
        // 탭할땐 애니메이션 적용, 다음화면일땐 그냥 사라지기
        let duration = animation ? 0 : 0.25
        UIView.animate(withDuration: duration) {
            self.backView.alpha = 0.0
            self.slideView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 107)
        }
    }
    
    @objc func showSlideView() {
        if reservationModel?.cleaning_type == "2" {
            showCancelPopUp()
        } else if reservationModel?.cycle == "한번만 예약" {
            cancelReservation()
        } else {
            
            // 정기예약
            if slideView.subviews.count == 0 {
                setBottomSlideView()
            }
            
            UIView.animate(withDuration: 0.25) {
                self.backView.alpha = 1.0
                self.slideView.frame = CGRect(x: 0, y: self.view.frame.height-107, width: self.view.frame.width, height: 107)
            }
        }
    }
    
    func showCancelPopUp() {
        AlertView().alert(self, title: nil, message: "이사·입주 청소 예약을 취소하시겠습니까?", cancelButtonTitle: "취소", otherButtonTitles: ["확인"]) { (AlertView, buttonIndex) in
            if buttonIndex == 0 {
                SKHUD.show()
                NetworkManager.cancelReservation(reservation_seq: (self.reservationModel?.seq!)!, reason: nil, is_all: false, block: { (jsonData, error) in
                    if let networkError = error as? NetworkError {
                        AlertView.alert(self, title: nil,
                                          message: networkError.description)
                    }
                    else {
                        let json = JSON(jsonData!)
                        let return_message = json["return_message"].string!
                        if return_message != "" {
                            AlertView.alert(self,
                                              title: nil,
                                              message: return_message,
                                              cancelButtonTitle: "확인",
                                              otherButtonTitles: []) {
                                                (_ alert: AlertView, _ buttonIndex:Int) in
                                                if let vc = self.navigationController?.previousViewController() as? ReservationPageViewController {
                                                    vc.pages[0].getReservationList() //갱신
                                                    vc.pages[1].getReservationList()
                                                    vc.pages[2].getReservationList()
                                                }
                                                self.navigationController!.popViewController(animated: true)
                            }
                        }
                    }
                    SKHUD.hide()
                })
            }
        }
    }

    @objc func clickAddCard() {
        let vc = CardListViewController(nibName: "CardListViewController", bundle: nil)
        if let seq = reservationModel?.card?.seq {
            vc.selectedCardIndex = seq
        }
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickSetCoupon() {
        if reservationModel?.coupon == nil {
            let vc = CouponListViewController(nibName: "CouponListViewController", bundle: nil)
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        } else {
            AlertView().alert(self, title: "", message: "쿠폰 등록을 취소하시겠습니까?", cancelButtonTitle: "아니오", otherButtonTitles: ["예"], buttonColor: [UIColor.alert, UIColor.rgb(fromHex: 0x999999)], block: { (AlertView, buttonIndex) in
                if buttonIndex == 0 {
                    self.updateReservation(card: nil, coupon: "-1")
                } else {
                    self.dismiss(animated: true)
                }
            })
        }
    }
    
    @objc func clickBottomCancel() {
        if let vc = UIStoryboard.init(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "bottomCancelViewController") as? BottomCancelViewController {
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.delegate = self
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" // 서버에서 오는 날짜 포맷
            let reservationDate = dateFormatter.date(from: reservationModel?.date ?? "2019-11-11")
            
            dateFormatter.dateFormat = "yyyy.M.d E" // 뷰에 띄울 포맷
            let date = dateFormatter.string(from: reservationDate!)
            
            // 시작시간 끝시간
            dateFormatter.dateFormat = "HH:mm:ss" // 서버에서 오는 날짜 포맷
            
            let reservationBeginTimestamp = dateFormatter.date(from: reservationModel?.begin_time ?? "09:00:00")
            let reservationEndTimestamp = dateFormatter.date(from: reservationModel?.end_time ?? "13:00:00")
            
            dateFormatter.dateFormat = "a h시"
            if reservationModel?.begin_time?.components(separatedBy: ":")[1] != "00" { // 분 단위 인지 검사
                dateFormatter.dateFormat = "a h시 mm분"
            }
            let beginTime = dateFormatter.string(from: reservationBeginTimestamp!)
            
            dateFormatter.dateFormat = "a h시"
            if reservationModel?.end_time?.components(separatedBy: ":")[1] != "00" {
                dateFormatter.dateFormat = "a h시 mm분"
            }
            let endTime = dateFormatter.string(from: reservationEndTimestamp!)
            
            vc.time = "\(date) \(beginTime) ~ \(endTime)"
            
            dateFormatter.dateFormat = "MM월 dd일"
            
            let stringDate = dateFormatter.string(from: reservationDate!)
            
            vc.thisTime = "\(stringDate) 하루만 취소"
            vc.nextCancel = "\(stringDate) 이후 정기서비스 취소"
            
        
            present(vc, animated: true) {
//                vc.btnThisTurnCancel.addTarget(self, action: #selector(self.clickOnceCancel), for: .touchUpInside)
//                vc.btnNextTurnAllCancel.addTarget(self, action: #selector(self.nextTurnAllCancel), for: .touchUpInside)
//                vc.btnAllCancel.addTarget(self, action: #selector(self.clickAllCancel), for: .touchUpInside)
            }
        }
    }
    
    func clickChangeMaster() {
        if let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "defaultPopUpViewController") as? DefaultPopUpViewController {
            popupVC.titles = "정말 변경하시겠습니까?"
            popupVC.desc = "마스터 변경 시 고객님 일정에 방문 가능한 새로운 마스터님을 재매칭 시켜드립니다."
            popupVC.delegate = self
            popupVC.modalPresentationStyle = .overCurrentContext
            popupVC.modalTransitionStyle = .crossDissolve
            
            present(popupVC, animated: true)
        }
    }
    
    //CardListDelegate
    func didSelectCard(card_seq:String) {
        log.debug("select card seq : \(card_seq)")
        updateReservation(card: card_seq, coupon: nil)
    }
    
    //MARK: - CouponListDelegate
    func didSelectCoupon(coupon_seq:String) {
        log.debug("select coupon seq : \(coupon_seq)")
        updateReservation(card: nil, coupon: coupon_seq)
    }
    
    func selectCoupon(coupon: CouponInfo) {
        
    }
    
    func updateReservation(card:String?, coupon:String?) {
        SKHUD.show()
        
        NetworkManager.updateReservation(self.seq, card_seq: card, coupon_seq: coupon) { (jsonData, error) in
            if error != nil {
                log.error(error!)
                AlertView().alert(self, title: nil,
                                  message: (error as? NetworkError)!.description)
            }
            else {
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let json = JSON(jsonData!)
                let mapper = Mapper<ReservationModel>()
                
                self.reservationModel = mapper.map(JSONString: json["result"].rawString()!)
                
                if let model = self.reservationModel {
                    self.setModel(model)
                }
            }
            
            SKHUD.hide()
        }
    }

    @objc func clickCancel(_ sender: UILongPressGestureRecognizer) {
        let view = sender.view
//        let radio = view?.subviews[0] as! UIImageView
        let location = sender.location(in: view)
        let touchInside = ((view?.bounds)!).contains(location)
        
        let tag = view?.tag

        switch(sender.state) {
            
        case .began:
//            radio.isHighlighted = true
            view?.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
            
        case .changed:
            if touchInside {
//                radio.isHighlighted = true
                view?.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
            } else {
//                radio.isHighlighted = false
                view?.backgroundColor = UIColor.white
            }
            
        case .ended:
            if touchInside {
                cancelReservation(tag == 1 ? true : false)
                hideSlideView(animation: true)
            }
//            radio.isHighlighted = false
            view?.backgroundColor = UIColor.white
            
        default: break // unknown tap
        }
    }
    
    func cancelReservation(_ isAll : Bool = false) {
        let vc = CancelViewController(nibName: "CancelViewController", bundle: nil)
        vc.model = self.reservationModel
        vc.isAll = false
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ReservationDetailViewController: DPopUpOkDelegate {
    func actionOk() {
        if let model = reservationModel {
            SKHUD.show()
            NetworkManager.updateReservationV2(model.seq!, master_seq: model.master?.seq) {
                (jsonData, error) in
                
                SKHUD.hide()
                
                if let networkError = error as? NetworkError {
                    AlertView().alert(self, title: nil, message: networkError.description)
                }
                else {
                    let json = JSON(jsonData!)
                    let return_code = json["return_code"].int!
                    let return_message = json["return_message"].string!
                    
                    if return_code == 1 {
                        AlertView().alert(self,
                                          title: nil,
                                          message: "마스터 변경 요청이 완료되었습니다.",
                                          cancelButtonTitle: "확인",
                                          otherButtonTitles: []) {
                                            (_ alert: AlertView, _ buttonIndex:Int) in
                                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                                            if let vc = viewControllers[viewControllers.count - 2] as? ReservationPageViewController {
                                                vc.pages[0].getReservationList() //갱신
                                                vc.pages[1].getReservationList()
                                            }
                                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
                                            
                        }
                        
                    }
                }
            }
        }
    }
}

extension ReservationDetailViewController: BottomCancelDelegate {
    func thisTurnCancel() {
        cancelReservation()
    }
    // 전체취소(이번 회차포함)
    func allCancel() {
        if let cancel = UIStoryboard(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "cancelCauseViewController") as? CancelCauseViewController {
            if let model = reservationModel {
                cancel.reservationModel = model
                cancel.isRemain = false
                self.navigationController?.pushViewController(cancel, animated: true)
            } else {
                let alertController = UIAlertController(title: "알림", message: "잘못된 접근 방식입니다. 고객센터로 문의 부탁드립니다.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "확인", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    // 전체취소(이번 회차 포함)
    func nextTurnAllCancel() {
        if let cancel = UIStoryboard(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "cancelCauseViewController") as? CancelCauseViewController {
            if let model = reservationModel {
                cancel.reservationModel = model
                cancel.isRemain = true
                self.navigationController?.pushViewController(cancel, animated: true)
            } else {
                let alertController = UIAlertController(title: "알림", message: "잘못된 접근 방식입니다. 고객센터로 문의 부탁드립니다.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "확인", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}

// MARK: - Detail Top Cell Delegate
extension ReservationDetailViewController: DetailTopTableViewCellDelegate {
    func clickChangeButton() {
        clickChangeMaster()
    }
    
    func changeState(cell: DetailTopTableViewCell) {
        cell.setModel(model: reservationModel)
    }
    
    func clickChatButton() {
        log.debug("chat")
        if let phoneNumber = reservationModel?.master?.phone_number {
            UIApplication.shared.openURL(NSURL(string: "sms:\(phoneNumber)")! as URL)
        } else {
            
        }
//        let text = "Some message"
//        NSURL(string: "sms:\(phoneNumber)&body=\(text)")
        
//        let number = "sms:+12345678901"
//        UIApplication.sharedApplication().openURL(NSURL(string: number)!)

        
        /*
        // 로그인 되어있지 않으면 return
        guard SBDMain.getCurrentUser() != nil else {
            return
        }
        
        // 기존에 채팅방이 있으면 서버에서 받은 값으로 쿼리하여 channel을 얻고, ChatViewController에 channel을 넘겨준다.
        // 처음 대화할 경우 채널을 도우미 id와 내 id로 그룹채널을 생성하고 서버에 보내기
        self.getChannel()
        */
    }
    
    func clickEvalButton() {
        log.debug("eval")
        let vc = EvaluationViewController(nibName: "EvaluationViewController", bundle: nil)
        vc.model = reservationModel
        let naviVC = UINavigationController(rootViewController: vc)
        present(naviVC, animated: true)
    }
}

// MARK: - TableView Delegate
extension ReservationDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (reservationModel == nil) {
            return 0
        }
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (reservationModel == nil) {
            return 0
        }
        
        if (section == 0) {
            return 1
        }
        else if (section == 1) {
            if reservationModel?.cleaning_type == "1" {
                return 4
            } else { //이사입주일때
                return 3
            }
        }
        else if (section == 2) {
            if reservationModel?.cleaning_type == "1" {
                return 2
            } else {
                return 0
            }
        }
        else if (section == 3) {
            if reservationModel?.cleaning_type == "1" {
                return 1
            } else {
                return 0
            }
        } else if section == 4 {
            if reservationModel?.cleaning_type == "1" {
                return 1
            } else {
                return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            if self.reservationModel?.cleaning_type == "2" {
                if reservationModel?.master?.name == nil && reservationModel?.cycle_status != .Reservation {
                    return 111
                } else {
                    return 125
                }
            }
            
            // 평가완료
            if reservationModel?.isGraded == "1" {
                return CycleStatus.GradeFinished.getCellHeight()
            }
            
            // 취소된 예약에 마스터 이름이 없을때
            if reservationModel?.cycle_status == .Cancel && reservationModel?.master?.name == nil{
                return 111
            }
            
            // 이사입주 연락처가 없을때
//            if self.reservationModel?.cleaning_type == "2" && reservationModel?.master?.phone_number == nil {
//                return self.status.getCellHeight(status: CycleStatus.Reservation)
//            }
            
            // default
            return self.status.getCellHeight()
        case 1:
            return ReservationDetailInfoTableViewCell.height
        case 2:
            return ReservationCardTableViewCell.height
        case 3:
            return ReservationCalcTableViewCell.height
        case 4:
            return BottomCancelTableViewCell.height
        default:
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        
        switch indexPath.section {
        case 0:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as DetailTopTableViewCell
            
            tmpCell.delegate = self
            tmpCell.setModel(model: self.reservationModel)
            
            cell = tmpCell
            
        case 1:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ReservationDetailInfoTableViewCell
            tmpCell.titleLabel.text = infoDataSource[indexPath.row].first!
            tmpCell.descriptionLabel.text = infoDataSource[indexPath.row].last!
            cell = tmpCell
            
        case 2:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ReservationCardTableViewCell
            
            if indexPath.row == 0 {
                tmpCell.titleLabel.text = "결제 정보"
                if  cardDataSource[0][1] == "카드 정보가 없습니다." {
                    tmpCell.registButton.setTitle("선택", for: .normal)
                } else {
                    tmpCell.registButton.setTitle("변경", for: .normal)
                }
                
                tmpCell.registButton.addTarget(self, action: #selector(clickAddCard), for: .touchUpInside) // 카드 등록
            } else {
                tmpCell.titleLabel.text = "쿠폰 정보"
                if  cardDataSource[1][1] == "사용된 쿠폰이 없습니다." {
                    tmpCell.registButton.setTitle("선택", for: .normal)
                } else {
                    tmpCell.registButton.setTitle("취소", for: .normal)
                }
                tmpCell.registButton.addTarget(self, action: #selector(clickSetCoupon), for: .touchUpInside) // 쿠폰 지정
            }
            
            // 서비스 완료일때 버튼 제거
            if self.status == .ServiceFinished || self.status == .Cancel {
                tmpCell.registButton.isHidden = true
            } else {
                tmpCell.registButton.isHidden = false
            }
            
            // 미결제일때 버튼 보이기
            if reservationModel?.is_unpaid == true && indexPath.row == 0 {
                tmpCell.registButton.isHidden = false
            }
            
            tmpCell.descriptionLabel.text = cardDataSource[indexPath.row].last
            
            cell = tmpCell
            
        case 3:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ReservationCalcTableViewCell
            if let priceModel = reservationModel?.price {
                tmpCell.priceLabel.text = "\(priceModel.price!)".stringFormatDecimal()! + " 원";
            }
            if let insurancePrice = reservationModel?.insurance_price {
                tmpCell.insurancePriceLabel.text = "+ " + "\(insurancePrice)".stringFormatDecimal()! + " 원"
            }
            if reservationModel?.join_insurance == "1" { //보험 가입되있을때
                if let couponPrice = reservationModel?.coupon_price {
                    tmpCell.couponDiscountLabel.text = "- " + "\(couponPrice)".stringFormatDecimal()! + " 원"
                }
            }
            if let totalPrice = reservationModel?.total_price {
                tmpCell.totalPriceLabel.text = "\(totalPrice)".stringFormatDecimal()! + " 원"
            }
            cell = tmpCell
            
        case 4:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as BottomCancelTableViewCell
            
            if self.status == .Reservation || self.status == .ServiceReady || self.status == .ServicePresent {
                tmpCell.lbCancel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickBottomCancel)))
                tmpCell.lbCancel.isHidden = false
                tmpCell.cancelLine.isHidden = false
            } else {
                tmpCell.lbCancel.isHidden = true
                tmpCell.cancelLine.isHidden = true
            }
            
            cell = tmpCell
            
        default:
            break
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: - header footer
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 14
        } else if section == 2 {
            if self.reservationModel?.cleaning_type == "1" {
                return 27
            }
        } else if section == 3 {
            if self.reservationModel?.cleaning_type == "1" {
                return 15
            }
        }
        return 0.0001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 14
        } else if section == 2 {
            if self.reservationModel?.cleaning_type == "1" {
                return 14
            }
        }
//        else if section == 3 {
//            return self.status.getFootViewHeight()
//        }
        return 0.0001
    }
    
    // 여백
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 14)
            view.backgroundColor = UIColor.white
            return view
        }
        
        if self.reservationModel?.cleaning_type == "2" {
            return nil
        }
        
        if section == 2 {
            let rootView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 27))
            rootView.backgroundColor = UIColor.background
            
            let topLine = UIView()
            rootView.addSubview(topLine)
            topLine.backgroundColor = UIColor.line
            
            topLine.snp.makeConstraints({ (make) in
                make.left.equalTo(rootView).offset(0)
                make.right.equalTo(rootView).offset(0)
                make.height.equalTo(1)
                make.top.equalTo(rootView.snp.top).offset(0)
            })
            
            let bottomLine = UIView()
            rootView.addSubview(bottomLine)
            bottomLine.backgroundColor = UIColor.line
            
            bottomLine.snp.makeConstraints({ (make) in
                make.left.equalTo(rootView).offset(0)
                make.right.equalTo(rootView).offset(0)
                make.height.equalTo(1)
                make.top.equalTo(topLine.snp.bottom).offset(13)
            })
            
            let view = UIView()
            rootView.addSubview(view)
            view.backgroundColor = UIColor.white
            
            view.snp.makeConstraints({ (make) in
                make.left.equalTo(rootView).offset(0)
                make.right.equalTo(rootView).offset(0)
                make.top.equalTo(bottomLine.snp.bottom).offset(0)
                make.bottom.equalTo(rootView.snp.bottom).offset(0)
            })
            return rootView
        } else {
            return nil
        }
        
//        else if section == 3 {
//            let rootView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 15))
//            rootView.backgroundColor = UIColor.background
//
//            let topLine = UIView()
//            rootView.addSubview(topLine)
//            topLine.backgroundColor = UIColor.line
//
//            topLine.snp.makeConstraints({ (make) in
//                make.left.equalTo(rootView).offset(0)
//                make.right.equalTo(rootView).offset(0)
//                make.height.equalTo(1)
//                make.top.equalTo(rootView.snp.top).offset(0)
//            })
//
//            let bottomLine = UIView()
//            rootView.addSubview(bottomLine)
//            bottomLine.backgroundColor = UIColor.line
//
//            bottomLine.snp.makeConstraints({ (make) in
//                make.left.equalTo(rootView).offset(0)
//                make.right.equalTo(rootView).offset(0)
//                make.height.equalTo(1)
//                make.bottom.equalTo(rootView.snp.bottom).offset(0)
//            })
//
//            return rootView
//        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1 || section == 2{
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 14)
            view.backgroundColor = UIColor.white
            return view
        } else {
            return nil
        }
//        else if section == 3 {
//            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: self.status.getFootViewHeight()))
//            view.backgroundColor = UIColor.background
            
            // 라인
//            let topLine = UIView()
//            view.addSubview(topLine)
//            topLine.backgroundColor = UIColor.line
//
//            topLine.snp.makeConstraints({ (make) in
//                make.left.equalTo(view).offset(0)
//                make.right.equalTo(view).offset(0)
//                make.height.equalTo(1)
//                make.top.equalTo(view.snp.top).offset(0)
//            })
            
            // 라벨
//            let label = UILabel()
//            label.text = "・ 서비스가 끝나는 시간에 해당 서비스의 결제가 자동으로 진행됩니다."
//            label.font = UIFont(name: label.font.fontName, size: UIView.getFlexibleFontSize(10))
//            label.textColor = UIColor.main
//            label.textAlignment = .center
//            view.addSubview(label)
//
//            let views = ["label": label]
//
//            label.translatesAutoresizingMaskIntoConstraints = false
//            let vertConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|-13-[label(10)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//            let horzConstraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|-18-[label]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//
//            view.addConstraints(vertConstraint)
//            view.addConstraints(horzConstraint)
            
            // 버튼
//            if self.status == .Reservation || self.status == .ServiceReady || self.status == .ServicePresent {
//                let button = UIButton()
//                view.addSubview(button)
//
//                button.setTitleColor(UIColor.main, for: .normal)
//                button.setAttributedTitle(NSAttributedString(), for: .normal)
//
//                let attrs = [
//                    NSAttributedStringKey.font : UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(11)),
//                    NSAttributedStringKey.foregroundColor : UIColor.main,
//                    NSAttributedStringKey.underlineStyle : 1] as [NSAttributedStringKey : Any]
//
//                let attributedString = NSMutableAttributedString(string:"")
//
//                let buttonTitleStr = NSMutableAttributedString(string:"예약 취소하기", attributes:attrs)
//                attributedString.append(buttonTitleStr)
//                button.setAttributedTitle(attributedString, for: .normal)
//
//                // 정기예약일 경우 액션시트 띄움
//                button.addTarget(self, action: #selector(showSlideView), for: .touchUpInside)
//
//                if self.reservationModel?.cleaning_type == "1" {
//                    button.snp.makeConstraints({ (make) in
//                        make.centerX.equalTo(view.snp.centerX)
//                        make.top.equalTo(label.snp.bottom).offset(27)
//                        make.height.equalTo(31)
//                    })
//                } else {
//                    button.snp.makeConstraints({ (make) in
//                        make.centerX.equalTo(view.snp.centerX)
//                        make.top.equalTo(view.snp.top).offset(13)
//                        make.height.equalTo(31)
//                    })
//                }
//                button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
//
//            }
            
            // 이사입주일때 라벨 숨김
//            if self.reservationModel?.cleaning_type == "2" {
//                label.isHidden = true
//            }
//            return view
//        }
    }
}
