//
//  CancelViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 4..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON

/// 예약 취소뷰컨트롤러
class CancelViewController: SKPViewController {
    @IBOutlet weak var naviHeight: NSLayoutConstraint!
    var category = 0 // 0: 한번만예약, 1: 정기예약, 2: 정기예약 이 회차만 취소
    
    let onceDataSource = ["지금은 서비스가 필요하지 않아요", "집에 없어서 문을 열어 드릴 수가 없어요", "시간이 마음에 들지 않아요", "서비스를 잊고 있었네요", "원하는 마스터가 아니에요", "직접입력"]
    let periodicalDataSource = ["가격이 너무 비싸요", "마스터님이 불친절해요", "이사 갑니다", "마스터님이 시간 약속을 지키지 않아요", "서비스 이용 과정이 불편해요", "직접입력"]
    var inputData = "" // 직접입력 데이터
    
    var dataSource: [String]?
    
    var model:ReservationModel!
    var isAll = false
    
    @IBOutlet weak var cancleInfoLabel: UILabel! // 예약종류에 따라 수정
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nextButton: NextButton!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    
    @IBAction func clickCancelGuide(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "CancelGuideViewController") as! CancelGuideViewController
        present(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if model.cleaning_type == "2" {
            self.cancleInfoLabel.text = "신청하신 '이사 · 입주 예약' 내역이 삭제됩니다."
        } else if model.order_type == .Once {
            self.cancleInfoLabel.text = "신청하신 '한번만 예약' 내역이 삭제됩니다."
        } else if isAll {
            self.cancleInfoLabel.text = "신청하신 '정기 예약' 내역이 삭제됩니다."
        } else {
            self.cancleInfoLabel.text = "신청하신 '정기 예약' 이번 회차 내역만 삭제됩니다."
        }

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = false
        tableView.register(RadioCell.self)

        dataSource = category == 0 ? onceDataSource : periodicalDataSource
        
        setNavigationTitle(title: "취소")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        topViewHeight.setFlexibleSize()
        
        if DeviceInfo.modelName == "iPhone X" {
            naviHeight.constant = 77.0
        }
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        tableViewHeight.constant = tableView.contentSize.height
    }
    
    func isCancelCommission() -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // 서버에서 오는 날짜 포맷
        let reservationDate = dateFormatter.date(from: (model?.date ?? "2019-01-01"))
        let interval = reservationDate?.timeIntervalSince(Date())
        
        if Float(interval! / (60 * 60 * 24)) < 1.0 {
            return true
        } else {
            return false
        }
    }
    
    @IBAction func clickCancelReservation(_ sender: Any) {
        
        if isCancelCommission() {
            if let popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cancelNoticeViewController") as? CancelNoticeViewController {
                popupVC.modalPresentationStyle = .overCurrentContext
                popupVC.modalTransitionStyle = .crossDissolve
                
                present(popupVC, animated: true) {
                    popupVC.btnReservationCancel.addTarget(self, action: #selector(self.cencel), for: .touchUpInside)
                    
                }
                
            }
        } else {
            actionCencel()
        }
    }
    
    @objc func cencel() {
        actionCencel()
    }
    
    func actionCencel() {
        if let indexpath = self.tableView.indexPathForSelectedRow {
            
            var reason: String?
            if indexpath.row == onceDataSource.count-1 { // 직접입력일 경우에
                reason = inputData
            } else {
                reason = onceDataSource[indexpath.row]
            }
            SKHUD.show()
            NetworkManager.cancelReservation(reservation_seq: model.seq!, reason: reason, is_all: isAll, block: { (jsonData, error) in
                if let networkError = error as? NetworkError {
                    AlertView().alert(self, title: nil,
                                      message: networkError.description)
                }
                else {
                    let json = JSON(jsonData!)
                    let return_message = json["return_message"].string!
                    if return_message != "" {
                        AlertView().alert(self,
                                          title: nil,
                                          message: return_message,
                                          cancelButtonTitle: "확인",
                                          otherButtonTitles: []) {
                                            (_ alert: AlertView, _ buttonIndex:Int) in
                                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                                            if let vc = viewControllers[viewControllers.count - 3] as? ReservationPageViewController {
                                                vc.pages[0].getReservationList() //갱신
                                                vc.pages[1].getReservationList()
                                                vc.pages[2].getReservationList()
                                            }
                                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    }
                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.reservation_cancel_completed.rawValue)
                }
                SKHUD.hide()
            })
            
        }
    }
}

extension CancelViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return onceDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RadioCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RadioCell
        
        cell.selectionStyle = .none
        cell.leftButton.setImage(UIImage(named: "btn_radio_off"), for: UIControlState.normal)
        cell.leftButton.setImage(UIImage(named: "btn_radio_on"), for: UIControlState.selected)
        cell.leftButton.isUserInteractionEnabled = false
        
        cell.setData(leftButton:cell.leftButton, name: (dataSource?[indexPath.row])!)
        
        if indexPath.row == (dataSource?.count)!-1 {
            cell.lineView.isHidden = false
        } else {
            cell.lineView.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        nextButton.setNextButton(bool: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // 직접입력 텍스트 입력창
        
        if indexPath.row == (dataSource?.count)!-1 {
            let alert = UIAlertController(title: "취소사유를 입력해 주세요", message: nil, preferredStyle: .alert)
            let cell = tableView.cellForRow(at: indexPath) as! RadioCell

            alert.addAction(UIAlertAction(title: "취소", style: .cancel) { (UIAlertAction) in
                if self.inputData == "" {
                    cell.nameLabel.text = "직접입력"
                    cell.lineView.isHidden = false
                    tableView.deselectRow(at: indexPath, animated: true)
                    self.nextButton.setNextButton(bool: false)
                }
            })
            alert.addTextField(configurationHandler: nil)
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (UIAlertAction) in
                if alert.textFields?.first?.text != "" {
                    self.inputData = (alert.textFields?.first?.text)!
                    cell.nameLabel.text = alert.textFields?.first?.text
                    cell.lineView.isHidden = true
                } else {
                    self.inputData = ""
                    cell.nameLabel.text = "직접입력"
                    cell.lineView.isHidden = false
                    tableView.deselectRow(at: indexPath, animated: true)
                    if tableView.indexPathsForSelectedRows == nil {
                        self.nextButton.setNextButton(bool: false)
                    }
                }
            }))
            self.present(alert, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        // 직접입력 텍스트 입력창
        if indexPath.row == (dataSource?.count)!-1 {
            let cell = tableView.cellForRow(at: indexPath) as! RadioCell
            self.inputData = ""
            cell.nameLabel.text = "직접입력"
            cell.lineView.isHidden = false
        }
    }
}
