//
//  ServiceStatusView.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 28..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

@IBDesignable
class ServiceStatusView: UIView {
    
    @IBOutlet var numberLabels: [UILabel]?
    @IBOutlet var statusLabels: [UILabel]?
    
    @IBOutlet weak var line1to2View: UIView?
    @IBOutlet weak var line2to3View: UIView?
    
    var status: Int = 0 {
        
        didSet {
            setNeedsDisplay()
            
            // 라인 설정
            if self.status == 0 || self.status == 1 {
                line1to2View?.backgroundColor = UIColor.silver
                line2to3View?.backgroundColor = UIColor.silver
            }
            else if self.status == 2 {
                line1to2View?.backgroundColor = UIColor.main
                line2to3View?.backgroundColor = UIColor.silver
            }
            else if self.status == 3 {
                line1to2View?.backgroundColor = UIColor.main
                line2to3View?.backgroundColor = UIColor.main
            }
            
            // 라벨 설정
            for i in 0...2 {
                numberLabels?[i].textColor = UIColor.silver
                numberLabels?[i].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.silver)
                statusLabels?[i].textColor = UIColor.silver
            }
            
            if status > 0 {
                numberLabels?[status-1].textColor = UIColor.main
                numberLabels?[status-1].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.main)
                statusLabels?[status-1].textColor = UIColor.main
            }
        }
    }
    
    func instanceFromNib() -> ServiceStatusView {
        return UINib(nibName: "ServiceStatusView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ServiceStatusView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
