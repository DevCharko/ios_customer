//
//  PagingMenuViewController.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 7. 25..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import PagingMenuController

typealias ReservationPage = (vc: ReservationListViewController, page: Int) // vc, page
enum Direction {
    case left
    case right
}

class PagingMenuViewController: SKPViewController {
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    
    var selectedDate = Date() // 현재 선택된 날짜
    
    var options: PagingMenuOption?
    
    var currentVC: ReservationPage?
    var prevVC: ReservationPage?
    var nextVC: ReservationPage?
    
    var pagingMenuController: PagingMenuController?

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: "예약 내역")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        leftButton.isExclusiveTouch = true
        rightButton.isExclusiveTouch = true
        
        setCalendarLabel()
        setPageController()
    }

    @IBAction func clickLeftButton(_ sender: Any) {
        if let vc = prevVC {
            pagingMenuController?.move(toPage: vc.page, animated: true)
        }
    }
    
    @IBAction func clickRightButton(_ sender: Any) {
        if let vc = nextVC {
            pagingMenuController?.move(toPage: vc.page, animated: true)
        }
    }
    
    func setCalendarLabel() {
        yearLabel.text = selectedDate.getYearString() + "년"
        monthLabel.text = selectedDate.getSingleMonthString() + "월"
    }
    
    func setPageController() {
        let prevDate = Calendar.current.date(byAdding: .month, value: -1, to: selectedDate)
        let nextDate = Calendar.current.date(byAdding: .month, value: 1, to: selectedDate)
        
        options = PagingMenuOption(selectedDate, prevDate!, nextDate!)
        
        prevVC = (options?.reservationVC1, 0) as? ReservationPage
        currentVC = (options?.reservationVC2, 1) as? ReservationPage
        nextVC = (options?.reservationVC3, 2) as? ReservationPage
        
        pagingMenuController = self.childViewControllers.first as? PagingMenuController
        pagingMenuController?.setup(options!)
        pagingMenuController?.onMove = { state in
            switch state {
            case let .willMoveController(menuController, previousMenuController):
                if let loadDate = ((previousMenuController as? ReservationListViewController)?.reservationDate),
                    let currentDate = ((menuController as? ReservationListViewController)?.reservationDate) {
                    if currentDate.compare(loadDate) == .orderedAscending {
                        self.changeTopDateLabel(.left)
                        self.changeVC(.left) // reload nextVC
                    } else {
                        self.changeTopDateLabel(.right)
                        self.changeVC(.right) // reload prevVC
                    }
                }
            case let .didMoveController(menuController, previousMenuController):
                log.debug(previousMenuController)
                log.debug(menuController)
            case let .willMoveItem(menuItemView, previousMenuItemView):
                log.debug(previousMenuItemView)
                log.debug(menuItemView)
            case let .didMoveItem(menuItemView, previousMenuItemView):
                log.debug(previousMenuItemView)
                log.debug(menuItemView)
            case .didScrollStart:
                log.debug("Scroll start")
            case .didScrollEnd:
                log.debug("Scroll end")
            }
        }
    }
    
    func changeTopDateLabel(_ direction: Direction) {
        var changedDate: Date?
        
        if direction == .left { // < 버튼
            changedDate = Calendar.current.date(byAdding: .month, value: -1, to: selectedDate)
        } else { // > 버튼
            changedDate = Calendar.current.date(byAdding: .month, value: 1, to: selectedDate)
        }
        
        guard let date = changedDate else {
            return
        }
        
        selectedDate = date
        
        yearLabel.text = date.getYearString() + "년"
        monthLabel.text = date.getSingleMonthString() + "월"
    }
    
    func changeVC(_ direction: Direction) {
        if direction == .left { // left
            let tmpVC = nextVC
            nextVC = currentVC
            currentVC = prevVC
            prevVC = tmpVC
            prevVC?.vc.reservationList = nil
            prevVC?.vc.reservationDate = Calendar.current.date(byAdding: .month, value: -1, to: selectedDate)
            prevVC?.vc.getReservationList()
        } else {
            let tmpVC = prevVC
            prevVC = currentVC
            currentVC = nextVC
            nextVC = tmpVC
            nextVC?.vc.reservationList = nil
            nextVC?.vc.reservationDate = Calendar.current.date(byAdding: .month, value: 1, to: selectedDate)
            nextVC?.vc.getReservationList()
        }
    }
}

struct MenuItem: MenuItemViewCustomizable {var displayMode: MenuItemDisplayMode {return .custom(view: UIView())}}

struct PagingMenuOption: PagingMenuControllerCustomizable {
    let reservationVC1 = ReservationListViewController()
    let reservationVC2 = ReservationListViewController()
    let reservationVC3 = ReservationListViewController()
    
    init(_ currentDate: Date, _ prevDate: Date, _ nextDate: Date) {
        reservationVC1.reservationDate = prevDate
        reservationVC2.reservationDate = currentDate
        reservationVC3.reservationDate = nextDate
    }
    
    var componentType: ComponentType {
        return .all(menuOptions: MenuOptions(), pagingControllers: [reservationVC1, reservationVC2, reservationVC3])
    }
    var lazyLoadingPage: LazyLoadingPage {
        return .three
    }
    var defaultPage: Int {
        return 1
    }
    
    // infinite옵션 때문에 빈 메뉴를 넣어줌
    struct MenuOptions: MenuViewCustomizable {
        var displayMode: MenuDisplayMode {
            return .infinite(widthMode: .fixed(width: 80), scrollingMode: .scrollEnabled)
        }
        var itemsOptions: [MenuItemViewCustomizable] {
            return [MenuItem(), MenuItem(), MenuItem()]
        }
        var height: CGFloat {
            return 0
        }
        var defaultPage: Int {
            return 1
        }
    }
}
