//
//  DetailTopTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 3..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

protocol DetailTopTableViewCellDelegate: class {
    func changeState(cell: DetailTopTableViewCell)
    func clickChangeButton()
    func clickChatButton()
    func clickEvalButton()
}

class DetailTopTableViewCell: UITableViewCell {

    weak var delegate: DetailTopTableViewCellDelegate?

    static var height : CGFloat {
        return 185
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImageView.setRound()
        setGestureRecognizer()
        evaluationButton.backgroundColor = UIColor.alert
    }
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var changeButton: UIView!
    @IBOutlet var chatButton: UIView!
    @IBOutlet var evaluationButton: UIView!
    @IBOutlet weak var evalBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet var subLabel: UILabel! // 평가는 서비스 완료후...
    
    @IBOutlet weak var serviceStatusView: UIView!
    
    @IBOutlet var evalAlertLabelHeight: NSLayoutConstraint! //평가는 서비스 완료후... 뷰 높이
//    @IBOutlet var buttonHeight: NSLayoutConstraint!
//    @IBOutlet weak var changeButtonWidth: NSLayoutConstraint!
//    @IBOutlet var chatButtonWidth: NSLayoutConstraint!
//    @IBOutlet var evaluationButtonWidth: NSLayoutConstraint!
    
    @IBOutlet var defaultConstraint: NSLayoutConstraint!
    @IBOutlet var serviceingConstraint: NSLayoutConstraint!
    @IBOutlet var serviceEndConstraint: NSLayoutConstraint!
    
    // 상태 번호 뷰
    @IBOutlet var numberLabels: [UILabel]?
    @IBOutlet var statusLabels: [UILabel]?
    
    @IBOutlet weak var line1to2View: UIView?
    @IBOutlet weak var line2to3View: UIView?
    
    func setModel(model: ReservationModel?) {
        guard model != nil || model?.cycle_status != nil else {
            return
        }
        
        // 프로필 이미지
        if let profileImageURL = model!.master?.profile_image_url {
            if model?.cleaning_type == "1"{
                self.profileImageView.kf.setImage(with: URL.init(string: profileImageURL), placeholder: UIImage(named: "img_b_profile_default"), options: nil, progressBlock: nil, completionHandler: nil)
            } else {
                self.profileImageView.kf.setImage(with: URL.init(string: profileImageURL), placeholder: UIImage(named: "img_b_profile_movin"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
        } else {
            if model?.cleaning_type == "1"{
                self.profileImageView.image = UIImage(named: "img_b_profile_default")
            } else {
                self.profileImageView.image = UIImage(named: "img_b_profile_movin")
            }
        }
        
        // 마스터 이름
        if let masterName = model!.master?.name {
            nameLabel.attributedText = String.makeAttributedString(boldString: masterName,
                                                                   boldSize: UIView.getFlexibleFontSize(11),
                                                                   boldColor: .main,
                                                                   systemString: " 마스터",
                                                                   systemSize: UIView.getFlexibleFontSize(11),
                                                                   systemColor: .main)
        } else {
            if model?.cleaning_type == "1"{
                nameLabel.text = "마스터를 찾고 있습니다..."
            } else {
                nameLabel.text = "이사업체를 찾고 있습니다..."
            }
        }
        
        // 상태 진행 표시
        switch model!.cycle_status! {
        case .Reservation:
            evalAlertLabelHeight.constant = 0
            
            if let _ = model!.master?.name {
                changeButton.isHidden = false
                chatButton.isHidden = false
                stackViewHeight.constant = 30
            } else {
                changeButton.isHidden = true
                chatButton.isHidden = true
                stackViewHeight.constant = 0
            }
        
            evalBtnWidthConstraint.constant = 0
            
            serviceingConstraint.isActive = false
            serviceEndConstraint.isActive = false
            defaultConstraint.isActive = true
            
            line1to2View?.backgroundColor = UIColor.silver
            line2to3View?.backgroundColor = UIColor.silver
            
//            for i in 0...2{
//                numberLabels?[i].textColor = UIColor.silver
//                numberLabels?[i].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.silver)
//                statusLabels?[i].textColor = UIColor.silver
//            }
            for i in 1...2{
                numberLabels?[i].textColor = UIColor.silver
                numberLabels?[i].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.silver)
                statusLabels?[i].textColor = UIColor.silver
            }
            numberLabels?[0].textColor = UIColor.main
            numberLabels?[0].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.main)
            statusLabels?[0].textColor = UIColor.main
            statusLabels?[0].font = UIFont.boldSystemFont(ofSize: 10)
            
        case .ServiceReady:
            evalAlertLabelHeight.constant = 0
//            buttonHeight.constant = 38
            
            if let _ = model!.master?.name {
                changeButton.isHidden = false
                chatButton.isHidden = false
                stackViewHeight.constant = 30
            } else {
                changeButton.isHidden = true
                chatButton.isHidden = true
                stackViewHeight.constant = 0
            }
            
            evalBtnWidthConstraint.constant = 0
            
            serviceingConstraint.isActive = true
            serviceEndConstraint.isActive = false
            defaultConstraint.isActive = false
            
            line1to2View?.backgroundColor = UIColor.silver
            line2to3View?.backgroundColor = UIColor.silver
            
            for i in 1...2{
                numberLabels?[i].textColor = UIColor.silver
                numberLabels?[i].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.silver)
                statusLabels?[i].textColor = UIColor.silver
            }
            numberLabels?[0].textColor = UIColor.main
            numberLabels?[0].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.main)
            statusLabels?[0].textColor = UIColor.main
            statusLabels?[0].font = UIFont.boldSystemFont(ofSize: 10)

        case .ServicePresent:
            evalAlertLabelHeight.constant = 0
//            buttonHeight.constant = 38
//            evaluationButtonWidth.constant = 0
//            buttonStackView.spacing = 0
            
            stackViewHeight.constant = 30
            changeButton.isHidden = true
            chatButton.isHidden = false
            evalBtnWidthConstraint.constant = 0
//            evaluationButton.isHidden = true
            
            serviceingConstraint.isActive = true
            serviceEndConstraint.isActive = false
            defaultConstraint.isActive = false
            
            serviceingConstraint.isActive = true
            serviceEndConstraint.isActive = false
            defaultConstraint.isActive = false
            
            line1to2View?.backgroundColor = UIColor.main
            line2to3View?.backgroundColor = UIColor.silver
            
            numberLabels?[2].textColor = UIColor.silver
            numberLabels?[2].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.silver)
            statusLabels?[2].textColor = UIColor.silver
            
            for i in 0...1{
                numberLabels?[i].textColor = UIColor.main
                numberLabels?[i].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.main)
                statusLabels?[i].textColor = UIColor.main
            }
            statusLabels?[1].font = UIFont.boldSystemFont(ofSize: 10)
            
        case .ServiceFinished, .Cancel:
            evalAlertLabelHeight.constant = 0
//            buttonHeight.constant = 38
            
            changeButton.isHidden = true
            chatButton.isHidden = true
            stackViewHeight.constant = 0
            evalBtnWidthConstraint.constant = 80
            
            serviceingConstraint.isActive = true
            serviceEndConstraint.isActive = false
            defaultConstraint.isActive = false

            line1to2View?.backgroundColor = UIColor.main
            line2to3View?.backgroundColor = UIColor.main
            
            for i in 0...2{
                numberLabels?[i].textColor = UIColor.main
                numberLabels?[i].drawBorder(lineWidth: 1, cornerRadius: 27/2, color: UIColor.main)
                statusLabels?[i].textColor = UIColor.main
            }
            statusLabels?[2].font = UIFont.boldSystemFont(ofSize: 10)

        default:
            stackViewHeight.constant = 0
            evalAlertLabelHeight.constant = 15
//            buttonHeight.constant = 38
            evalBtnWidthConstraint.constant = 80
            serviceingConstraint.isActive = false
            serviceEndConstraint.isActive = true
            defaultConstraint.isActive = false

        }
        
        // 이사일때 연락처 없으면 대화하기 버튼 제거
        // **  현재 사용중 아님
//        if model?.cleaning_type == "2" {
//            if model!.master?.phone_number == nil {
//                chatButtonWidth.constant = 0
//                buttonStackView.spacing = 0
//                if model?.cycle_status != CycleStatus.ServiceFinished {
////                    buttonHeight.constant = 0
//                } else {
////                    buttonHeight.constant = 38
//                }
//            } else {
////                buttonStackView.spacing = 5
////                chatButtonWidth.constant = 80
////                changeButtonWidth.constant = 90
//            }
//        }
        
        // 평가 끝난 후
        if model!.isGraded! == "1" || model!.cycle_status! == .Cancel {
//            evaluationButtonWidth.constant = 0
//            chatButtonWidth.constant = 0
//            changeButtonWidth.constant = 0
            stackViewHeight.constant = 0
            serviceStatusView.isHidden = true
            changeButton.isHidden = true
            chatButton.isHidden = true
            evaluationButton.isHidden = true
            subLabel.isHidden = true
        }
        else {
            stackViewHeight.constant = 30
            serviceStatusView.isHidden = false
            changeButton.isHidden = false
            chatButton.isHidden = false
            evaluationButton.isHidden = false
            subLabel.isHidden = false
        }
        
        //평가가 안되었을 때 체크
        if let grade = model?.isGraded, grade == "0" {
            //평가가 안되었을 때 2일이 지나면 버튼 감추기
            let df = DateFormatter()
            df.locale = Locale(identifier: "ko_KR")

            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            if let dateString = model?.date, let timeString = model?.begin_time {
                let fullDate = "\(dateString) \(timeString)"
                log.debug(fullDate)

                if let date = df.date(from: fullDate), let today = df.date(from: df.string(from: Date())) {
                    if date.timeIntervalSince(today) < -60*60*24*2 {
                        log.debug("\(today)")
                        log.debug("\(date) : 평가 가능 시점 지남, 평가버튼을 감춥니다...")
                        
                        changeButton.isHidden = true
                        chatButton.isHidden = true
                        evaluationButton.isHidden = true
                        stackViewHeight.constant = 0
                    }
                    else {
                        log.debug("\(today)")
                        log.debug("\(date) : 평가 가능")
                    }
                }
            }
        }
        
        if model?.cycle_status! == .Cancel && model?.master == nil {
            self.nameLabel.isHidden = true
        } else {
            self.nameLabel.isHidden = false
        }
    }
    
    func setGestureRecognizer() {
        let changeGesture = UILongPressGestureRecognizer(target: self, action: #selector(changeMasterGesture))
        changeGesture.minimumPressDuration = 0.01
        changeButton.addGestureRecognizer(changeGesture)
        
        let chatGesture = UILongPressGestureRecognizer(target: self, action: #selector(chatTapGesture))
        chatGesture.minimumPressDuration = 0.01
        chatButton.addGestureRecognizer(chatGesture)
        
        let evalGesture = UILongPressGestureRecognizer(target: self, action: #selector(evalTapGesture))
        evalGesture.minimumPressDuration = 0.01
        evaluationButton.addGestureRecognizer(evalGesture)
    }
    
    @objc func changeMasterGesture(_ sender: UILongPressGestureRecognizer) {
        let view = sender.view
        let location = sender.location(in: view)
        let touchInside = ((view?.bounds)!).contains(location)
        
        switch(sender.state) {
            
        case .began:
            //            iconImageView.image = UIImage(named: "")
            changeButton.backgroundColor = UIColor.rgb(fromHex: 0x82868f)
            
        case .changed:
            if touchInside {
                changeButton.backgroundColor = UIColor.rgb(fromHex: 0x82868f)
            } else {
                changeButton.backgroundColor = UIColor.rgb(fromHex: 0x4c525e)
            }
            
        case .ended:
            if touchInside {
                self.delegate?.clickChangeButton()
            }
            changeButton.backgroundColor = UIColor.rgb(fromHex: 0x4c525e)
            
        default: break // unknown tap
        }
    }
    
    @objc func chatTapGesture(_ sender: UILongPressGestureRecognizer) {
        let view = sender.view
        let location = sender.location(in: view)
        let touchInside = ((view?.bounds)!).contains(location)
        
        switch(sender.state) {
            
        case .began:
            //            iconImageView.image = UIImage(named: "")
            chatButton.backgroundColor = UIColor.rgb(fromHex: 0x5ad0e2)
        
        case .changed:
            if touchInside {
                chatButton.backgroundColor = UIColor.rgb(fromHex: 0x5ad0e2)
            } else {
                chatButton.backgroundColor = UIColor.rgb(fromHex: 0x11bed8)
            }
            
        case .ended:
            if touchInside {
                self.delegate?.clickChatButton()
            }
            chatButton.backgroundColor = UIColor.rgb(fromHex: 0x11bed8)

        default: break // unknown tap
        }
        
        
    }
    
    @objc func evalTapGesture(_ sender: UILongPressGestureRecognizer) {
        let view = sender.view
        let location = sender.location(in: view)
        let touchInside = ((view?.bounds)!).contains(location)

        switch(sender.state) {
            
        case .began:
            //            iconImageView.image = UIImage(named: "")
            evaluationButton.backgroundColor = UIColor.rgb(fromHex: 0xff8586)
            
        case .changed:
            if touchInside {
                evaluationButton.backgroundColor = UIColor.rgb(fromHex: 0xff8586)
            } else {
                evaluationButton.backgroundColor = UIColor.alert
            }
            
        case .ended:
            if touchInside {
                self.delegate?.clickEvalButton()
            }
            evaluationButton.backgroundColor = UIColor.alert
   
        default: break // unknown tap
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

}
