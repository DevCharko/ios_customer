//
//  BottomCancelTableViewCell.swift
//  HOMESwift
//
//  Created by HM_Charko on 28/02/2019.
//  Copyright © 2019 skoopmedia. All rights reserved.
//

import UIKit

class BottomCancelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbCancel: UILabel!
    @IBOutlet weak var cancelLine: UIView!
    
    static var height: CGFloat {
        return 170
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
