//
//  ReservationCollectionViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 3..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

enum ReservationType {
    case ONCE
    case PERIODICAL
}
//0: 취소, 1: 예약 (매니저 할당되지 않은 상태), 2: 서비스 준비 (매니저 할당된 상태), 3: 서비스 중, 4: 서비스 완료)

class ReservationCollectionViewCell: UICollectionViewCell {
    
    static var height : CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return 139 * screenHeight / 568
    }
    
    @IBOutlet weak var periodicalView: UIView! // 정기예약
    @IBOutlet weak var periodicalImageView: UIImageView!
    
    @IBOutlet weak var onceView: UIView! // 한번만예약
    @IBOutlet weak var onceImageView: UIImageView!
    
    @IBOutlet weak var movinView: UIView! // 이사입주
    @IBOutlet weak var movinImageView: UIImageView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var serviceStatusLabel: UILabel!
    @IBOutlet weak var cycleCountLabel: UILabel!
    
    var isFinished = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        drawBorder(lineWidth: 1, cornerRadius: 2, color: UIColor.placeholder)
        
        profileImageView.setRound()
        
        // 라운드처리 아이콘 + 왼쪽여백
        onceView.layer.cornerRadius = onceView.bounds.height/2.0
        onceView.clipsToBounds = true
        
        periodicalView.layer.cornerRadius = periodicalView.bounds.height/2.0
        periodicalView.clipsToBounds = true
        
        movinView.layer.cornerRadius = onceView.bounds.height/2.0
        movinView.clipsToBounds = true
    }
    
    func setModel(reservation:ReservationModel) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // 서버에서 오는 날짜 포맷
        
        //마스터 정보 설정
        if let master = reservation.master {
            if let image = master.profile_image_url {
                if reservation.cleaning_type == "1" {
                    self.profileImageView.kf.setImage(with: URL.init(string: image), placeholder: UIImage(named: "img_s_profile_default"), options: nil, progressBlock: nil, completionHandler: nil)
                } else {
                    self.profileImageView.kf.setImage(with: URL.init(string: image), placeholder: UIImage(named: "img_s_profile_movin"), options: nil, progressBlock: nil, completionHandler: nil)
                }
            } else {
                if reservation.cleaning_type == "1" {
                    self.profileImageView.image = UIImage(named: "img_s_profile_default")
                } else {
                    self.profileImageView.image = UIImage(named: "img_s_profile_movin")
                }
            }
            if reservation.cleaning_type == "1"{
                self.nameLabel.attributedText = String.makeAttributedString(boldString: master.name,
                                                                            boldSize: UIView.getFlexibleFontSize(11),
                                                                            boldColor: .main,
                                                                            systemString: " 마스터",
                                                                            systemSize: UIView.getFlexibleFontSize(11),
                                                                            systemColor: .main)
            } else {
                self.nameLabel.attributedText = String.makeAttributedString(boldString: master.name,
                                                                             boldSize: UIView.getFlexibleFontSize(11),
                                                                             boldColor: .main,
                                                                             systemString: "",
                                                                             systemSize: UIView.getFlexibleFontSize(11),
                                                                             systemColor: .main)
            }
            
        }
        else {
            if reservation.cleaning_type == "1"{
                self.profileImageView.image = UIImage(named: "img_s_profile_default")
                self.nameLabel.text = "마스터를 찾고 있습니다..."
            } else {
                self.profileImageView.image = UIImage(named: "img_s_profile_movin")
                self.nameLabel.text = ""
            }
            
            // 취소된 예약
            if reservation.cycle_status == .Cancel {
                self.nameLabel.text = ""
            }
        }
        
        dateFormatter.dateFormat = "yyyy.MM.dd.E. " // 뷰에 띄울 포맷
        // 시작시간 끝시간
        let reservationDate = dateFormatter.string(from: reservation.date!.toDate()!)
        
        dateFormatter.dateFormat = "HH:mm:ss" // 서버에서 오는 날짜 포맷
        
        if reservation.begin_time != nil {
            if let reservationBeginTimestamp = dateFormatter.date(from: reservation.begin_time!),
                let reservationEndTimestamp = dateFormatter.date(from: reservation.end_time!) {

                dateFormatter.dateFormat = "a h시"
                if reservation.begin_time?.components(separatedBy: ":")[1] != "00" { // 분 단위 인지 검사
                    dateFormatter.dateFormat = "a h시 mm분"
                }
                let beginTime = dateFormatter.string(from: reservationBeginTimestamp)
                
                dateFormatter.dateFormat = "a h시"
                if reservation.end_time?.components(separatedBy: ":")[1] != "00" {
                    dateFormatter.dateFormat = "a h시 mm분"
                }
                let endTime = dateFormatter.string(from: reservationEndTimestamp)
                self.timeLabel.text = reservationDate + beginTime + " ~ " + endTime
            }
        } else { // 이사 입주일때
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: reservation.date!)
            dateFormatter.dateFormat = "yyyy.MM.dd.E" // 뷰에 띄울 포맷
            self.timeLabel.text = dateFormatter.string(from: date!)
        }
        
        if let price = reservation.price {
            self.serviceTypeLabel.text = price.master_type == "1" ? "일반마스터" : "안심마스터"
        }
        
        guard let status = reservation.cycle_status else {
            log.error("cycle_status not found")
            return
        }
        
        if reservation.is_unpaid {
            serviceStatusLabel.text = "미결제 예약"
        } else if reservation.cleaning_type == "1" {
            serviceStatusLabel.text = status.getText(type: CleaningType.Normal)
        } else {
            serviceStatusLabel.text = status.getText(type: CleaningType.Move)
        }
        
//        log.debug("order_type = \(reservation.order_type ?? "nil")")
        
        if let isGrade = reservation.isGraded {

            if isGrade == "1" {
                isFinished = true
            } else {
                isFinished = false
            }
        }

        // dim
        if !reservation.is_unpaid && isFinished || status == CycleStatus.Cancel {
            timeLabel.textColor = UIColor.placeholder
            serviceTypeLabel.textColor = UIColor.placeholder
            nameLabel.textColor = UIColor.placeholder
            drawBorder(lineWidth: 1, cornerRadius: 2, color: UIColor.line)
            lineView.backgroundColor = UIColor.line
            profileImageView.alpha = 0.3
            serviceStatusLabel.textColor = UIColor.placeholder
            cycleCountLabel.textColor = UIColor.placeholder
        } else {
            timeLabel.textColor = UIColor.main
            serviceTypeLabel.textColor = UIColor(red: 130.0 / 255.0, green: 135.0 / 255.0, blue: 144.0 / 255.0, alpha: 1.0)
            drawBorder(lineWidth: 1, cornerRadius: 2, color: UIColor.placeholder)
            lineView.backgroundColor = UIColor.placeholder
            profileImageView.alpha = 1
            serviceStatusLabel.textColor = UIColor.alert
            cycleCountLabel.textColor = UIColor.main
        }
        
        // 한번만 / 정기 / 이사입주
        
        if let type = reservation.cleaning_type {
            if type == "1" {
                if let order_type = reservation.order_type {
                    if order_type == .Once { // 한번만 예약
                        self.movinView.isHidden = true
                        self.onceView.isHidden = false
                        self.periodicalView.isHidden = true
                        self.cycleCountLabel.isHidden = true
                        self.serviceTypeLabel.isHidden = false
                        if !reservation.is_unpaid && isFinished || status == CycleStatus.Cancel {
                            self.onceView.backgroundColor = UIColor.silver
                            self.onceImageView.isHighlighted = true
                        } else {
                            self.onceView.backgroundColor = UIColor.valid
                            self.onceImageView.isHighlighted = false
                        }
                    } else { // 정기예약
                        self.movinView.isHidden = true
                        self.onceView.isHidden = true
                        self.periodicalView.isHidden = false
                        self.cycleCountLabel.isHidden = false
                        self.serviceTypeLabel.isHidden = false
                        self.cycleCountLabel.text = "\(reservation.count!)회차"
                        if !reservation.is_unpaid && isFinished || status == CycleStatus.Cancel {
                            self.periodicalView.backgroundColor = UIColor.silver
                            self.periodicalImageView.isHighlighted = true
                        } else {
                            self.periodicalView.backgroundColor = UIColor.turquoiseBlue
                            self.periodicalImageView.isHighlighted = false
                        }
                    }
                }
            } else { // 이사입주일때
                self.movinView.isHidden = false
                self.onceView.isHidden = true
                self.periodicalView.isHidden = true
                self.cycleCountLabel.isHidden = true
                self.serviceTypeLabel.isHidden = true
                if !reservation.is_unpaid && isFinished || status == CycleStatus.Cancel {
                    self.movinView.backgroundColor = UIColor.silver
                    self.movinImageView.isHighlighted = true
                } else {
                    self.movinView.backgroundColor = UIColor.valid
                    self.movinImageView.isHighlighted = false
                }
            }
        }
    
    }
}
