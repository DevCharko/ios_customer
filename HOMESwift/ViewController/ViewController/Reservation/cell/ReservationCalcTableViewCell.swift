//
//  ReservationCalcTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 19..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class ReservationCalcTableViewCell: UITableViewCell {

    static var height: CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        return (13 + 35 * 4 + 1 + 13) * screenHeight / 568
    }
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var insurancePriceLabel: UILabel!
    @IBOutlet weak var couponDiscountLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
