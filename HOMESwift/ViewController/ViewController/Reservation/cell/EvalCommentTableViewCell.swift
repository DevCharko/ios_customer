//
//  EvalCommentTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 19..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

protocol EvalCommentTableViewCellDelegate: class {
    func moveFocus(cell: EvalCommentTableViewCell)
    func editMemo(text: String)
}

class EvalCommentTableViewCell: UITableViewCell {

    var cleaningType: String?
    weak var delegate: EvalCommentTableViewCellDelegate?
    @IBOutlet weak var tvComment: UITextView!
    
    static var height: CGFloat {
        return 160
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initTextView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setModel(model: ReservationModel?) {
        self.cleaningType = model?.cleaning_type
        if model?.cleaning_type == "1" {
            tvComment.text = "마스터님에겐 비공개됩니다.\n솔직한 의견 남겨주세요."
        } else {
            tvComment.text = "청소업체에겐 비공개됩니다.\n솔직한 의견 남겨주세요."
        }
    }
}

extension EvalCommentTableViewCell: UITextViewDelegate {
    
    func initTextView(){
        tvComment.delegate = self
        if let size = tvComment.font?.pointSize {
            tvComment.font = UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(size))
        }
        tvComment.drawBorder(lineWidth: 1, cornerRadius: 0, color: .line)
        tvComment.textColor = UIColor.placeholder
        tvComment.textContainer.maximumNumberOfLines = 15
        tvComment.textContainer.lineBreakMode = NSLineBreakMode.byTruncatingTail
        tvComment.textContainerInset = UIEdgeInsetsMake(13, 13, 13, 13);
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.placeholder {
            textView.text = nil
            textView.textColor = UIColor.main
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            if cleaningType == "1" {
                textView.text = "마스터님에겐 비공개됩니다.\n솔직한 의견 남겨주세요."
            } else {
                textView.text = "청소업체에겐 비공개됩니다.\n솔직한 의견 남겨주세요."
            }
            textView.textColor = UIColor.placeholder
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        delegate?.editMemo(text: tvComment.text)
    }
 
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        delegate?.moveFocus(cell: self)
        return true
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//        textView.frame.size.height = textView.contentSize.height
//    }
}
