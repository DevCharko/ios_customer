//
//  ReservationDetailInfoTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 19..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class ReservationDetailInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    static var height: CGFloat {
        return 63
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
