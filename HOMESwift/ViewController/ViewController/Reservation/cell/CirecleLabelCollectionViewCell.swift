//
//  CirecleLabelCollectionViewCell.swift
//  CircleLineView
//
//  Created by KimJingyu on 2017. 4. 18..
//  Copyright © 2017년 jingyu. All rights reserved.
//

import UIKit

class CirecleLabelCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lbCircle: UILabel!
    @IBOutlet weak var leftLineView: UIView!
    @IBOutlet weak var rightLineView: UIView!
    @IBOutlet weak var lbDescription: UILabel!

    var isFirst = false {
        didSet{
            if isFirst == true {
                leftLineView.isHidden = true
            } else {
                leftLineView.isHidden = false
            }
        }
    }
    
    var isLast = false {
        didSet{
            if isLast == true {
                rightLineView.isHidden = true
            } else {
                rightLineView.isHidden = false
            }
        }
    }
    
    var enableColor: UIColor = UIColor.main
    var disableColor: UIColor = UIColor.silver
    
    var color: UIColor = UIColor.lightGray {
        didSet{
            setNeedsDisplay()
            lbCircle.textColor = color
            lbDescription.textColor = color
            leftLineView.backgroundColor = color
        }
    }
    
    var isEnable: Bool = false {
        didSet{
            setNeedsDisplay()
            if isEnable == true {
                color = enableColor
            } else {
                color = disableColor
            }
        }
    }
    
    var lineWidth: CGFloat = 1.0
    
    var circleRadius: CGFloat {
        return lbCircle.bounds.width / 2
    }
    
    var circleCenter: CGPoint {
        return CGPoint(x: lbCircle.frame.midX, y: lbCircle.frame.midY)
    }
    
    private func createCircle(_ midPoint: CGPoint, withRadius radius: CGFloat) -> UIBezierPath {
        let path = UIBezierPath(arcCenter: midPoint, radius: radius, startAngle: 0.0, endAngle: CGFloat(2 * Double.pi), clockwise: true)
        path.lineWidth = lineWidth
        return path
    }

    func initailize() {
        leftLineView.backgroundColor = self.disableColor
        rightLineView.backgroundColor = self.disableColor
        color = disableColor
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        initailize()
    }
    
    override func draw(_ rect: CGRect) {        
        color.setStroke()
        createCircle(circleCenter, withRadius: circleRadius).stroke()
    }

}
