//
//  EvalStarTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 19..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import FloatRatingView

protocol EvalStarTableViewCellDelegate: class {
    func clickCheckEvent(_ button: UIButton)
    func increaseHeight(cell: EvalStarTableViewCell)
    func decreaseHeight(cell: EvalStarTableViewCell)
    func touchBeginRatingView()
}

class EvalStarTableViewCell: UITableViewCell, FloatRatingViewDelegate {

    weak var delegate: EvalStarTableViewCellDelegate?
    let gradeText = ["최악이에요", "별로에요", "보통이에요", "만족해요", "최고에요"]
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkViewLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!

    // 129 <-> 161
    static var height: CGFloat {
        return 129
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        // Required float rating view params
//        self.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
//        self.floatRatingView.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRatingView.delegate = self
        self.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 1
        self.floatRatingView.rating = 4
        self.floatRatingView.editable = true
        self.floatRatingView.halfRatings = false
        self.floatRatingView.floatRatings = false
        
        // Labels init
        self.ratingLabel.text =  gradeText[Int(self.floatRatingView.rating)-1]

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCheckView))
        self.checkView.addGestureRecognizer(tapGesture)
    }

 
    @objc func tapCheckView(_ sender: Any) {
        delegate?.clickCheckEvent(checkButton)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setRating(_ rating:Float) {
        self.floatRatingView.rating = rating;
    }
    
    // MARK: FloatRatingViewDelegate

    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float) {
        self.ratingLabel.text =  gradeText[Int(self.floatRatingView.rating)-1]
        delegate?.touchBeginRatingView()
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        self.ratingLabel.text =  gradeText[Int(self.floatRatingView.rating)-1]
        
        if self.floatRatingView.rating <= 3 {
            delegate?.increaseHeight(cell: self)
        } else {
            delegate?.decreaseHeight(cell: self)
        }
        
    }
}
