//
//  ProfileTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 19..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var reservationInfoLabel: UILabel!
    static var height : CGFloat {
        return 140
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImageView.setRound()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setModel(model: ReservationModel?) {
        guard model != nil else {
            return
        }
        
        profileImageView.imageFromUrl(model?.master?.profile_image_url, defaultImgPath: "img_b_profile_default")
        
        if let masterName = model!.master?.name {
            nicknameLabel.attributedText = String.makeAttributedString(boldString: masterName,
                                                                boldSize: UIView.getFlexibleFontSize(11),
                                                                boldColor: .main,
                                                                systemString: " 마스터",
                                                                systemSize: UIView.getFlexibleFontSize(11),
                                                                systemColor: .main)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // 서버에서 오는 날짜 포맷
        let reservationDate = dateFormatter.date(from: model!.date!)
        dateFormatter.dateFormat = "yyyy. MM. dd. E. a. " // 뷰에 띄울 포맷
        let date = dateFormatter.string(from: reservationDate!)
        if model?.order_type == .Cycle {
            reservationInfoLabel.text = "\(date) 정기예약"
        } else {
            reservationInfoLabel.text = "\(date)\(model!.cycle!)"
        }
    }
}
