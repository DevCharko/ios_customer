//
//  EvalListTableViewCell.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 4..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class EvalListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfInput: UITextField!
    @IBOutlet weak var lineView: UIView!
    
    static var height: CGFloat {
        return 44
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnCheck.isUserInteractionEnabled = false
        self.btnCheck.setImage(#imageLiteral(resourceName: "btn_chk_off"), for: UIControlState.normal)
        self.btnCheck.setImage(#imageLiteral(resourceName: "btn_chk_on"), for: UIControlState.selected)
    }
    
    func setData(title:String) {
        self.lbTitle.text = title
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            btnCheck.isSelected = true
        }
        else {
            btnCheck.isSelected = false
        }
    }
    
    @IBAction func tfInputChanged(_ sender: Any) {
        let isEmpty = (tfInput.text?.isEmpty)!
        setSelected(!isEmpty, animated: true)
    }
}
