//
//  ReservationListViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

/// 예약 리스트 뷰컨트롤러
class ReservationListViewController: SKPViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyView: UIView!
    
    var reservationDate: Date?
    
    // 전역에 저장
    var reservationList: ReservationModelList? {
        didSet{
            collectionView.reloadData()
            if self.reservationList == nil || self.reservationList?.count == 0 {
                emptyView.isHidden = false
                setRefreshControl(false)
            } else {
                emptyView.isHidden = true
                setRefreshControl(true)
            }
        }
    }
    
    var currentCalendar: Calendar?
    
    var tmpDate = Date()
    //    var animationFinished = true
    var rightClickCount = 0
    
    // MARK: - collectionView 사이즈 조정
    let columns = 1
    
    let spaceBetweenRows = 13
    let spaceHorisonMargin = 13
    
    var cellWidth: CGFloat {
        get {
            return collectionView.frame.width/CGFloat(columns)-CGFloat(spaceHorisonMargin*2)
        }
    }
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCollectionView()
//        getReservationList()
    }

    func setCollectionView() {
        collectionView.contentInset.top = 13
        collectionView.contentInset.bottom = 13
        collectionView.register(ReservationCollectionViewCell.self)
    }
    
    func setRefreshControl(_ isData: Bool) {
        if #available(iOS 10.0, *) {
            if isData {
                collectionView.refreshControl = refreshControl
            } else {
                collectionView.refreshControl = nil
            }
        } else {
            if isData {
                collectionView.addSubview(refreshControl)
            } else {
                collectionView.removeAllSubviews()
            }
        }
        refreshControl.addTarget(self, action: #selector(fetchData), for: .valueChanged)
    }
    
    @objc func fetchData() {
        guard let year = self.reservationDate?.getYearString(), let month = self.reservationDate?.getMonthString() else {
            return
        }
        
        NetworkManager.reservationList(year: year,
                                       month: month) { (jsonData, error) in
                                        if let networkError = error as? NetworkError {
                                            self.reservationList = nil
                                            log.debug(networkError.description)
                                            AlertView().alert(self, title: nil,
                                                              message: networkError.description)
                                        }
                                        else {
                                            let json = JSON(jsonData!)
                                            let mapper = Mapper<ReservationModelList>()
                                            self.reservationList = mapper.map(JSONString: json.rawString()!)
                                            self.reservationList?.debugPrint()
                                        }
                                        self.refreshControl.endRefreshing()
        }
    }
    
    func getReservationList() {
        guard let year = self.reservationDate?.getYearString(), let month = self.reservationDate?.getMonthString() else {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            SKHUD.addHUD(self)
        }
        
        log.debug("\(year)년 \(month)월 load")
        NetworkManager.reservationList(year: year,
                                       month: month) { (jsonData, error) in
                                        if let networkError = error as? NetworkError {
                                            self.reservationList = nil
                                            log.debug(networkError.description)
                                            AlertView().alert(self, title: nil,
                                                              message: networkError.description)
                                        }
                                        else {
                                            let json = JSON(jsonData!)
                                            let mapper = Mapper<ReservationModelList>()
                                            self.reservationList = mapper.map(JSONString: json.rawString()!)
//                                            self.reservationList?.debugPrint()
                                        }
                                        
                                        SKHUD.removeHUD(self)
        }
    }
}

extension ReservationListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return reservationList == nil ? 0 : reservationList!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as ReservationCollectionViewCell

        cell.setModel(reservation: (reservationList?.list?[indexPath.row])!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let model = reservationList?.list?[indexPath.row]
        
        log.debug("예약 번호 :\(String(describing: model?.seq))!")
        
        if let seq = model?.seq {
            let vc = ReservationDetailViewController(nibName: "ReservationDetailViewController", bundle: nil)
//            vc.reservationModel = mapper.map(JSONString: json["result"].rawString()!)
            vc.seq = seq
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //cell 간격 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(spaceBetweenRows)
    }
    
    //cell 사이즈를 조정
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: ReservationCollectionViewCell.height)
    }
    
}
