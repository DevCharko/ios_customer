//
//  EvaluationViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 19..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

/// 도우미 평가 뷰컨트롤러
class EvaluationViewController: SKPViewController {
    let dataSource = ["불친절해요", "지각을해요", "시간을 채우지 않고 나가세요", "일을 잘 못하세요", "전달 사항이 이루어지지 않았어요", "직접입력"]
    var inputText = "" // 직접입력
    var memoText = ""
    
    var isChecked = false
    var evalStarCellHeight = EvalStarTableViewCell.height
    var indexSet = IndexSet() // show and hide 위한 인덱스
    var rating: Float = 4 // 기본 값
    var model: ReservationModel?
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var nextButton: NextButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "평가하기")
        setLeftBarButton(type: .None, action: nil)
        setRightBarButton(type: .Exit, action: #selector(clickExit))

        tableView.register(ProfileTableViewCell.self)
        tableView.register(EvalStarTableViewCell.self)
        tableView.register(EvalListTableViewCell.self)
        tableView.register(EvalCommentTableViewCell.self)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = true
        
        indexSet.insert(2) // 이유 리스트 인덱스 섹션

        setupViewResizerOnKeyboardShown()
        
        nextButton.setNextButton(bool: true)
    }

    @IBAction func clickSendButton(_ sender: Any) {
        guard model != nil else {
            return
        }
        
        var reason = [String]()
        if let rows = tableView.indexPathsForSelectedRows {
            for item in rows {
                if dataSource[item.row] == "직접입력" {
                    reason.append(inputText)
                } else {
                    reason.append(dataSource[item.row])
                }
            }
        }
        
        // 플레이스 홀더일 경우 처리
        if memoText == "마스터님에겐 비공개됩니다.\n솔직한 의견 남겨주세요." || memoText == "청소업체에겐 비공개됩니다.\n솔직한 의견 남겨주세요." {
            memoText = ""
        }
        
        SKHUD.show()
        NetworkManager.createGrade(type: "1", reservation_cycle_seq: model!.seq!, grade: rating.string!, memo: memoText, is_rejected: isChecked, reason: reason) { (jsonData, error) in
            if error != nil {
                log.error(error!)
                if let networkError = error as? NetworkError {
                    AlertView().alert(self, title: nil,
                                      message: networkError.description)
                }
            } else {
                FirebaseAnalyticsManager.shared.sendLog(ScreenName.reservation_grade_completed.rawValue)
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self,
                                      title: nil,
                                      message: return_message,
                                      cancelButtonTitle: "확인",
                                      otherButtonTitles: []) {
                                        (_ alert: AlertView, _ buttonIndex:Int) in
                                        let naviVC = self.presentingViewController as? UINavigationController
                                        naviVC?.popViewController(animated: false)
                                        if let vc = naviVC?.topViewController as? ReservationPageViewController {
                                                vc.pages[0].getReservationList() //갱신
                                                vc.pages[1].getReservationList()
                                                vc.pages[2].getReservationList()
                                        }
                                        self.dismiss(animated: true)
                    }
                }
            }
            
            SKHUD.hide()
        }
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.tableView.contentInset.bottom = keyboardFrame.size.height
            self.view.layoutIfNeeded()
        })
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.tableView.contentInset.bottom = 0
            self.view.layoutIfNeeded()
        })
    }

    @objc func saveInputText(_ sender: UITextField) {
        let indexPath = IndexPath(row: 5, section: 2)
        
        inputText = sender.text!
        if inputText.isEmpty {
            tableView.deselectRow(at: indexPath, animated: true)
        } else {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
}

extension EvaluationViewController: EvalStarTableViewCellDelegate {
    
    func clickCheckEvent(_ button: UIButton) {
        button.isSelected = !button.isSelected
        if button.isSelected {
            isChecked = true
        } else {
            isChecked = false
            inputText = ""
        }
        
        tableView.reloadSections(indexSet, with: .automatic)
    }
    
    func increaseHeight(cell: EvalStarTableViewCell) {
        self.rating = cell.floatRatingView.rating

        if evalStarCellHeight == EvalStarTableViewCell.height + 41 {
            return
        }
        
        evalStarCellHeight = EvalStarTableViewCell.height + 41
        
        if let indexPath = tableView.indexPath(for: cell) {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        self.tableView.isScrollEnabled = true
    }
    
    func decreaseHeight(cell: EvalStarTableViewCell) {
        self.rating = cell.floatRatingView.rating

        if evalStarCellHeight == EvalStarTableViewCell.height {
            return
        }
        
        evalStarCellHeight = EvalStarTableViewCell.height
        
        // 닫을때 리스트가 열려있으면 닫기
        if isChecked {
            clickCheckEvent(cell.checkButton)
        }
        
        if let indexPath = tableView.indexPath(for: cell) {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        self.tableView.isScrollEnabled = true
    }
    
    func touchBeginRatingView() {
        self.tableView.isScrollEnabled = false
    }
}

extension EvaluationViewController: EvalCommentTableViewCellDelegate{
    func moveFocus(cell: EvalCommentTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    func editMemo(text: String) {
        self.memoText = text
    }
}

extension EvaluationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return 6
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return ProfileTableViewCell.height
        case 1:
            return self.evalStarCellHeight
        case 2:
            if isChecked {
                return EvalListTableViewCell.height
            } else {
                return 0.00001
            }
        case 3:
            if UITableViewAutomaticDimension < EvalCommentTableViewCell.height {
                return EvalCommentTableViewCell.height
            } else{
                return UITableViewAutomaticDimension
            }
        default:
            return 0.00001
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2 && isChecked {
            return 18
        } else {
            return 0.0001
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        
        switch indexPath.section {
        case 0:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ProfileTableViewCell
            
            tmpCell.backgroundColor = UIColor.background
            tmpCell.setModel(model: self.model)
            cell = tmpCell
            
        case 1:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EvalStarTableViewCell
            tmpCell.delegate = self
            if model?.cleaning_type == "1" {
                tmpCell.questionLabel.text = "마스터님은 어떠셨나요?"
                tmpCell.checkViewLabel.text = "이 분과 다시 만나지 않고 싶어요 (이유 복수 선택)"
            } else {
                tmpCell.questionLabel.text = "청소업체는 어떠셨나요?"
                tmpCell.checkViewLabel.text = "이 업체와 다시 만나지 않고 싶어요 (이유 복수 선택)"
            }
            tmpCell.setRating(self.rating)
            tmpCell.ratingLabel.text = tmpCell.gradeText[Int(self.rating-1)]

            cell = tmpCell
            
        case 2:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EvalListTableViewCell
            
            tmpCell.tfInput.inputAccessoryView = getKeyboardToolbar()
            tmpCell.setData(title: dataSource[indexPath.row])

            // 직접입력
            if indexPath.row == 5 {
                tmpCell.lbTitle.isHidden = true
                tmpCell.tfInput.isHidden = false
                tmpCell.lineView.isHidden = false
                tmpCell.tfInput.text = inputText
                tmpCell.tfInput.addTarget(self, action: #selector(saveInputText(_ :)), for: .editingChanged)
            } else {
                tmpCell.lbTitle.isHidden = false
                tmpCell.tfInput.isHidden = true
                tmpCell.lineView.isHidden = true
            }

            cell = tmpCell
            cell.clipsToBounds = true
            
        case 3:
            let tmpCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EvalCommentTableViewCell

            tmpCell.delegate = self
            tmpCell.setModel(model: model)
            tmpCell.tvComment.inputAccessoryView = getKeyboardToolbar()
            cell = tmpCell
            
        default:
            break
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        // 직접입력은 클릭으로 안되게
        if indexPath.row == 5 { return nil }
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        // 직접입력은 클릭으로 안되게
        if indexPath.row == 5 { return nil }
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
}
