//
//  PageViewController.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 8. 1..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 예약 뷰컨트롤러
class ReservationPageViewController: SKPViewController {
    
    @IBOutlet weak var dateLabelView: UIView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var pageView: UIView!

    @IBOutlet weak var dateLabelViewTop: NSLayoutConstraint!
    
    var pageViewController : UIPageViewController!
    lazy var pages = [ReservationListViewController]()
    lazy var nowDate = Date() // 현재 선택된 날짜
    
    var transitionInProgress = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: "예약 내역")
        setLeftBarButton(type: .Back, action: #selector(clickBack))

        leftButton.isExclusiveTouch = true
        rightButton.isExclusiveTouch = true
        pageView.isExclusiveTouch = true
        
        setCalendarLabel()
        setPageController()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        if #available(iOS 11.0, *) {
//             dateLabelViewTop.constant = self.view.safeAreaInsets.top
//        }
//        else {
//            dateLabelViewTop.constant = (self.navigationController?.navigationBar.frame.origin.y ?? 0) + (self.navigationController?.navigationBar.frame.size.height ?? 0)
//        }
    }

    func setCalendarLabel() {
        yearLabel.text = nowDate.getYearString() + "년"
        monthLabel.text = nowDate.getSingleMonthString() + "월"
    }

    func setPageController() {
        let prevDate = Calendar.current.date(byAdding: .month, value: -1, to: nowDate)
        let nextDate = Calendar.current.date(byAdding: .month, value: 1, to: nowDate)
        
        let prevPage = ReservationListViewController(nibName:"ReservationListViewController", bundle:nil)
        let currentPage = ReservationListViewController(nibName:"ReservationListViewController", bundle:nil)
        let nextPage = ReservationListViewController(nibName:"ReservationListViewController", bundle:nil)
        
        prevPage.reservationDate = prevDate
        currentPage.reservationDate = nowDate
        nextPage.reservationDate = nextDate
        
        prevPage.getReservationList()
        currentPage.getReservationList()
        nextPage.getReservationList()
        
        pages = [prevPage, currentPage, nextPage]
        
        self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        
        self.pageViewController.setViewControllers([currentPage], direction: .forward, animated: false, completion: nil)
        self.pageViewController.view.frame = pageView.bounds//CGRect(x: 0, y: 135, width: self.view.frame.width, height: self.view.frame.height - 135)
        
        self.addChildViewController(pageViewController)
        pageView.addSubview(self.pageViewController.view)
    }
    
    @IBAction func clickLeftButton(_ sender: Any) {
        guard let viewControllers = pageViewController.viewControllers else { return }
        guard let viewControllerIndex = self.pages.index(of: viewControllers[0] as! ReservationListViewController) else { return }
        
        let index = viewControllerIndex == 0 ? 2 : viewControllerIndex-1
        self.changePage(.reverse, index: index)
    }
    
    @IBAction func clickRightButton(_ sender: Any) {
        guard let viewControllers = pageViewController.viewControllers else { return }
        guard let viewControllerIndex = self.pages.index(of: viewControllers[0] as! ReservationListViewController) else { return }
        
        let index = viewControllerIndex == 2 ? 0 : viewControllerIndex+1
        self.changePage(.forward, index: index)
    }
    
    func changePage(_ direction: UIPageViewControllerNavigationDirection, index: Int) {
        if (!transitionInProgress) {
            self.leftButton.isUserInteractionEnabled = false
            self.rightButton.isUserInteractionEnabled = false
            DispatchQueue.main.async {
                self.pageViewController.setViewControllers([self.pages[index]], direction: direction, animated: true, completion: { (finished) in
                    self.transitionInProgress = !finished
                    self.leftButton.isUserInteractionEnabled = true
                    self.rightButton.isUserInteractionEnabled = true
                })
            }
            if let date = self.pages[index].reservationDate {
                self.yearLabel.text = date.getYearString() + "년"
                self.monthLabel.text = date.getSingleMonthString() + "월"
            }
            arrangPage(index)
        }
    }
    
    // 이전월, 현재월, 다음월 순서대로 되있는지 검사하여 세팅
    func arrangPage(_ viewControllerIndex: Int) {
        let prevIndex = viewControllerIndex == 0 ? 2 : viewControllerIndex-1
        let nextIndex = viewControllerIndex == 2 ? 0 : viewControllerIndex+1
        
        if let prevDate = pages[prevIndex].reservationDate, let nextDate = pages[nextIndex].reservationDate, let shoingDate = pages[viewControllerIndex].reservationDate {
            if prevDate.compare(Calendar.current.date(byAdding: .month, value: -1, to: shoingDate)!) != ComparisonResult.orderedSame {
                pages[prevIndex].reservationDate = Calendar.current.date(byAdding: .month, value: -1, to: shoingDate)!
                pages[prevIndex].reservationList = nil
                pages[prevIndex].getReservationList()
            }
            if nextDate.compare(Calendar.current.date(byAdding: .month, value: 1, to: shoingDate)!) != ComparisonResult.orderedSame {
                pages[nextIndex].reservationDate = Calendar.current.date(byAdding: .month, value: 1, to: shoingDate)!
                pages[nextIndex].reservationList = nil
                pages[nextIndex].getReservationList()
            }
        }
    }
}

extension ReservationPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let viewControllerIndex = self.pages.index(of: viewController as! ReservationListViewController) {
            if viewControllerIndex == 0 {
                // wrap to last page in array
                return self.pages.last
            } else {
                // go to previous page in array
                return self.pages[viewControllerIndex - 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let viewControllerIndex = self.pages.index(of: viewController as! ReservationListViewController) {
            if viewControllerIndex < self.pages.count - 1 {
                // go to next page in array
                return self.pages[viewControllerIndex + 1]
            } else {
                // wrap to first page in array
                return self.pages.first
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        // 스크롤 이동시 버튼 비활성화
        self.leftButton.isUserInteractionEnabled = false
        self.rightButton.isUserInteractionEnabled = false
    }
    
    // 페이지 애니메이션 끝난 후 호출
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if let viewControllers = pageViewController.viewControllers {
            let showingVC = viewControllers[0] as! ReservationListViewController
            
            // 현재 페이지 인덱스를 기준으로 순서대로 정렬
            if let viewControllerIndex = self.pages.index(of: showingVC) {
                arrangPage(viewControllerIndex)
            }
            
            // 바뀐 달력으로 라벨 초기화
            if let showingDate = showingVC.reservationDate {
                yearLabel.text = showingDate.getYearString() + "년"
                monthLabel.text = showingDate.getSingleMonthString() + "월"
            }
        }
        
        // 버튼 활성화 딜레이
        DispatchQueue.global().asyncAfter(deadline: .now()+0.1) {
            self.transitionInProgress = !completed
            self.leftButton.isUserInteractionEnabled = true
            self.rightButton.isUserInteractionEnabled = true
        }
    }
}
