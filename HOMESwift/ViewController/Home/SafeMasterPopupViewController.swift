//
//  SafeMasterPopupViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 2..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 마스터 설명 뷰컨트롤러
class SafeMasterPopupViewController: SKPViewController, UITableViewDelegate, UITableViewDataSource {
    
    let titles = ["고객평가 우수", "전문성", "교육이수", "인성/성실성", "자체평가"]
    let descriptions = ["고객 평가 평점 4.5점을 이상을\n정해진 기간동안 유지한 분",
                        "홈마스터에서 최소 100회 이상의\n크리닝을 성실하게 수행한 분",
                        "정기적인 교육을 모두 이수하신 분",
                        "본사의 인성 면접을 통과하고\n시간약속을 잘지키시는 분",
                        "홈마스터의 엄격한 자체평가를 거치신 분"]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        setNavigationTitle(title: "안심마스터는 무엇이 다른가요?")
        setRightBarButton(type: .Exit, action: #selector(clickExit))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SafeMasterTableViewCell
        
        cell.selectionStyle = .none
        cell.lbNumber.text = "0\(indexPath.row+1)"
        cell.lbTitle.text = titles[indexPath.row]
        cell.lbDescription.text = descriptions[indexPath.row]
        return cell
    }
    
    func exitSegue() {
        
    }
}
