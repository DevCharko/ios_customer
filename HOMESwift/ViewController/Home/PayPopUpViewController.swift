//
//  PayPopUpViewController.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 8. 3..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

enum PayPopUpStyle: Int {
    case WhenLogin = 0
    case WhenTapButton = 1
}

/// 미결제 내역 안내 뷰컨트롤러
class PayPopUpViewController: SKPViewController {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var serviceUnPaidLabel: FlexibleLabel!
    @IBOutlet weak var serviceDateLabel: FlexibleLabel!
    @IBOutlet weak var serviceCycleLabel: FlexibleLabel!
    @IBOutlet weak var servicePriceLabel: FlexibleLabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!

    var style: PayPopUpStyle?

    var service_date: String?
    var service_cycle: String?
    var unpaid_price: Int?
    var fail_msg: String?
    
    var reservationModel: ReservationModel?
    
    let popUpMainLabelTitle = ["고객님.\n미결제 내역이 있어요!", "고객님.\n미결제 내역이 있으실 땐,\n추가 예약이 어려우세요."]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeAnimate)))
        self.okButton.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        self.okButton.setBackgroundImage(UIImage.imageFromColor(UIColor.alert), for: .normal)
        self.cancelButton.setBackgroundImage(UIImage.imageFromRGB(0x828790), for: .highlighted)
        self.cancelButton.setBackgroundImage(UIImage.imageFromColor(UIColor.main), for: .normal)
        self.showAnimate()
        
        if let style = self.style?.rawValue {
            mainLabel.text = popUpMainLabelTitle[style]
        }
        serviceUnPaidLabel.text = fail_msg
        serviceDateLabel.text = service_date
        serviceCycleLabel.text = service_cycle
        if let price = "\(unpaid_price ?? 0)".stringFormatDecimal() {
            servicePriceLabel.text = price + " 원"
        }
    }
    
    @IBAction func clickCancelButton(_ sender: Any) {
        self.removeAnimate()
    }
    
    @IBAction func clickOkButton(_ sender: Any) {
        if let seq = reservationModel?.seq {
            let vc = ReservationDetailViewController()
            vc.reservationModel = reservationModel
            vc.seq = seq
            self.navigationController?.pushViewController(vc, animated: true)
        }
        self.removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @objc func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        })
//            , completion:{(finished : Bool)  in
//            if (finished)
//            {
//                self.view.removeFromSuperview()
//                self.navigationController?.popToRootViewController(animated: true)
//                self.removeFromParentViewController()
//            }
//        }
//        )
    }
    
}
