//
//  SelectFavoriteViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  선호하는 마스터 선택
//

import UIKit
import SwiftyJSON
import ObjectMapper
import FacebookCore
import FirebaseAnalytics

/// 선호 마스터 선택 뷰컨트롤러
class SelectFavoriteViewController: SKPViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var nextButton: NextButton!
    
    @IBOutlet var tableViewHeightConstrant: NSLayoutConstraint!
    
    var buttonController = ButtonController()
    let dataSource = ["청소 잘하시는 분", "정리정돈 잘하시는 분", "친절하신 분", "시간 약속 잘지키시는 분"]

    @IBAction func clickNextButton(_ sender: Any) {
        SKHUD.show()
        
        if let rows = tableView.indexPathsForSelectedRows {
            var list = [String]()
            for item in rows {
                list.append(dataSource[item.row])
            }
            ReservationModel.shared.user_preference = list
        }

        ReservationModel.shared.debugPrint()
        
        // 예약생성
        NetworkManager.createReservation { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                let json = JSON(jsonData!)
                let isUnPaid = json["result"]["is_unpaid"].bool
                
                if isUnPaid == true { // 미결제가 있을경우
                    let payPopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PayPopUpViewController") as! PayPopUpViewController
                    payPopUpVC.style = .WhenTapButton
                    
                    payPopUpVC.service_date = json["result"]["service_date"].string
                    payPopUpVC.service_cycle = json["result"]["service_cycle"].string
                    payPopUpVC.unpaid_price = json["result"]["unpaid_price"].int
                    payPopUpVC.fail_msg = json["result"]["fail_msg"].string
                    
                    let mapper = Mapper<ReservationModel>()
                    if let reservation_infos = json["result"]["reservation_infos"].rawString() {
                        payPopUpVC.reservationModel = mapper.map(JSONString: reservation_infos)
                    }
                    
                    self.addChildViewController(payPopUpVC)
                    
                    payPopUpVC.view.frame = self.view.frame
                    self.view.addSubview(payPopUpVC.view)
                    payPopUpVC.didMove(toParentViewController: self)
                } else { // 미결제 없을때
                    let total_price = ReservationModel.shared.total_price!
                    let currency = "KRW"

                    AppEventsLogger.log(.purchased(amount: Double(total_price), currency: currency))
                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.reservation_completed.rawValue, parameters: [
                        AnalyticsParameterValue: total_price,
                        AnalyticsParameterCurrency: currency
                    ])

                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "FinishReserveViewController") as! FinishReserveViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            SKHUD.hide()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: DataStore.naviTitleName)
        setLeftBarButton(type: .Back, action: #selector(clickBack))

        nextButton.setNextButton(bool: true)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RadioCell.self)
        
        tableView.allowsMultipleSelection = true

    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        tableViewHeightConstrant.constant = tableView.contentSize.height
    }
}

extension SelectFavoriteViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RadioCell.height
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RadioCell
        
        cell.selectionStyle = .none
        cell.leftButton.setImage(UIImage(named: "btn_chk_off"), for: UIControlState.normal)
        cell.leftButton.setImage(UIImage(named: "btn_chk_on"), for: UIControlState.selected)
        cell.leftButton.isUserInteractionEnabled = false
        
        cell.setData(leftButton:cell.leftButton, name: dataSource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
    
    // 셀 클릭할때 색깔
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
        cell.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
        cell.backgroundColor = UIColor.white
    }
}
