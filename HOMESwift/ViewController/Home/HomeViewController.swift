//
//  HomeViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 16..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import ChannelIO
//import SendBirdSDK
import Firebase

/// 메인 뷰컨트롤러.
class HomeViewController: SKPViewController, GoCouponListDelegate, UIGestureRecognizerDelegate {
    @IBAction func newMainAction(_ sender: Any) {
        let newMainVC = UIStoryboard(name: "mainV2", bundle: nil).instantiateViewController(withIdentifier: "newMainNavigationController") as! NewMainNavigationController
        UIApplication.shared.keyWindow?.rootViewController = newMainVC
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var floatingButton: UIButton!
    
    //    fileprivate var channels: [SBDGroupChannel] = []
    //    fileprivate var groupChannelListQuery: SBDGroupChannelListQuery?
    
    var userId = ""
    
    var bannerList: BannerListModel?
    var isGoCouponVC: Bool = false
    
    let mainCellDataSource = [["color": "fe5958",
                               "pressColor": "ff8586",
                               "image": "icon_main_01",
                               "title": "가사 클리닝",
                               "description": "일반 가정집의 일상청소 정기서비스\n마스터님 한분이 고정으로 방문합니다."],
                              //                              ["color": "11bed8",
        //                               "pressColor": "5ad0e2",
        //                               "image": "icon_main_02",
        //                               "title": "이사 · 입주 청소",
        //                               "description": "이사 / 입주 / 인테리어 후 청소\n깨끗한 공간으로 기분좋게 들어가세요."],
                            ["color": "3e9fff",
                             "pressColor": "8aadec",
                             "image": "icon_main_03",
                             "title": "20,000원 받기",
                             "description": "추천하고 크레딧받기로\n20,000 크레딧을 받아가세요."]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //log.info("홈메인뷰로드")
        //네비 타이틀 이미지, 네비 색상 변경
        setNavigationTitle()
        self.setLeftBarButton(type: .My, action: nil)
        
        tableView.register(DefaultCell.self)
        tableView.register(MainTableViewCell.self)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        // sendbird 연결
        //        connectSendBird()

        (UIApplication.shared.delegate as! AppDelegate).getCommonInfo()
        
        getBannerInfo()
        getUserInfo()
        checkUnPaid()
        getSettings()
        
        self.title = "정기서비스 주기 선택"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        floatingButton.alpha = 0
        
        // 메인화면과 채팅리스트화면에서 딜리게이트 전환
        log.info("델리게이트 전환")
        ChannelIO.delegate = self
        //        addDelegates()
        
        // 메시지 받으면 뱃지 갱신
        
        // badge 갱신
        //        setChatNewBadge() // 우상단 new뱃지
        //        self.floatingButton.setBadge(text: Constants.channelIOBadgeCount) // 채널 io
        
        if isGoCouponVC {
            isGoCouponVC = false
            goCouponListVC()
        }
    }
    
    //    func showLoginPopup() {
    //        AlertView().alert(self, title: nil, message: "홈마스터 서비스를 신청하신 후 이용할 수 있습니다.")
    //    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        ReservationModel.shared.removeAll()
        
        if let user = DataStore.userModel?.seq {
            Analytics.setUserID("\(user)")
        }
        // 채널 플로팅 버튼 애니메이션
        showFloatingButton()
        
        // 다른 화면일때 타이머 멈춤
        let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! BannerCell
        if cell.pageControl.numberOfPages > 0 {
            cell.startTimer()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        guard self.tableView != nil else { return }
        
        let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! BannerCell
        if cell.bannerList != nil && cell.pageControl.numberOfPages > 0 {
            cell.stopTimer()
            cell.collectionView.scrollToItem(at: IndexPath.init(row: 1, section: 0), at: .left, animated: false)
        }
        //        removeDelegates()
    }
    
    func checkInChannelIO() {
        // 실행시 deviceID checkIn
        log.info("체크인실행")
        let settings = ChannelPluginSettings()
        
        if let seq = DataStore.userModel?.seq {
            settings.userId = "\(seq)"
        }
        
        let profile = Profile()
        
        if let name = DataStore.userModel?.name {
            profile.set(name: name)
        }
        
        if let contact = DataStore.userModel?.phone_number {
            profile.set(mobileNumber: "+82\(contact)")
        }
        ChannelIO.boot(with:settings, profile: profile){ (ChannelPluginComletionStatus, Guest) in
        //ChannelPlugin.checkIn(checkinObj){ (ChannelPluginCompletionStatus) in
            log.debug(ChannelPluginComletionStatus)
            switch ChannelPluginComletionStatus {
            case .success:
                break
            //case .duplicated:
                //break
            case .networkTimeout:
                break
            default:
                break
            }
        }
    }
    
    func getBannerInfo() {
        NetworkManager.banner { (jsonData, error) in
            //            log.debug(jsonData ?? error!)
            if jsonData != nil {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                let mapper = Mapper<BannerListModel>()
                
                self.bannerList = mapper.map(JSONString: json.rawString()!)
                
                self.tableView.reloadData()
            }
            log.debug("배너정보 finish")
        }
    }
    
    func actionGo() {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "RegularDateViewController") as! RegularDateViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        goCouponListVC()
    }
    
    func getUserInfo() {
        SKHUD.show()
        
        NetworkManager.userInfo { (jsonData, error) in
            if jsonData != nil {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string ?? ""
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                SKHUD.hide()
                
                // 로그인 되어 있으면 마이페이지 네비게이션, 없으면 토스트메시지 띄우기
                if json["return_code"] == 1 { // 등록된 유저
                    let mapper = Mapper<UserModel>()
                    if let user = json["result"].rawString() {
                        DataStore.userModel = mapper.map(JSONString: user)
                        (self.navigationController as! MainController).sideMenu.tableView.reloadData()
                        
                        if (DataStore.userModel?.is_recommend_coupon) != nil ||
                            self.isCreate6Hour(create_at: DataStore.userModel?.created_at) {
                            let recommended = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "recommendPopUpViewController") as! RecommendPopUpViewController

                            recommended.modalPresentationStyle = .overCurrentContext
                            recommended.modalTransitionStyle = .crossDissolve
                            
                            recommended.delegate = self
                            self.present(recommended, animated: true)
                        }
                    }
                    // todo: 사이드메뉴 N 이미지 - 보류
                }
                // 네비 슬라이드에 유저정보세팅
            }
            self.setLeftBarButton(type: .My, action: #selector(self.clickMenu))
            self.checkInChannelIO()
            log.debug("유저정보 finish")
        }
    }
    
    func isCreate6Hour(create_at: String?) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // 서버에서 오는 날짜 포맷
        
        let joinDate = dateFormatter.date(from: create_at ?? "2019-11-11 09:00:00")!
        let nowDate = Date()
        
        if joinDate.addingTimeInterval(6 * 60 * 60) >= nowDate {
            return true
        } else {
            return false
        }
    }
    
    func checkUnPaid() {
        SKHUD.show()
        NetworkManager.checkUnPaid(block: { (jsonData, error) in
            if jsonData != nil {
                log.debug("checunpaid request")
                let json = JSON(jsonData!)
                let return_code = json["return_code"].int!
                if return_code == -1 { // 미결제 없음
                    
                } else { // 미결제 내역이 있으면
                    let payPopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PayPopUpViewController") as! PayPopUpViewController
                    payPopUpVC.style = .WhenLogin
                    
                    payPopUpVC.service_date = json["result"]["service_date"].string
                    payPopUpVC.service_cycle = json["result"]["service_cycle"].string
                    payPopUpVC.unpaid_price = json["result"]["unpaid_price"].int
                    payPopUpVC.fail_msg = json["result"]["fail_msg"].string
                    
                    let mapper = Mapper<ReservationModel>()
                    if let reservation_infos = json["result"]["reservation_infos"].rawString() {
                        payPopUpVC.reservationModel = mapper.map(JSONString: reservation_infos)
                    }
                    
                    self.addChildViewController(payPopUpVC)
                    payPopUpVC.view.frame = self.view.frame
                    self.view.addSubview(payPopUpVC.view)
                    payPopUpVC.didMove(toParentViewController: self)
                }
            }
            SKHUD.hide()
        })
    }

    // 채널io 대화생성
    @IBAction func clickFloatingButton(_ sender: Any) {
        FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.channelio.rawValue)
        log.info("채팅뷰 보이기")
        if (ChannelIO.isBooted) {
            ChannelIO.open(animated: true)
        } else {
            let Settings=ChannelPluginSettings()
            Settings.pluginKey = "e566696b-97b7-463c-8331-45550cbce995"
            Settings.debugMode = true
            ChannelIO.boot(with: Settings)
            
            ChannelIO.open(animated: true)
        }
    }
    
    func showFloatingButton() {
        UIView.animate(withDuration: 0.5) {
            self.floatingButton.alpha = 1.0
        }
        
            let Settings=ChannelPluginSettings()
            Settings.pluginKey = "e566696b-97b7-463c-8331-45550cbce995"
            Settings.debugMode = true
            ChannelIO.boot(with:Settings)
        
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        //        log.debug(navigationController!.viewControllers.count)
        return navigationController!.viewControllers.count > 1
    }
    
    func clickAlarm() {
        //        self.navigationController?.pushViewController(loginView, animated: true)
    }
    
    @objc func clickMenu() {
        log.debug("click menu")
        if DataStore.userModel != nil {
            (self.navigationController as! MainController).clickMenu()
        }
        else {
            DataStore.isLogin = false
            if let loginVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController {
                self.present(loginVC, animated: true)
            }
            //            AlertView().alert(self, title: nil, message: "홈마스터 서비스를 신청하신 후 이용할 수 있습니다.")
//            if self.childViewControllers.count < 1 {
//                let signPopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignPopUpViewController") as! SignPopUpViewController
//                self.addChildViewController(signPopUpVC)
//                signPopUpVC.view.frame = self.view.frame
//                self.view.addSubview(signPopUpVC.view)
//                signPopUpVC.didMove(toParentViewController: self)
//            }
        }
    }
    
    func getSettings() {
        NetworkManager.getSettings { (jsonData, Error) in
            let mapper = Mapper<SettingsModel>()
            
            if let settings = JSON(jsonData!)["result"].rawString() {
                DataStore.settings = mapper.map(JSONString: settings)
            }
        }
    }
    
    func goCouponListVC() {
        let view = CouponListViewController(nibName:"CouponListViewController", bundle:nil)
        self.navigationController?.pushViewController(view, animated: true)
    }
}

extension HomeViewController : ChannelPluginDelegate {
    
    @nonobjc func badgeDidChanged(count: Int) {
        //런처버튼의 뱃지카운트를 업데이트
        if count > 99 {
            self.floatingButton.setBadge(text: "99+")
            DataStore.channelIOBadgeCount = "99+"
        } else if count == 0 {
            self.floatingButton.setBadge(text: "")
            DataStore.channelIOBadgeCount = ""
        } else {
            self.floatingButton.setBadge(text: "\(count)")
            DataStore.channelIOBadgeCount = "\(count)"
        }
    }
}

// MARK: - tableview delegate
extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell:BannerCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as BannerCell
            cell.selectionStyle = .none
            if let model = bannerList {
                cell.setModel(model: model)
            }
            
            return cell
        }
        else {
            let cell:MainTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MainTableViewCell
            cell.selectionStyle = .none
            let color = mainCellDataSource[indexPath.row]["color"]?.string
            cell.colorView.backgroundColor = UIColor(hex: color!)
            cell.serviceImageView.image = UIImage(named: mainCellDataSource[indexPath.row]["image"]! )
            cell.serviceTitle.text = mainCellDataSource[indexPath.row]["title"]
            cell.serviceDescription.text = mainCellDataSource[indexPath.row]["description"]
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return BannerCell.height
        }
        else if indexPath.section == 1 {
            return MainTableViewCell.height
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        log.debug("select row")
        if indexPath.section == 0 {
            log.debug("select 0")
        }
            
        else if indexPath.section == 1 {
            // 미결제 체크
            DataStore.setPopUpDisable(isCheck: true) // 이사입주 제거시 팝업 필요없으므로
//            if indexPath.row == 0 || indexPath.row == 1 {
            if indexPath.row == 0 {
                SKHUD.show()
                //log.debug("send unpaid")
                
                NetworkManager.checkUnPaid(block: { (jsonData, error) in
                    //log.debug("보내기")
                    //log.debug(jsonData!)
                    if jsonData != nil {
                        log.debug("it is not nil")
                        let json = JSON(jsonData!)
                        let return_code = json["return_code"].int!
                        if return_code == -1 { // 미결제 없음
                            if indexPath.row == 0 {
                                ReservationModel.shared.cleaning_type = "1"
                                // 다음부터 보이지 않기 체크 되어있지 않으면
                                if DataStore.getPopUpDisable == nil || DataStore.getPopUpDisable == false {
                                    log.debug("문제 1")
                                    let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                                    popOverVC.delegate = self
                                    self.addChildViewController(popOverVC)
                                    popOverVC.view.frame = self.view.frame
                                    self.view.addSubview(popOverVC.view)
                                    popOverVC.didMove(toParentViewController: self)
                                    log.debug("문제 2")
                                } else {
                                    self.closePopUpView(isCheck: true)
                                }
                            }
                            else if indexPath.row == 1 {
                                ReservationModel.shared.cleaning_type = "2"
                                DataStore.naviTitleName = "이사/입주 청소"
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OnceDateViewController") as! OnceDateViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        } else { // 미결제 내역이 있으면
                            // 미결제 내역이 있으면
                            let payPopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PayPopUpViewController") as! PayPopUpViewController
                            payPopUpVC.style = .WhenTapButton
                            
                            let json = JSON(jsonData!)
                            
                            payPopUpVC.service_date = json["result"]["service_date"].string
                            payPopUpVC.service_cycle = json["result"]["service_cycle"].string
                            payPopUpVC.unpaid_price = json["result"]["unpaid_price"].int
                            payPopUpVC.fail_msg = json["result"]["fail_msg"].string
                            
                            //                payPopUpVC.cycle_seq = json["result"]["cycle_seq"].string
                            
                            let mapper = Mapper<ReservationModel>()
                            if let reservation_infos = json["result"]["reservation_infos"].rawString() {
                                payPopUpVC.reservationModel = mapper.map(JSONString: reservation_infos)
                            }
                            
                            self.addChildViewController(payPopUpVC)
                            
                            payPopUpVC.view.frame = self.view.frame
                            self.view.addSubview(payPopUpVC.view)
                            payPopUpVC.didMove(toParentViewController: self)
                        }
                    }
                    SKHUD.hide()
                })
                
            }
                
//            else if indexPath.row == 2 {
            else if indexPath.row == 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                    let view = RecommendViewController(nibName:"RecommendViewController", bundle:nil)
                    if let vc = UIStoryboard.init(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "referralViewController") as? ReferralViewController {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    //                    if UIApplication.shared.canOpenURL(URL.init(string: "http://homemaster.co.kr/office")!) {
                    //                        FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_office.rawValue)
                    //
                    //                        UIApplication.shared.openURL(URL.init(string: "http://homemaster.co.kr/office")!)
                    //                    }

                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let cell = tableView.cellForRow(at: indexPath) as? MainTableViewCell
        let color = mainCellDataSource[indexPath.row]["pressColor"]?.string
        cell?.colorView.backgroundColor = UIColor(hex: color!)
        return true
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? MainTableViewCell
        let color = mainCellDataSource[indexPath.row]["color"]?.string
        cell?.colorView.backgroundColor = UIColor(hex: color!)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0001
        } else {
            return 13
        }
    }
}

extension HomeViewController: popUpViewDelegate {
    func closePopUpView(isCheck: Bool) {
        log.debug("클로즈팝")
        DataStore.setPopUpDisable(isCheck: isCheck)
        ReservationModel.shared.cleaning_type = "1"
        //여기서 한번만, 정기 예약 선택하는 컨트롤러로 연결하는 기능
        /*let vc = storyboard?.instantiateViewController(withIdentifier: "SelectReserveViewController") as! SelectReserveViewController
        navigationController?.pushViewController(vc, animated: true)*/
        let vc = storyboard?.instantiateViewController(withIdentifier: "RegularDateViewController") as! RegularDateViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
