//
//  UserInputViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  고객정보 입력
//

import UIKit
import SwiftyJSON
import ObjectMapper
import ChannelIO
import FirebaseAnalytics
import FacebookCore

/// 유저 정보 입력 뷰컨트롤러
class UserInputViewController: SKPViewController {
    
    var address: Address = Address()
    
    var timer: Timer?
    var remainTime = 180
    var isAuthenticating = false {
        didSet {
            if isAuthenticating == true {
                authButton.isEnabled = false
                authTouchViewButton.isEnabled = false
            } else {
                authButton.isEnabled = true
                authTouchViewButton.isEnabled = true
            }
        }
    }
    
    var completeAuth = false {
        didSet {
            isAuthenticating = false
            if completeAuth == true {
                lbAuthRequire.text = "인증된 연락처입니다."
                authButton.isHidden = true
                authTouchViewButton.isHidden = true
                tfContact.isEnabled = false
                lbAuthRequire.textColor = UIColor.valid
                titleViewHide(isHide: true)
            } else {
                lbAuthRequire.text = "인증이 필요합니다."
                authButton.isHidden = false
                authTouchViewButton.isHidden = false
                authButton.borderColor = UIColor.main
                tfContact.isEnabled = true
                lbAuthRequire.textColor = UIColor.alert
                nextButton.setNextButton(bool: false)
                titleViewHide(isHide: false)
            }
            checkFormIsFill()
        }
    }
    
    var selectedLine: UIView?
    @IBOutlet weak var subTitleView: UIView!
    @IBOutlet weak var topConstration: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressDetailLabel: UILabel!
    @IBOutlet weak var tfName: SKTextField!
    @IBOutlet weak var tfContact: SKTextField!
    @IBOutlet weak var tfAddress: SKTextField!
    @IBOutlet weak var tfRecommand: SKTextField!
    @IBOutlet weak var tfArea: SKTextField!
    
    @IBOutlet var tfArray: [SKTextField]!
    
    @IBOutlet weak var nameLineView: UIView!
    @IBOutlet weak var contactLineView: UIView!
    
    @IBOutlet weak var nextButton: NextButton!
    @IBOutlet weak var authButton: BorderButton! // 인증 버튼
    @IBOutlet weak var authTouchViewButton: UIButton! // 터치 영역 넓게
    
    // 타이머 뷰
    @IBOutlet var confirmButton: BorderButton! // 확인 버튼
    @IBOutlet weak var confirmTouchViewButton: UIButton!
    @IBOutlet var tfAuth: SKTextField!
    @IBOutlet var lbTimer: UILabel!
    
    @IBOutlet var timerViews: [UIView]! // 타이머동안 보이기
    
    @IBOutlet var recommendCodeView: [UIView]! // 추천코드 뷰
    @IBOutlet var recommHeights: [NSLayoutConstraint]!
    
    @IBOutlet var areaView: [UIView]! // 평수 뷰
    @IBOutlet var areaHeights: [NSLayoutConstraint]!
    
    @IBOutlet var lbAuthRequire: UILabel! // 인증이 필요합니다.
    
    // 상세 주소 뷰
    @IBOutlet weak var address2View: UIView!
    @IBOutlet weak var tfAddress2: SKTextField!
    @IBOutlet var address2TopMargin: NSLayoutConstraint!
    @IBOutlet var address2Height: NSLayoutConstraint!
    @IBOutlet weak var address2TouchViewButton: UIButton!
    
    // Constraint 조절
    @IBOutlet var defaultConstraint: NSLayoutConstraint!
    @IBOutlet var lbAuthConstraint: NSLayoutConstraint!
    @IBOutlet var timerConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var personalInfoView: UIView! // 개인정보 취급방침 뷰
    
    // 타이머 백그라운드 task
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setupViewResizerOnKeyboardShown()
        
        //        setNavigationTitle(title: DataStore.naviTitleName)
        if ReservationModel.shared.order_type == .Once {
            setNavigationTitle(title: "정기서비스 고객 정보")
        } else {
            setNavigationTitle(title: "정기서비스 고객 정보")
        }
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)

        tfName.delegate = self
        tfContact.delegate = self
        tfAddress.delegate = self
        tfRecommand.delegate = self
        tfAuth.delegate = self
        tfAddress2.delegate = self
        tfArea.delegate = self
        
        tfName.setToolbar()
        tfContact.setToolbar()
        tfAddress.setToolbar()
        tfAddress2.setToolbar()
        tfRecommand.setToolbar()
        tfAuth.setToolbar()
        tfArea.setToolbar()
        
        tfName.addTarget(self, action: #selector(checkFormIsFill), for: .editingChanged)
        tfAddress.addTarget(self, action: #selector(checkFormIsFill), for: .editingChanged)
        tfAddress2.addTarget(self, action: #selector(checkFormIsFill), for: .editingChanged)
        tfArea.addTarget(self, action: #selector(checkFormIsFill), for: .editingChanged)
        
        setDefaultView()
        initializeView()
        checkFormIsFill()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // 로그인 되어있는경우 데이터 로딩, 주소 선택후 돌아오는 경우도 바뀜
        if DataStore.userModel != nil {
            self.address2TouchViewButton.isHidden = false //상세주소 클릭시 주소목록 링크연결
            
            tfName.text = DataStore.userModel?.name
            tfContact.text = DataStore.userModel?.phone_number
            tfAddress.text = DataStore.userModel?.address1
            tfAddress2.text = DataStore.userModel?.address2
            
            tfName.isEnabled = false
            tfContact.isEnabled = false
            nameLineView.isHidden = true
            contactLineView.isHidden = true
            self.address2View.isHidden = false
            self.address2TopMargin.constant = 27
            self.address2Height.constant = 50
            self.completeAuth = true
        }
    }
    
    func initializeView() {
        
        // 비회원일 경우 이용약관뷰 보임
        if DataStore.userModel == nil {
            self.personalInfoView.isHidden = false
        } else {
            self.personalInfoView.isHidden = true
        }
        
        if DataStore.naviTitleName == "가입" {
            setLeftBarButton(type: .None, action: nil)
            setRightBarButton(type: .Exit, action: #selector(clickExit))
            self.nextButton.setTitle("가입 완료", for: .normal)
            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.signup.rawValue)
        } else if ReservationModel.shared.cleaning_type == "2" { // 이사 입주일경우 세팅
//            FirebaseAnalyticsManager.shared.setAnalytics(ScreenName.reservation_customer_info_update.rawValue)

            self.nextButton.setTitle("예약 완료", for: .normal)
            self.addressLabel.attributedText = String.makeAttributedString(boldString: "청소지 주소",
                                                                           boldSize: UIView.getFlexibleFontSize(10),
                                                                           boldColor: .main,
                                                                           systemString: " *",
                                                                           systemSize: UIView.getFlexibleFontSize(10),
                                                                           systemColor: .alert)
            self.addressDetailLabel.attributedText = String.makeAttributedString(boldString: "청소지 상세 주소",
                                                                                 boldSize: UIView.getFlexibleFontSize(10),
                                                                                 boldColor: .main,
                                                                                 systemString: " *",
                                                                                 systemSize: UIView.getFlexibleFontSize(10),
                                                                                 systemColor: .alert)
        } else {
            if let masterType = ReservationModel.shared.masterType?.getPrefix(), let orderType = ReservationModel.shared.order_type?.getPrefix() {
                FirebaseAnalyticsManager.shared.setAnalytics(masterType + "_" + orderType + ScreenName.reservation_customer_info_update.rawValue)
            }
        }
        
        // 로그인 되어있거나 이사입주일 경우 추천코드 제거
        if DataStore.userModel != nil || ReservationModel.shared.cleaning_type == "2" {
            for view in recommendCodeView {
                view.isHidden = true
            }
            for height in recommHeights {
                height.constant = 0
            }
        }
        
        // 이사입주 아닐때 세팅
        if ReservationModel.shared.cleaning_type != "2" {
            // 평수 제거
            for view in areaView {
                view.isHidden = true
            }
            for height in areaHeights {
                height.constant = 0
            }
        }
    }
    
    @objc func reinstateBackgroundTask() {
        if timer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        log.debug("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
        self.timer?.invalidate()
    }
    
    // 뒤로가기할때 타이머 초기화, 주소추가화면일때는 유지
    override func didMove(toParentViewController parent: UIViewController?) {
        super.didMove(toParentViewController: parent)
        self.endBackgroundTask()
    }
    
    @IBAction func clickNextButton(_ sender: UIButton) {
        dismissKeyboard()
        
        if DataStore.userModel == nil { // 비회원일 경우
            SKHUD.show()
            NetworkManager.signUp(name: getUnWrappingValue(tfName.text),
                                  phone_number: getUnWrappingValue(tfContact.text),
                                  address1: getUnWrappingValue(tfAddress.text),
                                  address2: getUnWrappingValue(tfAddress2.text),
                                  recommend_code: tfRecommand.text,
                                  block: { (jsonData, error) in
                                    
                                    if let networkError = error as? NetworkError {
                                        AlertView().alert(self, title: nil, message: networkError.description)
                                        SKHUD.hide()
                                    }
                                    else {
                                        FirebaseAnalyticsManager.shared.sendLog(ScreenName.signup_complete.rawValue)
                                        AppEventsLogger.log(.completedRegistration())

                                        let json = JSON(jsonData!)
                                        let return_message = json["return_message"].string!
                                        if return_message != "" {
                                            AlertView().alert(self, title: nil, message: return_message)
                                        }
                                        
                                        if DataStore.naviTitleName == "가입" {
                                            SKHUD.hide()
                                            AlertView().alert(self, title: nil, message: "가입이 완료되었습니다.", cancelButtonTitle: "확인", otherButtonTitles: [], block: { (AlertView, _) in
                                                self.dismiss(animated: true)
                                            })
                                        }
                                        else if ReservationModel.shared.cleaning_type == "2" { // 이사 입주일 경우 예약하고 완료 페이지로
                                            self.createReservation()
                                        }
                                        else { // 가사도우미일 경우
                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestsViewController") as! RequestsViewController
                                            self.navigationController?.pushViewController(vc, animated: true)
                                            SKHUD.hide()
                                        }
                                        
                                        // 유저정보 저장
                                        DataStore.userModel = UserModel()
                                        let mapper = Mapper<UserModel>()
                                        if let user = json["result"].rawString() {
                                            DataStore.userModel = mapper.map(JSONString: user)
                                        }
                                        
                                        let homeVC = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers[0] as? HomeViewController
                                        homeVC?.setLeftBarButton(type: .My, action: #selector(homeVC?.clickMenu))
                                        (UIApplication.shared.keyWindow?.rootViewController as? MainController)?.sideMenu.tableView.reloadData()
                                        
                                        //채널 이름, seq로 재로그인
                                        if let id = DataStore.userModel?.seq, let name = DataStore.userModel?.name, let phone = DataStore.userModel?.phone_number {
                                            ChannelIO.shutdown()
                                            let settings = ChannelPluginSettings()
                                            settings.userId = "\(id)"
                                            let profile = Profile()
                                            profile.set(name: name)
                                                .set(mobileNumber: "+82\(phone)")
                                            ChannelIO.boot(with: settings, profile:profile)
                                        }
                                    }
            })
        } else { // 로그인 되어있는 경우
            if ReservationModel.shared.cleaning_type == "2" { // 이사 입주일 경우
                SKHUD.show()
                self.createReservation()
            } else { // 가사 도우미일 경우
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestsViewController") as! RequestsViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func createReservation() {
        NetworkManager.createReservation(cleaning_space: self.getUnWrappingValue(self.tfArea.text),
                                         block: { (jsonData, error) in
                                            if let networkError = error as? NetworkError {
                                                AlertView().alert(self, title: nil,
                                                                  message: networkError.description)
                                            }
                                            else {
                                                let json = JSON(jsonData!)
                                                let isUnPaid = json["result"]["is_unpaid"].bool
                                                
                                                if isUnPaid == true { // 미결제가 있을경우
                                                    let payPopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PayPopUpViewController") as! PayPopUpViewController
                                                    payPopUpVC.style = .WhenTapButton
                                                    
                                                    payPopUpVC.service_date = json["result"]["service_date"].string
                                                    payPopUpVC.service_cycle = json["result"]["service_cycle"].string
                                                    payPopUpVC.unpaid_price = json["result"]["unpaid_price"].int
                                                    payPopUpVC.fail_msg = json["result"]["fail_msg"].string
                                                    
                                                    let mapper = Mapper<ReservationModel>()
                                                    if let reservation_infos = json["result"]["reservation_infos"].rawString() {
                                                        payPopUpVC.reservationModel = mapper.map(JSONString: reservation_infos)
                                                    }
                                                    
                                                    self.addChildViewController(payPopUpVC)
                                                    
                                                    payPopUpVC.view.frame = self.view.frame
                                                    self.view.addSubview(payPopUpVC.view)
                                                    payPopUpVC.didMove(toParentViewController: self)
                                                } else { // 미결제가 없을때
                                                    let return_message = json["return_message"].string!
                                                    if return_message != "" {
                                                        AlertView().alert(self, title: nil, message: return_message)
                                                    }
                                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "FinishReserveViewController") as! FinishReserveViewController
                                                    self.navigationController?.pushViewController(vc, animated: true)
                                                }
                                            }
                                            SKHUD.hide()
        })
    }
    
    // MARK: - 인증 버튼 클릭
    @IBAction func clickAuth(_ sender: Any) {
        
        if isAuthenticating == true {
            return
        }
        isAuthenticating = true
        
        SKHUD.show()
        
        NetworkManager.requestSMS(phone_number: getUnWrappingValue(tfContact.text)) { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil,
                                  message: networkError.description)
                self.isAuthenticating = false
            }
            else {
                let return_message = JSON(jsonData!)["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                self.tfAuth.becomeFirstResponder()
                self.tfAuth.text = ""
                
                self.confirmButton.borderColor = UIColor.placeholder
                self.confirmButton.isEnabled = false
                self.confirmTouchViewButton.isEnabled = false
                self.tfContact.isEnabled = false
                self.authButton.borderColor = UIColor.placeholder
                
                self.setLine(tf: self.tfAuth)
                
                // 타이머 시간 설정 (180초)
                self.remainTime = 180
                self.lbTimer.text = "\(self.remainTime / 60)분 \(self.remainTime % 60)초 남았습니다"
                
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
                
                UIView.animate(withDuration: 0.1){
                    self.setTimerView()
                    self.view.layoutIfNeeded()
                }
                self.registerBackgroundTask()
            }
            
            SKHUD.hide()
        }
    }
    
    // 확인 버튼 클릭
    @IBAction func clickConfirm(_ sender: Any) {
        dismissKeyboard()
        
        SKHUD.show()
        
        NetworkManager.confirmSMS(phone_number: getUnWrappingValue(tfContact.text),
                                  auth_code: getUnWrappingValue(tfAuth.text)) { (jsonData, error) in
                                    if let networkError = error as? NetworkError {
                                        AlertView().alert(self, title: nil,
                                                          message: networkError.description)
                                    }
                                    else {
                                        let json = JSON(jsonData!)
                                        let return_message = json["return_message"].string!
                                        if return_message != "" {
                                            AlertView().alert(self, title: nil, message: return_message)
                                        }
                                        
                                        self.completeAuth = json["return_code"].boolValue
                                        
                                        UIView.animate(withDuration: 0.1){
                                            self.setConfirmView()
                                            self.view.layoutIfNeeded()
                                        }
                                        self.endBackgroundTask()
                                    }
                                    SKHUD.hide()
        }
    }
    
    @IBAction func clickPersonalInfo(_ sender: Any) {
        let vc = InformationViewController(nibName: "InformationViewController", bundle: nil)
        DataStore.naviTitleName = "개인정보 취급방침"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickServiceInfo(_ sender: Any) {
        let vc = InformationViewController(nibName: "InformationViewController", bundle: nil)
        DataStore.naviTitleName = "서비스 이용약관"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // 숫자 입력개수에 따른 버튼 활성화/비활성화
    @IBAction func editTfContact(_ sender: Any) {
        if (tfContact.text?.count)! < 8 {
            authButton.isEnabled = false
            authTouchViewButton.isEnabled = false
            authButton.borderColor = UIColor.placeholder
        } else {
            authButton.isEnabled = true
            authTouchViewButton.isEnabled = true
            authButton.borderColor = UIColor.main
        }
    }
    
    @IBAction func editTfAuth(_ sender: Any) {
        if (tfAuth.text?.count)! < 4 {
            confirmButton.isEnabled = false
            confirmTouchViewButton.isEnabled = false
            confirmButton.borderColor = UIColor.placeholder
        } else {
            confirmButton.isEnabled = true
            confirmTouchViewButton.isEnabled = true
            confirmButton.borderColor = UIColor.main
        }
    }
    
    // 주소 검색
    @IBAction func clickAddress(_ sender: UIButton?) {
        
        if DataStore.userModel != nil {
            //주소 리스트로 이동
            let addressVC = AddressListViewController(nibName: "AddressListViewController", bundle: nil)
            //            let naviVC = UINavigationController(rootViewController: addressVC)
            navigationController?.pushViewController(addressVC, animated: true)
        } else {
            let postcodeView = PostCodeWebViewController.init(nibName: PostCodeWebViewController.className, bundle: nil)
            postcodeView.delegate = self
            
            let transition = CATransition()
            transition.duration = 0.2
            transition.type = kCATransitionMoveIn
            transition.subtype = kCATransitionFromTop
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            
            self.showViewController(postcodeView, animated: false)
        }
    }
    
    func showAddressDetailView() {
        // 상세주소뷰 생성
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
            self.address2View.isHidden = false
            self.address2TopMargin.constant = 27
            self.address2Height.constant = 50
        }
    }
    
    @objc func updateTimer() {
        remainTime -= 1
        lbTimer.text = "\(self.remainTime / 60)분 \(self.remainTime % 60)초 남았습니다"
        
        if remainTime == 0 {
            completeAuth = false
            setConfirmView()
            self.endBackgroundTask()
            lbAuthRequire.isHidden = true
        }
    }
    
    // 초기상태
    func setDefaultView() {
        lbAuthRequire.isHidden = true
        for view in timerViews {
            view.isHidden = true
        }
        defaultConstraint.isActive = true
        timerConstraint.isActive = false
        lbAuthConstraint.isActive = false
        
        // 상세주소 숨김
        self.address2View.isHidden = true
        self.address2TopMargin.constant = 0
        self.address2Height.constant = 0
    }
    
    // 타이머 상태
    func setTimerView() {
        lbAuthRequire.isHidden = true
        for view in timerViews {
            view.isHidden = false
        }
        defaultConstraint.isActive = false
        lbAuthConstraint.isActive = false
        timerConstraint.isActive = true
    }
    
    // 인증 후 상태
    func setConfirmView() {
        lbAuthRequire.isHidden = false
        for view in timerViews {
            view.isHidden = true
        }
        defaultConstraint.isActive = false
        lbAuthConstraint.isActive = true
        timerConstraint.isActive = false
    }
    
    // 비어있는 텍스트필드를 검사해서 다음버튼 활성화
    @objc func checkFormIsFill() {
        
        // 비어있는 텍스트필드를 필터
        let checkedTfArray = tfArray.filter { (tf: SKTextField) -> Bool in
            if tf == tfRecommand { // 추천코드는 제외
                return false
            }
            if ReservationModel.shared.cleaning_type != "2" && tf == tfArea { //이사 입주일경우 세팅
                return false // 평수 제외
            }
            return (tf.text?.isEmpty)!
        }
        
        // 하나라도 빈 텍스트필드가 있으면 비활성화
        if !checkedTfArray.isEmpty || completeAuth == false {
            nextButton.setNextButton(bool: false)
            titleViewHide(isHide: false)
        } else {
            nextButton.setNextButton(bool: true)
            titleViewHide(isHide: true)
        }
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        scrollView.contentInset.bottom = keyboardFrame.size.height - nextButton.frame.height
        
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        
        scrollView.contentInset = UIEdgeInsets.zero
        selectedLine?.backgroundColor = UIColor.placeholder
    }
    
    func titleViewHide(isHide: Bool) {
        subTitleView.isHidden = isHide
        
        if isHide {
            topConstration.constant = 0
        } else {
            topConstration.constant = 40
        }
    }
    
}

extension UserInputViewController: PostCodeDelegate {
    func didSelectAddr(_ addr: String, postcode: String) {
        address.address1 = addr
        address.zipcode = postcode
        tfAddress.text = addr //텍스트 필드에 입력
        showAddressDetailView()
    }
    
    func didDisappear() {
        
    }
}

extension UserInputViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextTextField = textField.superview?.viewWithTag(textField.tag+1) {
            nextTextField.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if !isAuthenticating && completeAuth == false && textField.tag > 1 {
            setConfirmView()
        }
        setLine(tf: textField)
        
        // 주소일때 주소검색 웹뷰로 이동
        if textField == tfAddress {
            clickAddress(nil)
        }
        
        return true
    }
    
    func setLine(tf: UITextField) {
        // 라인의 tag를 해당 텍스트필드 tag+100으로 설정 후 검사
        if let line = tf.superview?.viewWithTag(tf.tag+100) {
            selectedLine?.backgroundColor = UIColor.placeholder
            line.backgroundColor = UIColor.main
            selectedLine = line
        }
    }
}
