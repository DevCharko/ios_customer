//
//  OnceDateViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  캘린더에서 날짜 선택
//

import UIKit
import FirebaseAnalytics

/// 날짜 선택 뷰컨트롤러
class OnceDateViewController: SKPViewController {
    
    var currentCalendar: Calendar?
    var shouldShowDaysOut = false // jingyu : 이전달, 다음달 날짜 보이기
    var selectedDay:DayView?
    
    @IBOutlet weak var subTitleView: UIView!
    @IBOutlet weak var noticeView: UIView!
    @IBOutlet weak var calendarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noticeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var nextButton: NextButton!
    
    //    @IBOutlet weak var nextBottonTopConstraint: NSLayoutConstraint!
    //    @IBOutlet weak var nextBottonBottomConstraint: NSLayoutConstraint!
    
    // 오늘 이후 예약 가능한 날짜
    var possibleDay:Double = 14
    
    override func awakeFromNib() {
        currentCalendar = Calendar.init(identifier: .gregorian)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //                if ReservationModel.shared.cleaning_type! == "2" { //이사입주
        //                    commentView.isHidden = true
        ////                    nextBottonTopConstraint.priority = UILayoutPriority(rawValue: 1)
        ////                    nextBottonBottomConstraint.priority = UILayoutPriority(rawValue: 1000)
        //                } else {
        //                    commentView.isHidden = false
        ////                    nextBottonTopConstraint.priority = UILayoutPriority(rawValue: 1000)
        ////                    nextBottonBottomConstraint.priority = UILayoutPriority(rawValue: 1)
        //                }
        
        //        setNavigationTitle(title: DataStore.naviTitleName)
        if ReservationModel.shared.order_type == .Once {
            setNavigationTitle(title: "정기서비스 주기 선택")
        } else {
            setNavigationTitle(title: "정기서비스 주기 선택")
        }
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        setCalendarLabel()
        
        leftButton.isExclusiveTouch = true
        rightButton.isExclusiveTouch = true
        calendarView.isExclusiveTouch = true
        
        initTextView()
        hideKeyboardWhenTappedAround()
        setupViewResizerOnKeyboardShown()
        calendarView.contentController.scrollView.accessibilityScroll(.previous)
        calendarView.contentController.scrollView.isMultipleTouchEnabled = false
        
        // noticeView default hidden.
        subTitleHidden(isHidden: false)
        setHiddenNoticeView(isHidden: true)
        
        if ReservationModel.shared.order_type == .Once {
            calendarView.coordinator.isMultiSelect = true
        } else {
            calendarView.coordinator.isMultiSelect = false
        }
    }
    
    func subTitleHidden(isHidden: Bool) {
        subTitleView.isHidden = isHidden
        
        if isHidden && noticeView.isHidden {
            calendarTopConstraint.constant = 0
        } else if isHidden && !noticeView.isHidden {
            calendarTopConstraint.constant = 100
            noticeTopConstraint.constant = 0
        } else if !isHidden && noticeView.isHidden {
            calendarTopConstraint.constant = 40
            noticeTopConstraint.constant = 40
        } else {
            calendarTopConstraint.constant = 140
            noticeTopConstraint.constant = 40
        }
    }
    
    func setHiddenNoticeView(isHidden: Bool) {
        noticeView.isHidden = isHidden
        
        if !isHidden && ReservationModel.shared.order_type != .Once {
            noticeView.isHidden = true
        }
        subTitleHidden(isHidden: subTitleView.isHidden)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        calendarView.commitCalendarViewUpdate()
        menuView.commitMenuViewUpdate()
    }
    
    func setCalendarLabel() {
        if let currentCalendar = currentCalendar {
            let dateText = CVDate(date: Date(), calendar: currentCalendar).koreanDescription
            let words = dateText.components(separatedBy: " ")
            
            yearLabel.text = words[0]
            monthLabel.text = words[1]
        }
    }
    
    @IBAction func clickPreMonth(_ sender: Any) {
        guard let calendar = currentCalendar else {
            return
        }
        guard !calendarView.contentController.scrollView.isDragging else {
            return
        }
        guard let date = calendarView.presentedDate.convertedDate(calendar: calendar) else {
            return
        }
        self.nextButton.setNextButton(bool: false)
        subTitleHidden(isHidden: false)
        
        // 현재 날짜보다 같거나 작으면 왼쪽버튼 막음
        if let prevMonth = calendar.date(byAdding: .month, value: -1, to: date) {
            if prevMonth.compare(Date()) == .orderedAscending {
                leftButton.isEnabled = false
            }
        }
        
        calendarView.loadPreviousView()
    }
    
    @IBAction func clickNextMonth(_ sender: Any) {
        if calendarView.contentController.scrollView.isDragging {
            return
        }
        
        self.nextButton.setNextButton(bool: false)
        subTitleHidden(isHidden: false)
        leftButton.isEnabled = true
        calendarView.loadNextView()
    }
    
    @IBAction func clickNextButton(_ sender: Any) {
        guard let calendar = currentCalendar else {
            return
        }
        guard let date = selectedDay?.date.convertedDate(calendar: calendar) else {
            return
        }
        
        //        if tvComment.text != "ex ) 월수금 오전이라면 가능합니다.\n오전 10시 이후 시작도 가능합니다." {
        //            ReservationModel.shared.available_schedule_memo = tvComment.text
        //        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        ReservationModel.shared.date = dateFormatter.string(from: date)
        if ReservationModel.shared.cleaning_type! == "2" { //이사입주
            let vc = storyboard?.instantiateViewController(withIdentifier: "UserInputViewController") as! UserInputViewController
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "SelectTimeViewController") as! SelectTimeViewController
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // 선택된 날짜 제거
    func refreshCalendar(_ date: Date) {
        let controller = calendarView.contentController as! CVCalendarMonthContentViewController
        
        if let day = selectedDay?.date.day {
            if date.getMonthString() == self.selectedDay?.date.convertedDate(calendar: currentCalendar!)?.getMonthString() {
                controller.selectDayViewWithDay(day, inMonthView: controller.presentedMonthView)
                selectedDay = nil
            }
        }
        self.nextButton.setNextButton(bool: false)
        subTitleHidden(isHidden: false)
    }
}

extension OnceDateViewController : CVCalendarViewDelegate, CVCalendarMenuViewDelegate{
    
    func calendar() -> Calendar? {
        return currentCalendar
    }
    
    public func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    public func firstWeekday() -> Weekday {
        return .sunday
    }
    
    func topMarker(shouldDisplayOnDayView dayView: DayView) -> Bool {
        return false
    }
    
    func disableScrollingBeforeDate() -> Date {
        // 현재 날자보다 작거나 같으면 스크롤 비활성화
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyMM01" //마지막 날짜일때 적용이 안될때가 있으므로 해달 월의 첫날로 잡음
        let firstDay = formatter.string(from: date)
        
        return formatter.date(from: firstDay)!
    }
    
    func shouldSelectDayView(_ dayView: DayView) -> Bool {
        let now = Date()
        let twoWeek = Date(timeIntervalSinceNow: 86400 * 14) // 2주 선택 가능 할 수 있게.
        
        if let date = dayView.date.convertedDate(calendar: currentCalendar!), date < now || twoWeek < date {
//            log.debug(">>>>>> \(date) < \(now) <<<<<<")
    
//            self.calendarView.appearance.delegate?.dayLabelColor(by: <#T##Weekday#>, status: <#T##CVStatus#>, present: <#T##CVPresent#>)
//            coordi.disableUserInteractio(dayView: dayView)
            return false
        }
        
        // 정기예약일때 해당 요일만 선택가능
        if ReservationModel.shared.order_type == .Cycle {
            let weekDay = dayView.date?.weekDay(calendar: self.currentCalendar!) ?? .monday // Monday is default
            
            if let days = ReservationModel.shared.days {
                
                if days.contains(weekDay.rawValue - 1) {
                    return true
                } else {
                    return false
                }
            }
        }
        
        return true
    }
    
    func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
        if nextButton == nil {
            return
        }
        
        let year = calendarView.presentedDate.year
        let month = calendarView.presentedDate.month
        
        let dateFomatter = DateFormatter()
        dateFomatter.dateFormat = "\(year)-\(month)-\(dayView.dayLabel.text!)"
        let selectDayString = dateFomatter.string(from: Date())
        
        dateFomatter.dateFormat = "yyyy-M-d"
        let currentDayString = dateFomatter.string(from: Date())
        let currentDayTime = dateFomatter.date(from: currentDayString)
        let nextDay = currentCalendar?.date(byAdding: .day, value: 1, to: currentDayTime!)
        let nextDayString = dateFomatter.string(from: nextDay!)
        
        if nextDayString == selectDayString {
            // -- Charko test
//            if let hour = currentCalendar?.component(.hour, from: Date()) {
//                if hour >= 19 {
//                    AlertView().alert(self, title: nil, message: "고객님, 꼭 맞는 마스터를\n찾아드리기위해 오후 7시 이후엔\n다음날 예약이 어렵습니다.")
//                    selectedDay = nil
//                    nextButton.setNextButton(bool: false)
//                    subTitleHidden(isHidden: false)
//                    return
//                }
//            }
        }
        
        selectedDay = dayView
        
        if calendarView.coordinator.getSelectionSet().count > 0 {
            nextButton.setNextButton(bool: true)
            subTitleHidden(isHidden: true)
            
            if ReservationModel.shared.order_type == .Once {
                setHiddenNoticeView(isHidden: false)
            }
        }
        else {
            nextButton.setNextButton(bool: false)
            subTitleHidden(isHidden: false)
            
            if ReservationModel.shared.order_type == .Once {
                setHiddenNoticeView(isHidden: true)
            }
        }
    }
    
    func preliminaryView(viewOnDayView dayView: DayView) -> UIView {
        if let date = dayView.date.convertedDate(calendar: currentCalendar!) {
            log.debug("\(date)")
        }
        let circleView = CVAuxiliaryView(dayView: dayView, rect: dayView.frame, shape: CVShape.circle)
        // jingyu : 현재날짜 dot 기본 background 컬러
        circleView.fillColor = .placeholder
        return circleView
    }
    
    func preliminaryView(shouldDisplayOnDayView dayView: DayView) -> Bool {
        if (dayView.isCurrentDay) {
            return true
        }
        return false
    }
    
    // 달력 라벨 변경
    func presentedDateUpdated(_ date: CVDate) {
        
        if monthLabel == nil {
            return
        }
        
        if let showingDate = date.convertedDate(calendar: currentCalendar!) {
            self.yearLabel.text = showingDate.getYearString() + "년"
            self.monthLabel.text = showingDate.getSingleMonthString() + "월"
        }
    }
    
    func didShowNextMonthView(_ date: Date) {
        refreshCalendar(date)
        leftButton.isEnabled = true
        
        calendarView.coordinator.setSelectionSet()
    }
    
    func didShowPreviousMonthView(_ date: Date) {
        refreshCalendar(date)
        
        // 현재 날짜보다 같거나 작으면 왼쪽버튼 막음
        let prevMonth = currentCalendar?.date(byAdding: .month, value: -1, to: date)
        if prevMonth?.compare(Date()) == .orderedAscending {
            leftButton.isEnabled = false
        }
        
        calendarView.coordinator.setSelectionSet()
    }
}

extension CVDate {
    // jingyu : 가능한 날짜 키값 비교 위해
    public func getStringForm() -> String {
        return String(self.year * 10000 + self.month * 100 + self.day)
    }
    
    public var koreanDescription: String {
        return "\(year)년 \(month)월"
    }
}

// MARK: - CVCalendarViewAppearanceDelegate

extension OnceDateViewController: CVCalendarViewAppearanceDelegate {
    
    // jingyu : 일요일 토요일 색깔 변경
    func dayOfWeekTextColor(by weekday: Weekday) -> UIColor {
        switch weekday.rawValue {
        case 1: // 일요일
            return UIColor.alert
        case 7: // 토요일
            return UIColor.valid
        default:
            return UIColor.main
        }
    }
    
    func dayLabelColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        switch (weekDay.rawValue, status, present) {
        case (_, .selected, _ ):
            return .white
        case (_, _, .present ):
            return .white
        case (_, .`in`, _ ):
            return .placeholder
        case (1, .out, _ ):
            return .alert
        case (7, .out, _ ):
            return .valid
        default:
            return .main
        }
    }
    
    func dayLabelFont(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIFont {
        switch (status, present) {
        case (.selected, _):
            return UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(10))
        case (_, .present):
            return UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(10))
        default:
            return UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(10))
        }
    }
    
    func dayLabelPresentWeekdayInitallyBold() -> Bool {
        return false
    }
    
    // jingyu : 요일 한글로 표시
    func weekdaySymbolType() -> WeekdaySymbolType {
        return .hangeul
    }
    
}

extension OnceDateViewController: UITextViewDelegate {
    
    func initTextView(){
        //        tvComment.delegate = self
        //        tvComment.inputAccessoryView = getKeyboardToolbar()
        //
        //        if let size = tvComment.font?.pointSize {
        //            tvComment.font = UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(size))
        //        }
        //        tvComment.drawBorder(lineWidth: 1, cornerRadius: 0, color: .line)
        //        tvComment.textColor = UIColor.placeholder
        //        tvComment.textContainer.maximumNumberOfLines = 15
        //        tvComment.textContainer.lineBreakMode = NSLineBreakMode.byTruncatingTail
        //        tvComment.textContainerInset = UIEdgeInsetsMake(13, 13, 13, 13);
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.placeholder {
            textView.text = nil
            textView.textColor = UIColor.main
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "ex ) 월수금 오전이라면 가능합니다.\n오전 10시 이후 시작도 가능합니다."
            textView.textColor = UIColor.placeholder
        }
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        self.scrollView.contentInset.bottom = keyboardFrame.size.height
        
        var offset = scrollView.contentOffset
        offset.y = scrollView.contentSize.height + scrollView.contentInset.bottom - scrollView.bounds.size.height
        scrollView.setContentOffset(offset, animated: true)
        
        //        self.scrollView.setContentOffset(CGPoint(x: 0, y: UIScreen.main.bounds.height - keyboardFrame.size.height + 150), animated: true)
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        self.scrollView.contentInset = UIEdgeInsets.zero
        
    }
    
}
