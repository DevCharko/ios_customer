//
//  CouponShopViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 07/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class CouponShopViewController: SKPViewController, DPopUpOkDelegate {
    func actionOk() {
        if let indexPath = tvCoupon.indexPathForSelectedRow {
            if let item = list[indexPath.row] as? SellCouponModel {
                buyCoupon(coupon_seq: item.coupon_seq, coupon_sales_seq: item.seq)
            } else {
                AlertView().alert(self, title: nil, message: "데이터가 명확하지 않습니다. 구입할 쿠폰을 입력해주세요.")
            }
        }
    }
    

    @IBOutlet weak var lbCredit: UILabel!
    @IBOutlet weak var tvCoupon: UITableView!
    @IBOutlet weak var btnBuy: NextButton!
    
    var list: [SellCouponModel] = []
    var credit: Int = 0
    
    @IBAction func actionBuyCoupon(_ sender: Any) {
        if let dPopUP = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
            "defaultPopUpViewController") as? DefaultPopUpViewController {
            dPopUP.modalPresentationStyle = .overCurrentContext
            dPopUP.modalTransitionStyle = .crossDissolve

            dPopUP.delegate = self
            dPopUP.titles = "정말 구매하시겠습니까?"
            dPopUP.desc = "쿠폰 구매 신청 후 취소가 불가능합니다."
            
            self.present(dPopUP, animated: true)
        }
    }
    
    func buyCoupon(coupon_seq: String, coupon_sales_seq: String) {
        NetworkManager.buyCoupon(coupon_seq: coupon_seq, coupon_sales_seq: coupon_sales_seq, amount: "1") { (jsonData, error) in
            
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            } else {
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code != 1 {
                        let return_message = JSON(jsonData!)["return_message"].string!
                        
                        AlertView().alert(self, title: nil, message: return_message)
                    } else if return_code == 1 {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbCredit.text = "\(credit)".stringFormatDecimal()

        setNavigationTitle(title: "쿠폰 구매")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        tvCoupon.delegate = self
        tvCoupon.dataSource = self
        
        getCouponeSellList()
    }

    
    func getCouponeSellList() {
        SKHUD.show()
        NetworkManager.getSellCouponList() { (jsonData, error) in
            SKHUD.hide()
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            
            if let return_code = JSON(jsonData!)["return_code"].int {
                if return_code != 1 {
                    let return_message = JSON(jsonData!)["return_message"].string!
                    AlertView().alert(self, title: nil, message: return_message)
                    return
                } else {
                    let mapper = Mapper<SellCouponList>()
                    let model = mapper.map(JSONString: JSON(jsonData!).rawString()!)
                    self.list = model?.list ?? []
                    
                    self.tvCoupon.reloadData()
                }
            }
        }
    }
}


extension CouponShopViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "couponCell") as! CouponTableViewCell
        let item = list[indexPath.row]
        
        cell.selectionStyle = .none
        cell.lbCoupon.text = "\(item.coupon_price ?? 0)".stringFormatDecimal()! + "원"
        cell.lbCredit.text = item.saleprice.stringFormatDecimal()
        cell.btnRadio.setImage(#imageLiteral(resourceName: "btn_radio_off"), for: UIControlState.normal)
        cell.btnRadio.setImage(#imageLiteral(resourceName: "btn_radio_on"), for: UIControlState.selected)
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tvCoupon.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        btnBuy.setNextButton(bool: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tvCoupon.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
}
