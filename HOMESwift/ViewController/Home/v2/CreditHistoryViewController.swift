//
//  CreditHistoryViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 04/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import KRPullLoader
import SwiftyJSON
import ObjectMapper

class CreditHistoryViewController: SKPViewController {
    @IBAction func actionRecommenderList(_ sender: Any) {
        let recommender = UIStoryboard(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "bottomPopupViewController") as! BottomPopupViewController

        recommender.modalPresentationStyle = .overCurrentContext
        recommender.modalTransitionStyle = .crossDissolve
        present(recommender, animated: true)
    }
    
    let refreshView = KRPullLoadView()
    
    @IBOutlet weak var tvCleaner: UITableView!
    @IBOutlet weak var lbPoint: UILabel!
    @IBOutlet weak var lbTotalPoint: UILabel!
    
    @IBAction func actionExchange(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "withdrawViewController") as? WithdrawViewController {
            vc.credit = DataStore.userModel?.point ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func actionSellCoupon(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "couponShopViewController") as? CouponShopViewController {
            vc.credit = DataStore.userModel?.point ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    

    var totalPoint: Int = 0
    var point: Int = 0
    var page: Int = 1
    var isUpdate: Bool = true
    
    var pointHistory: [PointHistoryModel] = []
    
//    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
//        return IndicatorInfo(title: "크레딧 내역")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbPoint.text = "\(point)".stringFormatDecimal()
        lbTotalPoint.text = "누적 크레딧 " + ("\(totalPoint)".stringFormatDecimal()!)
        
        tvCleaner.delegate = self
        tvCleaner.dataSource = self
        refreshView.delegate = self

        // Do any additional setup after loading the view.
    }
    
    func getPointLog() {
        SKHUD.show()
        NetworkManager.getPointLog(page: "\(page)") { (jsonData, error) in
            self.page += 1
            
            SKHUD.hide()
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code != 1 {
                        return
                    }
                    
                    let json = JSON(jsonData!)["result"]
                    let totalpage = json["totalpage"].int!
                    
                    if let totalPoint = json["total_point"].string {
                        self.lbTotalPoint.text = "누적 크레딧 " + "\(totalPoint)".stringFormatDecimal()!
                    } else {
                        self.lbTotalPoint.text = "누적 크레딧 " + "00"
                    }
                    
                    if let point = json["point"].string {
                            self.lbPoint.text = "\(point)".stringFormatDecimal()
                    } else {
                        self.lbPoint.text = "00"
                    }
                    
                    let mapper = Mapper<PointHistoryList>()
                    
                    let model = mapper.map(JSONString: json.rawString()!)
                    
                    if self.page == 2 {
                        self.pointHistory = []
                        self.pointHistory = model?.list ?? []
                    } else {
                        for item in model?.list ?? [] {
                            self.pointHistory.append(item)
                        }
                    }
                    
                    if totalpage == self.page - 1 {
                        self.isUpdate = false
                        self.page -= 1
                    }
                    
                    self.tvCleaner.reloadData()   
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        page = 1
        isUpdate = true
        getPointLog()
        
        tvCleaner.addPullLoadableView(refreshView, type: .refresh)

    }
}

extension CreditHistoryViewController: KRPullLoadViewDelegate {
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        if type == .loadMore {
            switch state {
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    completionHandler()
                    self.page = 1
                    self.isUpdate = true
                    self.pointHistory = []
                    self.getPointLog()
                }
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
//                pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                pullLoadView.messageLabel.text = ""
            } else {
//                pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
//            pullLoadView.messageLabel.text = "불러오는중..."
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                completionHandler()
                self.page = 1
                self.pointHistory = []
                self.isUpdate = true
                self.getPointLog()
            }
        }
    }
}

extension CreditHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = pointHistory[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cleanerCell") as! CleanerTableViewCell
        
        let splitItems = item.createAt.split(separator: " ")
        
        if let first = splitItems.first {
            cell.lbDate.text = "\(first)"
        } else {
            cell.lbDate.text = item.createAt
        }
        cell.lbContent.text = item.comment
        
        if item.income != "0" {
            cell.point.text = "+" +  "\(item.income ?? "0")".stringFormatDecimal()!
        } else if item.outcome != "0" {
            cell.point.text = "-\(item.outcome ?? "0")".stringFormatDecimal()
        } else {
            cell.point.text = "0"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.pointHistory.count - 1 {
            if isUpdate {
                getPointLog()
            }
        }
    }
}
