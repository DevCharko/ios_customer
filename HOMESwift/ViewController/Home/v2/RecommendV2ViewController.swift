//
//  RecommendV2ViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 04/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import MessageUI

class RecommendV2ViewController: SKPViewController {
    
    @IBAction func actionCreditTerms(_ sender: Any) {
        if let creditDesc = UIStoryboard(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "creditDescViewController") as? CreditDescViewController {
            self.navigationController?.pushViewController(creditDesc, animated: true)
        }
    }
    
    @IBAction func actionSendKakao(_ sender: Any) {
        let text = "\(String(format: DataStore.settings?.recommend_comment?.replace(target: "%s", withString: DataStore.userModel?.recommend_code ?? "") ?? ""))\nhttps://web.homemaster.co.kr/r/\(DataStore.userModel?.recommend_code ?? "")"
        let textShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func actionSendSMS(_ sender: Any) {
        let messageVC = MFMessageComposeViewController()
        messageVC.body = "\(String(format: DataStore.settings?.recommend_comment?.replace(target: "%s", withString: DataStore.userModel?.recommend_code ?? "") ?? ""))\nhttps://web.homemaster.co.kr/r/\(DataStore.userModel?.recommend_code ?? "")"
        messageVC.messageComposeDelegate = self
        
        self.present(messageVC, animated: true, completion: nil)
    }
    
    @IBAction func actionCodeCopy(_ sender: Any) {
        UIPasteboard.general.string = DataStore.userModel?.recommend_code
        AlertView().alert(self, title: nil, message: "복사되었습니다.")
    }
    
    @IBOutlet weak var btnCode: UIButton!
    @IBOutlet weak var lbSubject: UILabel!
    @IBOutlet weak var lbRecommendedPoint: UILabel!
    
//    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
//        return IndicatorInfo(title:"추천하기")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCode.setTitle(DataStore.userModel?.recommend_code, for: .normal)
        
        let subject_point = "\(DataStore.settings?.subject_point ?? 15000)".stringFormatDecimal()
        let recommender_point = "\(DataStore.settings?.recommender_point ?? 20000)".stringFormatDecimal()
        
        lbSubject.text = "\(subject_point ?? "15,000")원을 선물하고\n\(recommender_point ?? "20,000") 크레딧을 받으세요"
        
        lbRecommendedPoint.text = "친구가 첫 클리닝을 완료하면\n회원님에게 현금으로 변환 가능한\n\(recommender_point ?? "20,000") 크레딧을 지급해드립니다."
        
    }

}

extension RecommendV2ViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        self.dismiss(animated: true) { () -> Void in
            switch (result) {
                
            case .cancelled:
                break
                
            case .failed:
                break
                
            case .sent:
                AlertView().alert(self, title: nil, message: "문자로 공유하기를 완료하였습니다.")
                break
            }
        }
    }
}
