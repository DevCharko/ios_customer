//
//  CancelListViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 01/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class CancelListViewController: SKPViewController {

    @IBOutlet weak var tvCancel: UITableView!
    @IBOutlet weak var nextButton: NextButton!
    
    var isRemain: Bool = false
    var reservationSeq: String?
    
    var cancelReson: [String] = []
    var inputData: String = ""
    
    @IBAction func actionCancel(_ sender: Any) {
        
        if let indexpath = tvCancel.indexPathForSelectedRow {
            var reason: String = ""
            if indexpath.row == cancelReson.count - 1 {
                reason = inputData
            } else {
                reason = cancelReson[indexpath.row]
            }
            
            if isRemain {
                allCancelRemain(reason: reason)
            } else {
                allCancel(reason: reason)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCancelList()
        
        setNavigationTitle(title: "서비스 해지하기")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        nextButton.setNextButton(bool: false)
        
        let cell = UINib(nibName: "RadioCell", bundle: nil)
        tvCancel.register(cell, forCellReuseIdentifier: "RadioCell")
    
        tvCancel.delegate = self
        tvCancel.dataSource = self
        
        
    }
    
    func getCancelList() {
        for item in (DataStore.settings?.cancel_reason)! {
            if item.level == "1" {
                cancelReson.append(item.contents ?? "none")
            }
        }
    }
    
    func allCancelRemain(reason: String) {
        SKHUD.show()
        if let seq = reservationSeq {
            NetworkManager.cancelReservationIsRemain(reservation_seq: seq, reason: reason, is_all: true, is_remain: true) { (jsonData, error) in
                if let networkError = error as? NetworkError {
                    AlertView().alert(self, title: nil,
                                      message: networkError.description)
                } else {
                    let json = JSON(jsonData!)
                    let return_message = json["return_message"].string!
                    if return_message != "" {
                        AlertView().alert(self,
                                          title: nil,
                                          message: return_message,
                                          cancelButtonTitle: "확인",
                                          otherButtonTitles: []) {
                                            (_ alert: AlertView, _ buttonIndex: Int) in
                                                self.navigationController?.popToRootViewController(animated: true)
                                            }
                        }
                    }
                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.reservation_cancel_completed.rawValue)
                }
                SKHUD.hide()
            }
        }
    
    func allCancel(reason: String) {
        SKHUD.show()
        if let seq = reservationSeq {
            NetworkManager.cancelReservation(reservation_seq: seq, reason: reason, is_all: true) { (jsonData, error) in
                if let networkError = error as? NetworkError {
                    AlertView().alert(self, title: nil,
                                      message: networkError.description)
                } else {
                    let json = JSON(jsonData!)
                    let return_message = json["return_message"].string!
                    if return_message != "" {
                        AlertView().alert(self,
                                          title: nil,
                                          message: return_message,
                                          cancelButtonTitle: "확인",
                                          otherButtonTitles: []) {
                                            (_ alert: AlertView, _ buttonIndex:Int) in
                                                self.navigationController?.popToRootViewController(animated: true)
                                            
                                            }
                    }
                }
                FirebaseAnalyticsManager.shared.sendLog(ScreenName.reservation_cancel_completed.rawValue)
                SKHUD.hide()
            }
        }
    }
}

extension CancelListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RadioCell.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cancelReson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RadioCell
        cell.selectionStyle = .none
        cell.leftButton.setImage(#imageLiteral(resourceName: "btn_radio_off"), for: UIControlState.normal)
        cell.leftButton.setImage(#imageLiteral(resourceName: "btn_radio_on"), for: UIControlState.selected)
        cell.leftButton.isUserInteractionEnabled = false
        cell.rightLabel.isHidden = true
        
        cell.nameLabel.text = cancelReson[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == cancelReson.count - 1 {
            let alert = UIAlertController(title: "취소사유를 입력해 주세요", message: nil, preferredStyle: .alert)
            let cell = tvCancel.cellForRow(at: indexPath) as! RadioCell
            
            alert.addAction(UIAlertAction(title: "취소", style: .cancel) { (UIAlertAction) in
                if self.inputData == "" {
                    cell.nameLabel.text = "직접입력"
                    cell.lineView.isHidden = false
                    tableView.deselectRow(at: indexPath, animated: true)
                    self.nextButton.setNextButton(bool: false)
                }
            })
            alert.addTextField(configurationHandler: nil)
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (UIAlertAction) in
                if alert.textFields?.first?.text != "" {
                    self.inputData = (alert.textFields?.first?.text)!
                    cell.nameLabel.text = alert.textFields?.first?.text
                    cell.lineView.isHidden = true
                } else {
                    self.inputData = ""
                    cell.nameLabel.text = "직접입력"
                    cell.lineView.isHidden = false
                    tableView.deselectRow(at: indexPath, animated: true)
                    if tableView.indexPathsForSelectedRows == nil {
                        self.nextButton.setNextButton(bool: false)
                    }
                }
            }))
            self.present(alert, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tvCancel.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        nextButton.setNextButton(bool: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tvCancel.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
}
