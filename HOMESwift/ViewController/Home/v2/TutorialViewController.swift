//
//  TutorialViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 25/02/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class TutorialViewController: SKPViewController {
    
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var svTutoImage: UIScrollView!
    
    lazy var getPage: [UIViewController] = {
        return [self.getVC(viewController: "tutoImage00"),
                self.getVC(viewController: "tutoImage01"),
                self.getVC(viewController: "tutoImage02"),
                self.getVC(viewController: "tutoImage03")]
    }()
    
    lazy var getWidth: CGFloat = {
        return svTutoImage.frame.width
    }()
    
    lazy var getHeight: CGFloat = {
        return svTutoImage.frame.height
    }()
    
    @IBAction func startAction(_ sender: Any) {
        DataStore.isTutorial = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        svTutoImage.delegate = self
//        setScrollView()
        self.setStatusBarColor()
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    override func viewDidLayoutSubviews() {
        setScrollView()
    }
    
    func setScrollView() {
        svTutoImage.contentSize = CGSize(width: getWidth * CGFloat(getPage.count), height: getHeight)
        
        print(">>>>>> getWidth * getPage : \(getWidth) * \(getPage.count)")
        
        for index in 0..<getPage.count {
            if let view = getPage[index].view {
                view.frame = CGRect(x: getWidth * CGFloat(index), y: 0, width: getWidth, height: getHeight)
                
                print(">>>>>> getWidth * index : \(getWidth) * \(index) = \(getWidth * CGFloat(index))")
                svTutoImage.addSubview(view)
            }
        }
    }
    
    func getVC(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
}


extension TutorialViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let nowPage = scrollView.contentOffset.x / getWidth
        
        pager.currentPage = NSInteger(nowPage)
    }
}
