//
//  CancelNoticeViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 28/02/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class CancelNoticeViewController: SKPViewController {

    @IBAction func actionClose(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBOutlet weak var btnReservationCancel: UIButton!
    
    @IBAction func actionOk(_ sender: Any) {
        dismiss(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
