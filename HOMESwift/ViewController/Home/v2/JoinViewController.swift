//
//  JoinViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 25/02/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import ChannelIO
import FirebaseAnalytics
import FacebookCore


class JoinViewController: SKPViewController {
    
    var phoneNumber: String?
    var address: Address = Address()
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var lineName: UIView!
    
    @IBOutlet weak var tfAddress1: UITextField!
    @IBAction func actionAddress(_ sender: Any) {
        
        let postcodeView = PostCodeWebViewController.init(nibName: PostCodeWebViewController.className, bundle: nil)
        postcodeView.delegate = self
        
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        startBtnEnabled(isEnabled: false)
        
        self.showViewController(postcodeView, animated: false)
    }
    
    @IBOutlet weak var viewAddressTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewAddress2: UIView!
    @IBOutlet weak var tfAddress2: UITextField!
    @IBOutlet weak var lineAddress2: UIView!
    
    @IBOutlet weak var tfRecommended: UITextField!
    @IBOutlet weak var lineRecommended: UIView!
    
    @IBOutlet weak var btnStart: UIButton!
    @IBAction func actionStart(_ sender: Any) {
        sendJoin()
    }
    
    @IBAction func ActionChanges(_ sender: Any) {
        isActivity()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (phoneNumber == nil) {
            self.dismiss(animated: true, completion: nil)
        }
        self.navigationController?.isNavigationBarHidden = true
//        DataStore.naviTitleName = "가입"
        
        setStatusBarColor()
        
        initAddresView()
        initTextfield()
    }
    
    func initAddresView() {
        viewAddress2.isHidden = true
        viewAddressTopConstraint.constant = 34
    }
    
    func initTextfield() {
        tfName.delegate = self
        tfAddress2.delegate = self
        tfRecommended.delegate = self
    }
    
    func showAddressDetailView() {
        // 상세주소뷰 생성
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
            self.viewAddress2.isHidden = false
            self.viewAddressTopConstraint.constant = 64
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func isActivity() {
        guard tfName.text?.count ?? 0 > 0 else {
            startBtnEnabled(isEnabled: false)
            return
        }
        
        guard tfAddress1.text?.count ?? 0 > 0 else {
            startBtnEnabled(isEnabled: false)
            return
        }
        
        guard tfAddress2.text?.count ?? 0 > 0 else {
            startBtnEnabled(isEnabled: false)
            return
        }
        
        startBtnEnabled(isEnabled: true)
    }
    
    func startBtnEnabled(isEnabled: Bool) {
        if isEnabled {
            btnStart.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            btnStart.setTitleColor(UIColor.init(cgColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)), for: .normal)
            
        } else {
            btnStart.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.6470588235, blue: 0.7294117647, alpha: 1)
            btnStart.setTitleColor(UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)), for: .normal)
        }
        
        btnStart.isEnabled = isEnabled
    }
    
    func sendJoin() {
        SKHUD.show()
        
        NetworkManager.signUp(name: getUnWrappingValue(tfName.text),
                              phone_number: getUnWrappingValue(phoneNumber),
                              address1: getUnWrappingValue(tfAddress1.text),
                              address2: getUnWrappingValue(tfAddress2.text),
                              recommend_code: getUnWrappingValue(tfRecommended.text)) { (jsonData, error) in
                                SKHUD.hide()
                                
                                if let networkError = error as? NetworkError {
                                    AlertView().alert(self, title: nil, message: networkError.description)
                                }
                                else {
                                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.signup_complete.rawValue)
                                    AppEventsLogger.log(.completedRegistration())
                                    
                                    let json = JSON(jsonData!)
                                    let return_message = json["return_message"].string!
                                    if return_message != "" {
                                        AlertView().alert(self, title: nil, message: return_message)
                                        SKHUD.hide()
                                        return
                                    }
                                
                                    AlertView().alert(self, title: nil, message: "가입이 완료되었습니다.", cancelButtonTitle: "확인", otherButtonTitles: [], block: { (AlertView, _) in
                                        self.dismiss(animated: true)
                                    })

                                    // 유저정보 저장
                                    
                                    DataStore.userModel = UserModel()
                                    let mapper = Mapper<UserModel>()
                                    if let user = json["result"].rawString() {
                                        DataStore.userModel = mapper.map(JSONString: user)
                                    }
                                    DataStore.isLogin = true
                                    
                                    let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainController") as! MainController
                                    UIApplication.shared.keyWindow?.rootViewController = mainVC
                                }
        }
    }
    
    
}


extension JoinViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let id = textField.restorationIdentifier {
            switch(id) {
            case "tfName" :
                lineName.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                break
            case "tfAddress2":
                lineAddress2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                break
            case "tfRecommended":
                lineRecommended.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                break
            default:
                break
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let id = textField.restorationIdentifier {
            switch(id) {
            case "tfName" :
                lineName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
                break
            case "tfAddress2":
                lineAddress2.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
                break
            case "tfRecommended":
                lineRecommended.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
                break
            default:
                break
            }
        }
    }
}

extension JoinViewController: PostCodeDelegate {
    func didSelectAddr(_ addr: String, postcode: String) {
        address.address1 = addr
        address.zipcode = postcode
        tfAddress1.text = addr //텍스트 필드에 입력
        showAddressDetailView()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func didDisappear() {
        self.navigationController?.isNavigationBarHidden = true
    }
}
