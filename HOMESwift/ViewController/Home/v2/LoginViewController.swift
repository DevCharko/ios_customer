//
//  LoginViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 25/02/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import UserNotifications

class LoginViewController: SKPViewController {

    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var btnAuthPhone: UIButton!
    
    @IBAction func authPhoneAction(_ sender: Any) {
        sendRequestSMS()
    }
    
    @IBOutlet weak var phoneUnderLine: UIView!
    @IBOutlet weak var lbAccountState: UILabel!
    
    @IBOutlet weak var viewAuthCode: UIView!

    @IBOutlet weak var tfAuthCode: UITextField!
    @IBOutlet weak var authCodeUnderLine: UIView!
    @IBOutlet weak var lbTimer: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    
    @IBAction func authCodeAction(_ sender: Any) {
        confirmAuth()
    }
    
    @IBOutlet weak var termsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewTerms: UIView!
    
    @IBOutlet weak var btnStart: UIButton!
    
    @IBOutlet weak var svPrivateTerms: UIStackView!
    @IBOutlet weak var svServiceTerms: UIStackView!
    private var isAuthenticating = false
    private var remainTime = 180
    private var timer: Timer?
    
    @IBAction func actionChangePhone(_ sender: Any) {
        if tfPhone.text?.count ?? 0 > 9 {
           authBtnEnable(isEnable: true)
        } else {
           authBtnEnable(isEnable: false)
        }
    }
    
    func authBtnEnable(isEnable: Bool) {
        if isEnable {
            btnAuthPhone.borderColor = UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) //UIColor.init(named:"FFFFFF")
            btnAuthPhone.setTitleColor(UIColor.init(cgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)), for: .normal)
        } else {
            btnAuthPhone.borderColor = UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5))
            btnAuthPhone.setTitleColor(UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)), for: .normal)
        }
        btnAuthPhone.isEnabled = isEnable
    }
    
    @IBAction func actionChangeCode(_ sender: Any) {
        if tfAuthCode.text?.count ?? 0 >= 4 {
            confirmBtnEnable(isEnable: true)
        } else {
            confirmBtnEnable(isEnable: false)
        }
    }
    
    func confirmBtnEnable(isEnable: Bool) {
        if isEnable {
            btnConfirm.borderColor = UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            btnConfirm.setTitleColor(UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)), for: .normal)
        } else {
            btnConfirm.borderColor = UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5))
            btnConfirm.setTitleColor(UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)), for: .normal)
        }
        btnConfirm.isEnabled = isEnable
    }
    
    var completeAuth = false {
        didSet {
            if completeAuth == true {
                lbAccountState.text = "인증 완료되었습니다."
                lbAccountState.textColor = UIColor.valid
                btnAuthPhone.isHidden = true
            } else {
                isAuthenticating = false
                lbAccountState.text = "인증이 필요합니다."
                btnAuthPhone.isHidden = false
            }
        }
    }
    
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    
    func endBackgroundTask() {
        log.debug("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
        self.timer?.invalidate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfPhone.delegate = self
        tfAuthCode.delegate = self

        setStatusBarColor()
        isConfirmView(isView: false)
        activatedStartBtn(isActivity: false)
        
        checkDevice()
        registerForRemoteNotification() // 푸시토큰 등록
        
        svPrivateTerms.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.clickedPrivateTerms)))
        svServiceTerms.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.clickedServiceTerms)))
    }
    
    func isConfirmView(isView: Bool) {
        viewAuthCode.isHidden = !isView
        if isView {
            termsTopConstraint.constant = 80 // 80 - 30
            tfAuthCode.becomeFirstResponder()
        } else {
            termsTopConstraint.constant = 30
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func sendRequestSMS() {
        SKHUD.show()
        
        NetworkManager.requestSMS(phone_number: getUnWrappingValue(tfPhone.text)) { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
                self.isAuthenticating = false
            }
            else {
                let return_message = JSON(jsonData!)["return_message"].string!
                self.isConfirmView(isView: true)
                
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                self.enablePhone(isEnabled: false)
                
                // 타이머 시간 설정 (180초)
                self.remainTime = 180
                self.lbTimer.text = "\(self.remainTime / 60)분 \(self.remainTime % 60)초 남았습니다"
                
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
                
                UIView.animate(withDuration: 0.1){
//                    self.setTimerView()
                    self.view.layoutIfNeeded()
                }
                self.registerBackgroundTask()
            }
            
            SKHUD.hide()
        }
    }
    
    func enablePhone(isEnabled: Bool) {
        authBtnEnable(isEnable: isEnabled)
        btnAuthPhone.isEnabled = isEnabled
        tfPhone.isEnabled = isEnabled
    }
    
    func confirmAuth() {
        SKHUD.show()
        
        NetworkManager.loginByPhone(phone_number: getUnWrappingValue(tfPhone.text), code: getUnWrappingValue(tfAuthCode.text)){ (jsonData, error) in
            
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
                self.isAuthenticating = false
            }
            else {
                let json = JSON(jsonData!)
                let return_message = json["return_message"].string!
                if return_message != "" {
                    AlertView().alert(self, title: nil, message: return_message)
                }
                
                if let return_code = json["return_code"].int {
                    if return_code == 2 {
                        self.goJoin()
                    } else if return_code == 1 {
                        self.tfAuthCode.text = ""
                        
                        //완료
                        self.isAuthenticating = true
                        self.completeAuth = true
                        
                        let mapper = Mapper<UserModel>()
                        if let user = json["result"].rawString() {
                            DataStore.userModel = mapper.map(JSONString: user)
                        }
                        DataStore.isLogin = true
                        
                        self.mainStepStart()
                    }
                    
                    self.endBackgroundTask()
                    self.isConfirmView(isView: false)
                }
            }
            SKHUD.hide()
        }
    }
    
    func goJoin() {
        if let joinVC = storyboard?.instantiateViewController(withIdentifier: "joinViewController") as? JoinViewController {
            joinVC.phoneNumber = tfPhone.text
            navigationController?.pushViewController(joinVC, animated: true)
        }
    }
    
    @objc func clickedPrivateTerms() {
        let view = InformationViewController(nibName:"InformationViewController", bundle:nil)
        DataStore.naviTitleName = "개인정보 취급방침"
        navigationController?.pushViewController(view, animated: true)
        navigationController?.isNavigationBarHidden = false
    }
    
    @objc func clickedServiceTerms() {
        let view = InformationViewController(nibName:"InformationViewController", bundle:nil)
        DataStore.naviTitleName = "서비스 이용약관"
        navigationController?.pushViewController(view, animated: true)
        navigationController?.isNavigationBarHidden = false
    }
    
    @objc func updateTimer() {
        remainTime -= 1
        
        lbTimer.text = "\(self.remainTime / 60)분 \(self.remainTime % 60)초 남았습니다"
        
        if remainTime == 0 {
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            
            self.enablePhone(isEnabled: true)
            self.isConfirmView(isView: false)
            
            completeAuth = false

            timer?.invalidate()
            isAuthenticating = false
        }
    }
    
    func mainStepStart() {
        if !isAuthenticating {
            return
        }
        
        activatedStartBtn(isActivity: true)
        view.endEditing(true)
        
        goToMain()
    }
    
    func activatedStartBtn(isActivity: Bool) {
        
        if isActivity {
//            btnStart.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            btnStart.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            btnStart.setTitleColor(UIColor(hex: "000000"), for: .normal)
        } else {
//            btnStart.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            btnStart.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.6470588235, blue: 0.7294117647, alpha: 1)
            btnStart.setTitleColor(UIColor(hex: "FFFFFF"), for: .normal)
        }
        
        btnStart.isEnabled = isActivity
    }
    
    
    func checkDevice() {
        log.info("checkDevice()")
        
        log.debug("Device ID ------")
        log.debug(DeviceInfo.deviceId)
        log.debug("Auth KEY ------")
        log.debug(DataStore.getAuthKey ?? "none")
        log.debug("Push TOKEN ------")
        log.debug(DataStore.getPushToken ?? "none")
        
        DeviceInfo.showDeviceInfo()
        
        if (DataStore.getAuthKey == nil || DataStore.getDeviceId?.count == 0) {
            createDevice()
        } else {
            sendPushToken()
        }
    }
    
    func createDevice() {
        log.info("createDevice()")
        NetworkManager.createDevice(deviceId: DeviceInfo.deviceId) { (jsonData, error) in
            if (jsonData != nil) {
                let json = JSON(jsonData!)
                
                DataStore.setAuthKey(authKey: json["result"]["auth_token"].stringValue)
                
                log.debug("Auth KEY ------")
                log.debug(DataStore.getAuthKey ?? "none")
                log.info("Complete!")
                self.checkDevice()
            }
            else {
                log.error(error!)
                AlertView().alert(self, title: nil, message: "네트워크에 연결 후 다시 시도해 주세요.", cancelButtonTitle: "취소", otherButtonTitles: ["확인"], block: { (AlertView, idx) in
                    if idx == 0 {
                        self.createDevice()
                    } else {
                        exit(0)
                    }
                })
            }
        }
    }
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.sound, .alert]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(
                UIUserNotificationSettings(types: [.sound, .alert], categories: nil)
            )
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func sendPushToken() {
        log.info("sendPushToken()")
        
        if let fcmToken = DataStore.getPushToken {
            NetworkManager.updatePushToken(pushToken: fcmToken) { (jsonData, error) in
                if let networkError = error as? NetworkError {
                    log.error(networkError.description)
                }
            }
        }
    }
    
    func goToMain() {
        let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainController") as! MainController
        UIApplication.shared.keyWindow?.rootViewController = mainVC
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}


extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let id = textField.restorationIdentifier {
            if id == "phoneNum" {
                phoneUnderLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else if id == "authNum" {
                authCodeUnderLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let id = textField.restorationIdentifier {
            if id == "phoneNum" {
                phoneUnderLine.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
            } else if id == "authNum" {
                authCodeUnderLine.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
            }
        }
    }
}
