//
//  RecommenderTableViewCell.swift
//  HOMESwift
//
//  Created by HM_Charko on 04/03/2019.
//  Copyright © 2019 skoopmedia. All rights reserved.
//

import UIKit

class RecommenderTableViewCell: UITableViewCell {

    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbList: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
