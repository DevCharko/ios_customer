//
//  CouponTableViewCell.swift
//  HOMESwift
//
//  Created by HM_Charko on 07/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class CouponTableViewCell: UITableViewCell {
    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var lbCoupon: UILabel!
    @IBOutlet weak var lbCredit: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            btnRadio.isSelected = true
        }
        else {
            btnRadio.isSelected = false
        }
    }

}
