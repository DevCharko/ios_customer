//
//  ReferralViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 04/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class ReferralViewController: SKPViewController {

    @IBOutlet weak var swipeMenuView: SwipeMenuView!
    
    var menu = ["추천하기", "크레딧 내역"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: "추천하고 할인받기")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        swipeMenuView.delegate = self
        swipeMenuView.dataSource = self
        
        var options: SwipeMenuViewOptions = .init()
        options.tabView.additionView.backgroundColor = #colorLiteral(red: 1, green: 0.3005838394, blue: 0.2565174997, alpha: 1)
        options.tabView.itemView.textColor = #colorLiteral(red: 0.2980392157, green: 0.3215686275, blue: 0.368627451, alpha: 1)
        options.tabView.itemView.selectedTextColor = #colorLiteral(red: 0.2980392157, green: 0.3215686275, blue: 0.368627451, alpha: 1)
        options.tabView.style = .segmented
        options.tabView.itemView.font = options.tabView.itemView.font.withSize(20)
        options.tabView.height = 56
        options.tabView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let recommendV2 = storyboard?.instantiateViewController(withIdentifier: "recommendV2ViewController") as! RecommendV2ViewController
        let creditVC = storyboard?.instantiateViewController(withIdentifier: "creditHistoryViewController") as! CreditHistoryViewController
        
        self.addChildViewController(recommendV2)
        self.addChildViewController(creditVC)
        
        swipeMenuView.reloadData(options: options)
    }
}

extension ReferralViewController: SwipeMenuViewDelegate, SwipeMenuViewDataSource {
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        
//        print("will setup SwipeMenuView")
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
        
//        print("did setup SwipeMenuView")
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        
//        print("will change from section\(fromIndex + 1)  to section\(toIndex + 1)")
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, didChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        
//        print("did change from section\(fromIndex + 1)  to section\(toIndex + 1)")
    }
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return menu.count
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return menu[index]
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        let vc = self.childViewControllers[index]
        return vc
    }
}
