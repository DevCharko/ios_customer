//
//  BottomCancelViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 07/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

protocol BottomCancelDelegate {
    func thisTurnCancel()
    func nextTurnAllCancel()
    func allCancel()
}


// %d 회차 취소
// %d 회차 이후 취소
class BottomCancelViewController: UIViewController {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var delegate: BottomCancelDelegate?
    
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var thisCancel: UIButton!
    @IBOutlet weak var nextTurnCancel: UIButton!
    @IBOutlet weak var allCancel: UIButton!
    
    var time: String?
    var thisTime: String?
    var nextCancel: String?
    
    @IBAction func actThisCancel(_ sender: Any) {
        dismiss(animated: true)
        delegate?.thisTurnCancel()
    }
    
    @IBAction func actNextTurnAllCancel(_ sender: Any) {
        dismiss(animated: true)
        delegate?.nextTurnAllCancel()
    }
    
    
    @IBAction func actAllCancel(_ sender: Any) {
        dismiss(animated: true)
        delegate?.allCancel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
        
        if time != nil {
            lbTime.text = time
        }
        
        if thisTime != nil {
            thisCancel.setTitle(thisTime, for: .normal)
            thisCancel.setTitle(thisTime, for: .selected)
        }
        
        if nextTurnCancel != nil {
            nextTurnCancel.setTitle(nextCancel, for: .normal)
            nextTurnCancel.setTitle(nextCancel, for: .selected)
        }
    }
    
    @objc func close() {
        dismiss(animated: true)
    }

}
