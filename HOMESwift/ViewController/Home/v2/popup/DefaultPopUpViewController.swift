//
//  DefaultPopUpViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 28/02/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

protocol DPopUpOkDelegate {
    func actionOk()
}

class DefaultPopUpViewController: SKPViewController {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    
    var titles: String?
    var desc: String?
    
    var cancelText: String?
    var okText: String?
    var delegate: DPopUpOkDelegate?
    
    @IBAction func actionCancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func actionOK(_ sender: Any) {
        dismiss(animated: true)
        
        delegate?.actionOk()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbTitle.text = titles ?? ""
        lbDesc.text = desc ?? ""
        
        if cancelText != nil {
            btnCancel.setTitle(cancelText, for: .normal) 
        }
        
        if okText != nil {
            btnOk.setTitle(okText, for: .normal)
        }
    }
}
