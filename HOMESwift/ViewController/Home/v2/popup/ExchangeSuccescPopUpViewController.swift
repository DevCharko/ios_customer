//
//  ExchangeSuccescPopUpViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 06/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class ExchangeSuccescPopUpViewController: SKPViewController {
    @IBOutlet weak var lbExchangeMoney: UILabel!
    var money: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbExchangeMoney.text = "\("\(money)".stringFormatDecimal() ?? "70,000") 원"
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(3000)) {
            self.dismiss(animated: true)
        }
    }
    
}
