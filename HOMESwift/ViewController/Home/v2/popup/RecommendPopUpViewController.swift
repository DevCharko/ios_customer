//
//  RecommendPopUpViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 28/02/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

protocol GoCouponListDelegate {
    func actionGo()
}

class RecommendPopUpViewController: SKPViewController {
    var delegate: GoCouponListDelegate?
    
    @IBOutlet weak var backView: UIView!
    @IBAction func actionGo(_ sender: Any) {
        delegate?.actionGo()
        self.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeAnimate)))
    }
    
    @objc func removeAnimate() {
        self.dismiss(animated: true)
    }
}
