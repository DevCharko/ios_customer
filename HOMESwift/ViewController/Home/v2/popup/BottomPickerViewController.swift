//
//  BottomPickerViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 05/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class BottomPickerViewController: SKPViewController {
    
    var index: Int = 0
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var pvPicker: UIPickerView!
    
    var resultText: String = ""
    
    @IBAction func actionSend(_ sender: Any) {
        switch self.presentingViewController {
        case let naviVC as UINavigationController:
            if let withVC = naviVC.topViewController as? WithdrawViewController {
                withVC.pickerViewClick(index: index)
            }
        case let withVC as WithdrawViewController:
            withVC.pickerViewClick(index: index)
        default:
            break
        }
        self.dismiss(animated: true)
    }
    
    var list: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pvPicker.delegate = self
        pvPicker.dataSource = self
        
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
    }
    
    @objc func close() {
        self.dismiss(animated: true)
    }
}


extension BottomPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return list[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        index = row
    }
}
