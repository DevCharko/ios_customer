//
//  BottonPopupViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 04/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import KRPullLoader
import SwiftyJSON
import ObjectMapper

class BottomPopupViewController: SKPViewController {
    @IBOutlet weak var lbWaiting: UILabel!
    @IBOutlet weak var tvRecommender: UITableView!
    @IBOutlet weak var backView: UIView!
    
    var isUpdate: Bool = true
    var page: Int = 1
    var recommenders: [ReferralModel] = []
    
    let refreshView = KRPullLoadView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeAnimate)))
        
        tvRecommender.delegate = self
        tvRecommender.dataSource = self
        
        refreshView.delegate = self
        tvRecommender.addPullLoadableView(refreshView, type: .refresh)
        getReferralList()
    }
    
    @objc func removeAnimate() {
        self.dismiss(animated: true)
    }
    
    func getReferralList() {
        SKHUD.show()
        NetworkManager.getReferralList(page: "\(page)") { (jsonData, error) in
            self.page += 1
            SKHUD.hide()
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code != 1 {
                        return
                    }
                    
                    let json = JSON(jsonData!)["result"]
                    let totalpage = json["totalpage"].int!
                    
                    if totalpage == self.page - 1 {
                        self.isUpdate = false
                        self.page -= 1
                    }
                    
                    let mapper = Mapper<ReferralList>()
                    
                    let model = mapper.map(JSONString: json.rawString()!)
                    
                    if self.page == 2 {
                        self.recommenders = model?.list ?? []
                    } else {
                        for item in model?.list ?? [] {
                            self.recommenders.append(item)
                        }
                    }
                    
                    self.tvRecommender.reloadData()
                    self.lbWaiting.text = "\(self.recommenders.count)명"
                }
            }
        }
    }
}

extension BottomPopupViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recommenders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recommender") as! RecommenderTableViewCell
        
        if let splitItems = recommenders[indexPath.row].createdAt?.split(separator: " ") {
            cell.lbDate.text = "\(splitItems.first!)"
        } else {
            cell.lbDate.text = recommenders[indexPath.row].createdAt
        }
        let name = recommenders[indexPath.row].name ?? "*"

        cell.lbList.text = "\(name[0])** 가입을 하였으나\n클리닝을 완료한 적이 없습니다."
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.recommenders.count - 1 {
            if isUpdate {
                getReferralList()
            }
        }
    }
}

extension BottomPopupViewController: KRPullLoadViewDelegate {
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        
        if type == .loadMore {
            switch state {
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                    completionHandler()
                    self.page = 1
                    self.isUpdate = true
                    self.recommenders = []
                    self.getReferralList()
                }
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //                pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                pullLoadView.messageLabel.text = ""
            } else {
                //                pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                pullLoadView.messageLabel.text = ""
            }
            
        case let .loading(completionHandler):
            //            pullLoadView.messageLabel.text = "불러오는중..."
            pullLoadView.messageLabel.text = ""
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                completionHandler()
                self.page = 1
                self.recommenders = []
                self.isUpdate = true
                self.getReferralList()
            }
        }
    }
    
}
