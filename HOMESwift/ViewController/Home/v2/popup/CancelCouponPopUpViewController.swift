//
//  CancelCouponPopUpViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 10/04/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

protocol CancelCouponDelegate {
    func noCoupon()
    func getCoupon()
}

class CancelCouponPopUpViewController: SKPViewController {
    @IBOutlet weak var backView: UIView!
    
    var delegate: CancelCouponDelegate?
    
    @IBAction func actionCancel(_ sender: Any) {
        delegate?.noCoupon()
        self.dismiss(animated: false)
    }
    
    @IBAction func actionGetCoupon(_ sender: Any) {
        delegate?.getCoupon()
        self.dismiss(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
    }
    
    @objc func close() {
        self.dismiss(animated: false)
    }
}
