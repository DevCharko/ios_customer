//
//  CreditDescViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 04/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class CreditDescViewController: SKPViewController {
    @IBOutlet weak var lbHowTo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "홈마스터 크레딧")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        let subject_point = "\(DataStore.settings?.subject_point ?? 15000)".stringFormatDecimal()
        let recommender_point = "\(DataStore.settings?.recommender_point ?? 20000)".stringFormatDecimal()
        
        lbHowTo.text = "회원들은 친구에게 내 코드번호를 복사하여 보내주거나, 카카오톡/문자로 보내주세요. 친구가 홈마스터 가입 시 내 코드번호를 추천인 코드에 입력 후 가입 시 친구에게는 \(subject_point ?? "15,000")원 할인 쿠폰이 발급됩니다.\n친구가 첫 클리닝을 완료하면 추천인 회원님께 홈마스터 클리닝 서비스에 사용하거나 현금으로 변환 가능한 \(recommender_point ?? "20,000") 크레딧을 드립니다. 친구 추천 횟수는 제한이 없습니다."
    }
    
    
    
}
