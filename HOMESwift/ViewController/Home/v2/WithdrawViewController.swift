//
//  WithdrawViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 05/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

// exchangePoint
class WithdrawViewController: SKPViewController, DPopUpOkDelegate {
    func actionOk() {
        NetworkManager.exchangePoint(point: "\(exchangeCredit_Ori)"
            , bankcode: bankList[bankIndex].bankcode
            , accountnumber: tfAccountNum.text ?? ""
        , accountname: tfAccountName.text ?? "") { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code == 1 {
                        self.exchangeSuccessPopUp()
                    } else {
                        let return_message = JSON(jsonData!)["return_message"].string!
                        AlertView().alert(self, title: nil, message: return_message)
                    }
                }
            }
        }
    }
    
    func exchangeSuccessPopUp() {
        navigationController?.popViewController(animated: true)
        
        if let exchangePopUp = self.storyboard?.instantiateViewController(withIdentifier: "exchangeSuccescPopUpViewController") as? ExchangeSuccescPopUpViewController {
            exchangePopUp.money = exchangeCredit
            exchangePopUp.modalPresentationStyle = .overCurrentContext
            exchangePopUp.modalTransitionStyle = .crossDissolve
            
            present(exchangePopUp, animated: true)
        }
    }
    
    func moneyHidden(isHidden: Bool) {
        if isHidden {
            viewMoney.isHidden = true
            viewMoneyTopConstraint.constant = 18
        } else {
            viewMoney.isHidden = false
            viewMoneyTopConstraint.constant = 100
        }
    }
    
    @IBOutlet weak var lbCredit: UILabel!
    @IBOutlet weak var viewMoneyTopConstraint: NSLayoutConstraint! // 100 / 35
    @IBOutlet weak var viewMoney: UIView!
    @IBOutlet weak var lbGetMoney: UILabel!
    @IBOutlet weak var btnMoney: UIButton!
    @IBOutlet weak var btnBank: UIButton!
    @IBOutlet weak var tfAccountNum: UITextField!
    @IBOutlet weak var tfAccountName: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnExchangeMoney: NextButton!
    @IBAction func actionExchangeMoney(_ sender: Any) {
        
        if let defaultPopup = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "defaultPopUpViewController") as? DefaultPopUpViewController {
            defaultPopup.titles = "정말 변환 하시겠습니까?"
            defaultPopup.desc = "현금 변환 신청 후 취소가 불가능합니다.\n*현금 변환 버튼 클릭 시 계좌 정보 저장에 동의하는것으로 간주됩니다."
            
            defaultPopup.modalPresentationStyle = .overCurrentContext
            defaultPopup.modalTransitionStyle = .crossDissolve
            
            defaultPopup.delegate = self
            
            self.present(defaultPopup, animated: true)
        }
    }
    var moneyList: [String] = []
    var bankList: [BankModel] = []
    
    var credit: Int = 0
    var exchangeCredit_Ori: Int = 0
    var exchangeCredit: Int = 0 {
        didSet {
            exchangeCredit_Ori = exchangeCredit
            self.exchangeCredit = Int(Float(exchangeCredit) * 0.7)
        }
    }
    var isMoneyPicker: Bool = true
    
    var unitIndex: Int = 0
    var bankIndex: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbCredit.text = "\(credit)".stringFormatDecimal()
        
        makeMoneyList()
        getBankList()
        moneyHidden(isHidden: true)
        
        setNavigationTitle(title: "현금 변환")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
//          스토리보드에서 이미 히든 설정함.
//        pvMoney.isHidden = true
//        pvBank.isHidden = true
        
        btnExchangeMoney.setNextButton(bool: false)
        btnMoney.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickMoney)))
        btnBank.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickBank)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(RequestsViewController.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RequestsViewController.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -200 // Move view 200 points upward
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    func pickerViewClick(index: Int) {
        if isMoneyPicker {
            btnMoney.setTitle(moneyList[index], for: .normal)
            if index != 0 {
                btnMoney.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                exchangeCredit = Int(moneyList[index].replacingOccurrences(of: ",", with: "")) ?? 0
                
                lbGetMoney.text = "\("\(exchangeCredit)".stringFormatDecimal() ?? "00") 원"
                moneyHidden(isHidden: false)
            } else {
                btnMoney.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
                moneyHidden(isHidden: true)
            }
            unitIndex = index
        } else {
            btnBank.setTitle(bankList[index].bankname, for: .normal)
            btnBank.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            bankIndex = index
        }
        
        isCheckEnableExchange()
    }
    
    @objc func clickMoney() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "bottomPickerViewController") as? BottomPickerViewController {
            vc.list = moneyList
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true)
            isMoneyPicker = true
        }
    }
    
    @objc func clickBank() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "bottomPickerViewController") as? BottomPickerViewController {
            
            var items: [String] = []
            
            for item in bankList {
                items.append(item.bankname)
            }
            
            vc.list = items
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true)
            isMoneyPicker = false
        }
    }

    func makeMoneyList() {
        moneyList.append("10만원 단위로 인출가능")
        
        let value = credit / 100000
        if value > 0 {
            for i in 1...value {
                moneyList.append(String("\(i * 100000)").stringFormatDecimal() ?? "0")
            }
        }
    }
    
    func getBankList() { // banklist를 수행 후 무조건 bankinfo를 가져온다. 단 전송 실패시 가져오지 않으며 SKHUD는 bankinfo에서 없앤다.
        SKHUD.show()
        
        NetworkManager.getBankInfo() { (jsonData, error) in
            if let networkError = error as? NetworkError {
                SKHUD.hide()
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code != 1 {
                        return
                    }
                    
                    let mapper = Mapper<BankList>()
                    let model = mapper.map(JSONString: JSON(jsonData!).rawString()!)
                    
                    self.bankList = model?.list ?? []
                    
                    self.getBankInfo()
                }
            }
        }
    }
    
    func getBankInfo() {
        // SKHUD.show() // 하지 않는다.getgankList에서 한다.
        NetworkManager.getBankAccount() { (jsonData, error) in
            SKHUD.hide()
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code != 1 {
                        return
                    }
                    
                    let json = JSON(jsonData!)["result"]
                    if let bankSeq = json["bank_seq"].string {
                    
                        if let seq = Int(bankSeq) {
                            for (index, item) in self.bankList.enumerated() {
                                if item.bankcode == String(format: "%03d", seq) {
                                    self.isMoneyPicker = false
                                    self.pickerViewClick(index: index)
                                    break
                                }
                            }
                        }
                        
                        let accountNumber = json["number"].string!
                        if accountNumber.count > 0 {
                            self.tfAccountNum.text = accountNumber
                        }
                        
                        let accountName = json["owner"].string!
                        if accountName.count > 0 {
                            self.tfAccountName.text = accountName
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func textChange(_ sender: Any) {
        
        if tfAccountNum.text?.count ?? 0 > 0 && tfAccountName.text?.count ?? 0 > 0 && bankIndex > 0 && unitIndex > 0 {
            isCheckEnableExchange()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func isCheckEnableExchange() {
        if unitIndex == 0 {
            btnExchangeMoney.setNextButton(bool: false)
            return
        }
        
        if bankIndex == -1 {
            btnExchangeMoney.setNextButton(bool: false)
            return
        }
        
        if tfAccountNum.text?.count ?? 0 <= 0 {
            btnExchangeMoney.setNextButton(bool: false)
            return
        }
        
        if tfAccountName.text?.count ?? 0 <= 0 {
            btnExchangeMoney.setNextButton(bool: false)
            return
        }

        btnExchangeMoney.setNextButton(bool: true)
    }
}
