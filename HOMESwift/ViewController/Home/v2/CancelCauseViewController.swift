//
//  CancelCauseViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 01/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit
import SwiftyJSON

class CancelCauseViewController: SKPViewController, CancelCouponDelegate {
    
    var reservationModel: ReservationModel?
    var isRemain: Bool = false
    

    
    @IBAction func actionReferral(_ sender: Any) {
        if let referralVC = UIStoryboard.init(name: "v2", bundle: nil).instantiateViewController(withIdentifier:
            "referralViewController") as? ReferralViewController {
            self.navigationController?.pushViewController(referralVC, animated: true)
        }
    }
    
    @IBAction func actionGoWebsite(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "https://blog.naver.com/homemaster0000/221182251574")!)
    }
    
    @IBAction func actionChangeMaster(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPartCleaning(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "https://blog.naver.com/homemaster0000/221182251574")!)
    }
    
    @IBAction func actionChangeAddress(_ sender: Any) {
        let addressVC = AddressListViewController(nibName: "AddressListViewController", bundle: nil)
        navigationController?.pushViewController(addressVC, animated: true)
    }
    
    @IBAction func actionContinue(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionServiceCancel(_ sender: Any) {
        SKHUD.show()
        NetworkManager.checkCacelCoupon { (jsonData, error) in
            SKHUD.hide()
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code == 1 {
                        if let cancelCouponPopUp = UIStoryboard.init(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "cancelCouponPopUpViewController") as? CancelCouponPopUpViewController {
                            cancelCouponPopUp.delegate = self
                            
                            cancelCouponPopUp.modalPresentationStyle = .overCurrentContext
                            cancelCouponPopUp.modalTransitionStyle = .crossDissolve

                            self.present(cancelCouponPopUp, animated: true)
                        } else {
                            self.goCancel()
                        }
                    } else {
                        self.goCancel()
                    }
                }
            }
        }
    }
    
    func noCoupon() {
        // -- 쿠폰 안받을때
        goCancel()
    }
    
    func getCoupon() {
        SKHUD.show()
        NetworkManager.giveCancelCoupon { (jsonData, error) in
            SKHUD.hide()
            
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                if let return_code = JSON(jsonData!)["return_code"].int {
                    if return_code == 1 {
                        
                        if let preVc = self.navigationController?.viewControllers[0] as? HomeViewController {
                            preVc.isGoCouponVC = true
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func goCancel() {
        if let cancelList = self.storyboard?.instantiateViewController(withIdentifier: "cancelListViewController") as? CancelListViewController {
            cancelList.isRemain = isRemain
            cancelList.reservationSeq = reservationModel?.seq
            navigationController?.pushViewController(cancelList, animated: true)
        }
    }
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbReferral: UILabel!
    @IBOutlet weak var lbService: UILabel!
    @IBOutlet weak var lbChangeMaster: UILabel!
    @IBOutlet weak var lbPartCleaning: UILabel!
    @IBOutlet weak var lbChangeAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationTitle(title: "서비스 해지하기")
        setLeftBarButton(type: .Back, action: #selector(clickBack))

        lbTitle.text = "\(DataStore.userModel?.name ?? "") 고객님,\n홈마스터 서비스를 왜\n해지하려고 하시나요?"
        
        if let money = "\(DataStore.settings?.recommender_point ?? 20000)".stringFormatDecimal() {
            lbReferral.text = "\(money)원 받으러 가기"
        }
    }
}
