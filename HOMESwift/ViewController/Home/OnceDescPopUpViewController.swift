//
//  OnceDescPopUpViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 01/02/2019.
//  Copyright © 2019 skoopmedia. All rights reserved.
//

import UIKit

class OnceDescPopUpViewController: SKPViewController {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var okButton: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeAnimate)))
        self.okButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeAnimate)))
        self.okButton.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        self.okButton.setBackgroundImage(UIImage.imageFromColor(UIColor.alert), for: .normal)
        
    }
    
//    func showAnimate() {
//        self.mainView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//        self.mainView.alpha = 0.0;
//        UIView.animate(withDuration: 0.25, animations: {
//            self.mainView.alpha = 1.0
//            self.mainView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//        })
//    }
    
    @objc func removeAnimate() {
        self.dismiss(animated: true)
    }
}
