//
//  RequestsViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  요청사항 선택
//

import UIKit
import SwiftyJSON
import ObjectMapper
import FacebookCore
import FirebaseAnalytics

/// 요청사항 뷰컨트롤러
class RequestsViewController: SKPViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var nextButton: NextButton!
    
    @IBOutlet var tableViewHeightConstrant: NSLayoutConstraint!
    
    @IBOutlet weak var tvEtcRequest: UITextView!
    @IBOutlet weak var bottomConstrant: NSLayoutConstraint!
    
    @IBOutlet weak var bgView: UIView!
    var buttonController = ButtonController()
    let dataSource = [" 아기가 있어요", " 강아지가 있어요", " 고양이가 있어요", " 40평 이상이에요", " 가정집이 아니에요"]
    
    var realText: String = ""
    
    @IBAction func clickNextButton(_ sender: NextButton) {
        SKHUD.show()
        
        if let rows = tableView.indexPathsForSelectedRows {
            var list = [String]()
            for item in rows {
                list.append(dataSource[item.row])
            }
            ReservationModel.shared.user_request = list
        }
        
        ReservationModel.shared.debugPrint()

        
        ReservationModel.shared.available_schedule_memo = realText
        
        // 예약생성
        NetworkManager.createReservation { (jsonData, error) in
            if let networkError = error as? NetworkError {
                AlertView().alert(self, title: nil, message: networkError.description)
            }
            else {
                let json = JSON(jsonData!)
                let isUnPaid = json["result"]["is_unpaid"].bool

                if isUnPaid == true { // 미결제가 있을경우
                    let payPopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PayPopUpViewController") as! PayPopUpViewController
                    payPopUpVC.style = .WhenTapButton

                    payPopUpVC.service_date = json["result"]["service_date"].string
                    payPopUpVC.service_cycle = json["result"]["service_cycle"].string
                    payPopUpVC.unpaid_price = json["result"]["unpaid_price"].int
                    payPopUpVC.fail_msg = json["result"]["fail_msg"].string

                    let mapper = Mapper<ReservationModel>()
                    if let reservation_infos = json["result"]["reservation_infos"].rawString() {
                        payPopUpVC.reservationModel = mapper.map(JSONString: reservation_infos)
                    }

                    self.addChildViewController(payPopUpVC)

                    payPopUpVC.view.frame = self.view.frame
                    self.view.addSubview(payPopUpVC.view)
                    payPopUpVC.didMove(toParentViewController: self)
                } else { // 미결제 없을때
                    let total_price = ReservationModel.shared.total_price!
                    let currency = "KRW"

                    AppEventsLogger.log(.purchased(amount: Double(total_price), currency: currency))
                    FirebaseAnalyticsManager.shared.sendLog(ScreenName.reservation_completed.rawValue, parameters: [
                        AnalyticsParameterValue: total_price,
                        AnalyticsParameterCurrency: currency
                        ])

                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "FinishReserveViewController") as! FinishReserveViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }

            SKHUD.hide()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: DataStore.naviTitleName)
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        
        nextButton.setNextButton(bool: true)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RadioCell.self)
        
        tableView.allowsMultipleSelection = true
        
        tvEtcRequest.delegate = self
        
        setNavigationTitle(title: "정기서비스 요청 사항")
        
        NotificationCenter.default.addObserver(self, selector: #selector(RequestsViewController.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RequestsViewController.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        bgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -200 // Move view 200 points upward
//        self.bottomConstrant.constant = 200
//
//
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "hideKeyboard")
//        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
//        self.bottomConstrant.constant = 0
//        self.view.removeGestureRecognizer(UIGestureRecognizer(target: self, action: nil))
    }
//
//    @objc func hideKeyboard() {
//        view.endEditing(true)
//    }
    
}

extension RequestsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RadioCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RadioCell
        
        cell.selectionStyle = .none
        cell.leftButton.setImage(UIImage(named: "btn_chk_off"), for: UIControlState.normal)
        cell.leftButton.setImage(UIImage(named: "btn_chk_on"), for: UIControlState.selected)
        cell.leftButton.isUserInteractionEnabled = false
        
        cell.setData(leftButton:cell.leftButton, name: dataSource[indexPath.row])
        
        if indexPath.row == 3 {
            cell.nameLabel.attributedText = String.makeAttributedString(boldString: dataSource[indexPath.row], boldSize: UIView.getFlexibleFontSize(12), systemString: "", systemSize: UIView.getFlexibleFontSize(10))
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        dismissKeyboard()
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        dismissKeyboard()
        return indexPath
    }
    
    // 셀 클릭할때 색깔
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
        cell.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
        
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
        cell.backgroundColor = UIColor.white
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        tableViewHeightConstrant.constant = tableView.contentSize.height
    }
}

extension RequestsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if realText.count <= 0 {
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0.39951998, green: 0.4280002117, blue: 0.4717291594, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        realText = textView.text
        
        if realText.count == 0 {
            textView.text = "ex) 오전 10시 이후 시작도 가능합니다.\n화장실 청소를 우선으로 꼼꼼히 해주세요."
            textView.textColor = #colorLiteral(red: 0.6822843552, green: 0.6824010015, blue: 0.6822689772, alpha: 1)
        }
    }
}
