//
//  RegularDateViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  방문 횟수 및 방문 요일 선택
//

import UIKit

/// 방문 횟수 및 방문요일 선택 뷰컨트롤러
class RegularDateViewController: SKPViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateSelectView: UIView!
    @IBOutlet weak var dayStackView: UIStackView!
    @IBOutlet weak var nextButton: NextButton!
    @IBOutlet weak var tableViewHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var subTitleView: UIView!
    @IBOutlet weak var subTitleTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var onceDescConstrant: NSLayoutConstraint!
    
    
    @IBAction func OnceDescPopUpViewController(_ sender: Any) {
        
        let onceDescPopUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnceDescPopUpViewController") as! OnceDescPopUpViewController
        
        onceDescPopUp.modalPresentationStyle = .overCurrentContext
        onceDescPopUp.modalTransitionStyle = .crossDissolve
        present(onceDescPopUp, animated: true)
    }
    
    let periodicList = ["매주 여러 번", "매주 한 번", "2주에 한 번"]
    
    var dayButtonController = ButtonController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        setNavigationTitle(title: DataStore.naviTitleName)
        setNavigationTitle(title: "정기서비스 주기 선택")
        setLeftBarButton(type: .Back, action: #selector(clickBack))

        setButtons()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RadioCell.self)
        
        onceDescConstrant.constant = 32
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setRoundImage()
    }
    
    func setButtons() {
//        let boldAttrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 10), NSForegroundColorAttributeName : UIColor.white]
        
//        let systemAttrs = [NSFontAttributeName : UIFont.systemFont(ofSize: 10), NSForegroundColorAttributeName : UIColor.main]

        //월~금
        for case let item as UIButton in dayStackView.subviews {
//            item.setAttributedTitle(NSAttributedString(string: item.currentTitle!, attributes: boldAttrs), for: .selected)
//            item.setAttributedTitle(NSAttributedString(string: item.currentTitle!, attributes: systemAttrs), for: .normal)
            item.setFlexibleFontSize()
            item.addTarget(self, action: #selector(daySelector(_:)), for: .touchUpInside)
        }
        
        dayButtonController.setRadioButtons(buttons: dayStackView.subviews as! [UIButton])
    }
    
    func setRoundImage() {
        let colorImage = UIImage.imageFromColor(UIColor.alert)

        for case let item as UIButton in dayStackView.subviews {
            item.layer.cornerRadius = item.bounds.width/2
            item.clipsToBounds = true
            item.setTitleColor(UIColor.main, for: UIControlState.normal)
            item.setTitleColor(UIColor.white, for: UIControlState.selected)
            
            //            item.setAttributedTitle(NSAttributedString(string: item.currentTitle!, attributes: boldAttrs), for: .selected)
            //            item.setAttributedTitle(NSAttributedString(string: item.currentTitle!, attributes: systemAttrs), for: .normal)
            item.setBackgroundImage(colorImage, for: .selected)
        }
    }
    
    @objc func daySelector(_ sender: UIButton) {
        dayButtonController.clickButton(clickedButton: sender)
        
        if sender.isSelected {
            sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.1, options: .beginFromCurrentState, animations: {
                sender.transform = CGAffineTransform(scaleX: 1, y: 1)
            }, completion: nil)
        }
        
        if dayButtonController.selectedButtons.isEmpty {
            setEnableNextBtn(isEnable: false)
        }
        else {
            setEnableNextBtn(isEnable: true)
        }
    }
    
    @IBAction func clickNextButton(_ sender: Any) {
        if let row = tableView.indexPathForSelectedRow?.row {
            if row == 2 { // 2주에 한번
                ReservationModel.shared.week_cycle = WeekCycle.TwiceWeek.rawValue
            } else if row == 3 {
                performSegue(withIdentifier: "onceSegue", sender: self)
                return
            } else { // 매주 한번, 매주 여러번
                ReservationModel.shared.week_cycle = WeekCycle.OnceWeek.rawValue
            }
        }
        
        var dayList = [Int]()
        for button in dayButtonController.selectedButtons {
            dayList.append(button.tag)
        }
        ReservationModel.shared.days = dayList // 요일 저장
        performSegue(withIdentifier: "onceSegue", sender: self)
    }

    // 예약 가능일을 오늘 기준 3일 이후로 변경.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is OnceDateViewController {
            let viewcontroller = segue.destination as! OnceDateViewController
            viewcontroller.possibleDay = 3
        }
        
        log.debug("")
    }
    
    func setEnableNextBtn(isEnable : Bool) {
        nextButton.setNextButton(bool: isEnable)
        subTitleView.isHidden = isEnable
        
        if isEnable {
            subTitleTopConstraint.constant = 0
        } else {
            subTitleTopConstraint.constant = 40
        }
    }
}

extension RegularDateViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return periodicList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RadioCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RadioCell
        
        cell.selectionStyle = .none
        cell.leftButton.setImage(#imageLiteral(resourceName: "btn_radio_off"), for: UIControlState.normal)
        cell.leftButton.setImage(#imageLiteral(resourceName: "btn_radio_on"), for: UIControlState.selected)
        cell.leftButton.isUserInteractionEnabled = false
        cell.rightLabel.isHidden = true
        
        cell.nameLabel.text = periodicList[indexPath.row]
        
        return cell
    }
    
    // 셀 클릭할때 색깔
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
        cell.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
        cell.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            dayButtonController.isMultipleSelect = true
        } else {
            dayButtonController.isMultipleSelect = false
        }
        
        if indexPath.row == 3 {
            ReservationModel.shared.order_type = .Once
            DataStore.naviTitleName = "한 번만 예약"
            dateSelectView.isHidden = true
            onceDescConstrant.constant = 138 - dateSelectView.bounds.height
            setEnableNextBtn(isEnable: true)
//            performSegue(withIdentifier: "onceSegue", sender: self)
        }
        else {
            ReservationModel.shared.order_type = .Cycle
            dateSelectView.isHidden = false
            onceDescConstrant.constant = 32 + dateSelectView.bounds.height
            dayButtonController.cancleAllButton()
            nextButton.setNextButton(bool: false)
            setEnableNextBtn(isEnable: false)
            
//            DataStore.naviTitleName = "정기 예약"
//            performSegue(withIdentifier: "periodicalSegue", sender: self)
        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        tableViewHeightConstrant.constant = tableView.contentSize.height
    }
}
