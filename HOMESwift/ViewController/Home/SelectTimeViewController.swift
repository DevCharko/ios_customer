//
//  SelectTimeViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  일반/안심 마스터 선택 및 시간 선택
//

import UIKit
import SwiftyJSON
import ObjectMapper

/// 마스터 선택 및 시간 선택 뷰컨트롤러
class SelectTimeViewController: SKPViewController {
    
    @IBOutlet weak var subTitleView: UIView!
    @IBOutlet weak var subTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var retryView: UIView!
    @IBOutlet var radioViewCollection: [UIView]!
    @IBOutlet var radioButtonCollection: [UIButton]!
    @IBOutlet var timeLabelCollection: [FlexibleLabel]!
    @IBOutlet var priceLabelCollection: [FlexibleLabel]!
    
    @IBOutlet weak var masterInfoButton: FlexibleButton! // 안심마스터는 무엇이 다른가요?
    
    @IBOutlet weak var insuranceTitleLabel: FlexibleLabel!
    @IBOutlet weak var insuranceMemoLabel: FlexibleLabel!
    @IBOutlet weak var insuranceView: UIView! // 없으면 숨김처리
    @IBOutlet weak var insuranceInfoView: UIView! // 없으면 숨김처리
    @IBOutlet weak var insuranceCheckButton: UIButton! // 파손보험은 어떤건가요?
    @IBOutlet weak var insurancePriceLabel: FlexibleLabel!
    @IBOutlet weak var insuranceInfoButton: FlexibleButton!
    
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var nextButton: NextButton!
    
    var selectedPrice = 0
    
    var priceModelList = [PriceModel]()
    var seqList = ["","","","","",""]
    
    //쿠폰 관련
    var coupon: CouponInfo? {
        didSet {
            if coupon == nil {
                couponSelectImage.isHighlighted = false
                couponLightImage.isHighlighted = false
            }
            else {
                
                couponSelectImage.isHighlighted = true
                couponLightImage.isHighlighted = true
            }
        }
    }
    @IBOutlet weak var couponSelectImage: UIImageView!
    @IBOutlet weak var couponLightImage: UIImageView!
    
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var couponPriceView: UIView!
    
    @IBOutlet weak var couponNameLabel: FlexibleLabel!
    @IBOutlet weak var couponPriceLabel: FlexibleLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setNavigationTitle(title: DataStore.naviTitleName)
        if ReservationModel.shared.order_type == .Once {
            setNavigationTitle(title: "정기서비스 주기 선택")
        } else {
            setNavigationTitle(title: "정기서비스 주기 선택")
        }
        setLeftBarButton(type: .Back, action: #selector(clickBack))

        ReservationModel.shared.masterType = MasterType.NormalMaster //노말
        
        couponPriceView.isHidden = true
        getPrice()
        
        couponSelectImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickRemoveCoupon(_:))))
        
        self.radioButtonCollection.forEach {
            $0.setImage(#imageLiteral(resourceName: "btn_radio_on"), for: .selected)
            $0.setImage(#imageLiteral(resourceName: "btn_radio_off"), for: .normal)
        }
        
        self.radioViewCollection.forEach {
            let radioTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectMenu(_ :)))
            $0.addGestureRecognizer(radioTapGesture)
        }
        
        /*_ = self.radioButtonCollection.map({
            $0.setImage(#imageLiteral(resourceName: "btn_radio_on"), for: .selected)
            $0.setImage(#imageLiteral(resourceName: "btn_radio_off"), for: .normal)
        })*/
        
        /*_ = self.radioViewCollection.map {
            let radioTapGesture = UITapGestureRecognizer(target: self, action: #selector(selectMenu(_ :)))
            $0.addGestureRecognizer(radioTapGesture)
        }*/

        self.insuranceCheckButton.setImage(#imageLiteral(resourceName: "btn_chk_off"), for: .normal)
        self.insuranceCheckButton.setImage(#imageLiteral(resourceName: "btn_chk_on"), for: .selected)
        self.insuranceCheckButton.isUserInteractionEnabled = false
        
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(clickInsurance))
        insuranceView.addGestureRecognizer(gesture)
        
        if DataStore.userModel == nil {
            couponView.isHidden = true
        }
    }
    
    @objc func selectMenu(_ sender: UITapGestureRecognizer) {
        guard let tag = sender.view?.tag else { return }
        
        // 선택된 색깔 초기화
        _ = self.radioButtonCollection.map {
            $0.isSelected = false
        }
        _ = self.priceLabelCollection.map {
            $0.textColor = UIColor.main
        }
        
        // 선택된 셀 색깔변경
        self.priceLabelCollection[tag].textColor = UIColor.alert
        self.radioButtonCollection[tag].isSelected = true
        ReservationModel.shared.price_seq = self.seqList[tag] //실제 가격표 seq
        
        // 선택된 셀의 실제 모델의 index를 찾아서 가격정보 얻기
        let index = self.priceModelList[tag].getListIndex()
        selectedPrice = Int(self.priceModelList[index].price)!
        if self.priceModelList[index].master_type == "1" {
            ReservationModel.shared.masterType = MasterType.NormalMaster
        } else {
            ReservationModel.shared.masterType = MasterType.SafeMaster
        }
        
        // 선택할때마다 가격 갱신
        setTotalPrice()
        
//        self.nextButton.setNextButton(bool: true)
        setEnableNextBtn(isEnable: true)
    }
    
    @objc func clickInsurance() {
        self.insuranceCheckButton.isSelected = !self.insuranceCheckButton.isSelected
        self.insurancePriceLabel.textColor = self.insuranceCheckButton.isSelected ? UIColor.alert : UIColor.main
        setTotalPrice()
    }
    
    func getPrice() {
        SKHUD.show()
        self.priceModelList = []
        self.emptyView.isHidden = false
        self.retryView.isHidden = true
        NetworkManager.getPrice { (jsonData, error) in
            if jsonData != nil {
                let json = JSON(jsonData!)
                
                for price in json["result"] {
                    let model = PriceModel.setBy(jsonString: price.1.rawString()!)
                    self.priceModelList.append(model)
                }
                self.setPrice()
                self.emptyView.isHidden = true
            }
            else {
                self.retryView.isHidden = false
            }
            SKHUD.hide()
        }
    }
    
    func setPrice() {

        for model in self.priceModelList {
            // 뷰의 몇번째 셀인지 tag찾기
            let index = model.getListIndex()
            guard index < self.priceModelList.count else {
                return
            }
            
            if index == -1 { //파손보험일때
                self.insuranceView.isHidden = false
                self.insuranceInfoView.isHidden = false
                self.insurancePriceLabel.text = (model.price.stringFormatDecimal() ?? "0") + "원"
                self.insuranceTitleLabel.text = model.title
                self.insuranceMemoLabel.text = model.memo
            }
            else {
                self.insuranceView.isHidden = true
                self.insuranceInfoView.isHidden = true
                
                let title = model.title
                //이 아래 부분이 멀티(오전/오후 시간에 대한 값이 없는 키)가 오게 되면 Fatal Error가 뜸 nil 값이 들어가기 때문
                let time = " \(model.begin_time.getTimeString()) ~ \(model.end_time.getTimeString())"
                timeLabelCollection[index].attributedText = String.makeAttributedString(boldString: title!, boldSize: UIView.getFlexibleFontSize(12), systemString: time, systemSize: UIView.getFlexibleFontSize(10))
                priceLabelCollection[index].text = (model.price.stringFormatDecimal() ?? "0") + "원"
                self.seqList[index] = model.seq // 인덱스 0~5까지 가격seq 저장
            }
        }
        
        self.totalPriceLabel.text = self.insurancePriceLabel.text
    }

    func setTotalPrice() {
        var totalPrice = 0
        if insuranceCheckButton.isSelected {
            totalPrice += Int(self.priceModelList[6].price)! // 보험료
        }
        totalPrice += selectedPrice
        
        if let selectedCoupon = coupon?.coupon.percent_type {
            let insurancePrice: Int = insuranceCheckButton.isSelected ? 1500 : 0
            if selectedCoupon == "2" {
                totalPrice = (selectedPrice - selectedPrice * 1) + insurancePrice
            } else if selectedCoupon == "1" {
                totalPrice = (selectedPrice - Int(Double(selectedPrice) * 0.5)) + insurancePrice
            } else {
                totalPrice = selectedPrice + insurancePrice
            }
        }
        
        ReservationModel.shared.total_price = totalPrice
        self.totalPriceLabel.text = ("\(totalPrice)".stringFormatDecimal() ?? "") + "원"
    }
    
    @IBAction func clickRetryButton(_ sender: Any) {
        getPrice()
    }
    
    @IBAction func clickPopupView(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SafeMasterPopupViewController") as! SafeMasterPopupViewController
        let naviVC = UINavigationController(rootViewController: vc)
        present(naviVC, animated: true, completion: nil)
    }
    
    @IBAction func clickInsuranceInfo(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "InsuranceInfoViewController") as! InsuranceInfoViewController
        let naviVC = UINavigationController(rootViewController: vc)
        present(naviVC, animated: true, completion: nil)
    }
    
    @IBAction func clickNextButton(_ sender: Any) {
        ReservationModel.shared.join_insurance = self.insuranceCheckButton.isSelected ? "1" : "0"
        
        if let masterType = ReservationModel.shared.masterType?.getPrefix(), let orderType = ReservationModel.shared.order_type?.getPrefix() {
            FirebaseAnalyticsManager.shared.setAnalytics(masterType + "_" + orderType + ScreenName.reservation_price_choose.rawValue)
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "UserInputViewController") as! UserInputViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickCoupon(_ sender: Any) {
        let vc = CouponListViewController(nibName:"CouponListViewController", bundle:nil)
        vc.delegate = self
        vc.callSelectTimeView = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickRemoveCoupon(_ sender: Any) {
        if (coupon) != nil {
            coupon = nil
            ReservationModel.shared.coupon = nil
            ReservationModel.shared.coupon_seq = nil
            
            couponPriceView.isHidden = true
            
            // 쿠폰 삭제 후에도 가격 갱신
            setTotalPrice()
        } else {
            clickCoupon(sender)
        }
    }
    
    func setEnableNextBtn(isEnable : Bool) {
        nextButton.setNextButton(bool: isEnable)
        subTitleView.isHidden = isEnable
        
        if isEnable {
            subTitleTopConstraint.constant = 0
        } else {
            subTitleTopConstraint.constant = 40
        }
    }
}

extension SelectTimeViewController: CouponListDelegate {
    func selectCoupon(coupon: CouponInfo) {
        self.coupon = coupon
        ReservationModel.shared.coupon = coupon.coupon
        ReservationModel.shared.coupon_seq = coupon.seq
        
        log.debug(coupon.coupon.title + " " + coupon.seq)
        
        // 쿠폰 선택 후에도 가격 갱신
        setTotalPrice()
        
        couponPriceView.isHidden = false
        couponNameLabel.text = self.coupon?.coupon.title
        couponPriceLabel.text = self.coupon?.coupon.discount_price_string ?? (("\(self.coupon?.coupon.discount_price ?? 0)".stringFormatDecimal() ?? "0") + "원")
    }
    func didSelectCoupon(coupon_seq: String) {
        
        setTotalPrice()
    }
}

//    // 셀 클릭할때 색깔
//    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
//        cell.backgroundColor = UIColor.rgb(fromHex: 0xf4f5f7)
//    }
//    
//    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! RadioCell
//        cell.backgroundColor = UIColor.white
//    }
//    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
//        return indexPath
//    }
//    
//    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
//        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
//        
//        return indexPath
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        nextButton.setNextButton(bool: true)
//    }
