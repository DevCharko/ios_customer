//
//  NewMainViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 28/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class NewMainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setStatusBarColor()
        // Do any additional setup after loading the view.
        self.navigationController?.isToolbarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = true
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setStatusBarColor() {
        let bar = UIView(frame: UIApplication.shared.statusBarFrame)
        
        bar.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2862745098, blue: 0.3254901961, alpha: 1)
        self.view.addSubview(bar)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
