//
//  ReservationMainViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 28/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class ReservationMainViewController: SKPViewController {
    
    @IBOutlet weak var tvReservation: UITableView!
    @IBOutlet weak var cvEvent: UICollectionView!
    
    var list: [EventModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tvReservation.delegate = self
        tvReservation.dataSource = self
        
        cvEvent.delegate = self
        cvEvent.dataSource = self
        
        list.append(EventModel.init(seq: "1", url: "d", title: "title1", content: "content1")!)
        list.append(EventModel.init(seq: "2", url: "d11", title: "title2", content: "content2")!)
        list.append(EventModel.init(seq: "3", url: "d22", title: "title3", content: "content3")!)
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = true
//    }
}

extension ReservationMainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reservationCell", for: indexPath) as! ReservationCell
        
        cell.lbDate.text = "2019.02.17 일"
        cell.lbTime.text = "14:00 - 18:00"
        
        return cell
    }
}

extension ReservationMainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventCell", for: indexPath) as! EventCell
        
        cell.ivThumbnail.loadImageUsingUrlString(urlString: "https://3.bp.blogspot.com/-VsT-CRJvpXI/WySx0fv9uQI/AAAAAAAABKM/r07bEGg6TiYnSffYFiujar2-0TIFJNA2gCLcBGAs/s1600/www.png")
        
        cell.lbTitle.text = list[indexPath.row].title
        cell.lbContent.text = list[indexPath.row].content
        
        return cell
    }
}

class EventCell: UICollectionViewCell {
    @IBOutlet weak var ivThumbnail: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbContent: UILabel!
}


class ReservationCell: UITableViewCell {
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    
}
