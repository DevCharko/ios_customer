//
//  MyReservationViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 01/04/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class MyReservationViewController: SKPViewController {
    @IBOutlet weak var cvIsReservation: UIView!
    
    var isReservation: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isReservation {
            let reservationVC = self.storyboard?.instantiateViewController(withIdentifier: "reservationMainViewController") as! ReservationMainViewController
        
            cvIsReservation.addSubview(reservationVC.view)
            self.addChildViewController(reservationVC)
            
            reservationVC.didMove(toParentViewController: self)
        } else {
            let noReservationVC = self.storyboard?.instantiateViewController(withIdentifier: "noReservationMainViewController") as! NoReservationMainViewController
            
            cvIsReservation.addSubview(noReservationVC.view)
            
            self.addChildViewController(noReservationVC)
            noReservationVC.didMove(toParentViewController: self)
            
            isReservation = true
        }
    }
}
