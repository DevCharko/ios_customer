//
//  NoReservationMainViewController.swift
//  HOMESwift
//
//  Created by HM_Charko on 28/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import UIKit

class NoReservationMainViewController: SKPViewController {
    @IBOutlet weak var cvPresent: UICollectionView!
    
//    var items: [PresentModel] = []
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBAction func actReservation(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegularDateViewController") as! RegularDateViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvPresent.delegate = self
        cvPresent.dataSource = self
        
        lbTitle.text = lbTitle.text?.replace(target: "%s", withString: DataStore.userModel?.name ??  "고객님")
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = true
//    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = false
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NoReservationMainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "presentCell", for: indexPath) as! PresentViewCell
        
        
        cell.lbTitle.text = "title11"
        cell.lbContent.text = "집안일에 지쳐있었는데.."
        cell.imageView.loadImageUsingUrlString(urlString: "https://3.bp.blogspot.com/-VsT-CRJvpXI/WySx0fv9uQI/AAAAAAAABKM/r07bEGg6TiYnSffYFiujar2-0TIFJNA2gCLcBGAs/s1600/www.png")
        
        return cell
    }
}

extension UIImageView {
    func loadImageUsingUrlString(urlString: String) {
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!)
                    return
                }
                
                DispatchQueue.main.async {
                    self.image = UIImage(data: data!)
                }
            }.resume()
        }
    }
}

class PresentViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbContent: UILabel!
    
}
