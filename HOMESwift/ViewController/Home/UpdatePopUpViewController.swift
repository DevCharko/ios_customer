//
//  UpdatePopUpViewController.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 9. 11..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 앱 업데이트 알림 팝업 뷰컨트롤러
class UpdatePopUpViewController: SKPViewController {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var popUpLabel: UILabel!
    @IBOutlet weak var unForceButtonStackView: UIStackView!
    @IBOutlet weak var forceButton: UIButton! //업데이트하러가기
    @IBOutlet weak var lateButton: UIButton! //나중에
    @IBOutlet weak var updateButton: UIButton! //업데이트
    
    var isForceUpdate = false
    
    var message = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.popUpLabel.text = message
        self.unForceButtonStackView.isHidden = isForceUpdate
        self.forceButton.isHidden = !isForceUpdate
        
        self.backView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.forceButton.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        self.forceButton.setBackgroundImage(UIImage.imageFromColor(UIColor.alert), for: .normal)
        self.updateButton.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        self.updateButton.setBackgroundImage(UIImage.imageFromColor(UIColor.alert), for: .normal)
        self.lateButton.setBackgroundImage(UIImage.imageFromRGB(0x828790), for: .highlighted)
        self.lateButton.setBackgroundImage(UIImage.imageFromColor(UIColor.main), for: .normal)
        self.showAnimate()
    }

    @IBAction func clickLateButton(_ sender: Any) {
        self.removeAnimate()
    }

    @IBAction func clickUpdate(_ sender: Any) {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1083523259"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.openURL(url)
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
//                self.removeFromParentViewController()
                self.dismiss(animated: false, completion: nil)
            }
        })
    }
}
