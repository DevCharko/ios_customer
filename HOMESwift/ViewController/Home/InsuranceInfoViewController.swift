//
//  InsuranceInfoViewController.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 9. 20..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import SwiftyJSON

/// 파손보험 설명 뷰컨트롤러
class InsuranceInfoViewController: SKPViewController {

    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var retryView: UIView!
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationTitle(title: "파손보험은 어떤건가요?")
        setRightBarButton(type: .Exit, action: #selector(clickExit))
        
        getImage()
    }
    
    func getImage() {
        SKHUD.show()

        self.emptyView.isHidden = false
        self.retryView.isHidden = true
        
        NetworkManager.getInsuranceImage { (jsonData, error) in
            if jsonData != nil {
                let json = JSON(jsonData!)
                
                if let imageUrl = json["result"].string {
                    self.imageView.kf.setImage(with: URL(string: imageUrl), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, type, url) in
                        guard error == nil else { return }
                        let height = (image?.size.height)! / (image?.size.width)! * UIScreen.main.bounds.width
                        
                        self.imageHeight.constant = height
                    })
                }
                self.emptyView.isHidden = true
            } else {
                self.retryView.isHidden = false
            }
            SKHUD.hide()
        }
    }
    
    @IBAction func clickRetryButton(_ sender: Any) {
        getImage()
    }
    
}
