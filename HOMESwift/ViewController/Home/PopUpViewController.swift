//
//  PopUpViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 8..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 팝업 뷰컨트롤러
protocol popUpViewDelegate : class {
    func closePopUpView(isCheck: Bool)
}

class PopUpViewController: UIViewController {
    weak var delegate: popUpViewDelegate?
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeAnimate)))
        self.okButton.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        self.okButton.setBackgroundImage(UIImage.imageFromColor(UIColor.alert), for: .normal)
        self.showAnimate()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickCheckButton(_ sender: Any) {
        self.checkButton.isSelected = !self.checkButton.isSelected
    }
    
    @IBAction func closePopUp(_ sender: AnyObject) {
        self.removeAnimate()
        //self.view.removeFromSuperview()
        self.delegate?.closePopUpView(isCheck: self.checkButton.isSelected)
    }

    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @objc func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        })
    }

}
