//
//  SelectReserveViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  한번만 예약, 정기 예약 선택
//

import UIKit

/// 예약 타입 선택 뷰 컨트롤러
class SelectReserveViewController: SKPViewController {
    
    @IBOutlet weak var onceView: RoundView!
    @IBOutlet weak var periodicalView: RoundView!
    @IBOutlet weak var nextButton: NextButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationTitle(title: "가사 마스터")
        setLeftBarButton(type: .Back, action: #selector(clickBack))
        setTapGuesture()
        setRoundView()
    }

    func setRoundView() {
        onceView.subButtons = onceView.subviews.first?.subviews as? [UIButton]
        periodicalView.subButtons = periodicalView.subviews.first?.subviews as? [UIButton]
    }
    
    func setTapGuesture() {
        onceView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapButton(_ :))))
        periodicalView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapButton(_ :))))
    }
    
    @objc func tapButton(_ sender: UITapGestureRecognizer) {
        let view = sender.view as! RoundView
        if view == onceView {
            onceView.isSelected = true
            periodicalView.isSelected = false
        }else {
            onceView.isSelected = false
            periodicalView.isSelected = true
        }
        
        nextButton.setNextButton(bool: true)
    }
    
    @IBAction func clickReservationButton(_ sender: UIButton) {
        
    }
    
    @IBAction func clickNextButton(_ sender: Any) {
        
        if onceView.isSelected == true {
            ReservationModel.shared.order_type = OrderType.Once
            DataStore.naviTitleName = "한 번만 예약"
            performSegue(withIdentifier: "onceSegue", sender: self)
        }
        else {
            ReservationModel.shared.order_type = OrderType.Cycle
            DataStore.naviTitleName = "정기 예약"
            performSegue(withIdentifier: "periodicalSegue", sender: self)
        }
        
    }
    
}
