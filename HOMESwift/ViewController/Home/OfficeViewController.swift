//
//  OfficeViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 8..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class OfficeViewController: SKPViewController {
    
    @IBOutlet weak var officeView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLeftBarButton(type: .Back, action: #selector(clickBack))

        SKHUD.show()
        officeView.delegate = self
        officeView.loadRequest(URLRequest(url: URL(string: "https://homemaster.co.kr/office")!))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Analytics.setScreenName(ScreenName.Office.rawValue, screenClass: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension OfficeViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SKHUD.hide()
    }
}
