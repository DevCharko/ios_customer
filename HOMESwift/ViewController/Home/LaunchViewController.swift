//
//  LaunchViewController.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 7. 18..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//
import UIKit
import UserNotifications
import SwiftyJSON

///

/*!
 스플래시 뷰컨트롤러. 스플래시화면을 보여주며 앱실행전 필요한 처리를 하는 뷰컨트롤러
 */
class LaunchViewController: SKPViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        checkDevice()
        registerForRemoteNotification() // 푸시토큰 등록
    }
    
    func checkDevice() {
        log.info("checkDevice()")

        log.debug("Device ID ------")
        log.debug(DeviceInfo.deviceId)
        log.debug("Auth KEY ------")
        log.debug(DataStore.getAuthKey ?? "none")
        log.debug("Push TOKEN ------")
        log.debug(DataStore.getPushToken ?? "none")
        
        DeviceInfo.showDeviceInfo()
        
        if (DataStore.getAuthKey == nil || DataStore.getDeviceId?.count == 0) {
            createDevice()
        } else {
            sendPushToken()
            goToMain()
        }
    }
    
    func createDevice() {
        log.info("createDevice()")
        NetworkManager.createDevice(deviceId: DeviceInfo.deviceId) { (jsonData, error) in
            if (jsonData != nil) {
                let json = JSON(jsonData!)
                
                DataStore.setAuthKey(authKey: json["result"]["auth_token"].stringValue)
                
                log.debug("Auth KEY ------")
                log.debug(DataStore.getAuthKey ?? "none")
                log.info("Complete!")
                self.checkDevice()
            }
            else {
                log.error(error!)
                AlertView().alert(self, title: nil, message: "네트워크에 연결 후 다시 시도해 주세요.", cancelButtonTitle: "취소", otherButtonTitles: ["확인"], block: { (AlertView, idx) in
                    if idx == 0 {
                        self.createDevice()
                    } else {
                        exit(0)
                    }
                })
            }
        }
    }
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.sound, .alert]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(
                UIUserNotificationSettings(types: [.sound, .alert], categories: nil)
            )
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func sendPushToken() {
        log.info("sendPushToken()")

        if let fcmToken = DataStore.getPushToken {
            NetworkManager.updatePushToken(pushToken: fcmToken) { (jsonData, error) in
                if let networkError = error as? NetworkError {
                    log.error(networkError.description)
                }
            }
        }
    }
    
    func goToMain() {
        if DataStore.isLogin {
            let mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainController") as! MainController
            UIApplication.shared.keyWindow?.rootViewController = mainVC
        } else if !DataStore.isTutorial {
            if let tutorialVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tutorialViewController") as? TutorialViewController {
                present(tutorialVC, animated: true, completion: nil)
            }
        } else {
            if let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController {
                let navigationView = UINavigationController(rootViewController: loginVC)
                navigationView.navigationBar.isTranslucent = false
                UIApplication.shared.keyWindow?.rootViewController = navigationView
//                present(loginVC, animated: true, completion: nil)
            }
        }
    }
}
