//
//  SignPopUpViewController.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 13..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//


import UIKit

/// 가입 유도 뷰컨트롤러
class SignPopUpViewController: SKPViewController {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeAnimate)))
        self.okButton.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        self.okButton.setBackgroundImage(UIImage.imageFromColor(UIColor.alert), for: .normal)
        self.cancelButton.setBackgroundImage(UIImage.imageFromRGB(0x828790), for: .highlighted)
        self.cancelButton.setBackgroundImage(UIImage.imageFromColor(UIColor.main), for: .normal)
        self.showAnimate()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickCancelButton(_ sender: Any) {
        self.removeAnimate()
    }
    
    
    @IBAction func clickOkButton(_ sender: Any) {
            self.removeAnimate()
            let vc = storyboard?.instantiateViewController(withIdentifier: "UserInputViewController") as! UserInputViewController
            DataStore.naviTitleName = "가입"

            let naviVC = UINavigationController(rootViewController: vc)

            present(naviVC, animated: true)
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @objc func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        })
    }
    
}
