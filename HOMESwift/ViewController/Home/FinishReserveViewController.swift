//
//  FinishReserveViewController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//  예약완료 페이지
//  카드등록뷰, 예약내역뷰와 연결
//

import UIKit

/// 예약완료 표시 뷰컨트롤러
class FinishReserveViewController: SKPViewController {

    @IBOutlet weak var movInView: UIView!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet weak var cardAddButton: UIButton!
    @IBOutlet weak var reservationListButton: UIButton!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    
    @IBAction func clickOkButton(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func clickAddCard(_ sender: Any) {
        guard navigationController?.viewControllers != nil else {
            return
        }
        self.cardAddButton.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        self.cardAddButton.setBackgroundImage(UIImage.imageFromColor(UIColor.alert), for: .normal)
        self.reservationListButton.setBackgroundImage(UIImage.imageFromRGB(0x828790), for: .highlighted)
        self.reservationListButton.setBackgroundImage(UIImage.imageFromColor(UIColor.main), for: .normal)

        let cardVC = CardListViewController(nibName: "CardListViewController", bundle: nil)
        var vcs = navigationController?.viewControllers
        
        for vc in (navigationController?.viewControllers)! {
            if (vc .isKind(of: FinishReserveViewController.self)) {
                vcs?.remove(at: ((navigationController?.viewControllers)?.index(of: vc))!)
                break;
            }
        }
        vcs?.append(cardVC)
        
        navigationController?.setViewControllers(vcs!, animated: true)
    }
    
    @IBAction func clickCheckReservation(_ sender: Any) {
        let reservationVC = ReservationPageViewController(nibName:"ReservationPageViewController", bundle:nil)

        var vcs = navigationController?.viewControllers
        
        for vc in (navigationController?.viewControllers)! {
            if (vc .isKind(of: FinishReserveViewController.self)) {
                vcs?.remove(at: ((navigationController?.viewControllers)?.index(of: vc))!)
                break;
            }
        }
        vcs?.append(reservationVC)
        
        navigationController?.setViewControllers(vcs!, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle(title: "정기서비스 접수 완료")
        self.topViewHeight.setFlexibleSize()
        
        if ReservationModel.shared.cleaning_type == "2" {
            movInView.isHidden = false
        } else if ReservationModel.shared.order_type == .Once {
            movInView.isHidden = true
            infoLabel.text = "정기서비스 예약이 접수되었습니다."
        } else {
            movInView.isHidden = true
            infoLabel.text = "정기서비스 예약이 접수되었습니다."
        }
        navigationItem.setHidesBackButton(true, animated: false)
        
        ReservationModel.shared.debugPrint()

        // vc 스택 정리
        if let vc = navigationController?.topViewController {
            navigationController?.popToRootViewController(animated: false)
            navigationController?.pushViewController(vc, animated: false)
        }
        
        // swipe막음
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

}
