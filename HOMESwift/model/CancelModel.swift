//
//  CancelModel.swift
//  HOMESwift
//
//  Created by HM_Charko on 01/03/2019.
//  Copyright © 2019 skoopmedia. All rights reserved.
//

import Foundation
import ObjectMapper

class CancelModel: Mappable {
    var seq: String?
    var type: String?
    var contents: String?
    var isVisible: String?
    var sortorder: String?
    var parentSeq: String?
    var level: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        seq <- map["seq"]
        type <- map["type"]
        contents <- map["contents"]
        isVisible <- map["isVisible"]
        sortorder <- map["sortorder"]
        parentSeq <- map["parentSeq"]
        level <- map["level"]
    }
}
