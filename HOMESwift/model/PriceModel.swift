//
//  PriceModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 25..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper

/// 가격 정보 모델
class PriceModel: Mappable {
    
    var seq: String!
    var master_type: String? // 1: 일반마스터, 2: 안심마스터
    var type: String! // 1: 오전, 2: 오후, 3: 종일, 11: 파손보험
    var price: String! // 서비스 요금
    var memo: String! // 회마다 결제
    var title: String! // 오전, 오후, 종일
    var begin_time: String!
    var end_time: String!
    var cleaning_time: Int!
    var created_at: String!
    var updated_at: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        seq <- map["seq"]
        master_type <- map["master_type"]
        type <- map["type"]
        title <- map["title"]
        begin_time <- map["begin_time"]
        end_time <- map["end_time"]
        cleaning_time <- map["cleaning_time"]
        price <- map["price"]
        if price == nil {
            price <- (map["price"], TransformOf<String, Int>(fromJSON: { "\($0 ?? 0)" }, toJSON: { $0.map { Int($0)! } }))
        }
        memo <- map["memo"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }

    var masterString :String {
        return master_type == "1" ? "일반마스터" : "안심마스터"
    }
    
    func getListIndex() -> Int {
        if type == "-1" { // 파손보험
            return -1
        }
        
        switch (master_type!, type!) { // 마스터타입, 시간 쌍으로 위치 인덱스 리턴
        case ("1", "1"):
            return 3
        case ("1", "2"):
            return 4
        case ("1", "3"):
            return 5
        case ("2", "1"):
            return 0
        case ("2", "2"):
            return 1
        case ("2", "3"):
            return 2
        default:
            return 0
        }
    }
    
    static func setBy(jsonString: String) -> PriceModel {
        let mapper = Mapper<PriceModel>()
        let model = mapper.map(JSONString:jsonString)
        
        return model!
    }
}
