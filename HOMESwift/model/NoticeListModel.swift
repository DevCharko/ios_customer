//
//  NoticeListModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 29..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper

/// 공지사항 정보 모델
class Notice: Mappable {
    var seq: String!
    var title: String!
    var contents: String!
    var is_master: String!
    var site_seq: Bool!
    var created_at: String!
    var updated_at: String!
    
    
    required init?(map: Map) {
        
    }
    
    init() {}
    
    func mapping(map: Map) {
        seq <- map["seq"]
        title <- map["title"]
        contents <- map["contents"]
        is_master <- map["is_master"]
        site_seq <- map["site_seq"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}

class NoticeListModel: Mappable {
    var list: [Notice]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    required init(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["result"]
    }
}
