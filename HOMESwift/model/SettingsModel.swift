//
//  SettingsModel.swift
//  HOMESwift
//
//  Created by HM_Charko on 01/03/2019.
//  Copyright © 2019 skoopmedia. All rights reserved.
//

import Foundation
import ObjectMapper

class SettingsModel: Mappable {
    var recommender_point: Int? // 추천자가 클리닝을 이용하면 받는 크레딧
    var subject_point: Int? // 추천한 사람이 받는 쿠폰.
    var cancel_reason: [CancelModel]?
    var recommend_comment: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        recommender_point <- map["recommender_point"]
        subject_point <- map["subject_point"]
        recommend_comment <- map["recommend_comment"]
        cancel_reason <- map["cancel_reason"]
        
        
//        print(">>>>>>>>>>>>>>>> \(map["cancel_reason"])")
        
        
//        let mapper = Mapper<CancelList>()
//        cancel_reason = mapper.map(JSONString: )
    }
}
