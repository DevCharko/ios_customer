//
//  CardModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper


/// 신용카드 모델
class Card: Mappable {
    var seq: String!
    var nickname: String!
    var company_name: String!
    var number: String!
    var month: String!
    var year: String!
    var card_type: String!
    var is_default: String!
    var created_at: String!
    var updated_at: String!
    
    init(){}
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        seq <- map["seq"]
        nickname <- map["nickname"]
        company_name <- map["company_name"]
        number <- map["number"]
        month <- map["month"]
        year <- map["year"]
        card_type <- map["card_type"]
        is_default <- map["is_default"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}


/// 신용카드 리스트 모델
class CardList: Mappable {
    var list: [Card]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    required init(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["result"]
    }
    
    func remove(at: Int) {
        list?.remove(at: at)
    }
}
