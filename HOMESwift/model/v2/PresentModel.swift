//
//  Present.swift
//  HOMESwift
//
//  Created by HM_Charko on 29/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import Foundation
//import ObjectMapper

class PresentModel {
    var seq: String!
    var url: String!
    var title: String!
    var content: String!
    
    
    init?(seq: String, url: String, title: String, content: String) {
        self.seq = seq
        self.url = url
        self.title = title
        self.content = content
    }
    
//    init(){}
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//        seq <- map["seq"]
//        url <- map["url"]
//        title <- map["title"]
//        content <- map["content"]
//    }
}
//
//class PresentList: Mappable {
//    var list: [PresentModel]?
//
//    var count: Int {
//        return list == nil ? 0 : list!.count
//    }
//
//    required init(map: Map) {}
//
//    func mapping(map: Map) {
//        list <- map["result"]
//    }
//
//    func remove(at: Int) {
//        list?.remove(at: at)
//    }
//}
