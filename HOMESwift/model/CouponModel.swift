//
//  CouponModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 25..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper

/// 쿠폰 상세 정보 모델
class CouponModel: Mappable {
    var seq: String!
    var title: String!
    var description: String!
    var discount_price_string: String?
    var discount_price: Int?
    var minimum_price: Int!
    var image_url: String?
    var period: String!
    var card_type: String!
    var limit_date: String?
    var memo: String!
    var code: String!
    var percent_type: String?
    
    init(){}
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        seq <- map["seq"]
        title <- map["title"]
        description <- map["description"]
        discount_price_string <- map["discount_price_string"]
        discount_price <- map["discount_price"]
        minimum_price <- map["minimum_price"]
        image_url <- map["image_url"]
        percent_type <- map["percent_type"]
        period <- map["period"]
        card_type <- map["card_type"]
        limit_date <- map["limit_date"]
        memo <- map["memo"]
        code <- map["code"]
    }
}

/// 쿠폰 정보 모델
class CouponInfo : Mappable {
    var seq: String!
    var account_seq: String!
    var coupon_seq: String!
    var received_date: String?
    var expiration_date: String?
    var expiration_date2: String?
    var reservation_cycle_seq: String?
    private var is_used_string:String?
    var is_used:Bool!
    var site_seq:String?
    var updated_at:String?
    
    var coupon: CouponModel!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        seq <- map["seq"]
        coupon_seq <- map["coupon_seq"]
        received_date <- map["received_date"]
        expiration_date <- map["expiration_date"]
        expiration_date2 <- map["expiration_date2"]
        reservation_cycle_seq <- map["reservation_cycle_seq"]
        is_used_string <- map["is_used"]
        
        site_seq <- map["site_seq"]
        received_date <- map["received_date"]
        updated_at <- map["updated_at"]
        
        coupon <- map["coupon"]
        
        is_used = is_used_string == "0" ? false : true
    }
}

class CouponList: Mappable {
    var list: [CouponInfo]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    required init(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["result"]
    }
    
    func remove(at: Int) {
        list?.remove(at: at)
    }
}
