//
//  Bank.swift
//  HOMESwift
//
//  Created by HM_Charko on 07/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import Foundation
import ObjectMapper

class BankModel: Mappable {
    
    var bankcode: String!
    var bankname: String!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        bankcode <- map["bankcode"]
        bankname <- map["bankname"]
    }
}

class BankList: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        list <- map["result"]
    }
    
    var list: [BankModel]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    func remove(at: Int) {
        list?.remove(at: at)
    }
}
