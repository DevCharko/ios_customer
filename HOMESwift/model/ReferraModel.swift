//
//  Referral.swift
//  HOMESwift
//
//  Created by HM_Charko on 07/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import Foundation
import ObjectMapper

class ReferralModel: Mappable {
    var name: String?
    var createdAt: String?
    var isCleaningCompleted: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name <- map["name"]
        createdAt <- map["created_at"]
        isCleaningCompleted <- map["isCleaningCompleted"]
    }
    
}


class ReferralList: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        list <- map["referrel_list"]
    }
    
    var list: [ReferralModel]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    func remove(at: Int) {
        list?.remove(at: at)
    }
    
    func appendAll(list: [ReferralModel]) {
        for item in list {
            self.list?.append(item)
        }
    }
}
