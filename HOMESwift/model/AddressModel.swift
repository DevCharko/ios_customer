//
//  AddressModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 22..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper

/// 주소 모델
class Address: Mappable {
    var seq: Int!
    var title: String!
    var zipcode: String!
    var phone_number: String!
    var address1: String!
    var address2: String?
    var created_at: String!
    var updated_at: String!
    
    var is_default:Bool!
    init(){}
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        seq <- map["seq"]
        title <- map["title"]
        zipcode <- map["zipcode"]
        address1 <- map["address1"]
        address2 <- map["address2"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        is_default <- map["is_default"]
    }
}

class AddressList: Mappable {
    var list: [Address]?
    
    required init(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["result"]
    }
    
    func remove(at: Int) {
        list?.remove(at: at)
    }
    
}






