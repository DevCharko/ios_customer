//
//  ReservationModel.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 27..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import ObjectMapper

enum MasterType: String {
    case NormalMaster = "0"
    case SafeMaster = "1"
    
    func getPrefix() -> String {
        switch self {
        case .NormalMaster:
            return "nm"
        case .SafeMaster:
            return "sm"
        }
    }
}

// 일반예약, 이사입주
enum CleaningType {
    case Normal
    case Move
}

// 정기 예약시 선택한 주기 (1 - 매주, 2 - 2주)
enum WeekCycle: String {
    case OnceWeek = "1"
    case TwiceWeek = "2"
}

enum OrderType: String {
    case Once = "1"
    case Cycle = "2"
    
    func getPrefix() -> String {
        switch self {
        case .Once:
            return "once"
        case .Cycle:
            return "cycle"
        }
    }
}

/// 예약 상세
///
/// - Cancel: 취소
/// - Reservation: 예약(마스터 미할당)
/// - ServiceReady: 서비스 준비(마스터 할당)
/// - ServicePresent: 서비스 중
/// - ServiceFinished: 서비스 완료
/// - GradeFinished: 평가 완료

enum CycleStatus: String {
    case Cancel = "0"
    case Reservation = "1"
    case ServiceReady = "2"
    case ServicePresent = "3"
    case ServiceFinished = "4"
    case GradeFinished = "5"
    
    func getText(type: CleaningType = .Normal) -> String {
        switch (self, type) {
        case (.Cancel, _):
            return "취소된 예약"
        case (_ , .Move):
            return "신청 완료"
        case (.Reservation, .Normal):
            return "서비스 준비"
        case (.ServiceReady, .Normal):
            return "서비스 준비"
        case (.ServicePresent, .Normal):
            return "서비스 중"
        case (.ServiceFinished, .Normal):
            return "서비스 완료"
        default:
            return "서비스 준비"
        }
    }
    
    func getFootViewHeight() -> CGFloat {
        switch self {
        case .ServiceFinished, .Cancel:
            return 58.5
        default:
            return 95.5
        }
    }
    
    func getCellHeight() -> CGFloat {
        switch self {
        case .Cancel, .GradeFinished:
            return 125
        case .Reservation:
            return DetailTopTableViewCell.height
        case .ServiceReady, .ServicePresent:
            return DetailTopTableViewCell.height + 38
        case .ServiceFinished:
            return DetailTopTableViewCell.height + 38 + 15
        }
    }
}

/// 예약 정보 모델
class ReservationModel: Mappable {

    static let shared: ReservationModel = {
        let model = ReservationModel()
        return model
    }()
    
    var seq: String?
    
    // 예약번호
    var reservation_seq: String?
    
    var cleaning_type: String? // 청소 타입 (1: 가사도우미, 2: 이사·입주 청소)
    
    // (1: 일회 예약, 2 : 정기 예약)
    var order_type: OrderType?
    
    // 일회 예약시 선택한 날짜 ex) 2017-05-23
    var date: String?
    var begin_time: String?
    var end_time: String?
    
    var cycle: String?
    
    // 일반 / 안심 마스터
    var masterType: MasterType?
    
    // 선택한 price.seq -> 9~1시 / 2~6시 / 9~5시
    var price_seq: String?
    
    // 유저 요청사항 (json) ex) ['아기가 있어요', '강아지가 있어요']
    var user_request: [String]?
    
    // 유저가 선호하는 마스터 (json)
    var user_preference: [String]?
    
    // 정기 예약시 선택한 주기 (1 - 매주, 2 - 2주)
    var week_cycle: String?
    
    // 정기 예약시 선택한 요일 ex) [0, 2, 4] (0-일, 1-월, 2-화…) (json)
    var days: [Int]?
    var status: Int?
    var count: String?
    
    var available_schedule_memo: String? // 날짜선택 부가 정보
    
    var cycle_status: CycleStatus?
    var isGraded: String? // 평가 완료되었는지
    var coupon_price: Int?
    var total_price: Int?
    var is_unpaid: Bool = false

    var master: UserModel?
    var address: Address?
    var price: PriceModel?
    var insurance_price: Int?
    var join_insurance: String?
    var card: Card?
    var coupon: CouponModel?
    var coupon_seq: String?
    
    var cleaning_space: String?
    
    func debugPrint() {
        log.debug("seq => \(seq ?? "none")")
        log.debug("reservation_seq => \(reservation_seq ?? "none")")
        log.debug("order_type => \(order_type.debugDescription)")
        log.debug("date => \(date ?? "none")")
        log.debug("begin_time => \(begin_time ?? "none")")
        log.debug("end_time => \(end_time ?? "none")")
        log.debug("serverCategory => \(masterType.debugDescription)")
        log.debug("price_seq => \(price_seq ?? "none")")
        log.debug("total_price => \(total_price ?? 0)")
        log.debug("user_request => \(user_request ?? [])")
        log.debug("week_cycle => \(week_cycle ?? "none")")
        log.debug("user_preference => \(user_preference ?? [])")
        log.debug("days => \(days ?? [])")
        log.debug("cycle_status => \(cycle_status ?? CycleStatus.Cancel)")
        log.debug("----------------------------")
    }
    
    func removeAll() {
        order_type = nil
        masterType = nil
        cleaning_type = nil
        price_seq = nil
        user_request = nil
        week_cycle = nil
        user_preference = nil
        days = nil
        status = nil
        price = nil
        insurance_price = nil
        join_insurance = nil
        available_schedule_memo = nil
    }
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        seq <- map["seq"]
        reservation_seq <- map["reservation_seq"]
        order_type <- map["order_type"]
        cleaning_type <- map["cleaning_type"]
        date <- map["date"]
        begin_time <- map["begin_time"]
        end_time <- map["end_time"]
        cycle <- map["cycle"]
        price_seq <- map["price_seq"]
        user_request <- map["user_request"]
        user_preference <- map["user_preference"]
        days <- map["days"]
        count <- map["count"]
        cycle_status <- map["cycle_status"]
        isGraded <- map["is_graded"]
        coupon_price <- map["coupon_price"]
        total_price <- map["total_price"]
        master <- map["master"]
        address <- map["address"]
        price <- map["price"]
        join_insurance <- map["join_insurance"]
        insurance_price <- map["insurance_price"]
        card <- map["card"]
        coupon <- map["coupon"]
        cleaning_space <- map["cleaning_space"]
        is_unpaid <- map["is_unpaid"]
    }
}

class ReservationModelList: Mappable {
    
    var list: [ReservationModel]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["result"]
    }
    
    func debugPrint() {
        log.debug("예약 갯수 : \(list?.count ?? 0)")
        if let reservationList = list {
            for item in reservationList {
                item.debugPrint()
            }
        }
    }
}
