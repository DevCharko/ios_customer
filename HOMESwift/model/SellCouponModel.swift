//
//  SellCouponModel.swift
//  HOMESwift
//
//  Created by HM_Charko on 07/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import Foundation
import ObjectMapper

class SellCouponModel: Mappable {
    
    var seq: String!
    var coupon_seq: String!
    var listprice: String!
    var saleprice: String! // 크레딧으로 살 수 있는 금액
    var couponsperpack: String!
    var coupon_price: Int! // 쿠폰 할인 가격(혜택가격?)
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        seq <- map["seq"]
        coupon_seq <- map["coupon_seq"]
        listprice <- map["listprice"]
        saleprice <- map["saleprice"]
        couponsperpack <- map["couponsperpack"]
        coupon_price <- map["coupon_price"]
    }
}

class SellCouponList: Mappable {
    var list: [SellCouponModel] = []
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        list <- map["result"]
    }
}

