//
//  PointHistoryModel.swift
//  HOMESwift
//
//  Created by HM_Charko on 11/03/2019.
//  Copyright © 2019 homemaster. All rights reserved.
//

import Foundation
import ObjectMapper

class PointHistoryModel: Mappable {

    var income: String!
    var outcome: String!
    var comment: String!
    var state: String!
    var createAt: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        income <- map["income"]
        outcome <- map["outcome"]
        comment <- map["comment"]
        state <- map["state"]
        createAt <- map["created_at"]
    }
}


class PointHistoryList: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        list <- map["contents"]
    }
    
    var list: [PointHistoryModel]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    func remove(at: Int) {
        list?.remove(at: at)
    }
    
    func appendAll(list: [PointHistoryModel]) {
        for item in list {
            self.list?.append(item)
        }
    }
}
