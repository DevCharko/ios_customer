//
//  BannerModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 29..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper

/// 메인 배너 데이터 모델
class BannerModel: Mappable {
    
    var seq: String!
    var site_seq: String!
    var type: String!
    var title: String!
    var description: String!
    var image_url: String!
    var landing: String!
    var is_using: String!
    var expiration_date: String!
    var created_at: String!
    var updated_at: Int!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        seq <- map["seq"]
        site_seq <- map["site_seq"]
        type <- map["type"]
        title <- map["title"]
        description <- map["description"]
        image_url <- map["image_url"]
        landing <- map["landing"]
        is_using <- map["is_using"]
        expiration_date <- map["expiration_date"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}

class BannerListModel: Mappable {
    var list = [BannerModel]()
    
    var count: Int {
        return list.count
    }
    
    required init(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["result"]
    }
}
