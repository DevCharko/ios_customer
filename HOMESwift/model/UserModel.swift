//
//  AccountModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 25..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper


/// 유저 모델
class UserModel: Mappable {
    
    var seq: String!
    var name: String!
    var phone_number: String!
    var bank_name: String!
    var bank_number: String!
    var created_at: String!
    var updated_at: String!
    var last_logged_at: String!
    var profile_image_url: String?
    var address: Address?
    var address_seq: Int?
    var address1: String!
    var address2: String?
    var recommend_code: String!
    var use_event_push: String?
    var use_notice_push: String?
    var use_chat_push: String!
    var is_master: String!
    var point: Int!
    var is_recommend_coupon: String?

    
    required init?(map: Map) {
        
    }
    
    init() {}
    
    func mapping(map: Map) {
        seq <- (map["seq"], StringTransform())
        name <- map["name"]
        address <- map["address"]
        address_seq <- map["address_seq"]
        address1 <- map["address1"]
        address2 <- map["address2"]
        phone_number <- map["phone_number"]
        bank_name <- map["bank_name"]
        bank_number <- map["bank_number"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        last_logged_at <- map["last_logged_at"]
        profile_image_url <- map["profile_image_url"]
        recommend_code <- map["recommend_code"]
        use_event_push <- map["use_event_push"]
        use_notice_push <- map["use_notice_push"]
        use_chat_push <- map["use_chat_push"]
        is_master <- map["is_master"]
        point <- map["point"]
        is_recommend_coupon <- map["is_recommend_coupon"]
    }
    
    
}

struct StringTransform: TransformType {
    
    func transformFromJSON(_ value: Any?) -> String? {
        return value.flatMap(String.init(describing:))
    }
    
    func transformToJSON(_ value: String?) -> Any? {
        return value
    }
    
}
