//
//  PayHistoryModel.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 29..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import ObjectMapper

/// 결제내역 모델
class PayHistoryModel: Mappable {
    var seq: String?
    var reservation_seq: String?
    var reservation_cycle_seq: String?
    var price: String?
    var is_complete: String?
    var completed_at: String?
    var cycle: String?
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        seq <- map["seq"]
        reservation_seq <- map["reservation_seq"]
        reservation_cycle_seq <- map["reservation_cycle_seq"]
        price <- map["price"]
        is_complete <- map["is_complete"]
        completed_at <- map["completed_at"]
        cycle <- map["cycle"]
    }
}

class PayHistoryListModel: Mappable {
    var list: [PayHistoryModel]?
    
    var count: Int {
        return list == nil ? 0 : list!.count
    }
    
    required init(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["result"]
    }
}
