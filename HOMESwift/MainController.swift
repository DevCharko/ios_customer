//
//  MainController.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 2. 28..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit


/// 메인 네비게이션 컨트롤러
class MainController: UINavigationController {
    
    var sideMenu : MenuViewController
    
    required init?(coder aDecoder: NSCoder) {
        sideMenu = MenuViewController()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenu.view.frame = CGRect(x: -self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        sideMenu.delegate = self
        self.view.addSubview(sideMenu.view)
        sideMenu.hide()
    }
    
    func clickMenu() {
        sideMenu.show(true)
    }
}

extension UINavigationController {
    public typealias VoidBlock = (Void) -> Void

    public func pushViewControllerWithBlock(viewController: UIViewController, animated: Bool, completion: @escaping VoidBlock) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
}

extension MainController: MenuDelegate {
    func selectMenuIndex(index: MenuType) {
        log.debug("index:\(index)")
        
        switch index {
        case .Main:
            break
        case .Reservation:
            let view = ReservationPageViewController(nibName:"ReservationPageViewController", bundle:nil)
            self.pushViewController(view, animated: true)

            break
        case .Card:
            let view = CardListViewController(nibName:"CardListViewController", bundle:nil)
            self.pushViewController(view, animated: true)
            break
        case .Coupon:
            let view = CouponListViewController(nibName:"CouponListViewController", bundle:nil)
            self.pushViewController(view, animated: true)
            break
        case .Recommend:
            let view = UIStoryboard(name: "v2", bundle: nil).instantiateViewController(withIdentifier: "referralViewController") as! ReferralViewController
            self.pushViewController(view, animated: true)
            break
//            let view = RecommendViewController(nibName:"RecommendViewController", bundle:nil)
//            self.pushViewController(view, animated: true)
//            break
        case .Notice:
            let view = NoticeWebViewController(nibName:"NoticeWebViewController", bundle:nil)
            self.pushViewController(view, animated: true)
            break
        case .Setting:
            let view = SettingsViewController(nibName:"SettingsViewController", bundle:nil)
            self.pushViewController(view, animated: true)
            break
        case .PersonalInfo:
            let view = InformationViewController(nibName:"InformationViewController", bundle:nil)
            DataStore.naviTitleName = "개인정보 취급방침"
            self.pushViewController(view, animated: true)
            break
        case .Terms:
            let view = InformationViewController(nibName:"InformationViewController", bundle:nil)
            DataStore.naviTitleName = "서비스 이용약관"
            self.pushViewController(view, animated: true)
            break
        case .PayHistroy:
            let view = PayHistoryViewController(nibName:"PayHistoryViewController", bundle:nil)
            self.pushViewController(view, animated: true)
            break
        }
    }
}

