//
//  NSString+StringConvertible.swift
//  SwiftValidators
//
//  Created by Γιώργος Καϊμακάς on 03/08/16.
//  Copyright © 2016 Γιώργος Καϊμακάς. All rights reserved.
//

import Foundation

// MARK: - NSString형을 스트링으로 변환
extension NSString {
	public var string: String {
		return self as String
	}
}
