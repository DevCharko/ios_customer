//
//  Float+StringConvertible.swift
//  SwiftValidators
//
//  Created by Γιώργος Καϊμακάς on 03/08/16.
//  Copyright © 2016 Γιώργος Καϊμακάς. All rights reserved.
//

import Foundation

/// MARK: - float형을 스트링으로 전환
extension Float: StringConvertible {
	public var string: String? {
		return String(self)
	}
}
