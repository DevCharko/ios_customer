//
//  GAManager.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 20..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import Foundation
import FirebaseAnalytics


/// FA 화면 일람
///
/// - splash: 스플래시 화면
/// - main:  앱 메인화면
/// - menu: 왼쪽 Drawer 메뉴
/// - reservation_house_keeper: 가사 도우미 버튼 클릭
/// - reservation_move: 이사 및 입주 청소 버튼 클릭
/// - reservation_office: 오피스 클리닝 버튼 클릭
/// - reservation_service_choose: 한번만 예약 및 정기예약 선택 화면
/// - reservation_date_choose: 예약 날짜 선택
/// - reservation_price_choose: 가격 선택
/// - reservation_cycle_choose: 정기예약 이용 주기 선택 화면
/// - reservation_customer_info_update: 고객 정보 입력 화면
/// - reservation_request_choose: 요청사항 선택 화면
/// - reservation_favorite_master_choose: 선호 마스터 선택 화면
/// - reservation_complete: 예약 완료 화면
/// - reservation_refund_policy: 환불 정책
/// - reservation_list: 예약 내역
/// - reservation_detail: 예약 상세
/// - reservation_grade: 마스터 평가
/// - reservation_grade_completed: 마스터 평가 완료
/// - reservation_cancel: 예약 취소 화면
/// - reservation_cancel_completed:  예약 취소 완료
/// - channelio: 고객객상담 버튼 클릭
/// - signup: 회원 가입 화면
/// - signup_complete: 회원 가입 완료
/// - profile: 내 정보 화면 (설정 화면)
/// - phone_change: 전화번호 변경 화면
/// - phone_change_complete: 전화번호 변경 완료
/// - address_search: 주소 검색 화면
/// - address_list: 주소지 리스트 화면
/// - address_item_deleted: 주소지 제거
/// - address_item_mark: 기본 주소로 지정
/// - address_nickname: 주소지 닉네임 변경 화면
/// - address_nickname_update: 주소지 닉네임 수정
/// - payment_list: 결제 내역
/// - card_list: 결제 카드 리스트 화면
/// - card_add: 결제 카드 추가 화면
/// - card_add_completed: 결제 카드 추가 완료
/// - card_deleted: 결제 카드 제거
/// - card_mark: 기본 결제카드 지정
/// - card_nickname: 결제 카드 닉네임 수정 화면
/// - card_nickname_update: 결제 카드 닉네임 수정
/// - coupon_list: 쿠폰 리스트 화면
/// - coupon_add: 쿠폰 등록 화면
/// - coupon_add_completed: 쿠폰 등록 완료
/// - recommender: 친구에게 추천하기 화면
/// - notice: 공지 사항 화면
/// - cancel: 회원 탈퇴
/// - cancel_complete: 탈퇴 완료
/// - agree_1: 개인정보 취급 방침
/// - agree_2: 서비스 이용 약관
/// - master_1_description: 안심 마스터 설명 페이지
enum ScreenName:String {
    case splash = "splash" // 스플레시 화면
    case main = "main" // 앱 메인화면
    case menu = "menu" // 왼쪽 Drawer 메뉴
    case reservation_house_keeper = "reservation_house_keeper" // 가사 도우미 버튼 클릭
    case reservation_move = "reservation_move" // 이사 입주 청소 버튼 클릭
    case reservation_office = "reservation_office" // 오피스 클리닝 버튼 클릭
    case reservation_service_choose = "reservation_service_choose" // 한번만 예약 정기예약 선택 화면
    case reservation_date_choose = "_date_choose" // 예약 날짜 선택
    case reservation_price_choose = "_price_choose" // 가격 선택
    case reservation_cycle_choose = "reservation_cycle_choose" // 정기예약 이용 주기 선택 화면
    case reservation_customer_info_update = "_customer_info_update" // 고객 정보 입력 화면
    case reservation_request_choose = "_request_choose" // 요청사항 선택 화면
    case reservation_favorite_master_choose = "_favorite_master_choose" // 선호 마스터 선택 화면
    case reservation_complete = "_reservation_complete" // 예약 완료 화면
    case reservation_completed = "reservation_completed" // 예약 완료
    case reservation_refund_policy = "reservation_refund_policy" // 환불 정책
    case reservation_list = "reservation_list" // 예약 내역
    case reservation_detail = "reservation_detail" // 예약 상세
    case reservation_grade = "reservation_grade" // 마스터 평가
    case reservation_grade_completed = "reservation_grade_completed" // 마스터 평가 완료
    case reservation_cancel = "reservation_cancel" // 예약 취소 화면
    case reservation_cancel_completed = "reservation_cancel_completed" // 예약 취소 완료
    case channelio = "channelio" // 고객객상담 버튼 클릭
    case signup = "signup" // 회원 가입 화면
    case signup_complete = "signup_complete" // 회원 가입 완료
    case profile = "profile" // 내 정보 화면 (설정 화면)
    case phone_change = "phone_change" // 전화번호 변경 화면
    case phone_change_complete = "phone_change_complete" // 전화번호 변경 완료
    case address_search = "address_search" // 주소 검색 화면
    case address_list = "address_list" // 주소지 리스트 화면
    case address_item_deleted = "address_item_deleted" // 주소지 제거
    case address_item_mark = "address_item_mark" // 기본 주소로 지정
    case address_nickname = "address_nickname" // 주소지 닉네임 변경 화면
    case address_nickname_update = "address_nickname_update" // 주소지 닉네임 수정
    case payment_list = "payment_list" // 결제 내역
    case card_list = "card_list" // 결제 카드 리스트 화면
    case card_add = "card_add" // 결제 카드 추가 화면
    case card_add_completed = "card_add_completed" // 결제 카드 추가 완료
    case card_deleted = "card_deleted" // 결제 카드 제거
    case card_mark = "card_mark" // 기본 결제카드 지정
    case card_nickname = "card_nickname" // 결제 카드 닉네임 수정 화면
    case card_nickname_update = "card_nickname_update" // 결제 카드 닉네임 수정
    case coupon_list = "coupon_list" // 쿠폰 리스트 화면
    case coupon_add = "coupon_add" // 쿠폰 등록 화면
    case coupon_add_completed = "coupon_add_completed" // 쿠폰 등록 완료
    case recommender = "recommender" // 친구에게 추천하기 화면
    case notice = "notice" // 공지 사항 화면
    case cancel = "cancel" // 회원 탈퇴
    case cancel_complete = "cancel_complete" // 탈퇴 완료
    case agree_1 = "agree_1" // 개인정보 취급 방침
    case agree_2 = "agree_2" // 서비스 이용 약관
    case master_1_description = "master_1_description" // 안심 마스터 설명 페이지
}


/// firebase analytics
class FirebaseAnalyticsManager {
    static let shared = FirebaseAnalyticsManager()
    
    func setAnalytics(_ screenName: String) {
        #if DEBUG
            
        #else
            Analytics.setScreenName(screenName, screenClass: nil)
            sendLog(screenName)
        #endif
    }

    func sendLog(_ api: String) {
        Analytics.logEvent(api, parameters: nil)
    }

    func sendLog(_ api: String, parameters: [String : Any]) {
        Analytics.logEvent(api, parameters: parameters)
    }
}
