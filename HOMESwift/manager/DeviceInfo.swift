//
//  DeviceInfo.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 28..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import KeychainSwift

let Keychain_ID_UUID = "Keychain_ID_UUID"

/// 기기 정보를 정리해놓은 클래스
class DeviceInfo: NSObject {
    
    //MARK: - Model Info
    /// 모델 정보
    static var modelName : String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                   return "iPod Touch 5"
        case "iPod7,1":                                   return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":       return "iPhone 4"
        case "iPhone4,1":                                 return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                    return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                    return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                    return "iPhone 5s"
        case "iPhone7,2":                                 return "iPhone 6"
        case "iPhone7,1":                                 return "iPhone 6 Plus"
        case "iPhone8,1":                                 return "iPhone 6s"
        case "iPhone8,2":                                 return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                    return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                    return "iPhone 7 Plus"
        case "iPhone8,4":                                 return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                  return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                  return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                  return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":  return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":             return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":             return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":             return "iPad Air"
        case "iPad5,3", "iPad5,4":                        return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":             return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":             return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":             return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                        return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":  return "iPad Pro"
        case "AppleTV5,3":                                return "Apple TV"
        case "i386", "x86_64":                            return "Simulator"
        default:                                          return identifier
        }
    }
    
    static func showDeviceInfo() {
        log.debug("model name : \(self.modelName)");
        log.debug("system : \(self.systemName), ver.\(self.systemVersion)")
        log.debug("App : \(self.appName), app ver.\(self.appVersion)")
    }
    
    // MARK: - System Info
    /// 시스템 명
    static var systemName : String {
        return UIDevice.current.systemName
    }
    
    /// 시스템 버전
    static var systemVersion : String {
        return UIDevice.current.systemVersion
    }
    
    // MARK: - App Info
    
    /// 앱 이름
    static var appName : String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as! String
    }
    
    /// 앱 버전
    static var appVersion : String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    /// 앱 빌드버전
    static var appBuild : String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    }
    
    
    /// 앱 번들ID
    static var appBundleId : String {
//        return "appbundle"
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as! String
    }

    // MARK: - Device ID
    
    /// 서버통신시 사용하는 device ID
    static var deviceId : String {
        let keyChain = KeychainSwift()
        if let uuid = keyChain.get(Keychain_ID_UUID) {
            return uuid
        }
        else {
            let uuidString = NSUUID().uuidString
            keyChain.set(uuidString, forKey: Keychain_ID_UUID)
            
            return uuidString
        }
//        let keychain = KeychainItemWrapper(identifier: self.appBundleId, accessGroup: nil)
//        if let uuid = keychain[Keychain_ID_UUID] {
//            log.debug("WOW uuid is exist!!")
//            DataStore.setDeviceId(uuid: uuid as! String)
//            return uuid as! String
//        }
//        else {
//            log.debug("NEW uuid is making...")
//            keychain[kSecAttrGeneric as String] = Keychain_ID_UUID as AnyObject?
//            keychain[kSecAttrService as String] = self.appBundleId as AnyObject?
//            keychain[Keychain_ID_UUID] = NSUUID().uuidString as AnyObject?
//            return keychain[Keychain_ID_UUID] as! String
//        }
    }
}
