//
//  NetworkManager.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 6..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import Rollbar

//let kAPI_DOMAIN = "https://hm-live.skoopmedia.co.kr"
//let kAPI_DOMAIN = "http://stage.homemaster.co.kr"
let kAPI_DOMAIN = "http://api.homemaster.co.kr"
//let kAPI_DOMAIN = "http://test.homemaster.co.kr"
let kAPI_DEV_DOMAIN = "http://hm-dev.skoopmedia.co.kr"
let kAPI_STAGE_DOMAIN = "http://hm-stage.skoopmedia.co.kr"
let kWEB_DAUM_POST_CODE = kAPI_DOMAIN + "/test/daum.html"

/// API 리스트
///
/// - Create: api/device/create
/// - UpdatePushToken: api/device/update_push_info
/// - CommonInfo: api/common/info
/// - SignUp: api/user/sign_up_by_phone
/// - RequestSMS: api/user/request_sms
/// - ConfirmSMS: api/user/confirm_sms
/// - UserInfo: api/user/info
/// - UpdateProfileImage: api/user/update_profile_image
/// - UpdateUser: api/user/update
/// - UsePush: api/user/update_use_push
/// - Retract: api/user/retract
/// - PayHistory: api/user/get_payment_lists
/// - checkUnPaid: api/user/unpaid
/// - AddressList: api/address/index
/// - AddressAdd: api/address/add
/// - AddressEdit: api/address/edit
/// - AddressRemove: api/address/remove
/// - AddressMark: api/address/mark
/// - AddressAvailable: api/address/check_available
/// - AddressUpdate: api/address/update
/// - ReservationList: api/reservation/index
/// - ReservationInfo: api/reservation/get
/// - CreateReservation: api/reservation/create
/// - CreateMoveReservation: api/reservation/move_create
/// - CancelReservation: api/reservation/cancel
/// - updateReservation: api/reservation/update
/// - CreateGrade: api/grade/create
/// - CardList: api/card/index
/// - RegistCard: api/card/register
/// - MarkCard: api/card/mark
/// - UpdateCard: api/card/update
/// - RemoveCard: api/card/remove
/// - CouponList: api/coupon/index
/// - RegistCoupon: api/coupon/register
/// - UseCoupon: api/coupon/use_coupon
/// - getPrice: api/price/index
/// - getInsuranceImage: api/price/get_insurance_image
/// - Version: api/version/index
/// - Banner: api/banner/index
/// - ResetPassword: api/user/request_reset_password
enum API:String {
    
    case Settings = "/api/reservation/get_reservation_config"
    //common
    case Create = "/api/device/create"
    case UpdatePushToken = "/api/device/update_push_info"
    case CommonInfo = "/api/common/info"
    
    //user
    case LoginByPhone = "/api/user/log_in_by_phone"
    case SignUp = "/api/user/sign_up_by_phone"
    case RequestSMS = "/api/user/request_sms"
    case ConfirmSMS = "/api/user/confirm_sms"
    case CheckCancelCoupon = "/api/user/checkCancelCoupon"
    case GiveCancelCopon = "/api/user/giveCancelCoupon"
//    case SignIn = "/api/user/sign_in"
//    case SignOut = "/api/user/sign_out"
    case UserInfo = "/api/user/info"
    case UpdateProfileImage = "/api/user/update_profile_image"
    case UpdateUser = "/api/user/update"
    case UsePush = "/api/user/update_use_push"
    case Retract = "/api/user/retract"
    case PayHistory = "/api/user/get_payment_lists"
    case checkUnPaid = "/api/user/unpaid"
    
    //Referral
    case ReferralList = "/api/user/referrel_list"
    case PointLog = "/api/user/point_log"
    case BankInfo = "/api/point/getbankinfo"
    case ExchangePoint = "/api/point/exchange_point"
    case SellCouponList = "/api/coupon/sell_coupon_list"
    case BuyCoupon = "/api/coupon/buy_coupon"
    case BankAccount = "/api/user/getBankAccount"
    
    

    //address
    case AddressList = "/api/address/index"
    case AddressAdd = "/api/address/add"
    case AddressEdit = "/api/address/edit"
    case AddressRemove = "/api/address/remove"
    case AddressMark = "/api/address/mark"
    case AddressAvailable = "/api/address/check_available"
    case AddressUpdate = "/api/address/update"
    
    //reservation
    case ReservationList = "/api/reservation/index"
    case ReservationInfo = "/api/reservation/get"
    case CreateReservation = "/api/reservation/create_new"
    case CreateMoveReservation = "/api/reservation/move_create"
    case CancelReservation = "/api/reservation/cancel"
    case updateReservation = "/api/reservation/update"
    case updateReservationV2 = "/api/reservation/update_20190123" // 20190123 이게 최신버전
    
    //grade
    case CreateGrade = "/api/grade/create"
    
    //card
    case CardList = "/api/card/index"
    case RegistCard = "/api/card/register"
    case MarkCard = "/api/card/mark"
    case UpdateCard = "/api/card/update"
    case RemoveCard = "/api/card/remove"
    
    //coupon
    case CouponList = "/api/coupon/index"
    case RegistCoupon = "/api/coupon/register"
    case UseCoupon = "/api/coupon/use_coupon"
    
    //price
    case getPrice = "/api/price/index"
    case getInsuranceImage = "/api/price/get_insurance_image"

    //etc
//    case Popup = "/api/popup/index"
    case Version = "/api/version/index"
    case Banner = "/api/banner/index"
    case ResetPassword = "/api/user/request_reset_password"
//    case Notice = "/api/notice/index"
}

protocol NetworkErrorProtocol: Error {
    var code: Int { get }
    var description: String { get }
}


/// 네트워크 에러를 표시하기 위한 클래스
struct NetworkError: NetworkErrorProtocol {
    
    var code: Int
    var description: String
    
    init(code: Int, description: String) {
        self.code = code
        self.description = description
    }
}

class Retrier: RequestRetrier {
    
    var defaultRetryCount = 2
    private var requestsAndRetryCounts: [(Request, Int)] = []
    private var lock = NSLock()
    
    private func index(request: Request) -> Int? {
        return requestsAndRetryCounts.index(where: { $0.0 === request })
    }
    
    func addRetryInfo(request: Request, retryCount: Int? = nil) {
        lock.lock() ; defer { lock.unlock() }
        guard index(request: request) == nil else {
//            log.debug("ERROR addRetryInfo called for already tracked request");
            return
        }
        
        requestsAndRetryCounts.append((request, retryCount ?? defaultRetryCount))
    }
    
    func cancel() {
        requestsAndRetryCounts = []
    }
    
    func deleteRetryInfo(request: Request) {
        lock.lock() ; defer { lock.unlock() }
        guard let index = index(request: request) else {
//            log.debug("ERROR deleteRetryInfo called for not tracked request");
            return
        }
        
        requestsAndRetryCounts.remove(at: index)
    }
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion){
        
        lock.lock() ; defer { lock.unlock() }
        
        guard let index = index(request: request) else { completion(false, 0); return }
        let (request, retryCount) = requestsAndRetryCounts[index]
        
        if retryCount == 0 {
            completion(false, 0)
        } else {
            requestsAndRetryCounts[index] = (request, retryCount - 1)
            let randomDelay = TimeInterval(Double(arc4random_uniform(10)) / 10 + 1)
            log.error("\(randomDelay)초 뒤에 재시도 합니다.")
            completion(true, randomDelay)
        }
//        log.error(retryCount)
    }
}


/// 네트워킹 관리 및 API 사용을 모아둔 클래스
class NetworkManager: NSObject {
    public typealias JsonBlock = ((_ json: Any?, _ error: Error? ) -> ())
    public typealias DataBlock = ((_ data: Data?, _ error: Error?) -> ())
    
    var manager: SessionManager?
    let retrier = Retrier()

    override init() {
        super.init()

        manager = getAlamofireManager()
        manager?.retrier = retrier
    }
    
    private func getAlamofireManager() -> SessionManager  {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = DataStore.timeoutInterval

        let alamofireManager = Alamofire.SessionManager(configuration: configuration)

        return alamofireManager
    }
    
    
    /// 싱글 인스턴스 생성자
    static let shareInstance: NetworkManager = {
        let instance = NetworkManager()
        
        return instance
    }()
    
    func cancelAllRequest() {
        self.retrier.cancel()
        manager?.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
    
    func APIPostRequest(API: API, parameters: Parameters = Parameters(), complete block:JsonBlock?) {
        log.debug("API : \(API)")
        
        var param = parameters
        param["auth_token"] = DataStore.getAuthKey
        param["version_name"] = DeviceInfo.appVersion
        param["version_code"] = DeviceInfo.appBuild
        param["platform"] = "1"
        param["os_ver"] = DeviceInfo.systemVersion
        
        log.debug("param : \(param)")

        let fullUrl = URL(string: kAPI_DOMAIN + API.rawValue)
//        let fullUrl = URL(string: kAPI_STAGE_DOMAIN + API.rawValue)
        
        let request = manager?
            .request(fullUrl!,
                     method: .post,
                     parameters:param,
                     encoding: URLEncoding.default,
                     headers: nil)
        
        if let req = request {
            retrier.addRetryInfo(request: req)
            req.responseString { (responseObject) in
                self.retrier.deleteRetryInfo(request: req)
                
                switch responseObject.result {
                case .success(let json):
                    log.debug(json)
                    break
                case .failure(let error):
                    log.error((error as NSError).description )
                }
            }
                
            req.responseJSON { (responseObject) in
                self.retrier.deleteRetryInfo(request: req)

                switch responseObject.result {
                case .success(let json):
                    log.debug(json)
                    if let return_message = JSON(json)["return_message"].string{
                        if return_message == "서비스 개선중..." {
                            Rollbar.info(withMessage: "서비스 개선중...", data: param)
                        }
                    }
                    
                    let return_code = JSON(json)["return_code"].int!
                    
                    if return_code == 401 {
                        log.error("디바이스키가 일치하지 않습니다.")
                        DataStore.clearData()
                        let launchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LaunchViewController") as! LaunchViewController
                        UIApplication.shared.keyWindow?.rootViewController = nil
                        UIApplication.shared.keyWindow?.rootViewController = launchVC
                    } else if return_code == 0 {
                        let error = NetworkError.init(code: return_code, description: JSON(json)["return_message"].string ?? "")
                        block?(nil, error)
                        log.warning("code \(return_code) : \(error.description)")
                    } else {
                        block?(json, nil)
                    }
                    break
                case .failure(let error):
                    log.error((error as NSError).description )
                    let nError = NetworkError.init(code: -100, description: error.localizedDescription)
                    block?(nil, nError)
                    
                    if error.localizedDescription != "cancelled" &&
                        error.localizedDescription != "The Internet connection appears to be offline." {
                        log.error("code -100 : \(error.localizedDescription)")
                        if param["card_number"] != nil || param["cvc"] != nil { // 카드정보가 있을 경우 롤바x
                           param["card_number"] = ""
                            param["cvc"] = ""
                        }
//                        Rollbar.error(withMessage: "Network err : \(error.localizedDescription) API : \(API)", data: param)
                    }
                    break
                }
            }
        }
    }
    
    func APIGetRequest(API: String, parameters: NSMutableDictionary, complete block:JsonBlock?) {
        let dic = (parameters as NSDictionary) as! Dictionary<String, Any>
        log.debug("API : \(API)")
        log.debug(dic)
        
        Alamofire
            .request(URL(string: API)!,
                     method:.get,
                     parameters: dic,
                     encoding: URLEncoding.default,
                     headers: nil)
            .responseJSON { (responseObject) in
                switch responseObject.result {
                case .success(let json):
                    block?(json, nil)
                    break
                case .failure(let error):
                    block?(nil, error)
                    break
                }
        }
    }
    
    static func createDevice(deviceId: String,
                             complete block:@escaping JsonBlock) {
        var parameters: Parameters = Parameters()
        parameters["device_id"] = deviceId
        parameters["model_name"] = DeviceInfo.modelName
        parameters["platform"] = "1"
        parameters["os_ver"] = DeviceInfo.systemVersion
        
        shareInstance.APIPostRequest(API:API.Create, parameters: parameters, complete: block)
    }
    
    static func updateProfile(profile_image: UIImage,
                              profile_name: String,
                              block: @escaping JsonBlock) {
        let parameters = NSMutableDictionary()
        parameters.setValue(profile_name, forKey: "profile_name")
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                let data = UIImageJPEGRepresentation(profile_image, 1.0) as Data?
                multipartFormData.append(data!, withName: profile_name)
        },
            to: URL(string: "")!,
            encodingCompletion: { (encodingResult) in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    debugPrint(response)
                                }
                                break
                            case .failure(let error):
                                log.debug(error)
                                break
                            }
        })
    }
    
    static func updatePushToken(pushToken: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["push_token"] = pushToken
        shareInstance.APIPostRequest(API: API.UpdatePushToken, parameters: parameters, complete: block)
    }
    
    static func getCommonInfo(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API:API.CommonInfo, complete: block)
    }
    
    static func loginByPhone(phone_number:String, code:String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["phone_number"] = phone_number
        parameters["code"] = code
        
        shareInstance.APIPostRequest(API: API.LoginByPhone, parameters: parameters ,complete: block)
    }
    
    // MARK: - User
    static func signUp(name: String, phone_number:String, address1:String, address2:String?, recommend_code:String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["name"] = name
        parameters["phone_number"] = phone_number
        parameters["address1"] = address1
        parameters["address2"] = address2
        parameters["is_master"] = 0
        if let code = recommend_code {
            parameters["recommend_code"] = code
        }
        shareInstance.APIPostRequest(API: API.SignUp, parameters: parameters, complete: block)
    }
    
    static func requestSMS(phone_number:String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["phone_number"] = phone_number
        
        shareInstance.APIPostRequest(API: API.RequestSMS, parameters: parameters, complete: block)
    }
    
    static func confirmSMS(phone_number:String, auth_code:String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["phone_number"] = phone_number
        parameters["code"] = auth_code
        
        shareInstance.APIPostRequest(API: API.ConfirmSMS, parameters: parameters, complete: block)
    }
    
    static func checkCacelCoupon(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.CheckCancelCoupon, complete: block)
    }
    
    static func giveCancelCoupon(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.GiveCancelCopon, complete: block)
    }

    static func userInfo(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.UserInfo, complete: block)
    }

    static func retract(reason:String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["reason"] = reason
        shareInstance.APIPostRequest(API: API.Retract, parameters: parameters, complete: block)
    }
    
    static func getPayHistoryList(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.PayHistory, complete: block)
    }
    
    static func checkUnPaid(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.checkUnPaid, complete: block)
    }

    static func updateUser(phone_number:String?, use_event_push:Bool?, use_chat_push:Bool?, use_notice_push:Bool?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["phone_number"] = phone_number
        parameters["use_event_push"] = use_event_push
        parameters["use_chat_push"] = use_chat_push
        parameters["use_notice_push"] = use_notice_push
        shareInstance.APIPostRequest(API: API.UpdateUser, parameters: parameters, complete: block)
    }
    
    
    
    // Referral
    static func getReferralList(page: String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["page"] = page
        
        shareInstance.APIPostRequest(API: API.ReferralList, parameters: parameters, complete: block)
    }
    
    static func getPointLog(page: String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["page"] = page
        
        shareInstance.APIPostRequest(API: API.PointLog, parameters: parameters, complete: block)
    }
    
    static func getBankInfo(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.BankInfo, complete: block)
    }
    
    static func exchangePoint(point: String?, bankcode: String?, accountnumber: String?, accountname:String, block:  @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["point"] = point
        parameters["bankcode"] = bankcode
        parameters["accountnumber"] = accountnumber
        parameters["accountname"] = accountname
        
        shareInstance.APIPostRequest(API: API.ExchangePoint, parameters: parameters, complete: block)
    }
    
    static func getSellCouponList(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.SellCouponList, complete: block)
    }
    
    static func buyCoupon(coupon_seq: String?, coupon_sales_seq: String?, amount: String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["coupon_seq"] = coupon_seq
        parameters["coupon_sales_seq"] = coupon_sales_seq
        parameters["amount"] = amount
        
        shareInstance.APIPostRequest(API: API.BuyCoupon, parameters: parameters, complete: block)
    }
    
    static func getBankAccount(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.BankAccount, complete: block)
    }

    
//    static func signIn(_ email: String,
//                       _ password: String,
//                       block: @escaping JsonBlock) {
//        var parameters = Parameters()
//        parameters["email"] = email
//        parameters["password"] = password
//        
//        shareInstance.APIPostRequest(API: API.SignIn, parameters: parameters, complete: block)
//    }
    
//    static func signOut(block: @escaping JsonBlock) {
//        shareInstance.APIPostRequest(API:API.SignOut, complete: block)
//    }
    
    // MARK: - Address
    static func getAddressList(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API:API.AddressList, complete: block)
    }
    
    static func addAddress(title: String?, address1: String, address2: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        if title != nil {
            parameters["title"] = title!
        }
        parameters["address1"] = address1
        parameters["address2"] = address2
        
        shareInstance.APIPostRequest(API: API.AddressAdd, parameters: parameters, complete: block)
    }
    
    static func editAddress(address_seq: Int, title: String, zipcode: String, phone_number: String, address1: String, address2: String, is_default:Bool, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["address_seq"] = address_seq
        parameters["title"] = title
        parameters["zipcode"] = zipcode
        parameters["phone_number"] = phone_number
        parameters["address1"] = address1
        parameters["address2"] = address2
        parameters["is_default"] = is_default ? "1" : "0"
        
        shareInstance.APIPostRequest(API: API.AddressEdit, parameters: parameters, complete: block)
    }
    
    static func removeAddress(address_seq: Int, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["address_seq"] = address_seq
        
        shareInstance.APIPostRequest(API: API.AddressRemove, parameters: parameters, complete: block)
    }
    
    static func markAddress(address_seq: Int, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["address_seq"] = address_seq
        
        shareInstance.APIPostRequest(API: API.AddressMark, parameters: parameters, complete: block)
    }
    
    static func checkAvailableAddress(address: String, jibun: String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["address"] = address
        if let unwrap_jibun = jibun {
            parameters["jibun"] = unwrap_jibun
        }
        
        shareInstance.APIPostRequest(API: API.AddressAvailable, parameters: parameters, complete: block)
    }
    
    static func updateAddress(address_seq: Int?, title: String?, address1: String?, address2: String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["address_seq"] = address_seq
        parameters["title"] = title
        parameters["address1"] = address1
        parameters["address2"] = address2
        
        shareInstance.APIPostRequest(API: API.AddressUpdate, parameters: parameters, complete: block)
    }
    
    // MARK: - Reservation
    static func reservationList(year: String, month: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["year"] = Int(year)
        parameters["month"] = month
        
        shareInstance.APIPostRequest(API: API.ReservationList, parameters: parameters, complete: block)
    }
    
    static func reservationInfo(reservation_seq: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["reservation_seq"] = reservation_seq
        

        shareInstance.APIPostRequest(API: API.ReservationInfo, parameters: parameters, complete: block)
    }
    
    static func createReservation(cleaning_space: String? = nil, block: @escaping JsonBlock) {
        var parameters = Parameters()
        
        if let order_type = ReservationModel.shared.order_type?.rawValue {
            parameters["order_type"] = order_type
        }
        if let cleaning_type = ReservationModel.shared.cleaning_type {
            parameters["cleaning_type"] = cleaning_type
        }
        if let address_seq = DataStore.userModel?.address_seq {
            parameters["address_seq"] = address_seq
        }
        if let date = ReservationModel.shared.date {
            parameters["date"] = date
        }
        if let price_seq = ReservationModel.shared.price_seq {
            parameters["price_seq"] = price_seq
        }
        if let user_request = ReservationModel.shared.user_request {
            parameters["user_request"] = "\(JSON(user_request).rawValue)"
        }
        if let user_preference = ReservationModel.shared.user_preference {
            parameters["user_preference"] = "\(JSON(user_preference).rawValue)"
        }
        if let week_cycle = ReservationModel.shared.week_cycle {
            parameters["week_cycle"] = week_cycle
        }
        if let days = ReservationModel.shared.days {
            parameters["days"] = "\(JSON(days.sorted()).rawValue)"
        }
        if let cleaning_space = cleaning_space {
            parameters["cleaning_space"] = cleaning_space
        }
        if let available_schedule_memo = ReservationModel.shared.available_schedule_memo {
            parameters["available_schedule_memo"] = available_schedule_memo
        }
        if let join_insurance = ReservationModel.shared.join_insurance {
            parameters["join_insurance"] = join_insurance
        }
        if let my_coupon_seq = ReservationModel.shared.coupon_seq {
            parameters["my_coupon_seq"] = my_coupon_seq
        }

        shareInstance.APIPostRequest(API: API.CreateReservation, parameters: parameters, complete: block)
    }
    
    static func cancelReservation(reservation_seq: String, reason: String?, is_all: Bool, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["reservation_seq"] = reservation_seq
        parameters["reason"] = reason
        parameters["is_all"] = is_all ? "1" : "0"
        
        shareInstance.APIPostRequest(API: API.CancelReservation, parameters: parameters, complete: block)
    }
    
    static func cancelReservationIsRemain(reservation_seq: String, reason: String?, is_all: Bool, is_remain: Bool, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["reservation_seq"] = reservation_seq
        parameters["reason"] = reason
        parameters["is_all"] = is_all ? "1" : "0"
        parameters["is_remain"] = is_remain ? "1" : "0"
        
        shareInstance.APIPostRequest(API: API.CancelReservation, parameters: parameters, complete: block)
    }
    
    static func updateReservation(_ reservation_seq: String, card_seq: String?, coupon_seq: String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["reservation_seq"] = reservation_seq
        if card_seq != nil {
            parameters["card_seq"] = card_seq
        }
        if coupon_seq != nil {
            parameters["coupon_seq"] = coupon_seq
        }
        
        shareInstance.APIPostRequest(API: .updateReservation, parameters: parameters, complete: block)
    }
    
    static func updateReservationV2(_ reservation_seq: String, master_seq: String?, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["reservation_seq"] = reservation_seq
        parameters["master_seq"] = master_seq ?? ""
        
        shareInstance.APIPostRequest(API: .updateReservationV2, parameters: parameters, complete: block)
    }
    
    // grade
    static func createGrade(type: String, reservation_cycle_seq: String, grade: String, memo: String, is_rejected: Bool, reason: [String], block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["type"] = type
        parameters["reservation_cycle_seq"] = reservation_cycle_seq
        parameters["grade"] = grade
        parameters["memo"] = memo
        parameters["is_rejected"] = is_rejected
        parameters["reason"] = "\(JSON(reason).rawValue)"
        
        shareInstance.APIPostRequest(API: API.CreateGrade, parameters: parameters, complete: block)
    }
    
    // MARK: - ETC
    // Popup
//    static func popup(block: @escaping JsonBlock) {
//        shareInstance.APIPostRequest(API: API.Popup, complete: block)
//    }
    
    // Version
    static func version(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.Version, complete: block)
    }
    
    // Banner
    static func banner(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.Banner, complete: block)
    }
    
    // ResetPassword
    static func resetPassword(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.ResetPassword, complete: block)
    }
    
//    static func getNotice(is_master: Int, block: @escaping JsonBlock) {
//        var parameters = Parameters()
//        parameters["is_master"] = is_master
//        shareInstance.APIPostRequest(API: API.Notice, parameters: parameters, complete: block)
//    }
    
    // MARK: - Card
    static func getCardList(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.CardList, complete: block)
    }
    
    static func registCard(card_type: Int, card_number: String,
                               month: String, year: String, birth: String,
                               password: String, cvc: String, business_number: String,
                               nickname: String, is_default: String = "0", block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["card_type"] = card_type
        parameters["card_number"] = card_number
        parameters["month"] = month
        parameters["year"] = year
        parameters["password"] = password
        parameters["cvc"] = cvc
        parameters["is_default"] = is_default
        if birth != "" {
            parameters["birth"] = birth
        }
        if business_number != "" {
            parameters["business_number"] = business_number
        }
        if nickname != "" {
            parameters["nickname"] = nickname
        }

        shareInstance.APIPostRequest(API: API.RegistCard, parameters: parameters, complete: block)
    }
    
    static func removeCard(card_seq: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["card_seq"] = card_seq
        
        shareInstance.APIPostRequest(API: API.RemoveCard, parameters: parameters, complete: block)
    }
    
    static func markCard(card_seq: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["card_seq"] = card_seq
        
        shareInstance.APIPostRequest(API: API.MarkCard, parameters: parameters, complete: block)
    }
    
    static func updateCard(nickname: String, card_seq:String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["card_seq"] = card_seq
        parameters["nickname"] = nickname
        
        shareInstance.APIPostRequest(API: API.UpdateCard, parameters: parameters, complete: block)
    }
    
    // MARK: - Coupon
    static func getCouponList(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.CouponList, complete: block)
    }
    
    static func registCoupon(code: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["code"] = code
        
        shareInstance.APIPostRequest(API: API.RegistCoupon, parameters: parameters, complete: block)
    }
    
    static func useCoupon(coupon_seq: String, reservation_seq: String, block: @escaping JsonBlock) {
        var parameters = Parameters()
        parameters["coupon_seq"] = coupon_seq
        parameters["reservation_seq"] = reservation_seq
        
        shareInstance.APIPostRequest(API: API.UseCoupon, parameters: parameters, complete: block)
    }
    
    // MARK: - Price
    static func getPrice(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.getPrice, complete: block)
    }
    
    static func getInsuranceImage(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.getInsuranceImage, complete: block)
    }
    
    static func getSettings(block: @escaping JsonBlock) {
        shareInstance.APIPostRequest(API: API.Settings, complete: block)
    }
}
