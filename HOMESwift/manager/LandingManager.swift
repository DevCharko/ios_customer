//
//  LandingManager.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 9. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import Foundation
import ChannelIO


/// 앱 용 url 랜딩 처리 클래스
class LandingManager {
    
    static let shared = LandingManager()

    func checkLanding(url: URL) {
        log.debug(url)
        
        var data: String!
        var tg: String!
        
        let parameters = (url.absoluteString.components(separatedBy: "?").last)?.components(separatedBy: "&")
        tg = parameters?.first?.components(separatedBy: "=").last
        data = parameters?.last?.components(separatedBy: "=").last
        
        let currentVC = currentViewController()
        
        if tg == "web" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if UIApplication.shared.canOpenURL(URL.init(string: data)!) {
                    UIApplication.shared.openURL(URL.init(string: data)!)
                }
            }
        }
            
        else if tg == "alarm" {
            
        }
            
        else if tg == "reservation_list" {
            if currentVC is ReservationPageViewController {
                
                (currentVC as! ReservationPageViewController).pages[0].getReservationList()
                (currentVC as! ReservationPageViewController).pages[1].getReservationList()
                (currentVC as! ReservationPageViewController).pages[2].getReservationList()
            } else {
                let pageVC = ReservationPageViewController(nibName:"ReservationPageViewController", bundle:nil)
                pushViewController(pageVC)
            }
        }
            
        else if tg == "reservation_detail" {
            if currentVC is ReservationDetailViewController {
                (currentVC as! ReservationDetailViewController).getReservation(data)
            } else if currentVC is HomeViewController {
                let pageVC = ReservationPageViewController(nibName:"ReservationPageViewController", bundle:nil)
                
                let detailVC = ReservationDetailViewController(nibName: "ReservationDetailViewController", bundle: nil)
                detailVC.seq = data // reservation_cycle_seq
                pushViewControllers([pageVC, detailVC])
            } else {
                let vc = ReservationDetailViewController(nibName: "ReservationDetailViewController", bundle: nil)
                vc.seq = data // reservation_cycle_seq
                pushViewController(vc)
            }
        }
            
        else if tg == "card_list" {
            if currentVC is CardListViewController {
                (currentVC as! CardListViewController).getCardList()
            } else {
                let vc = CardListViewController(nibName: "CardListViewController", bundle: nil)
                pushViewController(vc)
            }
        }
            
        else if tg == "card_register" {
            if currentVC is CardAddViewController {
                return
            } else if currentVC is HomeViewController {
                let listVC = CardListViewController(nibName: "CardListViewController", bundle: nil)
                let detailVC = CardAddViewController(nibName: "CardAddViewController", bundle: nil)
                pushViewControllers([listVC, detailVC])
            }
            let vc = CardAddViewController(nibName: "CardAddViewController", bundle: nil)
            presentViewController(vc)
        }
            
        else if tg == "coupon_list" {
            if currentVC is CouponListViewController {
                (currentVC as! CouponListViewController).getCouponList()
            } else {
                let vc = CouponListViewController(nibName: "CouponListViewController", bundle: nil)
                pushViewController(vc)
            }
        }
            
        else if tg == "coupon_register" {
            if currentVC is CouponAddViewController {
                return
            } else if currentVC is HomeViewController {
                let listVC = CouponListViewController(nibName: "CouponListViewController", bundle: nil)
                let detailVC = CouponAddViewController(nibName: "CouponAddViewController", bundle: nil)
                pushViewControllers([listVC, detailVC])
            }
            let vc = CouponAddViewController(nibName: "CouponAddViewController", bundle: nil)
            presentViewController(vc)
            
        }
            
        else if tg == "recommend" {
            if currentVC is RecommendViewController {
                return
            }
            let vc = RecommendViewController(nibName: "RecommendViewController", bundle: nil)
            pushViewController(vc)
        }
            
        else if tg == "notice" {
            if currentVC is NoticeWebViewController {
                return
            }
            let vc = NoticeWebViewController(nibName: "NoticeWebViewController", bundle: nil)
            pushViewController(vc)
        }
            
        else if tg == "setting" {
            if currentVC is SettingsViewController {
                return
            }
            let vc = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
            pushViewController(vc)
        }
            
        else if tg == "channel_list" {
            ChannelIO.show(animated: true)
        }
            
        else if tg == "channel_detail" {
            ChannelIO.show(animated: true)
        }
    }
    
    //present -> push
    //push -> push
    //inactive -> push : list push -> push
    //현재페이지 -> 갱신만
    //        setLeftBarButton(type: .Exit, action: #selector(clickExitNavi))
    
    private func pushViewController(_ vc: UIViewController) {
        if let popUpVC = currentViewController()?.presentedViewController {
            popUpVC.navigationController?.pushViewController(vc, animated: true)
            
            //            pushViewControllers([popUpVC, vc])
        } else {
            currentViewController()?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func pushViewControllers(_ vcs: [UIViewController]) {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            var currentVCS = rootVC.viewControllers
            for vc in vcs {
                currentVCS.append(vc)
            }
            rootVC.setViewControllers(currentVCS, animated: true)
        }
    }
    
    // present -> present : o
    // push -> present : o
    // inactive -> present : 리스트 push -> present
    private func presentViewController(_ vc: SKPViewController) {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            let naviVC = UINavigationController(rootViewController: vc)
            rootVC.present(naviVC, animated: true, completion: nil)
        }
    }
    
    private func currentViewController() -> UIViewController? {
        
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? MainController {
            return rootVC.topViewController!
        } else {
            return nil
        }
    }
}
