//
//  DataStore.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 23..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

let kDeviceId = "kDeviceId" // UUID
let kAuthKey = "kAuthKey" // Server device id
let kPushToken = "kPushToken" // Push token
let kPopUpEnable = "kPopUpEnable" // PopUp enable
let Tutorial = "tutorial"
let Login = "login"

/// 앱내 데이터를 관리하는 스토어 클래스
class DataStore: NSObject {
  
    static let shareInstance: DataStore = {
        let instance = DataStore()
        return instance
    }()
    
    static var getDeviceId : String? {
        return UserDefaults.standard.object(forKey: kDeviceId) as? String
    }
    
    static func setDeviceId(uuid: String) {
        UserDefaults.standard.set(uuid, forKey: kDeviceId)
    }
    
    static var isTutorial: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Tutorial)
        }
        set(newVal) {
            UserDefaults.standard.set(newVal, forKey: Tutorial)
        }
    }
    
    static var isLogin: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Login)
        }
        set(newVal) {
            UserDefaults.standard.set(newVal, forKey: Login)

        }
    }
    
    /// 통신용 토큰 값을 가져옴
    static var getAuthKey : String? {
        return UserDefaults.standard.object(forKey: kAuthKey) as? String
    }
    
    /// 통신용 토큰 값을 저장
    ///
    /// - Parameter authKey: 서버에서
    static func setAuthKey(authKey: String?) {
        UserDefaults.standard.set(authKey, forKey: kAuthKey)
    }

    static var getPushToken : String? {
        return UserDefaults.standard.object(forKey: kPushToken) as? String
    }
    
    static func setPushToken(token: String) {
        UserDefaults.standard.set(token, forKey: kPushToken)
    }

    /// 가사도우미 클릭시 팝업 다시보지않기 활성화
    static var getPopUpDisable : Bool? {
        return UserDefaults.standard.object(forKey: kPopUpEnable) as? Bool
    }
    
    
    /// 가사도우미 클릭시 팝업 다시보지않기 값을 저장
    ///
    /// - Parameter isCheck: true 이면 다시보지 않음.
    static func setPopUpDisable(isCheck: Bool) {
        UserDefaults.standard.set(isCheck, forKey: kPopUpEnable)
    }
    
    static let timeoutInterval:Double = 15
    
    static var naviTitleName = ""
    
    static var userModel: UserModel? // todo: userModel로 수정
    
    static var settings: SettingsModel?

    static var channelIOBadgeCount = ""
    
    static func messageFont() -> UIFont {
        return UIFont.systemFont(ofSize: 11)
    }
    
    static func messageDateFont() -> UIFont {
        return UIFont.systemFont(ofSize: 9)
    }
    
    static func messageDateColor() -> UIColor {
        return UIColor(red: 130.0/255.0, green: 135.0/255.0, blue: 144.0/255.0, alpha: 1)
    }

    static func clearData() {
        DataStore.setAuthKey(authKey: nil)
        DataStore.userModel = nil
    }
}
