//
//  BorderProtocol.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 8. 10..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

protocol Borderable {
    var cornerRadius: CGFloat { get set }
    var borderWidth: CGFloat { get set }
    var borderColor: UIColor? { get set }
}

extension UIView: Borderable {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            guard let borderColor = layer.borderColor else { return nil }
            return UIColor(cgColor: borderColor)
        }
        
        set {
            if (newValue != nil) {
                layer.borderColor = newValue?.cgColor
            } else {
                layer.borderColor = borderColor?.cgColor
            }
        }
    }
}
