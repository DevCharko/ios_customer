//
//  UIColor+Extension.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 2. 28..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

// MARK: - 이미지 익스텐션 클래스
extension UIImage {
    
    /// 파라미터 색상값으로 만들어진 UIImage 생성
    ///
    /// - Parameter color: 이미지를 생성할 color값
    /// - Returns: 단색의 image
    class func imageFromColor(_ color: UIColor) -> UIImage {
        
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.setFillColor(color.cgColor)
        context.fill(rect);

        let img = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return img!
    }
    
    /// rgb값으로 만들어진 UIImage 생성
    ///
    /// - Parameter rgb: 0xffffff
    /// - Returns: 단색의 image
    class func imageFromRGB(_ rgb: Int) -> UIImage {
        return imageFromColor(UIColor.rgb(fromHex: rgb))
    }
}
