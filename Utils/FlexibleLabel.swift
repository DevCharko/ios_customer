//
//  UILabel+Extension.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 6. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

public class FlexibleLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setFlexibleFontSize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setFlexibleFontSize()
    }
}
