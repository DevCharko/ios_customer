//
//  UnderLineButton.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 27..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class FlexibleButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setFlexibleFontSize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setFlexibleFontSize()
    }
}
