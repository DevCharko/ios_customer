//
//  BorderButton.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 7..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    override var isEnabled: Bool {
        didSet {
            isEnabled ? (borderColor = UIColor.main) : (borderColor = UIColor.placeholder)
            self.setTitleColor(borderColor, for: .normal)
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            isHighlighted ? (borderColor = UIColor.placeholder) : (borderColor = UIColor.main)
            self.setTitleColor(borderColor, for: .normal)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setFlexibleFontSize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setFlexibleFontSize()
    }
}
