
//
//  Radio-Enable-Button.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 23..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit


/// 라디오버튼 클래스
class ButtonController {
    
    var radioButtons = [UIButton]()
    var selectedButtons = [UIButton]()
    
    var enableView: UIView? 
    
    // defalt color
    var enableColor = UIColor.alert
    var disableColor = UIColor.placeholder
    
    var isEnableView = false // 활성화시킬 뷰가 있는지 여부
    var isMultipleSelect = false // 여러개 체크 가능 여부
    
    func reloadButtonControll() {
        cancleAllButton()
        initEnableView()
    }
    
    func cancleAllButton(){
        for item in radioButtons {
            item.isSelected = false
        }
        selectedButtons.removeAll()
    }
    
    func initEnableView() {
        selectedButtons.removeAll()
        enableView?.isUserInteractionEnabled = false
        enableView?.backgroundColor = disableColor
    }
    
    // 버튼 array등록
    func setRadioButtons(buttons: [UIButton]) {
        for item in buttons {
            item.isSelected = false
        }
        self.radioButtons = buttons
    }
    
    // 버튼 하나씩 등록
    func setRadioButton(button: UIButton) {
        button.isSelected = false
        self.radioButtons.append(button)
    }
    
    // 활성화 될 뷰 등록 (기본색상으로 설정)
    func setEnableView(view: UIView) {
        isEnableView = true
        enableView = view
        enableView?.isUserInteractionEnabled = false
    }
    
    // 활성화 될 뷰 색상과 함께 등록
    func setEnableView(view: UIView, enableColor: UIColor, disableColor: UIColor) {
        setEnableView(view: view)
        self.enableColor = enableColor
        self.disableColor = disableColor
    }
    
    func clickButton(clickedButton: UIButton) {
        
        // 1. 멀티체크가 불가능하면 나머지 버튼 제거
        if isMultipleSelect == false {
            cancleAllButton()
        }
        
        // 2. 클릭한 버튼이 선택되있으면 제거, 없으면 등록
        clickedButton.isSelected = !clickedButton.isSelected // 토글
        if let idx = selectedButtons.index(of: clickedButton) {
            selectedButtons.remove(at: idx)
        }
        else {
            selectedButtons.append(clickedButton)
        }

        // 활성화뷰 체크
        if isEnableView == true {
            checkEnableView()
        }
    }
    
    func checkEnableView() {
        
        if selectedButtons.isEmpty {
            enableView?.isUserInteractionEnabled = false
            enableView?.backgroundColor = disableColor
        }
        else {
            enableView?.isUserInteractionEnabled = true
            enableView?.backgroundColor = enableColor
        }
        
    }
    
    func debug(){
        // debug
        for item in selectedButtons {
            log.debug(item.titleLabel ?? "")
        }
    }
}
