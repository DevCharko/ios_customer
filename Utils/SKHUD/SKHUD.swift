//
//  SKHUD.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 5. 30..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 로딩 시 입력을 막는 HUD view
class SKHUD: UIView {
    
    private var hudView: UIView = UIView()
    static var HUDTimer: Timer?
    
    fileprivate struct Constants {
        static let sharedHUD = SKHUD()
    }
    
    open class var sharedHUD: SKHUD {
        return Constants.sharedHUD
    }
    
    static func show(touchDisable: Bool = true) {
        if sharedHUD.hudView.viewWithTag(1234) == nil {
            log.debug(sharedHUD.hudView.subviews.count)
            sharedHUD.hudView.frame = UIApplication.shared.keyWindow!.bounds
            sharedHUD.hudView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            
            let spinner = SpinView(frame: CGRect(x:0, y:0, width:80.0, height:80.0), kind: .threeBounce)
            spinner.tag = 1234
            sharedHUD.hudView.addSubview(spinner)
            spinner.center = UIApplication.shared.keyWindow!.center
            
            UIApplication.shared.keyWindow!.addSubview(sharedHUD.hudView)
            sharedHUD.hudView.isHidden = false
            sharedHUD.hudView.isUserInteractionEnabled = false
            
            if touchDisable {
                UIApplication.shared.beginIgnoringInteractionEvents()//터치 비활성화
            }
            HUDTimer = Timer.scheduledTimer(timeInterval: DataStore.timeoutInterval*3, target: self, selector: #selector(hide), userInfo: nil, repeats: false)
        }
    }
    
    @objc static func hide(touchDisable: Bool = true) {
        if let spinner = sharedHUD.hudView.viewWithTag(1234) {
            log.debug("hide")
            spinner.removeFromSuperview()
            sharedHUD.hudView.isHidden = true
            
            if touchDisable {
                UIApplication.shared.endIgnoringInteractionEvents()//터치 활성화
            }
            HUDTimer?.invalidate()
            log.debug("hide end")
        }
    }
    
    static func addHUD(_ viewController: UIViewController) {
        let hud = SpinView(frame: CGRect(x:0, y:0, width:80.0, height:80.0), kind: .threeBounce)
        hud.center = viewController.view.center
        hud.tag = viewController.hashValue
        viewController.view.addSubview(hud)
    }
    
    static func removeHUD(_ viewController: UIViewController) {
        if let hud = viewController.view.viewWithTag(viewController.hashValue) {
            hud.removeFromSuperview()
        }
    }
}
