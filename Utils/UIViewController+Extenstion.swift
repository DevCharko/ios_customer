//
//  UIViewController+Extenstion.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 3..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//
import UIKit

// MARK: - UIViewController 익스텐션 클래스
extension UIViewController {
    
    /// 빈곳 터치시 키보드 내리기
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    /// 키보드에 완료버튼 툴바 생성
    func getKeyboardToolbar() -> UIToolbar {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44))
        //create left side empty space so that done button set on right side
        
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "완료", style: .done, target: self, action: #selector(dismissKeyboard))
        doneBtn.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.alert], for: .normal)
        doneBtn.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.placeholder], for: .highlighted)
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let rightSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        rightSpace.width = 0
        
        //array of BarButtonItems
        var arr = [UIBarButtonItem]()
        arr.append(flexSpace)
        arr.append(doneBtn)
        arr.append(rightSpace)
        toolbar.setItems(arr, animated: false)
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    /// 키보드 올라올때 스크롤뷰 사이즈 조정하기
    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    /// default로 view의 사이즈 조정. 테이블뷰나 하단고정버튼이 있으면 override해서 값 조정할것.
    ///
    /// - Parameter notification: 키보드 노티피케이션
    @objc func keyboardWillShow(notification: NSNotification) {

        UIView.animate(withDuration: 0.0, animations: { () -> Void in
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
                let window = self.view.window?.frame {
                
                self.view.frame = CGRect(x: self.view.frame.origin.x,
                                         y: self.view.frame.origin.y,
                                         width: self.view.frame.width,
                                         height: window.origin.y + window.height - keyboardSize.height)
            } else {
                log.debug("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
            }
        })
        self.view.layoutIfNeeded()
    }
    
    /// default로 view의 사이즈 조정. 테이블뷰나 하단고정버튼이 있으면 override해서 값 조정할것.
    ///
    /// - Parameter notification: 키보드 노티피케이션
    @objc func keyboardWillHide(notification: NSNotification) {

        UIView.animate(withDuration: 0.0, animations: { () -> Void in
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                let viewHeight = self.view.frame.height
                self.view.frame = CGRect(x: self.view.frame.origin.x,
                                         y: self.view.frame.origin.y,
                                         width: self.view.frame.width,
                                         height: viewHeight + keyboardSize.height)
            } else {
                log.debug("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
            }
        })
        self.view.layoutIfNeeded()
    }
    
    /// 옵셔널 비강제 해제
    func getUnWrappingValue<T>(_ value: T?) -> T {
        if let val = value {
            return val
        }
        
        let type = T.self
        
        switch type {
        case is String.Type:
            return "" as! T
        default :
            return 0 as! T
        }
    }
}
