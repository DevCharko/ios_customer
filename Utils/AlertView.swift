//
//  UIViewController+Extension.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 31..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//
import UIKit

/// 얼릿컨트롤러 블럭 클래스
open class AlertView {
    public typealias AlertBlock = ((_ alert: AlertView, _ buttonIndex:Int) -> ())
    
//    static let shareInstance: AlertView = {
//        let instance = AlertView()
//        
//        return instance
//    }()
    
    /// 얼럿뷰 생성
    /// 얼럿뷰 버튼 클릭 이후 콜백액션이 없을 경우 해당 펑션을 사용
    ///
    /// - Parameters:
    ///   - vc: 얼럿을 호출할 viewcontroller
    ///   - title: 얼럿 타이틀
    ///   - message: 얼럿 메시지
    func alert(_ vc: UIViewController,
               title:String?,
               message:String) {
        alert(vc,
              title: title,
              message: message,
              cancelButtonTitle: "확인",
              otherButtonTitles: [],
              buttonColor: nil) { (alert, buttonIndex) in }
    }
    
    /// 얼럿뷰 생성
    /// 버튼 콜백 포함
    /// - Parameters:
    ///   - vc: 얼럿을 호출할 viewcontroller
    ///   - title: 얼럿 타이틀
    ///   - message: 얼럿 메시지
    ///   - cancelButtonTitle: 취소버튼 타이틀
    ///   - otherButtonTitles: 버튼 타이틀 리스트
    ///   - buttonColor: 취소버튼을 제외한 버튼 색상
    ///   - block: 콜백 closure 함수
    func alert(_ vc: UIViewController,
               title:String?,
               message:String,
               cancelButtonTitle:String,
               otherButtonTitles:[String], //["button0", "button1"]
               buttonColor:[UIColor]? = nil,
        block:@escaping AlertBlock) {
        
        var msg = message
        if message == "The request timed out." || message == "The Internet connection appears to be offline." {
            msg = "네트워크가 원활하지 않습니다.\n다시 시도해 주세요."
        } else if message == "서비스 개선중..." {
            msg = "현재 서비스 개선 작업 진행 중입니다. 잠시 후 다시 실행 해주세요."
        } else if message == "Response could not be serialized, input data was nil or zero length." {
            msg = "입력값이 잘못되었습니다. 다시 시도해 주세요."
        } else if message == "cancelled" {
            return
        }
        
        let alert = UIAlertController(title:title, message:msg, preferredStyle: .alert)
        //let defaultColor = UIColor.init(red: 14/255, green: 122/255, blue: 254/255, alpha: 1)
        
        for (i, other) in otherButtonTitles.enumerated() {
            let action = UIAlertAction(title: other, style: .default, handler: { (action) in
                block(self, i)
            })
            action.setValue(buttonColor?[i], forKey: "titleTextColor")

            alert.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (action) in
            block(self, otherButtonTitles.count)
        })
        
        cancelAction.setValue(buttonColor?[otherButtonTitles.count], forKey: "titleTextColor")
        
        alert.addAction(cancelAction)
        
        log.debug("\(vc)")
        vc.present(alert, animated: true)
    }
}
