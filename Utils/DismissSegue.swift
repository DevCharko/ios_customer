//
//  DismissSegue.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 7..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

class DismissSegue: UIStoryboardSegue {
    
    override func perform() {
        self.source.dismiss(animated: true, completion: nil)
    }
}
