//
//  LimitedLengthTextField.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 10..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/// 글자수 제한이 설정된 커스텀 텍스트필드
class LimitedLengthField: UITextField {
    @IBInspectable var maxLength: Int = 16 // set maximum number of characters
    var stringValue: String { return text ?? "" }
    override func awakeFromNib() {
        super.awakeFromNib()
//        keyboardType = .asciiCapable   // set the keyboard type
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        editingChanged(sender: self)
    }
    @objc func editingChanged(sender: UITextField) {
        sender.text = String(stringValue.characters.prefix(maxLength))
    }
    
}
