//
//  SKTextField.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 27..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

/**
 ## maxLength를 설정하고 키보드에 AccessoryView를 쉽게 추가할 수 있는 커스텀 TextField
 
 - - -
 
 ## 사용법
 *maxLength 설정*
 
 1.storyboard의 inspector panel에서 maxlength 설정
 
 2.source
 ```
 @IBOutlet weak var textField: SKTextField!
 textField.maxlength = 10
 ```
  _ _ _
 
 *툴바 생성하기*
 ```
 @IBOutlet weak var textField1: SKTextField!
 @IBOutlet weak var textField2: SKTextField!
 textField1.setToolbar(prev: nil, next: textField2)
 textField1.setToolbar(prev: textField1, next: nil)
 ```
 */
class SKTextField: UITextField {

    @IBInspectable var maxLength: Int = 100 // set maximum number of characters
    var stringValue: String { return text ?? "" }
    
    var prevTextField: SKTextField?
    var nextTextField: SKTextField?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        editingChanged(sender: self)
        setFlexibleFontSize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setFlexibleFontSize()
    }
    
    func setToolbar() {
        let screenWidth = UIScreen.main.bounds.width
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        //create left side empty space so that done button set on right side
        
        let doneView = UIView(frame: CGRect(x: 0, y: 0, width: 22, height: 44))
        let doneBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 44))
        doneBtn.setAttributedTitle(NSAttributedString(string: "완료", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.alert]), for: .normal)
        doneBtn.setAttributedTitle(NSAttributedString(string: "완료", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.placeholder]), for: .highlighted)
        doneBtn.addTarget(self, action: #selector(completeButtonHandler), for: .touchUpInside)
        doneView.addSubview(doneBtn)
        let doneButton = UIBarButtonItem(customView: doneView)

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let rightSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        rightSpace.width = 0
        
        //array of BarButtonItems
        var arr = [UIBarButtonItem]()
        arr.append(flexSpace)
        arr.append(doneButton)
        arr.append(rightSpace)
        toolbar.setItems(arr, animated: false)
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar

    }
    
    func setToolbar(prev: SKTextField?, next: SKTextField?) {

        self.prevTextField = prev
        self.nextTextField = next
        
        let screenWidth = UIScreen.main.bounds.width
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let leftBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        leftBtn.setImage(UIImage(named: "btn_left_arrow_key_dim"), for: .disabled)
        leftBtn.setImage(UIImage(named: "btn_left_arrow_key_nor"), for: .normal)
        leftBtn.addTarget(self, action: #selector(prevAction), for: .touchUpInside)
        leftView.addSubview(leftBtn)
        
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let rightBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        rightBtn.setImage(UIImage(named: "btn_right_arrow_key_dim"), for: .disabled)
        rightBtn.setImage(UIImage(named: "btn_right_arrow_key_nor"), for: .normal)
        rightBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        rightView.addSubview(rightBtn)
        
        let doneView = UIView(frame: CGRect(x: 0, y: 0, width: 22, height: 44))
        let doneBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 44))
        doneBtn.setAttributedTitle(NSAttributedString(string: "완료", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.alert]), for: .normal)
        doneBtn.setAttributedTitle(NSAttributedString(string: "완료", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedStringKey.foregroundColor : UIColor.placeholder]), for: .highlighted)
        doneBtn.addTarget(self, action: #selector(completeButtonHandler), for: .touchUpInside)
        doneView.addSubview(doneBtn)
        
        let leftSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        leftSpace.width = -16
        let rightSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        rightSpace.width = 0
        
        let leftButton = UIBarButtonItem(customView: leftView)
        let rightButton = UIBarButtonItem(customView: rightView)
        let doneButton = UIBarButtonItem(customView: doneView)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolbar.tintColor = UIColor.alert
        toolbar.setItems([leftSpace, leftButton, leftSpace, rightButton, flexSpace, doneButton, rightSpace], animated: false)
        toolbar.sizeToFit()
        
        if prev == nil {
            leftBtn.isEnabled = false
        } else if next == nil {
            rightBtn.isEnabled = false
        }
        
        self.inputAccessoryView = toolbar
    }
    
    func setNextTF(_ textField: SKTextField) {
        self.nextTextField = textField
    }

    @objc func completeButtonHandler() {
        self.resignFirstResponder()
    }
    
    @objc func editingChanged(sender: UITextField) {
        if (sender.text?.characters.count)! > maxLength {
            sender.text = String(stringValue.characters.prefix(maxLength))
        }
    }
}

extension SKTextField: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let next = nextTextField {
            next.becomeFirstResponder()
        } else {
            self.resignFirstResponder()
        }
        
        return true
    }
    
    @objc func nextAction() {
        if let next = nextTextField {
            next.becomeFirstResponder()
        }
    }
    
    @objc func prevAction() {
        if let prev = prevTextField {
            prev.becomeFirstResponder()
        }
    }
}

extension UITextField {
    
    func setFlexibleFontSize() {
        guard self.font != nil else {
            return
        }
        
        let size = self.font!.pointSize
        if self.font!.fontName.contains("bold") || self.font!.fontName.contains("Bold") {
            self.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(size))
        } else {
            self.font = UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(size))
        }
    }
}
