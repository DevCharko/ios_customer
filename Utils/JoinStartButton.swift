//
//  JoinStartButton.swift
//  HOMESwift
//
//  Created by HM_Charko on 22/02/2019.
//  Copyright © 2019 skoopmedia. All rights reserved.
//

import UIKit

class JoinStartButton: UIButton {

    // defalt color
    var enableColor = UIColor.alert
    var disableColor = UIColor.placeholder
    
    var textColor = UIColor.white
    
    func initialize(enable: Bool) {
        self.setTitleColor(textColor, for: .normal)
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
        self.setBackgroundImage(UIImage.imageFromColor(enableColor), for: .normal)
        self.setBackgroundImage(UIImage.imageFromColor(disableColor), for: .disabled)
        self.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)
        
        setNextButton(bool: enable)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize(enable: false)
    }
    
    func setEnableView(enableColor: UIColor, disableColor: UIColor) {
        self.enableColor = enableColor
        self.disableColor = disableColor
    }
    
    func setNextButton(bool: Bool) {
        self.isEnabled = bool
    }
    
    func setHidden(bool: Bool) {
        self.isHidden = bool
    }
}
