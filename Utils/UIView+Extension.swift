//
//  UIView+Extension.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 4..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

// MARK: - UIView 익스텐션 클래스
extension UIView {
    
    class func getFlexibleFontSize(_ size: CGFloat) -> CGFloat{
        return size * UIScreen.main.bounds.width / 320
    }
    
    func drawDashedLine(fromPoint start: CGPoint, toPoint end:CGPoint, lineWidth: CGFloat, color: UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = color.cgColor
        line.lineWidth = lineWidth
        line.lineJoin = kCALineJoinRound
        line.lineDashPattern = [4, 2]
        self.layer.addSublayer(line)
    }
    
    func drawPlainLine(frame: CGRect, lineWidth: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: frame.height*2/3-lineWidth, width: frame.width, height: lineWidth)
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
    
    func drawBorder(lineWidth: CGFloat, cornerRadius: CGFloat, color: UIColor) {
        self.layer.borderWidth = lineWidth
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = cornerRadius
    }
    
    func drawBorderBottom(frame: CGRect, lineWidth: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: frame.height-lineWidth, width: frame.width, height: lineWidth)
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
    
    func drawBorderTop(frame: CGRect, lineWidth: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: 0, width: frame.width, height: lineWidth)
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
    
    //yourViewName.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func setRound() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.bounds.width / 2
    }
    
}
