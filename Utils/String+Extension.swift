//
//  String+Extension.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 3. 16..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

// MARK: - 스트링 익스텐션 클래스
extension String {
    
    /// yyyy-MM-dd HH:mm:ss yyyy-MM-dd 스쿱 기본 날짜 형식을 Date형으로 변환
    ///
    /// - Returns: Date class, null이면 형식에 맞지 않음.
    public func toDate() -> Date? {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = df.date(from: self) {
            return date
        }
        df.dateFormat = "yyyy-MM-dd";
        
        if let date = df.date(from: self) {
            return date
        }
        
        return nil
    }
    
    /// HH:mm:ss 를 a h시 또는 a h시 mm분 형식으로 변환
    ///
    /// - Returns: 변환된 형식의 스트링
    func getTimeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss" // 서버에서 오는 날짜 포맷
        
        let timestamp = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "a h시"
        
        if self.components(separatedBy: ":")[1] != "00" { // 분 단위 인지 검사
            dateFormatter.dateFormat = "a h시 mm분"
        }
        let timeString = dateFormatter.string(from: timestamp!)
        
        return timeString
    }
    
    /// base64 형식으로 인코딩
    ///
    /// - Returns: base64 crypto text
    func encodingBase64() -> String? {
        let data = self.data(using: String.Encoding.utf8)
        let crypt = data?.base64EncodedString()
        
        return crypt
    }
    
    /// base64로 인코딩된 데이터를 디코딩
    ///
    /// - Returns: plain text
    func decodingBase64() -> String? {
        let data = Data(base64Encoded: self)
        let plain = String.init(data: data!, encoding: String.Encoding.utf8)
        
        return plain
    }
    
    /// 숫자를 3자리씩 컴마찍어 리턴
    /// 1000을 1,000으로 변환
    /// - Returns: decimal number
    func stringFormatDecimal() -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        
        if let number = formatter.number(from: self) {
            return formatter.string(from: number)
        } else {
            log.error("stringFormatDecimal")
            return nil
        }
    }
    
    func stringByRemovingAll(characters: [Character]) -> String {
        return String(self.filter({ !characters.contains($0) }))
    }
    
    func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{3})(\\d{4})(\\d+)", with: "$1-$2-$3", options: .regularExpression, range: nil)
    }
    
    
    /// 마진값을 포함한 스트링의 높이값을 구함
    ///
    /// - Parameters:
    ///   - width: 최대 width값
    ///   - leftMargin: 왼쪽 마진
    ///   - rightMargin: 오른쪽 마진
    ///   - topMargin: 상단 마진
    ///   - bottomMargin: 하단 마진
    ///   - font: string에 쓰일 폰트
    /// - Returns: 높이값
    func height(constraintedWidth width: CGFloat, leftMargin: CGFloat, rightMargin: CGFloat, topMargin: CGFloat, bottomMargin: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: leftMargin, y: topMargin, width: width-leftMargin-rightMargin, height: .greatestFiniteMagnitude))
        label.font = font
        label.numberOfLines = 0
        label.text = self
        label.sizeToFit()
        return label.frame.height + topMargin + bottomMargin // 위아래 여백
    }
    
    func textHeight(constraintedWidth width: CGFloat, margin: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width - margin, height: .greatestFiniteMagnitude))
        label.font = font
        label.numberOfLines = 0
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }

    // AttributedString
    static func makeAttributedString(boldString: String, boldSize: CGFloat, boldColor: UIColor = .main, systemString: String, systemSize: CGFloat, systemColor: UIColor = .main) -> NSMutableAttributedString {
        
        let boldAttrs = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: boldSize), NSAttributedStringKey.foregroundColor: boldColor]
        let boldAttributedString = NSMutableAttributedString(string:boldString, attributes:boldAttrs)
        
        let systemAttrs = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: systemSize), NSAttributedStringKey.foregroundColor: systemColor]
        let systemString = NSMutableAttributedString(string: systemString, attributes: systemAttrs)
        
        boldAttributedString.append(systemString)
        
        return boldAttributedString
    }

}
