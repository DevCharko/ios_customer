//
//  UILabel+Extension.swift
//  HOMESwift
//
//  Created by hanwool on 2017. 6. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit

extension UIFont {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

@IBDesignable
public class FlexibleLabel: UILabel {
    @IBInspectable var flexibleFontSize:CGFloat = 15
    
    
//    @IBInspectable var ffont: UIFont! {
//        UIFont .systemFont(ofSize: flexibleFontSize * UIScreen.main.bounds.width / 320.0)
//        return flexibleFontSize * UIScreen.main.bounds.width / 320.0
//    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        if self.font.fontName.contains("bold") || self.font.fontName.contains("Bold") {
            self.font = UIFont.boldSystemFont(ofSize: flexibleFontSize * UIScreen.main.bounds.width / 320.0)
        } else {
            self.font = UIFont.systemFont(ofSize: flexibleFontSize * UIScreen.main.bounds.width / 320.0)
        }
    }
}
