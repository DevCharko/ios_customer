//
//  UINavigationViewController+Extension.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 5. 31..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

/// 네비게이션 컨트롤러 익스텐션 클래스
extension UINavigationController {
    
    ///Get previous view controller of the navigation stack
    
    func previousViewController() -> UIViewController?{
        
        let lenght = self.viewControllers.count
        
        let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
        
        return previousViewController
    }
}
