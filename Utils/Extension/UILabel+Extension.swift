//
//  UILabel+Extension.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import Foundation

/// MARK: - 라벨 익스텐션 클래스
extension UILabel {
    func setFlexibleFontSize() {
        guard self.font != nil else {
            return
        }
        
        let size = self.font!.pointSize
        if self.font!.fontName.contains("bold") || self.font!.fontName.contains("Bold") {
            self.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(size))
        } else {
            self.font = UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(size))
        }
    }
}
