//
//  Date+Extension.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 7. 26..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import Foundation

// MARK: - 데이트 익스텐션 클래스
extension Date {
    func getYearString() -> String {
        return getDateString(format: "yyyy")
    }
    
    func getMonthString() -> String {
        return getDateString(format: "MM")
    }
    
    func getSingleMonthString() -> String {
        return getDateString(format: "M")
    }
    
    func getDateString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)
        return str
    }
}
