//
//  UIApplication+Extenstion.swift
//  HOMESwift
//
//  Created by Jingyu Kim on 2017. 8. 8..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

/// MARK: - UIApplication 익스텐션 클래스
extension UIApplication {
    /**
     ## 최상위 뷰컨트롤러를 찾는 메서드
     - Returns: UIViewController
     - Parameters:
        - controller: default로 rootviewController로 설정되어있어 필요없음
     ### 사용법:
     ```
     if let topController = UIApplication.topViewController() {
     
     }
     ```
     */
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
