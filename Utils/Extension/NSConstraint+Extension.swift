//
//  NSConstraint+Extension.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import Foundation

extension NSLayoutConstraint {
    func setFlexibleSize() {
        let size = self.constant
        self.constant = UIView.getFlexibleFontSize(size)
    }
}
