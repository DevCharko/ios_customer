//
//  UIButton+Extension.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 6. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import Foundation

/// MARK: - 버튼 익스텐션 클래스
extension UIButton {
    
    func setFlexibleFontSize() {
        guard let titleLabel = self.titleLabel else {
            return
        }
        
        let size = titleLabel.font.pointSize
        if titleLabel.font.fontName.contains("bold") || self.titleLabel!.font.fontName.contains("Bold") {
            titleLabel.font = UIFont.boldSystemFont(ofSize: UIView.getFlexibleFontSize(size))
        } else {
            titleLabel.font = UIFont.systemFont(ofSize: UIView.getFlexibleFontSize(size))
        }
    }
    
    /// 버튼 터치영역을 확대하는 메서드
    ///
    /// - Parameter offset: 상하좌우 추가될 영역의 크기
    func expandTouchArea(offset: CGFloat) {
        let touchAreaView = UIView()
        self.superview?.addSubview(touchAreaView)
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(touchButton))
        longPressGesture.minimumPressDuration = 0.001
        touchAreaView.addGestureRecognizer(longPressGesture)
        
        touchAreaView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(self.snp.width).offset(offset)
            make.height.equalTo(self.snp.height).offset(offset)
        }
    }
    
    @objc func touchButton(_ sender: UILongPressGestureRecognizer) {
        guard self.isEnabled else {
            return
        }
        
        let view = sender.view
        let location = sender.location(in: view)
        let touchInside = ((view?.bounds)!).contains(location)
        
        switch(sender.state) {
            
        case .began:
            self.isHighlighted = true
            
        case .changed:
            if touchInside {
                self.isHighlighted = true
            } else {
                self.isHighlighted = false
            }
            
        case .ended:
            if touchInside {
                self.sendActions(for: .touchUpInside)
            }
            self.isHighlighted = false
            
        default: break
        }
    }
}
