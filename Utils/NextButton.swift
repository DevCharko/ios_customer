//
//  NextButton.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 3. 25..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit


/// '다음' 버튼을 커스텀으로 만들어놓은 클래스
class NextButton: UIButton{
    
    // defalt color
    var enableColor = UIColor.alert
    var disableColor = UIColor.placeholder
    
    var textColor = UIColor.white
    
    func initialize(enable: Bool) {
        self.setTitleColor(textColor, for: .normal)
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
        self.setBackgroundImage(UIImage.imageFromColor(enableColor), for: .normal)
        self.setBackgroundImage(UIImage.imageFromColor(disableColor), for: .disabled)
        self.setBackgroundImage(UIImage.imageFromRGB(0xff8586), for: .highlighted)

        setNextButton(bool: enable)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize(enable: false)
    }

    func setEnableView(enableColor: UIColor, disableColor: UIColor) {
        self.enableColor = enableColor
        self.disableColor = disableColor
    }

    func setNextButton(bool: Bool) {
        self.isEnabled = bool
    }
    
    func setHidden(bool: Bool) {
        self.isHidden = bool
    }

}
