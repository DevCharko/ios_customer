//
//  RoundView.swift
//  HOMESwift
//
//  Created by KimJingyu on 2017. 4. 21..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import UIKit


/// 라운드 변수들을 설정해놓은 커스텀 뷰
class RoundView: UIView {
    
    var defaultColor = UIColor.silver
    var selectedColor = UIColor.alert
    
    var isSelected = false {
        didSet{
            setNeedsDisplay()
            if isSelected == false {
                color = defaultColor
                setButtonsDefault()
            } else {
                color = selectedColor
                setButtonsSelected()
            }
        }
    }
    var subButtons: [UIButton]?

    @objc var color:UIColor = UIColor.silver { didSet { setNeedsDisplay() } }
    
    @objc var lineWidth: CGFloat = 1.0 { didSet { setNeedsDisplay() } }
    
    var centerPoint: CGPoint {
        return CGPoint(x: self.bounds.midX, y: self.bounds.midY)
    }
    
    var radius: CGFloat {
        return min(self.bounds.width/2, self.bounds.height/2) - lineWidth
    }
    
    func setButtonsDefault() {
        if let btnArray = subButtons {
            for btn in btnArray {
                btn.isSelected = false
            }
        }
    }
    
    func setButtonsSelected() {
        if let btnArray = subButtons {
            for btn in btnArray {
                btn.isSelected = true
            }
        }
    }
    
    func drawCircle(_ center: CGPoint, _ radius: CGFloat) -> UIBezierPath {
        let path = UIBezierPath(arcCenter: centerPoint, radius: radius, startAngle: 0.0, endAngle: CGFloat(2*Double.pi), clockwise: true)
        path.lineWidth = lineWidth
        return path
    }
    
    override func draw(_ rect: CGRect) {
        if subButtons != nil {
            for button in subButtons! {
                button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14 * UIScreen.main.bounds.width / 320)
            }
        }
        color.setStroke()
        drawCircle(centerPoint, radius).stroke()
    }
}
