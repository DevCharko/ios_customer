//
//  HOMEMaster_ScreenShot.swift
//  HOMEMaster ScreenShot
//
//  Created by KimJingyu on 2017. 6. 27..
//  Copyright © 2017년 skoopmedia. All rights reserved.
//

import XCTest

class HOMEMaster_ScreenShot: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        snapshot("0_Main")
        let app = XCUIApplication()
        app.navigationBars["메인"].buttons["btn my nor"].tap()
        snapshot("1_ReservationList")
//        app.tables.staticTexts["예약내역"].tap()
//        snapshot("2_ReservationDetail")
//        app.collectionViews.cells.children(matching: .other).element.children(matching: .other).element(boundBy: 0).tap()
        
    }
    
}
